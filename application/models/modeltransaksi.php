<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : transaksi
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modeltransaksi extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListtransaksi() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar" .
                " FROM transaksi   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->idbooking;
        }
        return $xBuffResul;
    }

    function getListtransaksi($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idbooking like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar" .
                " FROM transaksi $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListtransaksiAll() {
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar,isfinal" .
                " FROM transaksi order by idx DESC";
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListtransaksibydate($xAwal, $xLimit, $xtglawal, $xtglakhir, $xSearch = '', $xiskonfirmasi) {
        $xQSearch = "WHERE 1=1 ";
        if (!empty($xSearch)) {
            $xQSearch = "Where idmember IN (SELECT idx FROM member WHERE Nama LIKE '%" . $xSearch . "%')";
        }
        $xDate = "";
        if (!empty($xtglawal) && !empty($xtglakhir)) {
            $xDate = " AND (DATE(tglbooking) >= '$xtglawal' AND DATE(tglbooking) <= '$xtglakhir') ";
        }
        $xKonfirmasi = "";
        if ($xiskonfirmasi === '1') {
            $xKonfirmasi = " AND (tglkonfirmasi = '0000-00-00 00:00:00' OR tglkonfirmasi = '' OR tglkonfirmasi IS null) ";
        } elseif ($xiskonfirmasi === '2') {
            $xKonfirmasi = " AND (tglkonfirmasi <> '0000-00-00 00:00:00' OR tglkonfirmasi <> '' OR tglkonfirmasi IS NOT null) ";
        }
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar,hargadibayar,isfinal,tglkonfirmasi,imgkonfirmasi,konfirmasitext" .
                " FROM transaksi $xQSearch $xDate $xKonfirmasi order by idx DESC limit " . $xAwal . "," . $xLimit;
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListtransaksibydatestatus($xstatus, $xtglawal, $xtglakhir) {
        $xDate = "";
        if (!empty($xtglawal) && !empty($xtglakhir)) {
            $xDate = " WHERE (DATE(tglbooking) >= '$xtglawal' AND DATE(tglbooking) <= '$xtglakhir') ";
        }
        $xQstatus = "";
        if ($xstatus === '1') {
            $xQstatus = "AND tglbayar='0000-00-00' AND isfinal='N' ";
        } elseif ($xstatus === '2') {
            $xQstatus = "AND tglbayar<>'0000-00-00' ";
        } elseif ($xstatus === '3') {
            $xQstatus = "AND tglbatalbooking<>'0000-00-00' ";
        }
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar" .
                " FROM transaksi $xDate $xQstatus order by idx DESC ";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListtransaksibydatejenisbayar($xidjenispembayaran, $xtglawal, $xtglakhir) {
        $xDate = "";
        if (!empty($xtglawal) && !empty($xtglakhir)) {
            $xDate = " AND (DATE(tglbayar) >= '$xtglawal' AND DATE(tglbayar) <= '$xtglakhir') ";
        }
        $xQjenisbayar = "";
        if ($xidjenispembayaran !== '0') {
            $xQjenisbayar = "AND idjenisbayar='$xidjenispembayaran' ";
        }
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "hargadibayar," .
                "nominalvoucher," .
                "tglbayar" .
                " FROM transaksi WHERE tglbayar<>'0000-00-00' AND isfinal='Y' $xDate $xQjenisbayar order by idx DESC ";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListbooking($xidtransaksi) {
        $xStr = "SELECT idbooking FROM transaksi WHERE transaksi.idx='$xidtransaksi'";
        $query = $this->db->query($xStr);
        $idbooking = '';
        foreach ($query->result() as $row) {
            if ($row->idbooking !== ',') {
                $idbooking.= $row->idbooking . ',';
            }
        }
        $idbooking.='0';
        $xStr2 = "SELECT * FROM booking 
                WHERE booking.idx 
                IN ($idbooking) ORDER BY booking.status ASC";
//        echo $xStr2;

        $query2 = $this->db->query($xStr2);
        return $query2;
    }

    function getDetailtransaksi($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar,hargadibayar,konfirmasitext,imgkonfirmasi,tglkonfirmasi" .
                " FROM transaksi  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndextransaksi() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar" .
                " FROM transaksi order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInserttransaksi($xidx, $xidbooking, $xtglbooking, $xtglbatalbooking, $xketeranganbatal, $xharganormal, $xhargadiscount, $xidvoucher, $xidmember, $xidpegawai, $xspesialrequest, $xtglupdate, $xidjenisbayar, $xtglbayar, $xNominalVoucher, $xHarusBayar) {
        $xStr = " INSERT INTO transaksi( " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar,NominalVoucher,hargadibayar) VALUES('" . $xidx . "','" . $xidbooking . "',now(),'" .
                $xtglbatalbooking . "','" . $xketeranganbatal . "','" . $xharganormal . "','" . $xhargadiscount . "','" .
                $xidvoucher . "','" . $xidmember . "','" . $xidpegawai . "','" . $xspesialrequest . "',NOW(),'" . $xidjenisbayar . "','" . $xtglbayar . "','" . $xNominalVoucher . "','" . $xHarusBayar . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatetransaksi($xidx, $xidbooking, $xtglbooking, $xtglbatalbooking, $xketeranganbatal, $xharganormal, $xhargadiscount, $xidvoucher, $xidmember, $xidpegawai, $xspesialrequest, $xtglupdate, $xidjenisbayar, $xtglbayar, $xNominalVoucher, $xHarusBayar) {
        $xStr = " UPDATE transaksi SET " .
                "idx='" . $xidx . "'," .
                "idbooking='" . $xidbooking . "'," .
                "tglbooking='" . $xtglbooking . "'," .
                "tglbatalbooking='" . $xtglbatalbooking . "'," .
                "keteranganbatal='" . $xketeranganbatal . "'," .
                "harganormal='" . $xharganormal . "'," .
                "hargadiscount='" . $xhargadiscount . "'," .
                "idvoucher='" . $xidvoucher . "'," .
                "idmember='" . $xidmember . "'," .
                "idpegawai='" . $xidpegawai . "'," .
                "spesialrequest='" . $xspesialrequest . "'," .
                "hargadibayar='" . $xHarusBayar . "'," .
                "NominalVoucher='" . $xNominalVoucher . "'," .
                "tglupdate=NOW()," .
                "idjenisbayar='" . $xidjenisbayar . "'," .
                "tglbayar='" . $xtglbayar . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setUpdatePembayaranbooking($xidx, $xstatus) {
        $xQupdate = "tglbayar=NOW(),isfinal='Y',";
        if ($xstatus == 'belum') {
            $xQupdate = "tglbayar='0000-00-00',isfinal='N',";
        }
        $xStr = " UPDATE transaksi SET " .
                "idx='" . $xidx . "'," .
                $xQupdate .
                "tglupdate=NOW() WHERE idx = '" . $xidx . "'";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setUpdateFinalBooking($xidtransaksi, $xstatusawal, $xstatusakhir) {
        $xStr = "SELECT idbooking FROM transaksi WHERE transaksi.idx='$xidtransaksi'";
        $query = $this->db->query($xStr);
        $idbooking = '';
        foreach ($query->result() as $row) {
            $idbooking.= $row->idbooking . ',';
        }
        $idbooking.='0';
        $xStr = "UPDATE booking 
                 SET booking.status='$xstatusakhir' WHERE idx IN ($idbooking) AND booking.status='$xstatusawal'";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $xidtransaksi;
    }

    function getTransaksiSudahBayar($xidmember) {
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar,hargadibayar" .
                " FROM transaksi  WHERE idmember = '" . $xidmember . "' AND tglbayar<>'0000-00-00'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getTransaksiTerlambatBayar($xidmember) {
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar,hargadibayar" .
                " FROM transaksi  WHERE idmember = '" . $xidmember . "' AND tglbayar='0000-00-00' AND DATEDIFF(NOW(),tglbooking)>((SELECT bataswaktu FROM bataswaktu)/24) AND isfinal='N'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getTransaksiBelumBayar($xidmember) {
        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar,hargadibayar" .
                " FROM transaksi WHERE idmember = '" . $xidmember . "' AND tglbayar='0000-00-00'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function setDeletetransaksi($xidx) {
        $xStr = " DELETE FROM transaksi WHERE transaksi.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletetransaksi($xidx);
    }

    function setInsertLogDeletetransaksi($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'transaksi',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

    function setUpdateProsestransaksi($xidx, $xstatus) {
        if ($xstatus === 'lanjut') {
            $xStr = " UPDATE transaksi SET " .
                    "idx='" . $xidx . "'," .
                    "tglbatalbooking='0000-00-00 00:00:00' WHERE idx = '" . $xidx . "'";
        } else {
            $xStr = " UPDATE transaksi SET " .
                    "idx='" . $xidx . "'," .
                    "tglbatalbooking=NOW() WHERE idx = '" . $xidx . "'";
        }
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setTerlambattransaksi($xidx, $xketerangan) {
        $xStr = " UPDATE transaksi SET " .
                "idx='" . $xidx . "'," .
                "keteranganbatal='" . $xketerangan . "'," .
                "isfinal='Y'," .
                "tglupdate=NOW()," .
                "tglbatalbooking=NOW() WHERE idx = '" . $xidx . "' AND tglbayar='0000-00-00' AND DATEDIFF(NOW(),tglbooking)>((SELECT bataswaktu FROM bataswaktu)/24)";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function getListtransaksiByIdMember($xIdMember) {

        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar" .
                " FROM transaksi WHERE idmember ='" . $xIdMember . "' order by tglbooking DESC limit 100";
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListtransaksiByIdMemberBelumBayar($xIdMember) {

        $xStr = "SELECT " .
                "idx," .
                "idbooking," .
                "tglbooking," .
                "tglbatalbooking," .
                "keteranganbatal," .
                "harganormal," .
                "hargadiscount," .
                "idvoucher," .
                "idmember," .
                "idpegawai," .
                "spesialrequest," .
                "tglupdate," .
                "idjenisbayar," .
                "tglbayar" .
                " FROM transaksi WHERE idmember ='" . $xIdMember . "' and tglbayar = '0000-00-00' order by tglbooking DESC limit 100";
        $query = $this->db->query($xStr);
        return $query;
    }

    function getCountVoucer($xidvoucher) {
        $xStr = "SELECT " .
                "Count(idx) jmlvoucher" .
                " FROM transaksi WHERE idvoucher ='" . $xidvoucher . "'";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return @$row->jmlvoucher + 0;
    }

    function getisHargaUnikAda($xhargadibayar) {
        $xStr = "SELECT " .
                "idx" .
                " FROM transaksi WHERE hargadibayar ='" . $xhargadibayar . "' and isfinal = 'N'";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return !empty($row->idx);
    }

    function updatetransaksi($xidx, $xkonfirmasitext, $ximgkonfirmasi) {

        $xStr = " UPDATE transaksi SET " .
                "konfirmasitext='" . $xkonfirmasitext . "'," .
                "imgkonfirmasi='" . $ximgkonfirmasi . "'," .
                "tglkonfirmasi=now() WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
    }

}

?>
