<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : imagedetail
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelimagedetail extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListimagedetail() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "linkimage," .
                "keteranganimage," .
                "rancode,idproduk," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM imagedetail   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->iddetailproduk;
        }
        return $xBuffResul;
    }

    function getListimagedetailbyidproduk($xidproduk) {

        $xStr = "SELECT " .
                "idx," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "linkimage," .
                "keteranganimage," .
                "rancode,idproduk," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM imagedetail Where idproduk = '$xidproduk' order by idx DESC ";
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailimagedetailbyidproduk($xidproduk) {

        $xStr = "SELECT " .
                "idx," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "linkimage," .
                "keteranganimage," .
                "rancode,idproduk," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM imagedetail Where idproduk = '$xidproduk' AND iddetailproduk='0' order by idx DESC ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getListimagedetailbyiddetailproduk($xidproduk, $xisfromproduk) {

        if (!strcmp($xisfromproduk, "Y")) {
            $xWhere = "Where idproduk = '$xidproduk'";
        } else {
            $xWhere = "Where iddetailproduk = '$xidproduk'";
        }

        $xStr = "SELECT " .
                "idx," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "linkimage," .
                "keteranganimage," .
                "rancode,idproduk," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM imagedetail $xWhere order by idx DESC ";
//        echo $xStr.'-->'.$xisfromproduk;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListimagedetail($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where iddetailproduk like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "linkimage," .
                "keteranganimage," .
                "rancode,idproduk," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM imagedetail $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailimagedetail($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "linkimage," .
                "keteranganimage," .
                "rancode,idproduk," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM imagedetail  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndeximagedetail() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "linkimage," .
                "keteranganimage," .
                "rancode,idproduk," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM imagedetail order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertimagedetail($xidx, $xiddetailproduk, $xidkategoriproduk, $xlinkimage, $xketeranganimage, $xrancode, $xtglinsert, $xtglupdate, $xidpegawai, $xidproduk) {
        $xStr = " INSERT INTO imagedetail( " .
                "idx," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "linkimage," .
                "keteranganimage," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai,idproduk) VALUES('" . $xidx . "','" . $xiddetailproduk . "','" .
                $xidkategoriproduk . "','" . $xlinkimage . "','" . $xketeranganimage . "','" .
                $xrancode . "',now(),now(),'" . $xidpegawai . "','" . $xidproduk . "')";
        $query = $this->db->query($xStr);
    }

    Function setUpdateimagedetail($xidx, $xiddetailproduk, $xidkategoriproduk, $xlinkimage, $xketeranganimage, $xrancode, $xtglinsert, $xtglupdate, $xidpegawai, $xidproduk) {
        $xStr = " UPDATE imagedetail SET " .
                "idx='" . $xidx . "'," .
                "iddetailproduk='" . $xiddetailproduk . "'," .
                "idkategoriproduk='" . $xidkategoriproduk . "'," .
                "linkimage='" . $xlinkimage . "'," .
                "keteranganimage='" . $xketeranganimage . "'," .
                "rancode='" . $xrancode . "'," .
                "tglupdate=now()," .
                "idproduk='" . $xidproduk . "'," .
                "idpegawai='" . $xidpegawai . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
    }

    function setDeleteimagedetail($xidx) {
        $xStr = " DELETE FROM imagedetail WHERE imagedetail.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeleteimagedetail($xidx);
    }

    function setInsertLogDeleteimagedetail($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'imagedetail',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

?>
