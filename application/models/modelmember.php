<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : member
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelmember extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListmember() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "Nama," .
                "Alamat," .
                "NoTelpon," .
                "idtoken," .
                "email," .
                "password," .
                "tglinsert," .
                "idjenismember," .
                "isblokir" .
                " FROM member order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->Nama;
        }
        return $xBuffResul;
    }

    function getListmember($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where Nama like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "Nama," .
                "Alamat," .
                "NoTelpon," .
                "idtoken," .
                "email," .
                "password," .
                "tglinsert," .
                "idjenismember," .
                "isblokir" .
                " FROM member $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListmemberbydatestatus($xisblokir, $xidjenismember, $xtglawal, $xtglakhir) {
        $xDate = " WHERE 1=1 ";
        if (!empty($xtglawal) && !empty($xtglakhir)) {
            $xDate = " WHERE (DATE(tglinsert) >= '$xtglawal' AND DATE(tglinsert) <= '$xtglakhir') ";
        }
        $xQstatus = "";
        if ($xisblokir !== '-') {
            $xQstatus = "AND isblokir='$xisblokir' ";
        } 
        $xQidjenismember = "";
        if ($xidjenismember !== "-") {
            $xQidjenismember="AND idjenismember='$xidjenismember' ";
        }

        $xStr = "SELECT " .
                "idx," .
                "Nama," .
                "Alamat," .
                "NoTelpon," .
                "idtoken," .
                "email," .
                "password," .
                "tglinsert," .
                "idjenismember," .
                "isblokir" .
                " FROM member $xDate $xQstatus $xQidjenismember order by Nama ASC";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailmember($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "Nama," .
                "Alamat," .
                "NoTelpon," .
                "idtoken," .
                "email," .
                "password," .
                "tglinsert," .
                "idjenismember," .
                "isblokir," . 
                "photoUrl" .
                " FROM member  WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getDetailByToken($xidtoken) {
        $xStr = "SELECT " .
                "idx," .
                "Nama," .
                "Alamat," .
                "NoTelpon," .
                "idtoken," .
                "email," .
                "password," .
                "tglinsert," .
                "idjenismember," .
                "isblokir" .
                " FROM member  WHERE idtoken = '" . $xidtoken . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexmember() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "Nama," .
                "Alamat," .
                "NoTelpon," .
                "idtoken," .
                "email," .
                "password," .
                "tglinsert," .
                "idjenismember," .
                "isblokir" .
                " FROM member order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertmember($xidx, $xNama, $xAlamat, $xNoTelpon, $xidtoken, $xemail, $xpassword, $xtglinsert, $xidjenismember, $xisblokir, $xphotoUrl) {
        $xStr = " INSERT INTO member( " .
                "idx," .
                "Nama," .
                "Alamat," .
                "NoTelpon," .
                "idtoken," .
                "email," .
                "password," .
                "tglinsert," .
                "idjenismember," .
                "isblokir,".
                "photoUrl) VALUES('" . $xidx . "','" . $xNama . "','" . $xAlamat . "','" .
                $xNoTelpon . "','" . $xidtoken . "','" . $xemail . "','" . $xpassword . "',NOW(),'" .
                $xidjenismember . "','" . $xisblokir ."','".$xphotoUrl. "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatemember($xidx, $xNama, $xAlamat, $xNoTelpon, $xidtoken, $xemail, $xpassword, $xtglinsert, $xidjenismember, $xisblokir) {
        $xStr = " UPDATE member SET " .
                "idx='" . $xidx . "'," .
                "Nama='" . $xNama . "'," .
                "Alamat='" . $xAlamat . "'," .
                "NoTelpon='" . $xNoTelpon . "'," .
                "idtoken='" . $xidtoken . "'," .
                "email='" . $xemail . "'," .
                "password='" . $xpassword . "'," .
//                "tglinsert='" . $xtglinsert . "'," .
                "idjenismember='" . $xidjenismember . "'," .
                "isblokir='" . $xisblokir . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdateToken($xidx, $xidtoken) {
        $xStr = " UPDATE member SET " .
                "idtoken='" . $xidtoken . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
    }

    function setDeletemember($xidx) {
        $xStr = " DELETE FROM member WHERE member.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletemember($xidx);
    }

    function setInsertLogDeletemember($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'member',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }
    
    function getIsEksis($xemail) {
        $xStr = "SELECT " .
                "email" .                
                " FROM member WHERE email = '" . addslashes($xemail) . "'";
        //echo $xStr;
        $query = $this->db->query($xStr);
        $row = $query->row();
        if(!empty($row)){
        return FALSE;
        } else {
            return TRUE;
        }
    }
        
    function getDataLoginMember($xemail, $xPassword) {
        $xStr = "SELECT " .
                "idx," .
                "Nama," .
                "Alamat," .
                "NoTelpon," .
                "idtoken," .
                "email," .
                "password," .
                "tglinsert," .
                "idjenismember," .
                "isblokir".
                " FROM member WHERE email = '" . addslashes($xemail) . "' and password = '" . addslashes($xPassword) . "'";
        //echo $xStr;
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getListmemberisidtokennotnull() {
        $xStr = "SELECT " .
                "idx," .
                "Nama," .
                "Alamat," .
                "NoTelpon," .
                "idtoken," .
                "email," .
                "password," .
                "tglinsert," .
                "idjenismember," .
                "isblokir" .
                " FROM member WHERE idtoken IS NOT NULL";
        $query = $this->db->query($xStr);
        return $query;
    }
	
	function getautocompletemember($member) {
        $xStr = "SELECT " .
                "idx," .
                "Nama," .
                "email," .
                "NoTelpon" .
                " FROM member WHERE Nama LIKE '%" . $member . "%' 
				OR email LIKE '%" . $member . "%' 
				OR NoTelpon LIKE '%" . $member . "%' order by idx ";
        $query = $this->db->query($xStr);
        return $query;
    }
}

?>
