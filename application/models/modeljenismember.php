<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : jenismember
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modeljenismember extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListjenismember() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "JenisMember" .
                " FROM jenismember   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->JenisMember;
        }
        return $xBuffResul;
    }
    function getArrayListjenismemberall() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "JenisMember" .
                " FROM jenismember   order by idx ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul['-'] = 'Semua';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->JenisMember;
        }
        return $xBuffResul;
    }

    function getListjenismember($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where JenisMember like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "JenisMember" .
                " FROM jenismember $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailjenismember($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "JenisMember" .
                " FROM jenismember  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexjenismember() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "JenisMember" .
                " FROM jenismember order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertjenismember($xidx, $xJenisMember) {
        $xStr = " INSERT INTO jenismember( " .
                "idx," .
                "JenisMember) VALUES('" . $xidx . "','" . $xJenisMember . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatejenismember($xidx, $xJenisMember) {
        $xStr = " UPDATE jenismember SET " .
                "idx='" . $xidx . "'," .
                "JenisMember='" . $xJenisMember . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletejenismember($xidx) {
        $xStr = " DELETE FROM jenismember WHERE jenismember.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletejenismember($xidx);
    }

    function setInsertLogDeletejenismember($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'jenismember',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

?>