<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : inboxfcm
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelinboxfcm extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListinboxfcm() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "judul," .
                "message," .
                "tglmessage," .
                "isterbaca,idmenuandroid" .
                " FROM inboxfcm   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->idmember;
        }
        return $xBuffResul;
    }

    function getListinboxfcm($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idmember like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "judul," .
                "message," .
                "tglmessage," .
                "isterbaca,idmenuandroid" .
                " FROM inboxfcm $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListinboxfcmandro($xidmember) {
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "judul," .
                "message," .
                "tglmessage," .
                "isterbaca,idmenuandroid" .
                " FROM inboxfcm Where idmember like '%" . $xidmember . "' order by idx DESC limit 0,20";
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailinboxfcm($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "judul," .
                "message," .
                "tglmessage," .
                "isterbaca,idmenuandroid" .
                " FROM inboxfcm  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getDetailinboxfcmIsAdapesan($xidmember) {
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "judul," .
                "message," .
                "tglmessage," .
                "isterbaca,idmenuandroid" .
                " FROM inboxfcm  WHERE idmember = '" . $xidmember . "' and isTerbaca = 'N'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexinboxfcm() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "judul," .
                "message," .
                "tglmessage," .
                "isterbaca,idmenuandroid" .
                " FROM inboxfcm order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertinboxfcm($xidx, $xidmember, $xjudul, $xmessage, $xtglmessage, $xisterbaca, $xidmenuandroid) {
        $xStr = " INSERT INTO inboxfcm( " .
                "idx," .
                "idmember," .
                "judul," .
                "message," .
                "tglmessage," .
                "isterbaca,idmenuandroid) VALUES('" . $xidx . "','" . $xidmember . "','" . $xjudul . "','" .
                $xmessage . "',NOW(),'" . $xisterbaca . "','" . $xidmenuandroid . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdateinboxfcm($xidx, $xidmember, $xjudul, $xmessage, $xtglmessage, $xisterbaca, $xidmenuandroid) {
        $xStr = " UPDATE inboxfcm SET " .
                "idx='" . $xidx . "'," .
                "idmember='" . $xidmember . "'," .
                "judul='" . $xjudul . "'," .
                "message='" . $xmessage . "'," .
                "tglmessage=NOW()," .
                "isterbaca='" . $xisterbaca . "',"
                . "idmenuandroid='" . $xidmenuandroid . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeleteinboxfcm($xidx) {
        $xStr = " DELETE FROM inboxfcm WHERE inboxfcm.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeleteinboxfcm($xidx);
    }

    function setInsertLogDeleteinboxfcm($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'inboxfcm',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

    Function setUpdateinboxfcmTerbaca($xidx) {
        $xStr = " UPDATE inboxfcm SET " .
                "isterbaca='Y' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
    }

    function isSudahDikirim($xidmember, $xmessage) {
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "judul," .
                "message," .
                "tglmessage," .
                "isterbaca,idmenuandroid" .
                " FROM inboxfcm Where idmember = '" . $xidmember . "' AND message='$xmessage'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        if (empty($row->idx)) {
            return false;
        } else {
            return true;
        }
    }

}

?>