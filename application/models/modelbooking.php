<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : booking
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelbooking extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListbooking() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "tglbooking," .
                "idproduk," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "idmember," .
                "tglperuntukandari," .
                "tglperuntukansampai," .
                "jmldewasa," .
                "jmlanak," .
                "jmlhewan," .
                "keterangantambahn," .
                "jmltransfer," .
                "idjenispembayaran," .
                "nomorkartu," .
                "tgltransfer," .
                "status," .
                "tglinsert," .
                "tglupdate,isFinal,tglFinal,isSubmit,tglSubmit" .
                " FROM booking   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->tglbooking;
        }
        return $xBuffResul;
    }

    function getListbooking($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where tglbooking like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "tglbooking," .
                "idproduk," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "idmember," .
                "tglperuntukandari," .
                "tglperuntukansampai," .
                "jmldewasa," .
                "jmlanak," .
                "jmlhewan," .
                "keterangantambahn," .
                "jmltransfer," .
                "idjenispembayaran," .
                "nomorkartu," .
                "tgltransfer," .
                "status," .
                "tglinsert," .
                "tglupdate" .
                " FROM booking $xSearch order by tglbooking DESC, status ASC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListbookingByIdMember($xidMember) {

        $xStr = "SELECT " .
                "idx," .
                "tglbooking," .
                "idproduk," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "idmember," .
                "tglperuntukandari," .
                "tglperuntukansampai,DATEDIFF(tglperuntukansampai,tglperuntukandari) as jmlhari," .
                "jmldewasa," .
                "jmlanak," .
                "jmlhewan," .
                "keterangantambahn," .
                "jmltransfer," .
                "idjenispembayaran," .
                "nomorkartu," .
                "tgltransfer," .
                "booking.status," .
                "tglinsert," .
                "tglupdate" .
                " FROM booking WHERE idmember ='" . $xidMember . "' and booking.status = '0'";
        $query = $this->db->query($xStr);

        return $query;
    }

    function getListdetailprodukbyidbooking($xidBooking) {
        $xWhere = " WHERE idx IN (" . $xidBooking . ")";
        if ($xidBooking === '0') {
            $xWhere = "";
        }
        $xStr = "SELECT " .
                "idx," .
                "iddetailproduk" .
                " FROM booking $xWhere order by idx ASC";
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailbooking($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "tglbooking," .
                "idproduk," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "idmember," .
                "tglperuntukandari," .
                "tglperuntukansampai," .
                "jmldewasa," .
                "jmlanak," .
                "jmlhewan," .
                "keterangantambahn," .
                "jmltransfer," .
                "idjenispembayaran," .
                "nomorkartu," .
                "tgltransfer," .
                "status," .
                "tglinsert," .
                "tglupdate" .
                " FROM booking  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexbooking() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "tglbooking," .
                "idproduk," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "idmember," .
                "tglperuntukandari," .
                "tglperuntukansampai," .
                "jmldewasa," .
                "jmlanak," .
                "jmlhewan," .
                "keterangantambahn," .
                "jmltransfer," .
                "idjenispembayaran," .
                "nomorkartu," .
                "tgltransfer," .
                "status," .
                "tglinsert," .
                "tglupdate" .
                " FROM booking order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertbooking($xidx, $xtglbooking, $xidproduk, $xiddetailproduk, $xidkategoriproduk, $xidmember, $xtglperuntukandari, $xtglperuntukansampai, $xjmldewasa, $xjmlanak, $xjmlhewan, $xketerangantambahn, $xjmltransfer, $xidjenispembayaran, $xnomorkartu, $xtgltransfer, $xharga, $xhargadiscount) {
        $xStr = " INSERT INTO booking( " .
                "idx," .
                "tglbooking," .
                "idproduk," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "idmember," .
                "tglperuntukandari," .
                "tglperuntukansampai," .
                "jmldewasa," .
                "jmlanak," .
                "jmlhewan," .
                "keterangantambahn," .
                "jmltransfer," .
                "idjenispembayaran," .
                "nomorkartu," .
                "tgltransfer," .
                "status," .
                "tglinsert," .
                "tglupdate,harga,hargadiscount,jmlhari) VALUES('" . $xidx . "',now(),'" . $xidproduk . "','" . $xiddetailproduk . "','" .
                $xidkategoriproduk . "','" . $xidmember . "','" . $xtglperuntukandari . "','" . $xtglperuntukansampai . "','" .
                $xjmldewasa . "','" . $xjmlanak . "','" . $xjmlhewan . "','" . $xketerangantambahn . "','" .
                $xjmltransfer . "','" . $xidjenispembayaran . "','" . $xnomorkartu . "','" .
                $xtgltransfer . "','0',NOW(),NOW(),'" . $xharga . "','" . $xhargadiscount . "',DATEDIFF('$xtglperuntukansampai','$xtglperuntukandari'))";
        $query = $this->db->query($xStr);
        //echo $xStr;
        //return $xidx;
    }

    Function setUpdatebooking($xidx, $xtglbooking, $xidproduk, $xiddetailproduk, $xidkategoriproduk, $xidmember, $xtglperuntukandari, $xtglperuntukansampai, $xjmldewasa, $xjmlanak, $xjmlhewan, $xketerangantambahn, $xjmltransfer, $xidjenispembayaran, $xnomorkartu, $xharga, $xhargadiscount) {
        $xStr = " UPDATE booking SET " .
                "idx='" . $xidx . "'," .
                //     "tglbooking='" . $xtglbooking . "'," .
                "idproduk='" . $xidproduk . "'," .
                "iddetailproduk='" . $xiddetailproduk . "'," .
                "idkategoriproduk='" . $xidkategoriproduk . "'," .
                "idmember='" . $xidmember . "'," .
                "tglperuntukandari='" . $xtglperuntukandari . "'," .
                "tglperuntukansampai='" . $xtglperuntukansampai . "'," .
                "jmldewasa='" . $xjmldewasa . "'," .
                "jmlanak='" . $xjmlanak . "'," .
                "jmlhewan='" . $xjmlhewan . "'," .
                "keterangantambahn='" . $xketerangantambahn . "'," .
                "jmltransfer='" . $xjmltransfer . "'," .
                "idjenispembayaran='" . $xidjenispembayaran . "'," .
                "harga='" . $xharga . "'," .
                "hargadiscount='" . $xhargadiscount . "'," .
//                "status='" . $xstatus . "'," .
//                "tglinsert='" . $xtglinsert . "'," .
                "tglupdate=NOW() WHERE idx = '" . $xidx . "'";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setUpdateProsesbooking($xidx, $xstatus) {
        $xStr = " UPDATE booking SET " .
                "idx='" . $xidx . "'," .
                "booking.status='" . $xstatus . "'," .
                "tglupdate=NOW() WHERE idx = '" . $xidx . "'";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setBatalbooking($xidtransaksi) {
        $xStr = "SELECT idbooking FROM transaksi WHERE transaksi.idx='$xidtransaksi' AND isfinal='Y'";
        $query = $this->db->query($xStr);
        $idbooking = '';
        $row = $query->row();
        if (!empty($row->idbooking)) {
            if ($row->idbooking !== ',') {
                $idbooking = $row->idbooking;
                $xStr2 = " UPDATE booking SET " .
                        "status='2'," .
                        "tglupdate=NOW() WHERE idx in (" . $idbooking . ") AND booking.status='1'";
//        echo $xStr2;
                $query2 = $this->db->query($xStr2);
                return $query2;
            }
        }
    }

    function setDeletebooking($xidx) {
        $xStr = " DELETE FROM booking WHERE booking.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletebooking($xidx);
    }

    function setInsertLogDeletebooking($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'booking',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

    function getJumlahBooking($xIdMember) {

        $xStr = "SELECT " .
                "count(idx) jmlbooking" .
                " FROM booking WHERE status='0' AND idmember = '" . $xIdMember . "'";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return @$row->jmlbooking + 0;
    }

    function getJumlahBookingbyIddetproduk($xIdDetProduk, $date_awal, $date_akhir) {
        $xQDate = '';
        if (!empty($date_awal) && !empty($date_akhir)) {
            $xQDate = "AND (DATE(tglbooking)>='$date_awal' AND DATE(tglbooking)<='$date_akhir')";
        }
        $xStr = "SELECT " .
                "count(idx) jmlbooking" .
                " FROM booking WHERE status<>'2' AND iddetailproduk = '" . $xIdDetProduk . "' $xQDate ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return @$row->jmlbooking + 0;
    }

    function getStatusBooking($xIdMember, $xidproduk, $xiddetailproduk) {

        $xStr = "SELECT " .
                "idx,status" .
                " FROM booking WHERE idproduk ='" . $xidproduk . "' AND iddetailproduk='" . $xiddetailproduk . "' AND idmember = '" . $xIdMember . "'";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getBookingDetailBookingByIdMember($xIdMember, $xidproduk, $xiddetailproduk) {

        $xStr = "SELECT " .
                "idx," .
                "tglbooking," .
                "idproduk," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "idmember," .
                "tglperuntukandari," .
                "tglperuntukansampai," .
                "jmldewasa," .
                "jmlanak," .
                "jmlhewan," .
                "keterangantambahn," .
                "jmltransfer," .
                "idjenispembayaran," .
                "nomorkartu," .
                "tgltransfer," .
                "status," .
                "tglinsert," .
                "tglupdate" .
                " FROM booking  WHERE idproduk ='" . $xidproduk . "' AND iddetailproduk='" . $xiddetailproduk . "'"
                . " AND idmember = '" . $xIdMember . "' AND (status in(0,1))";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setUpdatebookingFromBookingNow($xidx) {
        $xStr = " UPDATE booking SET " .
                "status='1'," .
                "tglupdate=NOW() WHERE idx in (" . $xidx . ")";
//        echo $xStr;
        $query = $this->db->query($xStr);
    }

    function getListbookingInIdx($xlistidx) {

        $xStr = "SELECT " .
                "idx," .
                "tglbooking," .
                "idproduk," .
                "iddetailproduk," .
                "idkategoriproduk," .
                "idmember," .
                "tglperuntukandari," .
                "tglperuntukansampai," .
                "jmldewasa," .
                "jmlanak," .
                "jmlhewan," .
                "keterangantambahn," .
                "jmltransfer," .
                "idjenispembayaran," .
                "nomorkartu," .
                "tgltransfer," .
                "status," .
                "tglinsert," .
                "tglupdate,status,harga,hargadiscount,DATEDIFF(tglperuntukansampai,tglperuntukandari) jmlhari" .
                " FROM booking WHERE idx in ($xlistidx) ";
        $query = $this->db->query($xStr);
        //   echo $xStr;
        return $query;
    }

}

?>
