<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : jenispembayaran
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modeljenispembayaran extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListjenispembayaran() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "jenispembayaran,jenispembayaranING" .
                " FROM jenispembayaran   order by idx ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul[0] = '-';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->jenispembayaran;
        }
        return $xBuffResul;
    }
    
    function getArrayListjenispembayaranall() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "jenispembayaran,jenispembayaranING" .
                " FROM jenispembayaran   order by idx ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul['0'] = 'Semua';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->jenispembayaran;
        }
        return $xBuffResul;
    }

    function getListjenispembayaran($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where jenispembayaran like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "jenispembayaran,jenispembayaranING" .
                " FROM jenispembayaran $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailjenispembayaran($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "jenispembayaran,jenispembayaranING" .
                " FROM jenispembayaran  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexjenispembayaran() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "jenispembayaran" .
                " FROM jenispembayaran order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertjenispembayaran($xidx, $xjenispembayaran,$xjenispembayaranING) {
        $xStr = " INSERT INTO jenispembayaran( " .
                "idx," .
                "jenispembayaran,jenispembayaranING) VALUES('" . $xidx . "','" . $xjenispembayaran . "','" . $xjenispembayaranING . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatejenispembayaran($xidx, $xjenispembayaran,$xjenispembayaranING) {
        $xStr = " UPDATE jenispembayaran SET " .
                "idx='" . $xidx . "'," .
                "jenispembayaran='" . $xjenispembayaran . "',jenispembayaranING ='".$xjenispembayaranING."' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletejenispembayaran($xidx) {
        $xStr = " DELETE FROM jenispembayaran WHERE jenispembayaran.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletejenispembayaran($xidx);
    }

    function setInsertLogDeletejenispembayaran($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'jenispembayaran',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

?>