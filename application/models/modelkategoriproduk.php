<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : kategoriproduk
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelkategoriproduk extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListkategoriproduk() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "Kategori" .
                " FROM kategoriproduk   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->Kategori;
        }
        return $xBuffResul;
    }

    function getArrayListkategoriprodukall() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "Kategori" .
                " FROM kategoriproduk   order by idx ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul['0'] = 'Semua';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->Kategori;
        }
        return $xBuffResul;
    }

    function getListkategoriproduk($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where Kategori like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "Kategori" .
                " FROM kategoriproduk $xSearch order by idx ASC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListkategoriprodukbyid($xidx) {
        $xSearch = "";
        if ($xidx !== '0') {
            $xSearch = "Where idx = '" . $xidx . "'";
        }
        $xStr = "SELECT " .
                "idx," .
                "Kategori" .
                " FROM kategoriproduk $xSearch order by idx ASC";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailkategoriproduk($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "Kategori" .
                " FROM kategoriproduk  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexkategoriproduk() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "Kategori" .
                " FROM kategoriproduk order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertkategoriproduk($xidx, $xKategori) {
        $xStr = " INSERT INTO kategoriproduk( " .
                "idx," .
                "Kategori) VALUES('" . $xidx . "','" . $xKategori . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatekategoriproduk($xidx, $xKategori) {
        $xStr = " UPDATE kategoriproduk SET " .
                "idx='" . $xidx . "'," .
                "Kategori='" . $xKategori . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletekategoriproduk($xidx) {
        $xStr = " DELETE FROM kategoriproduk WHERE kategoriproduk.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletekategoriproduk($xidx);
    }

    function setInsertLogDeletekategoriproduk($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'kategoriproduk',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

?>