<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : produk
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelproduk extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListproduk() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->JudulProduk;
        }
        return $xBuffResul;
    }

    function getArrayListprodukall($idkategoriproduk) { /* spertinya perlu lock table */
        $xWhere = " WHERE idKategoriProduk = " . $idkategoriproduk;
        if ($idkategoriproduk === '0') {
            $xWhere = "";
        }
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk $xWhere order by idx ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul['0'] = 'Semua';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->JudulProduk;
        }
        return $xBuffResul;
    }

    function getListproduk($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where JudulProduk like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListprodukidbyKategori($xAwal, $xLimit, $xidKategoriProduk, $xSearch = '') {
        $xWhere = " WHERE idKategoriProduk = " . $xidKategoriProduk;
        if (!empty($xSearch)) {
            $xWhere .= " AND JudulProduk like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk $xWhere order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListprodukbyidkategori($xidKategoriProduk, $xidProduk) {
        $xWhere = " WHERE idKategoriProduk = " . $xidKategoriProduk;
        if ($xidKategoriProduk === '0') {
            $xWhere = "";
        }
        $xWhereProduk = " AND idx = " . $xidProduk;
        if ($xidProduk === '0' || $xidProduk === 'undefined') {
            $xWhereProduk = "";
        }
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk $xWhere $xWhereProduk order by JudulProduk ASC";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailproduk($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexproduk() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xkapasitas, $xstandartpemakaian, $xidsatuan, $xidpegawai) {
        $xStr = " INSERT INTO produk( " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai) VALUES('" . $xidx . "','" . $xJudulProduk . "','" . $xidKategoriProduk . "','" . $xKeterangan . "','" . $xphonekontak . "','" . $xNamaKontak . "','" . $xDiskripsiProduk . "','" . $xmapaddress . "','" . $xrate . "','" . $xratediscount . "','" . $xrancode . "','" . $xtglinsert . "','" . $xtglupdate . "','" . $xkapasitas . "','" . $xstandartpemakaian . "','" . $xidsatuan . "','" . $xidpegawai . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdateproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xkapasitas, $xstandartpemakaian, $xidsatuan, $xidpegawai) {
        $xStr = " UPDATE produk SET " .
                "idx='" . $xidx . "'," .
                "JudulProduk='" . $xJudulProduk . "'," .
                "idKategoriProduk='" . $xidKategoriProduk . "'," .
                "Keterangan='" . $xKeterangan . "'," .
                "phonekontak='" . $xphonekontak . "'," .
                "NamaKontak='" . $xNamaKontak . "'," .
                "DiskripsiProduk='" . $xDiskripsiProduk . "'," .
                "mapaddress='" . $xmapaddress . "'," .
                "rate='" . $xrate . "'," .
                "ratediscount='" . $xratediscount . "'," .
                "rancode='" . $xrancode . "'," .
                "tglinsert='" . $xtglinsert . "'," .
                "tglupdate='" . $xtglupdate . "'," .
                "kapasitas='" . $xkapasitas . "'," .
                "standartpemakaian='" . $xstandartpemakaian . "'," .
                "idsatuan='" . $xidsatuan . "'," .
                "idpegawai='" . $xidpegawai . "'" .
                "WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeleteproduk($xidx) {
        $xStr = " DELETE FROM produk WHERE produk.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeleteproduk($xidx);
    }

    function setInsertLogDeleteproduk($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'produk',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

?>