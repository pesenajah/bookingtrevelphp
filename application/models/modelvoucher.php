<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : voucher
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelvoucher extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListvoucher() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                " FROM voucher   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->voucher;
        }
        return $xBuffResul;
    }

    function getArrayListvoucherall() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                " FROM voucher WHERE voucher<>'-' order by idx ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul['0'] = 'Semua';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->voucher;
        }
        return $xBuffResul;
    }

    function getListvoucher($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "AND voucher like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                " FROM voucher WHERE voucher<>'-' $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListpromo($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "AND voucher like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                " FROM voucher WHERE voucher='-' $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListvoucherbydatestatus($xishabis, $xidvoucher, $xtglawal, $xtglakhir) {
        $cek = "SELECT COUNT(idx) jmlterpakai FROM transaksi WHERE idvoucher=(SELECT voucher FROM voucher WHERE idx = '$xidvoucher')";
        $querycek = $this->db->query($cek);
        $rowcek = $querycek->row();
        $jmlterpakai = $rowcek->jmlterpakai;

        $xDate = "";
        if (!empty($xtglawal) && !empty($xtglakhir)) {
            $xDate = " AND (DATE(tglinsert) >= '$xtglawal' AND DATE(tglinsert) <= '$xtglakhir') ";
        }
        $xQstatus = "";
        if ($xishabis === 'N') {
            $xQstatus = "AND jumlahmaxpengguna > $jmlterpakai ";
        } elseif ($xishabis === 'Y') {
            $xQstatus = "AND jumlahmaxpengguna <= $jmlterpakai ";
        }
        $xQidvoucher = "";
        if ($xidvoucher !== '0') {
            $xQidvoucher = "AND idx = '$xidvoucher' ";
        }

        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                " FROM voucher WHERE voucher<>'-' $xDate $xQstatus $xQidvoucher order by idx DESC";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListpromobydatestatus($xisberlaku, $xtglawal, $xtglakhir) {

//        $xDate = "";
//        if (!empty($xtglawal) && !empty($xtglakhir)) {
//            $xDate = " AND (DATE(tglinsert) >= '$xtglawal' AND DATE(tglinsert) <= '$xtglakhir') ";
//        }
        $xQstatus = "";
        if ($xisberlaku === '1') {
            $xQstatus = "AND (tglberlakudari <= DATE(NOW()) AND tglberlakusampai >= DATE(NOW())) ";
        } elseif ($xisberlaku === '2') {
            $xQstatus = "AND (tglberlakudari < DATE(NOW()) AND tglberlakusampai < DATE(NOW())) ";
        } elseif ($xisberlaku === '0') {
            $xQstatus = "AND (tglberlakudari > DATE(NOW()) AND tglberlakusampai > DATE(NOW())) ";
        }
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                " FROM voucher WHERE voucher='-' $xQstatus order by idx DESC";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $query;
    }

    function cekStatusPromo($xidx) {
        $xStr = "SELECT " .
                "idx" .
                " FROM voucher WHERE voucher='-' AND idx = '$xidx' AND (tglberlakudari <= DATE(NOW()) AND tglberlakusampai >= DATE(NOW()))"
                . " order by idx DESC";
//        echo $xStr;
        $query = $this->db->query($xStr);
        $row = $query->row();
        
        $xStr2 = "SELECT " .
                "idx" .
                " FROM voucher WHERE voucher='-' AND idx = '$xidx' AND (tglberlakudari > DATE(NOW()) AND tglberlakusampai > DATE(NOW()))"
                . " order by idx DESC";
//        echo $xStr;
        $query2 = $this->db->query($xStr2);
        $row2 = $query2->row();
        
        if (!empty($row->idx)) {
            return '1';
        } elseif (!empty($row2->idx)) {
            return '0';
        } else {
            return '2';
        }
    }

    function getSisaVoucher($xidvoucher) {
        $cek = "SELECT COUNT(idx) jmlterpakai FROM transaksi WHERE idvoucher=(SELECT voucher FROM voucher WHERE idx = '$xidvoucher')";
        $querycek = $this->db->query($cek);
        $rowcek = $querycek->row();
        $jmlterpakai = $rowcek->jmlterpakai;

        $xStr = "SELECT " .
                "idx," .
                "(jumlahmaxpengguna - $jmlterpakai) sisa" .
                " FROM voucher order by idx DESC";
//        echo $xStr;
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getDetailvoucher($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                " FROM voucher  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexvoucher() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                " FROM voucher order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertvoucher($xidx, $xvoucher, $xnominal, $xjumlahmaxpengguna, $xtglberlakudari, $xtglberlakusampai, $xidproduk, $xpenjelasan, $xlinkimage) {
        $xStr = " INSERT INTO voucher( " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                ") VALUES('" . $xidx . "','" . $xvoucher . "','" . $xnominal . "','" . $xjumlahmaxpengguna . "','" . $xtglberlakudari . "','" . $xtglberlakusampai . "','" . $xidproduk . "','" . $xpenjelasan . "','" . $xlinkimage . "')";
//        echo $xStr;
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatevoucher($xidx, $xvoucher, $xnominal, $xjumlahmaxpengguna, $xtglberlakudari, $xtglberlakusampai, $xidproduk, $xpenjelasan, $xlinkimage) {
        $xStr = " UPDATE voucher SET " .
                "idx='" . $xidx . "'," .
                "voucher='" . $xvoucher . "'," .
                "nominal='" . $xnominal . "'," .
                "jumlahmaxpengguna='" . $xjumlahmaxpengguna . "'," .
                "tglberlakudari='" . $xtglberlakudari . "'," .
                "tglberlakusampai='" . $xtglberlakusampai . "'," .
                "idproduk='" . $xidproduk . "'," .
                "penjelasan='" . $xpenjelasan . "'," .
                "linkimage='" . $xlinkimage . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletevoucher($xidx) {
        $xStr = " DELETE FROM voucher WHERE voucher.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletevoucher($xidx);
    }

    function setInsertLogDeletevoucher($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'voucher',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

//    function cekKesamaanvoucher($xvoucher) {
//        $xStr = "SELECT " .
//                "idx," .
//                "voucher," .
//                "nominal," .
//                "tglberlakudari," .
//                "tglberlakusampai," .
//                "idmember," .
//                "isterpakai," .
//                "tglpakai" .
//                " FROM voucher  WHERE voucher = '" . $xvoucher . "'";
//
//        $query = $this->db->query($xStr);
//        $row = $query->row();
//        return $row;
//    }
//
//    function setAutoCompleteMember($nama_member) {
//        $perintah = "
//		SELECT 	`idx`, 
//                        `Nama` 
//		FROM 	`member` 
//		WHERE	`Nama` LIKE '%$nama_member%'
//	";
//        $hasilPerintah = $this->db->query($perintah);
//        return $hasilPerintah;
//    }

    function getListvoucherByIdMember($xidMember) {
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idmember," .
                "isterpakai," .
                "tglpakai" .
                " FROM voucher Where idmember ='" . $xidMember . "' and isterpakai= 'N' and date(tglberlakusampai)>= current_date";
        $query = $this->db->query($xStr);
        return $query;
    }

    Function setUpdatevoucherFromBooking($xvoucher) {
        if (!empty($xvoucher)) {
            $xvoucher = "'" . str_replace(",", "','", $xvoucher) . "'";
        } else {
            $xvoucher = "00000";
        }
        $xStr = " UPDATE voucher SET " .
                "isterpakai='Y'," .
                "tglpakai=now() WHERE voucher in (" . $xvoucher . ")";
        //echo $xStr;
        $query = $this->db->query($xStr);
    }

    function getListvoucherByListVC($xvoucher) {
        if (!empty($xvoucher)) {
            $xvoucher = "'" . str_replace(",", "','", $xvoucher) . "'";
        } else {
            $xvoucher = "00000";
        }
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idmember," .
                "isterpakai," .
                "tglpakai" .
                " FROM voucher Where voucher IN ($xvoucher)";
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailVoucherByIDVC($xvoucher) {
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "linkimage,jumlahmaxpengguna" .
                " FROM voucher  WHERE voucher = '" . $xvoucher . "' AND tglberlakudari <= current_date AND tglberlakusampai >= current_date";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getListvoucherAndroid() {
        $xStr = "SELECT " .
                "idx," .
                "voucher," .
                "nominal," .
                "jumlahmaxpengguna," .
                "tglberlakudari," .
                "tglberlakusampai," .
                "idproduk," .
                "penjelasan," .
                "linkimage" .
                " FROM voucher WHERE date(tglberlakusampai)>= current_date AND jumlahmaxpengguna > 0 ";
        $query = $this->db->query($xStr);
        return $query;
    }

}

?>
