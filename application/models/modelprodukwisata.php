<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : produk
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelprodukwisata extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListproduk() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk  where idKategoriProduk='3' order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->JudulProduk;
        }
        return $xBuffResul;
    }

    function getListproduk($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where JudulProduk like '%" . $xSearch . "%' and idKategoriProduk='3'";
        } else {
            $xSearch = "where idKategoriProduk='3'";
        }
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailproduk($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk where idKategoriProduk='3' and idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexproduk() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM produk where idKategoriProduk='3' order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xkapasitas, $xstandartpemakaian, $xidsatuan, $xidpegawai) {
        $xStr = " INSERT INTO produk( " .
                "idx," .
                "JudulProduk," .
                "idKategoriProduk," .
                "Keterangan," .
                "phonekontak," .
                "NamaKontak," .
                "DiskripsiProduk," .
                "mapaddress," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai) VALUES('" . $xidx . "','" . $xJudulProduk . "','" . $xidKategoriProduk . "','" . $xKeterangan . "','" . $xphonekontak . "','" . $xNamaKontak . "','" . $xDiskripsiProduk . "','" . $xmapaddress . "','" . $xrate . "','" . $xratediscount . "','" . $xrancode . "',NOW(),NOW(),'" . $xkapasitas . "','" . $xstandartpemakaian . "','" . $xidsatuan . "','" . $xidpegawai . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdateproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xkapasitas, $xstandartpemakaian, $xidsatuan, $xidpegawai) {
        $xStr = " UPDATE produk SET " .
                "idx='" . $xidx . "'," .
                "JudulProduk='" . $xJudulProduk . "'," .
                "idKategoriProduk='" . $xidKategoriProduk . "'," .
                "Keterangan='" . $xKeterangan . "'," .
                "phonekontak='" . $xphonekontak . "'," .
                "NamaKontak='" . $xNamaKontak . "'," .
                "DiskripsiProduk='" . $xDiskripsiProduk . "'," .
                "mapaddress='" . $xmapaddress . "'," .
                "rate='" . $xrate . "'," .
                "ratediscount='" . $xratediscount . "'," .
                "rancode='" . $xrancode . "'," .
//                "tglinsert='" . $xtglinsert . "'," .
                "tglupdate=NOW()," .
                "kapasitas='" . $xkapasitas . "'," .
                "standartpemakaian='" . $xstandartpemakaian . "'," .
                "idsatuan='" . $xidsatuan . "'," .
                "idpegawai='" . $xidpegawai . "'" .
                "WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeleteproduk($xidx) {
        $xStr = " DELETE FROM produk WHERE produk.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeleteproduk($xidx);
    }

    function setInsertLogDeleteproduk($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'produk',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

?>