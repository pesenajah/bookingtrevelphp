<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : detailproduk
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modeldetailproduk extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListdetailproduk() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "idproduk," .
                "idkategoriproduk," .
                "juduldetailproduk," .
                "diskripsiproduk," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM detailproduk   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->juduldetailproduk;
        }
        return $xBuffResul;
    }

    function getArrayListdetailprodukall($xidproduk) { /* spertinya perlu lock table */
        $xWhere = " WHERE idproduk = " . $xidproduk;
        if ($xidproduk === '0') {
            $xWhere = "";
        }
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "idproduk," .
                "idkategoriproduk," .
                "juduldetailproduk," .
                "diskripsiproduk," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM detailproduk $xWhere order by idx ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul['0'] = 'Semua';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->juduldetailproduk;
        }
        return $xBuffResul;
    }

    function getListdetailproduk($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idproduk like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "idproduk," .
                "idkategoriproduk," .
                "juduldetailproduk," .
                "diskripsiproduk," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM detailproduk $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListdetailprodukbyid($xAwal, $xLimit, $xSearch = '', $xidproduk) {
        if (!empty($xSearch)) {
            $xSearch = "AND juduldetailproduk like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "idproduk," .
                "idkategoriproduk," .
                "juduldetailproduk," .
                "diskripsiproduk," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM detailproduk Where idproduk = '$xidproduk' $xSearch order by idx DESC  limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListdetailprodukbyidproduk($xidProduk, $xidDetailProduk) {
        $xWhere = " WHERE idproduk = " . $xidProduk;
        if ($xidProduk === '0') {
            $xWhere = "";
        }
        $xWhereDetProduk = " AND idx = " . $xidDetailProduk;
        if ($xidDetailProduk === '0' || $xidDetailProduk === 'undefined') {
            $xWhereDetProduk = "";
        }
        $xStr = "SELECT " .
                "idx," .
                "idproduk," .
                "idkategoriproduk," .
                "juduldetailproduk," .
                "diskripsiproduk," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM detailproduk $xWhere $xWhereDetProduk order by juduldetailproduk ASC";
        $query = $this->db->query($xStr);
        return $query;
    }

    function getListtambahdetailprodukbyid($xidproduk) {
        $xStr = "SELECT " .
                "idx," .
                "idproduk," .
                "idkategoriproduk," .
                "juduldetailproduk," .
                "diskripsiproduk," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM detailproduk Where idproduk = '$xidproduk'";
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetaildetailproduk($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "idproduk," .
                "idkategoriproduk," .
                "juduldetailproduk," .
                "diskripsiproduk," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM detailproduk  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexdetailproduk() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "idproduk," .
                "idkategoriproduk," .
                "juduldetailproduk," .
                "diskripsiproduk," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai" .
                " FROM detailproduk order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertdetailproduk($xidx, $xidproduk, $xidkategoriproduk, $xjuduldetailproduk, $xdiskripsiproduk, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xkapasitas, $xstandartpemakaian, $xidsatuan, $xidpegawai) {
        $xStr = " INSERT INTO detailproduk( " .
                "idx," .
                "idproduk," .
                "idkategoriproduk," .
                "juduldetailproduk," .
                "diskripsiproduk," .
                "rate," .
                "ratediscount," .
                "rancode," .
                "tglinsert," .
                "tglupdate," .
                "kapasitas," .
                "standartpemakaian," .
                "idsatuan," .
                "idpegawai) VALUES('" . $xidx . "','" . $xidproduk . "','" . $xidkategoriproduk . "','" . $xjuduldetailproduk . "','" . $xdiskripsiproduk . "','" . $xrate . "','" . $xratediscount . "','" . $xrancode . "',now(),now(),'" . $xkapasitas . "','" . $xstandartpemakaian . "','" . $xidsatuan . "','" . $xidpegawai . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatedetailproduk($xidx, $xidproduk, $xidkategoriproduk, $xjuduldetailproduk, $xdiskripsiproduk, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xkapasitas, $xstandartpemakaian, $xidsatuan, $xidpegawai) {
        $xStr = " UPDATE detailproduk SET " .
                "idx='" . $xidx . "'," .
                "idproduk='" . $xidproduk . "'," .
                "idkategoriproduk='" . $xidkategoriproduk . "'," .
                "juduldetailproduk='" . $xjuduldetailproduk . "'," .
                "diskripsiproduk='" . $xdiskripsiproduk . "'," .
                "rate='" . $xrate . "'," .
                "ratediscount='" . $xratediscount . "'," .
                "rancode='" . $xrancode . "'," .
                "tglupdate=now()," .
                "kapasitas='" . $xkapasitas . "'," .
                "standartpemakaian='" . $xstandartpemakaian . "'," .
                "idsatuan='" . $xidsatuan . "'," .
                "idpegawai='" . $xidpegawai . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletedetailproduk($xidx) {
        $xStr = " DELETE FROM detailproduk WHERE detailproduk.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletedetailproduk($xidx);
    }

    function setInsertLogDeletedetailproduk($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'detailproduk',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

    function getRangeHargaproduk($idproduk) { /* spertinya perlu lock table */
        $xStrLow = "SELECT rate FROM detailproduk WHERE idproduk = " . $idproduk . " ORDER BY rate ASC LIMIT 0,1;";
        $xStrHigh = "SELECT rate FROM detailproduk WHERE idproduk = " . $idproduk . " ORDER BY rate DESC LIMIT 0,1;";
        $queryLow = $this->db->query($xStrLow);
        $queryHigh = $this->db->query($xStrHigh);
        $rowLow = $queryLow->row();
        $rowHigh = $queryHigh->row();
        // $lowRate = substr($rowLow->rate,0,-3);
        // $highRate = substr($rowHigh->rate,0,-3);
        $lowRate = number_format($rowLow->rate, 0, '.', ',');
        $highRate = number_format($rowHigh->rate, 0, '.', ',');
        $rangeHarga = "Rp " . $lowRate . " - Rp " . $highRate;
        if ($rowLow == $rowHigh) {
            $rangeHarga = "Rp " . $lowRate;
        }
        return $rangeHarga;
    }

}

?>