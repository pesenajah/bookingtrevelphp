<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : bataswaktu
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelbataswaktu extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListbataswaktu() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "bataswaktu" .
                " FROM bataswaktu   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->bataswaktu;
        }
        return $xBuffResul;
    }

    function getListbataswaktu($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where bataswaktu like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "bataswaktu" .
                " FROM bataswaktu $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailbataswaktu($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "bataswaktu" .
                " FROM bataswaktu  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexbataswaktu() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "bataswaktu" .
                " FROM bataswaktu order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertbataswaktu($xidx, $xbataswaktu) {
        $xcek = "SELECT * FROM bataswaktu";
        $query = $this->db->query($xcek);
        $row = $query->row();
//        echo $row->idx;
        if (empty($row->idx)) {
            $xStr = " INSERT INTO bataswaktu( " .
                    "idx," .
                    "bataswaktu) VALUES('" . $xidx . "','" . $xbataswaktu . "')";
            $query = $this->db->query($xStr);
        } else {
            $this->setUpdatebataswaktu($row->idx, $xbataswaktu);
        }
        return $xidx;
    }

    Function setUpdatebataswaktu($xidx, $xbataswaktu) {
        $xStr = " UPDATE bataswaktu SET " .
                "idx='" . $xidx . "'," .
                "bataswaktu='" . $xbataswaktu . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletebataswaktu($xidx) {
        $xStr = " DELETE FROM bataswaktu WHERE bataswaktu.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletebataswaktu($xidx);
    }

    function setInsertLogDeletebataswaktu($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'bataswaktu',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

?>