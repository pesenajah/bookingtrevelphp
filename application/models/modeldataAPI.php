<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : dataAPI
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modeldataAPI extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //General API
    function getAPIListCurrency($token) {
        $linkAPIListCurrency = getAPILink() . "general_api/listCurrency?token=" . $token . "&output=json";
        $jsonAPIListCurrency = getDataWithCURL($linkAPIListCurrency);
        $objAPIListCurrency = json_decode($jsonAPIListCurrency);
        isAPISuccess($objAPIListCurrency);
        return $objAPIListCurrency->result;
    }

    function getAPIListLanguage($token) {
        $linkAPIListLanguage = getAPILink() . "general_api/listLanguage?token=" . $token . "&output=json";
        $jsonAPIListLanguage = getDataWithCURL($linkAPIListLanguage);
        $objAPIListLanguage = json_decode($jsonAPIListLanguage);
        isAPISuccess($objAPIListLanguage);
        return $objAPIListLanguage->result;
    }

    function getAPIListCountry($token) {
        $linkAPIListCountry = getAPILink() . "general_api/listCountry?token=" . $token . "&output=json";
        $jsonAPIListCountry = getDataWithCURL($linkAPIListCountry);
        $objAPIListCountry = json_decode($jsonAPIListCountry);
        isAPISuccess($objAPIListCountry);
        return $objAPIListCountry->listCountry;
    }

    function getAPIPolicies($token) {
        $linkAPIListCountry = getAPILink() . "general_api/getPolicies?token=" . $token . "&output=json";
        $jsonAPIListCountry = getDataWithCURL($linkAPIListCountry);
        $objAPIListCountry = json_decode($jsonAPIListCountry);
        isAPISuccess($objAPIListCountry);
//        return $objAPIListCountry->listCountry;
        return $jsonAPIListCountry;
    }

    //Hotel API
//ini cuma contoh json hotel
//    https://api-sandbox.tiket.com/search/hotel?q=Indonesia&startdate=2012-06-11&night=1&enddate=2012-06-12&room=1&adult=2&child=0&token=1c78d7bc29690cd96dfce9e0350cfc51&output=json
//    {"diagnostic":{"status":200,"elapsetime":"1.8805","memoryusage":"21.75MB","unix_timestamp":1480406353,"confirm":"success","lang":"id","currency":"IDR"}
//    ,"output_type":"json"
//        ,"search_queries":{"q":"Yogyakarta","uid":"","startdate":"2016-11-29","enddate":"2016-11-30","night":"1","room":1,"adult":"2","child":0,"sort":false,"minstar":0,"maxstar":5,"minprice":"118000.00","maxprice":"1000000000.00","distance":100000}
//        ,"results":{"result":[{"province_name":"Daerah Istimewa Yogyakarta"
//            ,"kecamatan_name":"Jetis"
//            ,"kelurahan_name":"Gowongan"
//            ,"business_uri":"https:\/\/api-sandbox.tiket.com\/hotel\/indonesia\/yogyakarta\/tugu-jogja\/the-101-yogyakarta-tugu?startdate=2016-11-29&night=1&room=1&adult=2&child=0&is_partner=0&star_rating=0&hotel_chain=0&facilities=0&latitude=0&longitude=0&distance=0&uid=business%3A20426620"
//            ,"photo_primary":"https:\/\/sandbox.tiket.com\/img\/business\/i\/m\/business-img_2590.sq2.jpg"
//            ,"star_rating":"4"
//            ,"id":"48410720161129"
//            ,"room_available":"1"
//            ,"latitude":"-7.78507033179146600000"
//            ,"longitude":"110.36680161952972000000"
//            ,"room_max_occupancies":"2"
//            ,"rating":""
//            ,"tripadvisor_avg_rating":
//    {"avg_rating":"4.0"
//        ,"image_url":"http:\/\/www.tripadvisor.co.id\/img\/cdsi\/img2\/ratings\/traveler\/4.0-18379-4.gif"
//        ,"review_count":"253"
//        ,"url":"http:\/\/www.tripadvisor.com\/WidgetEmbed-cdspropertydetail?locationId=6739367&lang=en_US&partnerId=176DD3B47F374A79ACF1E7E5AC852AC0&isTA=true&format=html&display=true"    
//    }
//        ,"room_facility_name":"Kamar bebas rokok ada tergantung ketersediaan|Brankas|AC|Internet - Wifi (gratis)|Televisi LCD/layar plasma|Satelit/TV kabel|Meja|Shower|Baju Handuk|Pengering Rambut|Bar kecil|Mesin pembuat kopi/teh|Air mineral botol gratis|Televisi|Telepon|Sandal|Cermin|Teras|Teras|Gratis Perlengkapan Mandi|Lemari|Shower Panas and Dingin"
//            ,"oldprice":"1005882.00"
//            ,"address":"Jalan Margoutomo No. 103 ( Mangkubumi), Yogyakarta , Jetis, Yogyakarta"
//            ,"wifi":"Wi-Fi Tersedia"
//            ,"promo_name":""
//            ,"price":"722990.00"
//            ,"total_price":"722990.00"
//            ,"regional":"Tugu Jogja - Yogyakarta - Daerah Istimewa Yogyakarta"
//            ,"name":"THE 1O1 Yogyakarta Tugu"
//            ,"hotel_id":"20426620"}
//            }
//        ,"pagination":{"total_found":131,"current_page":1,"offset":"20","lastPage":7}
//        ,"login_status":"false","token":"80f21dabf0b7b1406297adf87f570f043b7bfe6d"}

    function getAPIListHotel($token, $q, $startdate, $night, $enddate, $room, $adult, $child, $page, $sort = "", $minprice = "", $maxprice = "", $minstar = "", $maxstar = "", $latitude = "", $longitude = "") {
        $query_sort = "";
        if ($sort != "") {
            $query_sort = "&sort=" . $sort;
        }
        $query_minprice = "";
        if ($minprice != "") {
            $query_minprice = "&minprice=" . $minprice;
        }
        $query_maxprice = "";
        if ($maxprice != "") {
            $query_maxprice = "&maxprice=" . $maxprice;
        }
        $query_minstar = "";
        if ($minstar != "") {
            $query_minstar = "&minstar=" . $minstar;
        }
        $query_maxstar = "";
        if ($maxstar != "") {
            $query_maxstar = "&maxstar=" . $maxstar;
        }
        $query_latlong = "";
        if ($latitude != "" && $longitude != "") {
            $query_latlong = "&latitude=" . $latitude . "&longitude=" . $longitude;
        }
        //q is kata kunci
        $linkAPIListHotel = getAPILink() . "search/hotel?q=" . $q
                . "&startdate=" . $startdate
                . "&night=" . $night
                . "&enddate=" . $enddate
                . "&room=" . $room
                . "&adult=" . $adult
                . "&child=" . $child
                . "&page=" . $page
                . $query_sort
                . $query_minprice
                . $query_maxprice
                . $query_minstar
                . $query_maxstar
                . $query_latlong
                . "&token=" . $token . "&output=json";
        $jsonAPIListHotel = getDataWithCURL($linkAPIListHotel);
        $objAPIListHotel = json_decode($jsonAPIListHotel);
        isAPISuccess($objAPIListHotel);
        return $objAPIListHotel->results->result;
    }

//ini cuma contoh json detail hotel
//    {"diagnostic":
//    {"status":200,"elapsetime":"0.3285","memoryusage":"8.11MB","unix_timestamp":1480476672,"confirm":"success","lang":"id","currency":"IDR"}
//    ,"output_type":"json","primaryPhotos":"http:\/\/api-sandbox.tiket.com\/img\/business\/s\/i\/business-single-room-12.s.jpg"
//        ,"breadcrumb":
//    {"business_id":"15810","business_uri":"https:\/\/api-sandbox.tiket.com\/the-edelweis-hotel-yogyakarta","business_name":"The Edelweiss Hotel Yogyakarta"
//        ,"area_id":"87277","area_name":"Gejayan","kelurahan_id":"0","kelurahan_name":" ","kecamatan_id":"317","kecamatan_name":"Gondokusuman","city_id":"257"
//        ,"city_name":"Yogyakarta","province_id":"16","province_name":"Daerah Istimewa Yogyakarta","country_id":"id"
//        ,"country_name":"Indonesia","continent_id":"1"
//        ,"continent_name":"Asia"
//        ,"kelurahan_uri":"https:\/\/api-sandbox.tiket.com\/search\/hotel?uid=city:257"
//        ,"kecamatan_uri":"https:\/\/api-sandbox.tiket.com\/search\/hotel?uid=kecamatan:317"
//        ,"province_uri":"https:\/\/api-sandbox.tiket.com\/search\/hotel?uid=province:16"
//        ,"country_uri":"https:\/\/api-sandbox.tiket.com\/search\/hotel?uid=country:id"
//        ,"continent_uri":"https:\/\/api-sandbox.tiket.com\/search\/hotel?uid=continent:1","star_rating":"3"}
//        ,"results":
//    {"result":
//        [{"id":"107720161201","room_available":"20","ext_source":"native","room_id":"1077","currency":"IDR","minimum_stays":"1","with_breakfasts":"1"
//            ,"all_photo_room":
//            ["http:\/\/api-sandbox.tiket.com\/img\/business\/s\/i\/business-single-room-1.room.jpg"
//                ,"http:\/\/api-sandbox.tiket.com\/img\/business\/s\/u\/business-suites_1_copy.room.jpg"
//                ,"http:\/\/api-sandbox.tiket.com\/img\/business\/s\/u\/business-suites_2a.room.jpg"
//                ,"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/w\/business-twice_room_2.room.jpg"
//                ,"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/w\/business-twice-room-1.room.jpg"]
//                ,"photo_url":"http:\/\/api-sandbox.tiket.com\/img\/business\/s\/i\/business-single-room-1.s.jpg"
//            ,"room_name":"Superior Room","oldprice":"782595.00","price":"425000.00"
//            ,"bookUri":"https:\/\/api-sandbox.tiket.com\/order\/add\/hotel?startdate=2016-12-01&night=1&room=1&adult=2&child=0&minstar=0&maxstar=0&minprice=0&maxprice=0&facilities=0&locations=0&hotelname=0&is_partner=0&room_id=1077&hasPromo=0&enddate=2016-12-02&room_max_occupancy=2"
//            ,"room_facility":
//            ["AC","Mesin pembuat kopi/teh","Air mineral botol gratis","Internet - Wifi (gratis)","Televisi LCD/layar plasma","Cermin","Kulkas"
//                ,"Satelit/TV kabel","Shower","Sandal","Telepon"]
//                ,"room_description":false,"additional_surcharge_currency":""}]
//                
//        }
//                ,"addinfos":
//    {"addinfo":
//        ["Biaya makan pagi (jika tidak temasuk dalam harga kamar) : IDR 48.400,00","Checkout : 12:00","Jumlah Restoran : 1"
//            ,"Voltase ruangan : 220 V","Waktu untuk sampai di bandara : 20 menit","Tahun hotel dibangun : 2011"]}
//            ,"all_photo":
//    {"photo":
//        [{"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/2\/0\/business-2014-04-29_16.25-.00-.s.jpg","photo_type":"Kolam Renang"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/r\/o\/business-room_queen_-312x234-1.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/r\/o\/business-room_queena.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/s\/i\/business-single_room_1_-312x234-.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/s\/i\/business-single_room_1_-312x234-1.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/s\/i\/business-single-room-12.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/s\/u\/business-sup111.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/h\/business-the_edelweiss_hotel-16.s.jpg","photo_type":"Interior"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/t\/business-ttwice_room_1.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/w\/business-twice_room_1_-312x234-.s.jpg","photo_type":"Interior"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/r\/o\/business-room_queen_-312x234-.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/r\/e\/business-resto78.s.jpg","photo_type":"Rumah Makan"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/q\/u\/business-queen_staff_2_-312x234-2.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/3\/1\/business-312x234.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/9\/3\/business-93.s.jpg","photo_type":"Makanan"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/9\/3\/business-931.s.jpg","photo_type":"Makanan"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/b\/u\/business-business_center_a1.s.jpg","photo_type":"Pusat Bisnis"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/e\/i\/business-eight_bar_-312x234-1.s.jpg","photo_type":"Kolam Renang"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/f\/a\/business-facade_31.s.jpg","photo_type":"Pintu Masuk"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/f\/o\/business-food_-312x234-.s.jpg","photo_type":"Makanan"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/j\/u\/business-junior_suite_lr.s.jpg","photo_type":"Kamar Tamu"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/k\/u\/business-kursi_-312x234-.s.jpg","photo_type":"Interior"},
//        {"file_name":"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/w\/business-twice_room_2_-312x234-.s.jpg","photo_type":"Kamar Tamu"}]
//    }
//        ,"primaryPhotos_large":"http:\/\/api-sandbox.tiket.com\/img\/business\/s\/i\/business-single-room-12.crop.jpg"
//            ,"avail_facilities":
//    {"avail_facilitiy":
//        [{"facility_type":"hotel","facility_name":"Layanan Kamar 24 Jam"},
//        {"facility_type":"hotel","facility_name":"Antar Jemput Bandara Dikenakan Biaya"},
//        {"facility_type":"hotel","facility_name":"Bar"},
//        {"facility_type":"hotel","facility_name":"Pusat Bisnis"},
//        {"facility_type":"hotel","facility_name":"Tempat Parkir"},
//        {"facility_type":"hotel","facility_name":"Kedai Kopi"},
//        {"facility_type":"hotel","facility_name":"Staf Pribadi"},
//        {"facility_type":"hotel","facility_name":"Penjaga Pintu"},
//        {"facility_type":"hotel","facility_name":"Kotak Penyimpanan Barang Berharga"},
//        {"facility_type":"hotel","facility_name":"Layanan Dokter 24 Jam"},
//        {"facility_type":"hotel","facility_name":"Lift"},
//        {"facility_type":"hotel","facility_name":"Gratis Wifi di lounge"},
//        {"facility_type":"hotel","facility_name":"Gratis Wifi di kamar"},
//        {"facility_type":"hotel","facility_name":"Lounge"},
//        {"facility_type":"hotel","facility_name":"Bagasi Penyimpanan"},
//        {"facility_type":"hotel","facility_name":"Fasilitas Rapat"},
//        {"facility_type":"hotel","facility_name":"Bar Disamping Kolam Renang"},
//        {"facility_type":"hotel","facility_name":"Rumah Makan"},
//        {"facility_type":"hotel","facility_name":"Bar di atas gedung"},
//        {"facility_type":"hotel","facility_name":"Layanan Kamar"},
//        {"facility_type":"hotel","facility_name":"Kamar merokok ada tergantung ketersediaan"},
//        {"facility_type":"hotel","facility_name":"Informasi Tour"},
//        {"facility_type":"hotel","facility_name":"Layanan Parkir oleh Pelayan"},
//        {"facility_type":"hotel","facility_name":"Wi-Fi area umum"},
//        {"facility_type":"hotel","facility_name":"Wifi di Kamar"},
//        {"facility_type":"hotel","facility_name":"24-hour Front Desk "},
//        {"facility_type":"hotel","facility_name":"Air-conditioned Public Areas "},
//        {"facility_type":"hotel","facility_name":"Banquet Facilities "},
//        {"facility_type":"hotel","facility_name":"Bar\/lounge "},
//        {"facility_type":"hotel","facility_name":"Complimentary Newspapers In Lobby "},
//        {"facility_type":"hotel","facility_name":"Elevator\/lift "},
//        {"facility_type":"hotel","facility_name":"Express Check- In "},
//        {"facility_type":"hotel","facility_name":"Express Check-out "},
//        {"facility_type":"hotel","facility_name":"Laundry Facilities "},
//        {"facility_type":"hotel","facility_name":"Luggage Storage "},
//        {"facility_type":"hotel","facility_name":"Poolside Bar "},
//        {"facility_type":"hotel","facility_name":"Restaurant(s) In Hotel "},
//        {"facility_type":"hotel","facility_name":"Room Service "},
//        {"facility_type":"hotel","facility_name":"Room Service (24 Hours) "},
//        {"facility_type":"room","facility_name":"AC"},
//        {"facility_type":"room","facility_name":"Mesin pembuat kopi/teh"},
//        {"facility_type":"room","facility_name":"Air mineral botol gratis"},
//        {"facility_type":"room","facility_name":"Shower Panas and Dingin"},
//        {"facility_type":"room","facility_name":"Brankas"},
//        {"facility_type":"room","facility_name":"Internet - Wifi (gratis)"},
//        {"facility_type":"room","facility_name":"Televisi LCD/layar plasma"},
//        {"facility_type":"room","facility_name":"Bar kecil"},
//        {"facility_type":"room","facility_name":"Cermin"},
//        {"facility_type":"room","facility_name":"Kamar bebas rokok ada tergantung ketersediaan"},
//        {"facility_type":"room","facility_name":"Kulkas"},
//        {"facility_type":"room","facility_name":"Satelit/TV kabel"},
//        {"facility_type":"room","facility_name":"Shower"},
//        {"facility_type":"room","facility_name":"Sandal"},
//        {"facility_type":"room","facility_name":"Telepon"},
//        {"facility_type":"room","facility_name":"Televisi"},
//        {"facility_type":"room","facility_name":"Air Conditioning "},
//        {"facility_type":"room","facility_name":"Cable TV Service "},
//        {"facility_type":"room","facility_name":"City View "},
//        {"facility_type":"room","facility_name":"Coffee\/tea Maker "},
//        {"facility_type":"room","facility_name":"Complimentary Bottled Water "},
//        {"facility_type":"room","facility_name":"Complimentary Toiletries "},
//        {"facility_type":"room","facility_name":"Connecting \/ Adjoining Rooms Available "},
//        {"facility_type":"room","facility_name":"Daily Housekeeping "},
//        {"facility_type":"room","facility_name":"Desk "},
//        {"facility_type":"room","facility_name":"Free Wi-Fi "},
//        {"facility_type":"room","facility_name":"Hair Dryer (on Request) "},
//        {"facility_type":"room","facility_name":"In-room Safe "},
//        {"facility_type":"room","facility_name":"Makeup\/shaving Mirror "},
//        {"facility_type":"room","facility_name":"Minibar "},
//        {"facility_type":"room","facility_name":"Satellite TV Service "},
//        {"facility_type":"room","facility_name":"Slippers "},
//        {"facility_type":"room","facility_name":"Wake-up Calls "},
//        {"facility_type":"room","facility_name":"Wireless Internet Access "},
//        {"facility_type":"sport","facility_name":"Kolam Renang di Luar Ruangan"}]}
//        ,"nearby_attractions":{"nearby_attraction":
//            [{"distance":220,
//                    "business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/p\/a\/business-pancake-tiramisu-topping-ice-cream-vanilla-dixie2.s.jpg"
//                ,"business_name":"Dixie Easy Dinning","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-dixie-easy-dinning-2"},
//        {"distance":608,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/l\/a\/business-lana.s.jpg"
//            ,"business_name":"Lana Gallery","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-lana-gallery"},
//        {"distance":955,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/a\/f\/business-affandi_galeri_1.s.jpg"
//            ,"business_name":"Museum Affandi","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-museum-affandi"},
//        {"distance":1560,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/1\/business-t144.s.jpg"
//            ,"business_name":"Telkom Office","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-telkom-office"},
//        {"distance":1594,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/4\/4\/business-44737073.s.jpg"
//            ,"business_name":"Kridosono Stadium","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-kridosono-stadium"},
//        {"distance":1674,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/2\/business-t242.s.jpg"
//            ,"business_name":"Kotabaru","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-kotabaru-poi"},
//        {"distance":1694,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/l\/e\/business-lempuyangan.s.jpg"
//            ,"business_name":"Lempuyangan Railway station","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-lempuyangan-railway-station"},
//        {"distance":1839,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/r\/a\/business-raminten-21.s.jpg"
//            ,"business_name":"Raminten","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-raminten"},
//        {"distance":1879,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/1\/business-t160.s.jpg"
//            ,"business_name":"Rumah Sakit Soedirman","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-rumah-sakit-soedirman"},
//        {"distance":1945,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/t\/3\/business-t337.s.jpg"
//            ,"business_name":"Museum Batik","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-museum-batik"},
//        {"distance":2222,"business_primary_photo":"http:\/\/api-sandbox.tiket.com\/img\/business\/u\/s\/business-usd.s.jpg"
//            ,"business_name":"Sanata Darma University","business_uri":"https:\/\/api-sandbox.tiket.com\/attractions\/indonesia\/daerah-istimewa-yogyakarta\/hotel-dekat-sanata-darma-university"}
//            ]}
//            ,"list_internal_review":
//                [{"avatar":"http:\/\/sandbox.tiket.com\/img\/default\/d\/e\/default-default.s.jpg","person":"Aditya Ari Wibowo"
//                    ,"nationality_flag":"http:\/\/sandbox.tiket.com\/images\/nation_flag\/id.png","nationality":"Indonesia","rating":"8.0","max_rating":10
//                    ,"testimonial_text":"Hotel di perbatasan Kota Jogja dengan Sleman, tapi jangan salah, justru di lokasi\u2026"},
//        {"avatar":"http:\/\/sandbox.tiket.com\/img\/default\/d\/e\/default-default.s.jpg","person":"Sirazudin "
//            ,"nationality_flag":"http:\/\/sandbox.tiket.com\/images\/nation_flag\/id.png","nationality":"Indonesia","rating":"8.8"
//            ,"max_rating":10,"testimonial_text":"Saya pesan voucher hotel Edelweis melalui tiket.com, dengan harga yang sangat murah.\u2026"},
//        {"avatar":"http:\/\/sandbox.tiket.com\/img\/default\/d\/e\/default-default.s.jpg","person":"Bambang Aris "
//            ,"nationality_flag":"http:\/\/sandbox.tiket.com\/images\/nation_flag\/id.png","nationality":"Indonesia","rating":"8.0","max_rating":10
//            ,"testimonial_text":"Hotel yang nyaman, dengan menu makanan tradisional juga ada di sana..sayangnya waktu\u2026"},
//        {"avatar":"http:\/\/sandbox.tiket.com\/img\/default\/d\/e\/default-default.s.jpg","person":"Asril Khristaria Jc"
//            ,"nationality_flag":"http:\/\/sandbox.tiket.com\/images\/nation_flag\/id.png","nationality":"Indonesia","rating":"8.3","max_rating":10
//            ,"testimonial_text":"booking, pembayaran, dan check in di tanggal 17 januari 2014. saya tiba di hotel\u2026"},
//        {"avatar":"http:\/\/sandbox.tiket.com\/img\/default\/d\/e\/default-default.s.jpg","person":"Sherly Nur Afni"
//            ,"nationality_flag":"http:\/\/sandbox.tiket.com\/images\/nation_flag\/id.png","nationality":"Indonesia","rating":"8.0","max_rating":10
//            ,"testimonial_text":"Saya reservasi utk kado pernikahan temen,so far dbilang nya hotelnya nyaman n enak"}]
//            ,"summary_internal_review":{"average":"7.7","max_rating":10
//                ,"detail":[{"category":"Layanan Hotel","value":"7.6"},
//        {"category":"Kondisi Hotel","value":"7.7"},{"category":"Kenyamanan","value":"7.7"},
//        {"category":"Kebersihan","value":"7.8"}
//        ]}
//        ,"general":{"address":"Jl. Gejayan 17C, Gondokusuman, Yogyakarta"
//            ,"description":"The+Edelweiss+Hotel+Yogyakarta+berada+di+dalam+pusat+bisnis+dikota+Yogyakarta%2C+dekat+dengan+lingkungan+universitas+dan+distrik+perbelanjaan+di+sepanjang+jalan-jalan+yang+ramai+Jalan+Gejayan.+Lokasi+ini+menempatkan+pusat+perbelanjaan+besar+hanya+menit+dan+juga+15+menit+berkendara+ke+Bandara+Internasional+Yogyakarta.%3Cbr+%2F%3E%0A%3Cbr+%2F%3E%0A+Hotel+ini+menawarkan+95+kamar+dengan+desain+minimalis+modern+untuk+memperbaharui+dan+menginspirasi+Anda.+Akses+internet+berkecepatan+tinggi%2C+baik+kamar+yang+berkualitas+baik%2C+matrass+delightly+disediakan+untuk+layanan+bagi+para+pengunjung+yang+datang+ketempat+ini.%3Cbr+%2F%3E%0A%3Cbr+%2F%3E%0AFasilitas+terbaik+hotel+ini+termasuk+wi-fi+di+tempat-tempat+umum%2C+tempat+parkir+mobil%2C+layanan+laundry%2Fdry+cleaning%2C+restoran%2C+and+concierge.+Suasana+The+Edelweiss+Hotel+tercerminkan+dari+setiap+kamar+tamu.+kulkas%2C+kotak+penyimpanan+dalam-kamar%2C+TV+satelit%2Fkabel%2C+internet+wireless+%28gratis%29%2C+AC."
//            ,"latitude":-7.7816262945693,"longitude":110.38782497907}
//            ,"login_status":"false","token":"f607cab8c5cadb67af27169127510b75b8db2172"}

    function getAPIDetailHotel($business_uri) {
        $linkAPIDetailHotel = $business_uri . "&token=" . getToken() . "&output=json";
        $jsonAPIDetailHotel = getDataWithCURL($linkAPIDetailHotel);
        $objAPIDetailHotel = json_decode($jsonAPIDetailHotel);
        isAPISuccess($objAPIDetailHotel);
        return $objAPIDetailHotel;
//        return $jsonAPIDetailHotel;
    }

    function getbreadcrumb($objAPIDetailHotel) {
        return $objAPIDetailHotel->breadcrumb;
    }

    function getresult($objresults) {
        return $objresults->result;
    }

    function getaddinfos($objAPIDetailHotel) {
        return $objAPIDetailHotel->addinfos;
    }

    function getall_photo($objAPIDetailHotel) {
        return $objAPIDetailHotel->all_photo;
    }

    function getprimaryPhotos_large($objAPIDetailHotel) {
        return $objAPIDetailHotel->primaryPhotos_large;
    }

    function getavail_facilitiy($objavail_facilities) {
        return $objavail_facilities->avail_facilitiy;
    }

    function getnearby_attraction($objnearby_attractions) {
        return $objnearby_attractions->nearby_attraction;
    }

    function getlist_internal_review($objAPIDetailHotel) {
        return $objAPIDetailHotel->list_internal_review;
    }

    function getsummary_internal_review($objAPIDetailHotel) {
        return $objAPIDetailHotel->summary_internal_review;
    }

    function getdetailsummary_internal_review($objsummary_internal_review) {
        return $objsummary_internal_review->detail;
    }

    function getgeneral($objAPIDetailHotel) {
        return $objAPIDetailHotel->general;
    }

    //Flight API
    function getAPIListAirport($token) {
        $linkAPIListAirport = getAPILink() . "flight_api/all_airport?token=" . $token . "&output=json";
        $jsonAPIListAirport = getDataWithCURL($linkAPIListAirport);
        $objAPIListAirport = json_decode($jsonAPIListAirport);
        isAPISuccess($objAPIListAirport);
        return $objAPIListAirport->all_airport->airport;
//        return $jsonAPIListAirport;
    }

    function getAPIListAirportPopularDestination($token) {
        $linkAPIListAirportPopularDestination = getAPILink() . "flight_api/getPopularDestination?token=" . $token . "&output=json";
        $jsonAPIListAirportPopularDestination = getDataWithCURL($linkAPIListAirportPopularDestination);
        $objAPIListAirportPopularDestination = json_decode($jsonAPIListAirportPopularDestination);
        isAPISuccess($objAPIListAirportPopularDestination);
        return $objAPIListAirportPopularDestination->popular_destinations->airport;
//        return $jsonAPIListAirportPopularDestination;
    }

//    https://api-sandbox.tiket.com/ajax/mCheckFlightUpdated?
    function getAPICheckFlightUpdated($token, $d, $a, $date, $adult, $child, $infant) {
        $linkAPICheckFlightUpdated = getAPILink() . "ajax/mCheckFlightUpdated?d=" . $d . "&a=" . $a . "&date=" . $date . "&adult=" . $adult . "&child=" . $child . "&infant=" . $infant . "&token=" . $token . "&output=json";
        $jsonAPICheckFlightUpdated = getDataWithCURL($linkAPICheckFlightUpdated);
        $objAPICheckFlightUpdated = json_decode($jsonAPICheckFlightUpdated);
        isAPISuccess($objAPICheckFlightUpdated);
        return $objAPICheckFlightUpdated->update;
//        return $jsonAPICheckFlightUpdated;
    }

//    http://api-sandbox.tiket.com/search/flight?d=CGK&a=DPS&date=2014-05-25&ret_date=2014-05-30&adult=1&child=0&infant=0
    //
//    Array
//(
//    [diagnostic] => Array
//        (
//            [status] => 200
//            [elapsetime] => 1.1242
//            [memoryusage] => 12.88MB
//            [unix_timestamp] => 1480562724
//            [confirm] => success
//            [lang] => id
//            [currency] => IDR
//        )
//
//    [output_type] => array
//    [round_trip] => 1
//    [search_queries] => Array
//        (
//            [from] => CGK
//            [to] => DPS
//            [date] => 2016-12-01
//            [ret_date] => 2016-12-02
//            [adult] => 1
//            [child] => 0
//            [infant] => 0
//            [sort] => 
//        )
//
//    [go_det] => Array
//        (
//            [dep_airport] => Array
//                (
//                    [airport_code] => CGK
//                    [international] => 1
//                    [trans_name_id] => 7574
//                    [banner_image] => 
//                    [short_name_trans_id] => 1193637
//                    [business_name] => Soekarno Hatta
//                    [business_name_trans_id] => 5935
//                    [business_country] => id
//                    [business_id] => 20361
//                    [country_name] => Indonesia
//                    [city_name] => Jakarta Barat
//                    [province_name] => DKI Jakarta
//                    [short_name] => Jakarta
//                    [location_name] => Jakarta - Cengkareng
//                )
//
//            [arr_airport] => Array
//                (
//                    [airport_code] => DPS
//                    [international] => 1
//                    [trans_name_id] => 7572
//                    [banner_image] => banner-denpasar1.jpg
//                    [short_name_trans_id] => 1193606
//                    [business_name] => Ngurah Rai
//                    [business_name_trans_id] => 5931
//                    [business_country] => id
//                    [business_id] => 20357
//                    [country_name] => Indonesia
//                    [city_name] => Denpasar
//                    [province_name] => Bali
//                    [short_name] => Denpasar
//                    [location_name] => Denpasar, Bali
//                )
//
//            [date] => 2016-12-01
//            [formatted_date] => 01 Desember 2016
//        )
//
//    [ret_det] => Array
//        (
//            [dep_airport] => Array
//                (
//                    [airport_code] => DPS
//                    [international] => 1
//                    [trans_name_id] => 7572
//                    [banner_image] => banner-denpasar1.jpg
//                    [short_name_trans_id] => 1193606
//                    [business_name] => Ngurah Rai
//                    [business_name_trans_id] => 5931
//                    [business_country] => id
//                    [business_id] => 20357
//                    [country_name] => Indonesia
//                    [city_name] => Denpasar
//                    [province_name] => Bali
//                    [short_name] => Denpasar
//                    [location_name] => Denpasar, Bali
//                )
//
//            [arr_airport] => Array
//                (
//                    [airport_code] => CGK
//                    [international] => 1
//                    [trans_name_id] => 7574
//                    [banner_image] => 
//                    [short_name_trans_id] => 1193637
//                    [business_name] => Soekarno Hatta
//                    [business_name_trans_id] => 5935
//                    [business_country] => id
//                    [business_id] => 20361
//                    [country_name] => Indonesia
//                    [city_name] => Jakarta Barat
//                    [province_name] => DKI Jakarta
//                    [short_name] => Jakarta
//                    [location_name] => Jakarta - Cengkareng
//                )
//
//            [date] => 2016-12-02
//            [formatted_date] => 02 Desember 2016
//        )
//
//    [departures] => Array
//        (
//            [result] => Array
//                (
//                    [0] => Array
//                        (
//                            [flight_id] => 2574023
//                            [airlines_name] => CITILINK
//                            [flight_number] => QG-9743
//                            [departure_city] => CGK
//                            [arrival_city] => DPS
//                            [stop] => Langsung
//                            [price_value] => 1455300.00
//                            [price_adult] => 1455300.00
//                            [price_child] => 0.00
//                            [price_infant] => 0.00
//                            [timestamp] => 2016-12-01 10:01:21
//                            [has_food] => 0
//                            [check_in_baggage] => 20
//                            [is_promo] => 0
//                            [airport_tax] => 1
//                            [check_in_baggage_unit] => Kg
//                            [simple_departure_time] => 13:20
//                            [simple_arrival_time] => 16:10
//                            [long_via] => 
//                            [departure_city_name] => Jakarta
//                            [arrival_city_name] => Denpasar
//                            [full_via] => CGK - DPS (13:20 - 16:10)
//                            [markup_price_string] => 
//                            [need_baggage] => 0
//                            [best_deal] => 
//                            [duration] => 1 j 50 m
//                            [image] => https://sandbox.tiket.com/images/flight/logo/icon_citilink.png
//                            [departure_flight_date] => 2016-12-01 13:20:00
//                            [departure_flight_date_str] => Kamis, 01 Des 2016
//                            [departure_flight_date_str_short] => Kam, 01 Des 2016
//                            [arrival_flight_date] => 2016-12-01 16:10:00
//                            [arrival_flight_date_str] => Kamis, 01 Des 2016
//                            [arrival_flight_date_str_short] => Kam, 01 Des 2016
//                            [flight_infos] => Array
//                                (
//                                    [flight_info] => Array
//                                        (
//                                            [0] => Array
//                                                (
//                                                    [flight_number] => QG-9743
//                                                    [class] => B
//                                                    [departure_city] => CGK
//                                                    [departure_city_name] => Jakarta
//                                                    [arrival_city] => DPS
//                                                    [arrival_city_name] => Denpasar
//                                                    [airlines_name] => CITILINK
//                                                    [departure_date_time] => 2016-12-01 13:20:00
//                                                    [string_departure_date] => Kamis, 01 Des 2016
//                                                    [string_departure_date_short] => Kam, 01 Des 2016
//                                                    [simple_departure_time] => 13:20
//                                                    [arrival_date_time] => 2016-12-01 16:10:00
//                                                    [string_arrival_date] => Kamis, 01 Des 2016
//                                                    [string_arrival_date_short] => Kam, 01 Des 2016
//                                                    [simple_arrival_time] => 16:10
//                                                    [img_src] => https://sandbox.tiket.com/images/flight/logo/icon_citilink.png
//                                                    [duration_time] => 6600
//                                                    [duration_hour] => 1j
//                                                    [duration_minute] => 50m
//                                                    [check_in_baggage] => 20
//                                                    [check_in_baggage_unit] => Kg
//                                                    [terminal] => 
//                                                    [transit_duration_hour] => 0
//                                                    [transit_duration_minute] => 0
//                                                    [transit_arrival_text_city] => 
//                                                    [transit_arrival_text_time] => 
//                                                )
//
//                                        )
//
//                                )
//
//                            [sss_key] => 
//                        )
//    [returns] => Array
//        (
//            [result] => Array
//                (
//                    [0] => Array
//                        (
//                            [flight_id] => 2490235
//                            [airlines_name] => SRIWIJAYA
//                            [flight_number] => SJ-273
//                            [departure_city] => DPS
//                            [arrival_city] => CGK
//                            [stop] => Langsung
//                            [price_value] => 674000.00
//                            [price_adult] => 674000.00
//                            [price_child] => 0.00
//                            [price_infant] => 0.00
//                            [timestamp] => 2016-11-28 22:24:07
//                            [has_food] => 1
//                            [check_in_baggage] => 20
//                            [is_promo] => 0
//                            [airport_tax] => 1
//                            [check_in_baggage_unit] => Kg
//                            [simple_departure_time] => 16:20
//                            [simple_arrival_time] => 17:00
//                            [long_via] => 
//                            [departure_city_name] => Denpasar
//                            [arrival_city_name] => Jakarta
//                            [full_via] => DPS - CGK (16:20 - 17:00)
//                            [markup_price_string] => 
//                            [need_baggage] => 0
//                            [best_deal] => 
//                            [duration] => 1 j 40 m
//                            [image] => https://sandbox.tiket.com/images/flight/logo/icon_sriwijaya.png
//                            [departure_flight_date] => 2016-12-02 16:20:00
//                            [departure_flight_date_str] => Jumat, 02 Des 2016
//                            [departure_flight_date_str_short] => Jum, 02 Des 2016
//                            [arrival_flight_date] => 2016-12-02 17:00:00
//                            [arrival_flight_date_str] => Jumat, 02 Des 2016
//                            [arrival_flight_date_str_short] => Jum, 02 Des 2016
//                            [flight_infos] => Array
//                                (
//                                    [flight_info] => Array
//                                        (
//                                            [0] => Array
//                                                (
//                                                    [flight_number] => SJ-273
//                                                    [class] => G
//                                                    [departure_city] => DPS
//                                                    [departure_city_name] => Denpasar
//                                                    [arrival_city] => CGK
//                                                    [arrival_city_name] => Jakarta
//                                                    [airlines_name] => SRIWIJAYA
//                                                    [departure_date_time] => 2016-12-02 16:20:00
//                                                    [string_departure_date] => Jumat, 02 Des 2016
//                                                    [string_departure_date_short] => Jum, 02 Des 2016
//                                                    [simple_departure_time] => 16:20
//                                                    [arrival_date_time] => 2016-12-02 17:00:00
//                                                    [string_arrival_date] => Jumat, 02 Des 2016
//                                                    [string_arrival_date_short] => Jum, 02 Des 2016
//                                                    [simple_arrival_time] => 17:00
//                                                    [img_src] => https://sandbox.tiket.com/images/flight/logo/icon_sriwijaya.png
//                                                    [duration_time] => 6000
//                                                    [duration_hour] => 1j
//                                                    [duration_minute] => 40m
//                                                    [check_in_baggage] => 20
//                                                    [check_in_baggage_unit] => Kg
//                                                    [terminal] => 
//                                                    [transit_duration_hour] => 0
//                                                    [transit_duration_minute] => 0
//                                                    [transit_arrival_text_city] => 
//                                                    [transit_arrival_text_time] => 
//                                                )
//
//                                        )
//
//                                )
//
//                            [sss_key] => 
//                        )


    function getAPIListFlight($token, $d, $a, $date, $ret_date, $adult, $child, $infant) {
        $q_ret_date = "";
        if ($ret_date != "") {
            $q_ret_date = "&ret_date=" . $ret_date;
        }
        $linkAPIListFlight = getAPILink() . "search/flight?d=" . $d . "&a=" . $a . "&date=" . $date . $q_ret_date . "&adult=" . $adult . "&child=" . $child . "&infant=" . $infant . "&token=" . $token . "&output=json";
        $jsonAPIListFlight = getDataWithCURL($linkAPIListFlight);
        $objAPIListFlight = json_decode($jsonAPIListFlight);
        isAPISuccess($objAPIListFlight);

        $arrdepret = array();
        if ($ret_date != "") {
            $arrdepret[0] = $objAPIListFlight->departures->result;
            $arrdepret[1] = $objAPIListFlight->returns->result;
            return $arrdepret;
        } else {
            return $objAPIListFlight->departures->result;
        }
//        return $jsonAPIListFlight;
    }

    function getAPIFlightInfo($flight_infos) {//input departures->result[]->flight_infos
        return $flight_infos->flight_info;
    }

//    https://api-sandbox.tiket.com/order/add/flight?token=4a5ef3fb7627b9f2900f1aafa46b0f9f095f6aaf&flight_id=20203327&child=1&adult=1&infant=1&conSalutation=Mrs&conFirstName=budianto&conLastName=wijaya&conPhone=%2B6287880182218&conEmailAddress=you_julin@yahoo.com&firstnamea1=susi&lastnamea1=wijaya&ida1=1116057107900001&titlea1=Mr&conOtherPhone=%2B628521342534&titlec1=Ms&firstnamec1=carreen&lastnamec1=athalia&birthdatec1=2005-02-02&titlei1=Mr&parenti1=1&firstnamei1=wendy&lastnamei1=suprato&birthdatei1=2011-06-29&output=XML
    function getAPIAddOrderFlight($token
    , $flight_id
    , $ret_flight_id
    , $child
    , $adult
    , $infant
    , $conSalutation
    , $conFirstName
    , $conLastName
    , $conPhone
    , $conEmailAddress
    , $arrdataa
    , $arrconOtherPhone
    , $arrdatac
    , $arrdatai) {
        $listparameterfirstnamea = "";
        $listparameterlastnamea = "";
        $listparameterida = "";
        $listparametertitlea = "";
        $i_dataa = 1;
        foreach ($arrdataa as $value) {
            $listparameterfirstnamea .= "&firstnamea" . $i_dataa . "=" . $value->firstnamea;
            $listparameterlastnamea .= "&lastnamea" . $i_dataa . "=" . $value->lastnamea;
            $listparameterida .= "&ida" . $i_dataa . "=" . $value->ida;
            $listparametertitlea .= "&titlea" . $i_dataa . "=" . $value->titlea;
            $i_dataa++;
        }
        $listparameterconOtherPhone = "";
        $i_conOtherPhone = 1;
        foreach ($arrconOtherPhone as $value) {
            $listparameterconOtherPhone .= "&conOtherPhone" . $i_conOtherPhone . "=" . $value;
            $i_conOtherPhone++;
        }
        $listparametertitlec = "";
        $listparameterfirstnamec = "";
        $listparameterlastnamec = "";
        $listparameterbirthdatec = "";
        $i_datac = 1;
        if (!empty($arrdatac)) {
            foreach ($arrdatac as $value) {
                $listparametertitlec .= "&titlec" . $i_datac . "=" . $value->titlec;
                $listparameterfirstnamec .= "&firstnamec" . $i_datac . "=" . $value->firstnamec;
                $listparameterlastnamec .= "&lastnamec" . $i_datac . "=" . $value->lastnamec;
                $listparameterbirthdatec .= "&birthdatec" . $i_datac . "=" . $value->birthdatec;
                $i_datac++;
            }
        }
        $listparametertitlei = "";
        $listparameterparenti = "";
        $listparameterfirstnamei = "";
        $listparameterlastnamei = "";
        $listparameterbirthdatei = "";
        $i_datai = 1;
        if (!empty($arrdatai)) {
            foreach ($arrdatai as $value) {
                $listparametertitlei.= "&titlei" . $i_datai . "=" . $value->titlei;
                $listparameterparenti.= "&parenti" . $i_datai . "=" . $value->parenti;
                $listparameterfirstnamei.= "&firstnamei" . $i_datai . "=" . $value->firstnamei;
                $listparameterlastnamei.= "&lastnamei" . $i_datai . "=" . $value->lastnamei;
                $listparameterbirthdatei.= "&birthdatei" . $i_datai . "=" . $value->birthdatei;
                $i_datai++;
            }
        }
        $qret_flight_id = "";
        if ($ret_flight_id != "") {
            $qret_flight_id = "&ret_flight_id=" . $ret_flight_id;
        }
        $linkAPIListFlight = getAPILink() . "order/add/flight?flight_id=" . $flight_id . $qret_flight_id . "&child=" . $child . "&adult=" . $adult . "&infant=" . $infant
                . "&conSalutation=" . $conSalutation . "&conFirstName=" . $conFirstName
                . "&conLastName=" . $conLastName . "&conPhone=" . $conPhone . "&conEmailAddress=" . $conEmailAddress
                . $listparameterfirstnamea . $listparameterlastnamea . $listparameterida . $listparametertitlea . $listparameterconOtherPhone
                . $listparametertitlec . $listparameterfirstnamec . $listparameterlastnamec . $listparameterbirthdatec
                . $listparametertitlei . $listparameterparenti . $listparameterfirstnamei . $listparameterlastnamei . $listparameterbirthdatei
                . "&token=" . $token . "&output=json";
        $jsonAPIListFlight = getDataWithCURL($linkAPIListFlight);
        $objAPIListFlight = json_decode($jsonAPIListFlight);
        isAPISuccess($objAPIListFlight);
//        return $objAPIListFlight;
        return $jsonAPIListFlight;
    }

}

?>