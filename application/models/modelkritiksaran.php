<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : kritiksaran
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelkritiksaran extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListkritiksaran() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "Saran," .
                "tglsaran" .
                " FROM kritiksaran   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->idmember;
        }
        return $xBuffResul;
    }

    function getListkritiksaran($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idmember like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "Saran," .
                "tglsaran" .
                " FROM kritiksaran $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }
    
    function getListkritiksaranbydate( $date_awal, $date_akhir) {
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "Saran," .
                "tglsaran" .
                " FROM kritiksaran WHERE (tglsaran >='" . $date_awal . " 00:00:00' AND tglsaran <='" . $date_akhir . " 23:59:59') order by tglsaran";
        $query = $this->db->query($xStr);
        return $query;
    }


    function getDetailkritiksaran($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "Saran," .
                "tglsaran" .
                " FROM kritiksaran  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexkritiksaran() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "idmember," .
                "Saran," .
                "tglsaran" .
                " FROM kritiksaran order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertkritiksaran($xidx, $xidmember, $xSaran, $xtglsaran) {
        $xStr = " INSERT INTO kritiksaran( " .
                "idx," .
                "idmember," .
                "Saran," .
                "tglsaran) VALUES('" . $xidx . "','" . $xidmember . "','" . $xSaran . "',now())";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatekritiksaran($xidx, $xidmember, $xSaran, $xtglsaran) {
        $xStr = " UPDATE kritiksaran SET " .
                "idx='" . $xidx . "'," .
                "idmember='" . $xidmember . "'," .
                "Saran='" . $xSaran . "'," .
                "tglsaran='" . $xtglsaran . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletekritiksaran($xidx) {
        $xStr = " DELETE FROM kritiksaran WHERE kritiksaran.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletekritiksaran($xidx);
    }

    function setInsertLogDeletekritiksaran($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'kritiksaran',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

?>