<?php

class modelgetmenu extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getNumberRandom() {
        $i = 0;
        $xbufrandom = '';
        while ($i <= 6) {
            $xbufrandom .= rand(10000, 99999);
            $i++;
        }
        return $xbufrandom;
    }

    function getArrayKomponen($isFromAdmin, $xContent='') {
        $xBuffResult = array();
        $this->load->model('modelmenu');
        
        $xBuffResult[1] = $this->modelmenu->getMenuAtas();
        $xBuffResult[2] = $xContent;
        $xBuffResult[3] = "".  base_url()."resource/imgbtn/logockp.png";
        
        $xBuffResult[4] = $this->session->userdata('nama');
        $xBuffResult[5] = "";
        $xBuffResult[6] = "";
        $xBuffResult[7] = "";

        return $xBuffResult;
    }

    function SetViewAdmin($xContent, $xList, $buf1, $xAjax, $buf2) {
        
        $this->load->helper('common');
        $this->load->helper('html');
        $xBufResult = $xContent . $xList;
        if (strpos($xContent, '<form') > 0) {
            $xBufResult = $xContent . '</form></div> ' . $xList;
        }
        $xMenuKanan = ''; 
        $xShow = setviewadmin($this->getArrayKomponen(TRUE, $xBufResult));
        $xecho = '<!doctype html>
              <html xmlns="http://www.w3.org/1999/xhtml">
              <Title>
              </Title>
              <head>
              <meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
              <script type="text/javascript">
                 function getBaseURL() {
                          return "'.  base_url().'";
                        }
                </script>
               
                
                    ' . "\n" .
                link_tag('resource/js/jquery/themes/base/jquery-ui.css') . "\n" .
                link_tag('resource/css/admin/common.css') . "\n" .
                link_tag('resource/css/admin/frmlayout.css') . "\n" .
                link_tag('resource/js/smartmenus/css/sm-core-css.css') . "\n" .
                link_tag('resource/js/smartmenus/css/sm-blue/sm-blue.css') . "\n" .
                //link_tag('resource/css/admin/jqmenuatas.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/jquery/jquery-1.9.1.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/jquery/ui/jquery-ui.js"></script>' . "\n" .
               // '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/jqmenuatas.js"></script> ' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/smartmenus/jquery.smartmenus.js"></script> '
                . '<script type="text/javascript">
	        $(function() {
		$("#main-menu").smartmenus({
			subMenusSubOffsetX: 1,
			subMenusSubOffsetY: -8
		});
	        });
              </script> 
              <style type="text/css">
	@media screen and (min-width: 768px) {
		#main-menu {
			position:relative;
			z-index:100;
			width:auto;
		}
		#main-menu ul {
			width:12em; /* fixed width only please - you can use the "subMenusMinWidth"/"subMenusMaxWidth" script options to override this if you like */
		}
	}
</style>' . "\n" .
                $xAjax ."\n" .
               '</head>
                   <body>' .
                $xShow .
                '  </body>' .
                '  </html>';

        return $xecho;
    }

}

?>
