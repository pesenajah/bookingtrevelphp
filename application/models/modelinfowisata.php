<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : infowisata
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelinfowisata extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListinfowisata() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "judulinfo," .
                "deskripsihtml," .
                "imgutama," .
                "mapadress," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM infowisata   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->judulinfo;
        }
        return $xBuffResul;
    }

    function getListinfowisata($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where judulinfo like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "judulinfo," .
                "deskripsihtml," .
                "imgutama," .
                "mapadress," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM infowisata $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailinfowisata($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "judulinfo," .
                "deskripsihtml," .
                "imgutama," .
                "mapadress," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM infowisata  WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexinfowisata() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "judulinfo," .
                "deskripsihtml," .
                "imgutama," .
                "mapadress," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM infowisata order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertinfowisata($xidx, $xjudulinfo, $xdeskripsihtml, $ximgutama, $xmapadress, $xtglinsert, $xtglupdate, $xidpegawai) {
        $xStr = " INSERT INTO infowisata( " .
                "idx," .
                "judulinfo," .
                "deskripsihtml," .
                "imgutama," .
                "mapadress," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai) VALUES('" . $xidx . "','" . $xjudulinfo . "','" . $xdeskripsihtml . "','" . $ximgutama . "','" . $xmapadress . "','" . $xtglinsert . "','" . $xtglupdate . "','" . $xidpegawai . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdateinfowisata($xidx, $xjudulinfo, $xdeskripsihtml, $ximgutama, $xmapadress, $xtglinsert, $xtglupdate, $xidpegawai) {
        $xStr = " UPDATE infowisata SET " .
                "idx='" . $xidx . "'," .
                "judulinfo='" . $xjudulinfo . "'," .
                "deskripsihtml='" . $xdeskripsihtml . "'," .
                "imgutama='" . $ximgutama . "'," .
                "mapadress='" . $xmapadress . "'," .
                "tglinsert='" . $xtglinsert . "'," .
                "tglupdate='" . $xtglupdate . "'," .
                "idpegawai='" . $xidpegawai . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeleteinfowisata($xidx) {
        $xStr = " DELETE FROM infowisata WHERE infowisata.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeleteinfowisata($xidx);
    }

    function setInsertLogDeleteinfowisata($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'infowisata',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }
    
//    function getDetailimagedetailbytitle($xjudulinfo) {
//
//        $xStr = "SELECT " .                
//                "imgutama" .
//                " FROM infowisata Where judulinfo = '$xjudulinfo'";
//        $query = $this->db->query($xStr);
//        $row = $query->row();
//        return $row;
//    }
    
    function getgambarinfowisata($xjudulinfo) {
        $xStr = "SELECT " .
                "idx," .
                "judulinfo," .
                "deskripsihtml," .
                "imgutama," .
                "mapadress," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai" .
                " FROM infowisata  WHERE judulinfo = '" . $xjudulinfo . "'";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

}

?>