<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : notifikasi
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelnotifikasi extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListnotifikasi() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "id_pengirim," .
                "id_penerima," .
                "keterangan," .
                "tgl_kirim," .
                "is_clicked,idaplikasi" .
                " FROM notifikasi order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->id_pengirim;
        }
        return $xBuffResul;
    }

    function getListnotifikasi($xAwal, $xLimit, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai_sparta');
        $idBagPegawai = @$this->modelpegawai->getDetailpegawai($idpegawai)->idBagPegawai;
        if (!empty($xSearch)) {
            $xSearch = "Where id_pengirim like '%" . $xSearch . "%'";
        }
        if (($idBagPegawai != 9 && $idBagPegawai != 16)) {
            $xSearch = 'Where id_penerima = \'' . $idpegawai . '\'';
        }
        $xStr = "SELECT " .
                "idx," .
                "id_pengirim," .
                "id_penerima," .
                "keterangan," .
                "tgl_kirim," .
                "is_clicked,idaplikasi" .
                " FROM notifikasi $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        //print($xStr);
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailnotifikasi($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "id_pengirim," .
                "id_penerima," .
                "keterangan," .
                "tgl_kirim," .
                "is_clicked,idaplikasi" .
                " FROM notifikasi  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexnotifikasi() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "id_pengirim," .
                "id_penerima," .
                "keterangan," .
                "tgl_kirim," .
                "is_clicked,idaplikasi" .
                " FROM notifikasi order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function setInsertnotifikasi_bybagian($xidx, $xid_pengirim, $xid_penerima, $xketerangan, $xtgl_kirim, $xis_clicked, $xidBagPegawai) {
        $perintah = '
		SELECT 	`idx`, 
				`npp`, 
				`Nama`, 
				`alamat`, 
				`NoTelpon`, 
				`user`, 
				`password`, 
				`email`, 
				`ymid`, 
				`idBagPegawai`, 
				`idjabatanpegawai`
		FROM 	`pegawai` 
		WHERE	`idBagPegawai`		= \'' . $xidBagPegawai . '\'
	';
        $hasilPerintah = $this->db->query($perintah);
        $xid_pengirim = $this->session->userdata('idpegawai_sparta');
        if (empty($xid_pengirim)) {
            $xid_pengirim = -1;
        }
        if (!empty($xid_pengirim)) {
            foreach ($hasilPerintah->result() as $row) {
                $xid_penerima = $row->idx;
                $this->setInsertnotifikasi($xidx, $xid_pengirim, $xid_penerima, $xketerangan, $xtgl_kirim, $xis_clicked);
            }
        } else {
            die('Anda belumm login atau sesi sudah habis');
        }
    }

    Function setInsertnotifikasi($xidx, $xid_pengirim, $xid_penerima, $xketerangan, $xtgl_kirim, $xis_clicked) {
        $xStr = " INSERT INTO notifikasi( " .
                "idx," .
                "id_pengirim," .
                "id_penerima," .
                "keterangan," .
                "tgl_kirim," .
                "is_clicked,idaplikasi) VALUES('" . $xidx . "','" . $xid_pengirim . "','" . $xid_penerima . "','" . $xketerangan . "'," . $xtgl_kirim . ",'" . $xis_clicked . "',1)";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatenotifikasi($xidx, $xid_pengirim, $xid_penerima, $xketerangan, $xtgl_kirim, $xis_clicked) {
        $xStr = " UPDATE notifikasi SET " .
                "idx='" . $xidx . "'," .
                "id_pengirim='" . $xid_pengirim . "'," .
                "id_penerima='" . $xid_penerima . "'," .
                "keterangan='" . $xketerangan . "'," .
                "tgl_kirim='" . $xtgl_kirim . "'," .
                "is_clicked='" . $xis_clicked . "'," .
                "idaplikasi=1 WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletenotifikasi($xidx) {
        $xStr = " DELETE FROM notifikasi WHERE notifikasi.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
    }

    function checkNotifikasi_saya() {
        $id_penerima = $this->session->userdata('idpegawai_sparta');
        $hasilPerintah = $this->checkNotifikasi($id_penerima);
        return $hasilPerintah;
    }

    function checkNotifikasi($id_penerima) {
        $perintah = '
			SELECT 	COUNT(`idx`) `notifikasi_baru`
			FROM 	`notifikasi` 
			WHERE	`id_penerima`	= \'' . $id_penerima . '\'
			AND	`is_clicked`		= \'0\'
			AND	`idaplikasi`		= 1;
		';
        $hasilPerintah = $this->db->query($perintah);
        $baris = $hasilPerintah->row();
        $notifikasi_baru = $baris->notifikasi_baru;
        if ($notifikasi_baru == 0) {
            return false;
        } else {
            return true;
        }
    }

    function getNotifikasi_saya() {
        $idpegawai = $this->session->userdata('idpegawai_sparta');
        $hasilPerintah = $this->getNotifikasi($idpegawai);
        return $hasilPerintah;
    }

    function getNotifikasi($id_penerima) {
        $perintah = '
			SELECT 	`idx`, 
				`id_pengirim`, 
				`id_penerima`, 
				`keterangan`, 
				`tgl_kirim`, 
				`is_clicked`
			FROM 	`notifikasi` 
			WHERE	`id_penerima`	= \'' . $id_penerima . '\'
			AND		`is_clicked`		= \'0\' 
			AND		`idaplikasi`		= 1 ORDER BY tgl_kirim DESC
		';
        $hasilPerintah = $this->db->query($perintah);
        return $hasilPerintah;
    }

    function doclosenotif_json($id_penerima, $idx) {
        $perintah = '
			UPDATE	`notifikasi`
			SET 	`is_clicked`	= 1
			WHERE	`idx`			= \'' . $idx . '\'
			AND		`id_penerima`	= \'' . $id_penerima . '\'
		';
        $hasilPerintah = $this->db->query($perintah);
        return $hasilPerintah;
    }
    
    function getNotifikasiBaru() {
        $this->load->model('modelpegawai');
        $ada_notifikasi = $this->checkNotifikasi_saya();
        $xbufResult = '';
        if ($ada_notifikasi) {
            $hasilPerintah = $this->getNotifikasi_saya();
            foreach ($hasilPerintah->result() as $row) {
                //$tampilkan	.= '<tr><td width="20%">'.convdate($row -> tgl_kirim).'</td><td width="75%"> '.$row -> keterangan.'</td><td width="5%"><span class= "ui-icon ui-icon-closethick" id="doclosenotif_json" style="cursor:pointer" onclick="doclosenotif_json('.$row -> idx.')">'.$row -> idx.'</span></td></tr>';
                $nama_pengirim = @$this->modelpegawai->getDetailpegawai($row->id_pengirim)->Nama;
                if (empty($nama_pengirim)) {
                    $nama_pengirim = 'Pengguna PUSD';
                }
                $xbufResult = 'Tanggal Kirim:' . convdate($row->tgl_kirim) . ','
                        . 'Pengirim:' . $nama_pengirim . ','
                        . 'Pesan:' . $row->keterangan . '	';
            }
        } else {
            $xbufResult = 'TIDAK ADA NOTIFIKASI';
        }
        return $xbufResult;
    }
}

?>