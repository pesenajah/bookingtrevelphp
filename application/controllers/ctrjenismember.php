<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : jenismember  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrjenismember extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformjenismember('0', $xAwal);
    }

    function createformjenismember($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxjenismember.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormjenismember($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormjenismember($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Jenis Member</h3><div class="garis"></div>' . form_open_multipart('ctrjenismember/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edJenisMember', 'Jenis Member', form_input(getArrayObj('edJenisMember', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanjenismember();"') . form_button('btNew', 'new', 'onclick="doClearjenismember();"') . '<div class="spacer"></div><div id="tabledatajenismember">' . $this->getlistjenismember(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistjenismember($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=30%') .
                tbaddcell('Jenis Member', '', 'width=60%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modeljenismember');
        $xQuery = $this->modeljenismember->getListjenismember($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditjenismember(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusjenismember(\'' . $row->idx . '\',\'' . substr($row->JenisMember, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->JenisMember) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchjenismember(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchjenismember(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchjenismember(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10%') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =2'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecjenismember() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modeljenismember');
        $row = $this->modeljenismember->getDetailjenismember($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['JenisMember'] = $row->JenisMember;
        echo json_encode($this->json_data);
    }

    function deletetablejenismember() {
        $edidx = $_POST['edidx'];
        $this->load->model('modeljenismember');
        $this->modeljenismember->setDeletejenismember($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchjenismember() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatajenismember'] = $this->getlistjenismember($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpanjenismember() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xJenisMember = $_POST['edJenisMember'];
        $this->load->model('modeljenismember');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modeljenismember->setUpdatejenismember($xidx, $xJenisMember);
            } else {
                $xStr = $this->modeljenismember->setInsertjenismember($xidx, $xJenisMember);
            }
        }
        echo json_encode(null);
    }

}

?>