<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : komponen  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrkomponen extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformkomponen('0', $xAwal);
    }

    function createformkomponen($xidkomponen, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxkomponen.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormkomponen($xidkomponen), '', '', $xAddJs, '');
    }

    function setDetailFormkomponen($xidkomponen) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>komponen</h3><div class="garis"></div>' . form_open_multipart('ctrkomponen/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidkomponen" id="edidkomponen" value="0" />';
        $xBufResult .= setForm('edNmKomponen', 'NmKomponen', form_input(getArrayObj('edNmKomponen', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edisshow', 'isshow', form_input(getArrayObj('edisshow', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpankomponen();"') . form_button('btNew', 'new', 'onclick="doClearkomponen();"') . '<div class="spacer"></div><div id="tabledatakomponen">' . $this->getlistkomponen(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistkomponen($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idkomponen', '', 'width=10%') .
                tbaddcell('NmKomponen', '', 'width=10%') .
                tbaddcell('isshow', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelkomponen');
        $xQuery = $this->modelkomponen->getListkomponen($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditkomponen(\'' . $row->idkomponen . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapuskomponen(\'' . $row->idkomponen . '\',\'' . substr($row->NmKomponen, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idkomponen) .
                    tbaddcell($row->NmKomponen) .
                    tbaddcell($row->isshow) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchkomponen(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchkomponen(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchkomponen(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editreckomponen() {
        $xIdEdit = $_POST['edidkomponen'];
        $this->load->model('modelkomponen');
        $row = $this->modelkomponen->getDetailkomponen($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idkomponen'] = $row->idkomponen;
        $this->json_data['NmKomponen'] = $row->NmKomponen;
        $this->json_data['isshow'] = $row->isshow;
        echo json_encode($this->json_data);
    }

    function deletetablekomponen() {
        $edidkomponen = $_POST['edidkomponen'];
        $this->load->model('modelkomponen');
        $this->modelkomponen->setDeletekomponen($edidkomponen);
    }

    function searchkomponen() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatakomponen'] = $this->getlistkomponen($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpankomponen() {
        $this->load->helper('json');
        if (!empty($_POST['edidkomponen'])) {
            $xidkomponen = $_POST['edidkomponen'];
        } else {
            $xidkomponen = '0';
        }
        $xNmKomponen = $_POST['edNmKomponen'];
        $xisshow = $_POST['edisshow'];
        $this->load->model('modelkomponen');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidkomponen != '0') {
                $xStr = $this->modelkomponen->setUpdatekomponen($xidkomponen, $xNmKomponen, $xisshow);
            } else {
                $xStr = $this->modelkomponen->setInsertkomponen($xidkomponen, $xNmKomponen, $xisshow);
            }
        }
    }

}

?>