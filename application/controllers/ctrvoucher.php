<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : voucher  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrvoucher extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformvoucher('0', $xAwal);
    }

    function createformvoucher($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/css/admin/upload/css/upload.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/jQuery.angka.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/tiny_mce/tiny_mce_src.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/tiny_mce/jquery.tinymce.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxmce2.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.knob.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.iframe-transport.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.fileupload.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/myuploadphoto.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxvoucher.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormvoucher($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormvoucher($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Voucher</h3><div class="garis"></div>' . form_open_multipart('ctrvoucher/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modelproduk');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edvoucher', 'Kode Voucher', form_input(getArrayObj('edvoucher', '', '150'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('ednominal', 'Nominal', form_input(getArrayObj('ednominal', '', '150'), '', 'class="angka"')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edjumlahmaxpengguna', 'Jumlah Max Pengguna', form_input(getArrayObj('edjumlahmaxpengguna', '', '100'), '', 'class="angka"')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglberlakudari', 'Tanggal Berlaku Dari', form_input(getArrayObj('edtglberlakudari', '', '100')));
        $xBufResult .= setForm('edtglberlakusampai', 'Tanggal Berlaku Sampai', form_input(getArrayObj('edtglberlakusampai', '', '100'))) . '<div class="spacer"></div>';
        //$xBufResult .= setForm('edidproduk', 'Produk', form_dropdown('edidproduk', $this->modelproduk->getArrayListproduk(), '', 'id="edidproduk" style = "width:150px"')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edpenjelasan', 'Penjelasan Voucher', '');
        $xBufResult .= setForm('edpenjelasan', '', form_textarea(getArrayObj('edpenjelasan', '', '600'),'','class="tinymce"')) . '<div class="spacer"></div>';
        $xBufResult .= '<div id="uploadcover" style="position:relative;left:150px;">';
        $xBufResult .= '<input type="input" name="edlinkimage" id="edlinkimage" alt="upload image"/>';
        $xBufResult .= '</div>' . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanvoucher();"') . form_button('btNew', 'new', 'onclick="doClearvoucher();"') . '<div class="spacer"></div><div id="tabledatavoucher">' . $this->getlistvoucher(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistvoucher($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=5%') .
                tbaddcell('Kode Voucher', '', 'width=10%') .
                tbaddcell('Nominal', '', 'width=10%') .
                tbaddcell('Jumlah Max Pengguna', '', 'width=10%') .
                tbaddcell('Tgl Berlaku Dari', '', 'width=10%') .
                tbaddcell('Tgl Berlaku Sampai', '', 'width=10%') .
                //tbaddcell('Produk', '', 'width=20%') .
                tbaddcell('Penjelasan Voucher', '', 'width=20%') .
                tbaddcell('Link Image', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:5%;text-align:center;'), '', TRUE);
        $this->load->model('modelvoucher');
        $xQuery = $this->modelvoucher->getListvoucher($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $tglberlakudari = datetomysql($row->tglberlakudari);
            $tglberlakusampai = datetomysql($row->tglberlakusampai);

            $this->load->model('modelproduk');
            $rowproduk = $this->modelproduk->getDetailproduk($row->idproduk);

            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditvoucher(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusvoucher(\'' . $row->idx . '\',\'' . substr($row->voucher, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->voucher) .
                    tbaddcell(number_format($row->nominal, 0, '', '.')) .
                    tbaddcell($row->jumlahmaxpengguna) .
                    tbaddcell($tglberlakudari) .
                    tbaddcell($tglberlakusampai) .
                  //  tbaddcell($rowproduk->JudulProduk) .
                    tbaddcell($row->penjelasan) .
                    tbaddcell($row->linkimage) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchvoucher(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchvoucher(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchvoucher(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecvoucher() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelvoucher');
        $row = $this->modelvoucher->getDetailvoucher($xIdEdit);
        $this->load->helper('json');
        $this->load->helper('common');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['voucher'] = $row->voucher;
        $this->json_data['nominal'] = $row->nominal;
        $this->json_data['jumlahmaxpengguna'] = $row->jumlahmaxpengguna;
        $this->json_data['idproduk'] = $row->idproduk;
        $this->json_data['penjelasan'] = $row->penjelasan;
        $this->json_data['linkimage'] = $row->linkimage;
        $this->json_data['tglberlakudari'] = datetomysql($row->tglberlakudari);
        $this->json_data['tglberlakusampai'] = datetomysql($row->tglberlakusampai);
        echo json_encode($this->json_data);
    }

    function deletetablevoucher() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelvoucher');
        $this->modelvoucher->setDeletevoucher($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchvoucher() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatavoucher'] = $this->getlistvoucher($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpanvoucher() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }

        $xtglberlakudari = datetomysql($_POST['edtglberlakudari']);
        $xtglberlakusampai = datetomysql($_POST['edtglberlakusampai']);
        $xvoucher = $_POST['edvoucher'];
        $xnominal = $_POST['ednominal'];
        $xjumlahmaxpengguna = $_POST['edjumlahmaxpengguna'];
        $xidproduk = $_POST['edidproduk'];
        $xpenjelasan = $_POST['edpenjelasan'];
        $xlinkimage = $_POST['edlinkimage'];
        $this->load->model('modelvoucher');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelvoucher->setUpdatevoucher($xidx, $xvoucher, $xnominal, $xjumlahmaxpengguna, $xtglberlakudari, $xtglberlakusampai, $xidproduk, $xpenjelasan, $xlinkimage);
            } else {
                $xStr = $this->modelvoucher->setInsertvoucher($xidx, $xvoucher, $xnominal, $xjumlahmaxpengguna, $xtglberlakudari, $xtglberlakusampai, $xidproduk, $xpenjelasan, $xlinkimage);
            }
        }
        echo json_encode(null);
    }

//    function setAutoCompleteMember() {
//        $this->load->helper('json');
//        $this->load->model('modelvoucher');
//        $ednama_member = $_POST['ednama_member'];
//        $query = $this->modelvoucher->setAutoCompleteMember($ednama_member);
//
//        $xdata = array();
//        foreach ($query->result() as $row) {
//            $xdata[] = array('label' => $row->Nama, 'value' => $row->idx);
//        }
//        $this->json_data['data'] = $xdata;
//
//        echo json_encode($this->json_data);
//    }

    function getListVoucherBymember() {
        $this->load->helper('json');
        $this->load->model('modelvoucher');
        $xIdMember = $_POST['edidmember'];
        $xQuery = $this->modelvoucher->getListvoucherByIdMember($xIdMember);
        $response = array();
        foreach ($xQuery->result() as $row) {
            $this->json_data['voucher'] = $row->voucher;
            $this->json_data['Nominal'] = $row->nominal;
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function getVoucherByIdVoucher() {
        $this->load->helper('json');
        $xidVoucher = $_POST['idVoucher'];
        $this->load->model('modelvoucher');
        $this->load->model('modeltransaksi');

        $rowvoucher = $this->modelvoucher->getDetailVoucherByIDVC($xidVoucher);
        $this->json_data['Status'] = "fail";
        $this->json_data['Keterangan'] = "Tidak Ditemukan Voucher $xidVoucher";
        $this->json_data['Nominal'] = "0";

        $response = array();
        if (!empty($rowvoucher->idx)) {
            $this->json_data['Status'] = "fail";
            $this->json_data['Keterangan'] = "Voucher $xidVoucher Sudah Tidak Berlaku";
            $this->json_data['Nominal'] = "0";
            $jmlvoucher = $this->modeltransaksi->getCountVoucer($xidVoucher);
            if ((@$rowvoucher->jumlahmaxpengguna + 0) > $jmlvoucher) {
                $this->json_data['Status'] = "OK";
                $this->json_data['Keterangan'] = $xidVoucher;
                $this->json_data['Nominal'] = @$rowvoucher->nominal + 0;
            }
        }

        array_push($response, $this->json_data);
        echo json_encode($response);
    }

    function getListVoucherAndroid() {
        $this->load->helper('json');
        $this->load->model('modelvoucher');
        $xdummy = $_POST['dummy'];
        $xQuery = $this->modelvoucher->getListvoucherAndroid();
        $response = array();
        foreach ($xQuery->result() as $row) {
            $this->json_data['voucher'] = $row->voucher;
            $this->json_data['Nominal'] = $row->nominal;
            $this->json_data['linkimage'] = $this->data_uri(@$row->linkimage);
            $this->json_data['penjelasan'] = $row->penjelasan;
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function data_uri($fileuri) {
        $mime = 'image/jpeg';
        $imgsrc = base_url() . 'resource/imgbtn/ic_logo_usd.png';

        if (!empty($fileuri)) {

            $fileuri = str_replace(" ", "%20", $fileuri);

            $imgsrc = base_url() . 'resource/uploaded/img/' . $fileuri;

            //   }
        }

        return $imgsrc;
    }

}

?>
