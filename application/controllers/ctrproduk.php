<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : produk  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrproduk extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformproduk('0', $xAwal);
    }

    function createformproduk($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxproduk.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormproduk($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormproduk($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>produk</h3><div class="garis"></div>' . form_open_multipart('ctrproduk/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edJudulProduk', 'JudulProduk', form_input(getArrayObj('edJudulProduk', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidKategoriProduk', 'idKategoriProduk', form_input(getArrayObj('edidKategoriProduk', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edKeterangan', 'Keterangan', form_input(getArrayObj('edKeterangan', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edphonekontak', 'phonekontak', form_input(getArrayObj('edphonekontak', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edNamaKontak', 'NamaKontak', form_input(getArrayObj('edNamaKontak', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edDiskripsiProduk', 'DiskripsiProduk', form_input(getArrayObj('edDiskripsiProduk', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edmapaddress', 'mapaddress', form_input(getArrayObj('edmapaddress', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edrate', 'rate', form_input(getArrayObj('edrate', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edratediscount', 'ratediscount', form_input(getArrayObj('edratediscount', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edrancode', 'rancode', form_input(getArrayObj('edrancode', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglinsert', 'tglinsert', form_input(getArrayObj('edtglinsert', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglupdate', 'tglupdate', form_input(getArrayObj('edtglupdate', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidpegawai', 'idpegawai', form_input(getArrayObj('edidpegawai', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanproduk();"') . form_button('btNew', 'new', 'onclick="doClearproduk();"') . '<div class="spacer"></div><div id="tabledataproduk">' . $this->getlistproduk(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistproduk($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=10%') .
                tbaddcell('JudulProduk', '', 'width=10%') .
                tbaddcell('idKategoriProduk', '', 'width=10%') .
                tbaddcell('Keterangan', '', 'width=10%') .
                tbaddcell('phonekontak', '', 'width=10%') .
                tbaddcell('NamaKontak', '', 'width=10%') .
                tbaddcell('DiskripsiProduk', '', 'width=10%') .
                tbaddcell('mapaddress', '', 'width=10%') .
                tbaddcell('rate', '', 'width=10%') .
                tbaddcell('ratediscount', '', 'width=10%') .
                tbaddcell('rancode', '', 'width=10%') .
                tbaddcell('tglinsert', '', 'width=10%') .
                tbaddcell('tglupdate', '', 'width=10%') .
                tbaddcell('idpegawai', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelproduk');
        $xQuery = $this->modelproduk->getListproduk($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditproduk(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusproduk(\'' . $row->idx . '\',\'' . substr($row->JudulProduk, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->JudulProduk) .
                    tbaddcell($row->idKategoriProduk) .
                    tbaddcell($row->Keterangan) .
                    tbaddcell($row->phonekontak) .
                    tbaddcell($row->NamaKontak) .
                    tbaddcell($row->DiskripsiProduk) .
                    tbaddcell($row->mapaddress) .
                    tbaddcell($row->rate) .
                    tbaddcell($row->ratediscount) .
                    tbaddcell($row->rancode) .
                    tbaddcell($row->tglinsert) .
                    tbaddcell($row->tglupdate) .
                    tbaddcell($row->idpegawai) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchproduk(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchproduk(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchproduk(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecproduk() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelproduk');
        $row = $this->modelproduk->getDetailproduk($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['JudulProduk'] = $row->JudulProduk;
        $this->json_data['idKategoriProduk'] = $row->idKategoriProduk;
        $this->json_data['Keterangan'] = $row->Keterangan;
        $this->json_data['phonekontak'] = $row->phonekontak;
        $this->json_data['NamaKontak'] = $row->NamaKontak;
        $this->json_data['DiskripsiProduk'] = $row->DiskripsiProduk;
        $this->json_data['mapaddress'] = $row->mapaddress;
        $this->json_data['rate'] = $row->rate;
        $this->json_data['ratediscount'] = $row->ratediscount;
        $this->json_data['rancode'] = $row->rancode;
        $this->json_data['tglinsert'] = $row->tglinsert;
        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['idpegawai'] = $row->idpegawai;
        echo json_encode($this->json_data);
    }

    function deletetableproduk() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelproduk');
        $this->modelproduk->setDeleteproduk($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchproduk() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledataproduk'] = $this->getlistproduk($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpanproduk() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xJudulProduk = $_POST['edJudulProduk'];
        $xidKategoriProduk = $_POST['edidKategoriProduk'];
        $xKeterangan = $_POST['edKeterangan'];
        $xphonekontak = $_POST['edphonekontak'];
        $xNamaKontak = $_POST['edNamaKontak'];
        $xDiskripsiProduk = $_POST['edDiskripsiProduk'];
        $xmapaddress = $_POST['edmapaddress'];
        $xrate = $_POST['edrate'];
        $xratediscount = $_POST['edratediscount'];
        $xrancode = $_POST['edrancode'];
        $xtglinsert = $_POST['edtglinsert'];
        $xtglupdate = $_POST['edtglupdate'];
        $xidpegawai = $_POST['edidpegawai'];
        $this->load->model('modelproduk');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelproduk->setUpdateproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xidpegawai);
            } else {
                $xStr = $this->modelproduk->setInsertproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xidpegawai);
            }
        }
        echo json_encode(null);
    }

    
    function getlistprodukAndroid() {
        $this->load->helper('json');
        //$xSearch,$xAwal,$xLimit
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $xidMember = $_POST['idMember'];
        
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['JudulProduk'] = "";
        $this->json_data['idKategoriProduk'] = "";
        $this->json_data['Keterangan'] = "";
        $this->json_data['phonekontak'] = "";
        $this->json_data['NamaKontak'] = "";
        $this->json_data['DiskripsiProduk'] = "";
        $this->json_data['mapaddress'] = "";
        $this->json_data['rate'] = "";
        $this->json_data['ratediscount'] = "";
        $this->json_data['rancode'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['idpegawai'] = "";
        $this->json_data['kapasitas'] = "";
        $this->json_data['standartpemakaian'] = "";
        $this->json_data['idsatuan'] = "";
        $this->json_data['linkimage'] = "";
        $this->json_data['jmlBooking'] ="0";
        $this->load->model('modelbooking');
        $response = array();
        $this->load->model('modelproduk');
        $this->load->model('modelimagedetail');
        $this->load->model('modelproduk');
        $jmlbooking  = $this->modelbooking->getJumlahBooking($xidMember);
        $xQuery = $this->modelproduk->getListproduk($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $rowimage= $this->modelimagedetail->getDetailimagedetailbyidproduk($row->idx);
            
            $this->json_data['idx'] = $row->idx;
            $this->json_data['JudulProduk'] = $row->JudulProduk;
            $this->json_data['idKategoriProduk'] = $row->idKategoriProduk;
            $this->json_data['Keterangan'] = $row->Keterangan;
            $this->json_data['phonekontak'] = $row->phonekontak;
            $this->json_data['NamaKontak'] = $row->NamaKontak;
            $this->json_data['DiskripsiProduk'] = $row->DiskripsiProduk;
            $this->json_data['mapaddress'] = $row->mapaddress;
            $this->json_data['rate'] = $row->rate;
            $this->json_data['ratediscount'] = $row->ratediscount;
            $this->json_data['rancode'] = $row->rancode;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['tglupdate'] = $row->tglupdate;
            $this->json_data['idpegawai'] = $row->idpegawai;
            $this->json_data['kapasitas'] = $row->kapasitas;
            $this->json_data['standartpemakaian'] = $row->standartpemakaian;
            $this->json_data['idsatuan'] = $row->idsatuan;
            $this->json_data['linkimage'] = $this->data_uri(@$rowimage->linkimage);
            $this->json_data['jmlBooking'] =$jmlbooking;
            
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }
    
    function getlistprodukAndroidByIdKategori() {
        $this->load->helper('json');
        //$xSearch,$xAwal,$xLimit
		
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $xidkategori = $_POST['idkategori'];
        $xidMember = $_POST['idMember'];
        // $xSearch = "";
        // $xAwal = "0";
        // $xLimit = "10";
        // $xidkategori = "2";
        // $xidMember = "0";
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['JudulProduk'] = "";
        $this->json_data['idKategoriProduk'] = "";
        $this->json_data['Keterangan'] = "";
        $this->json_data['phonekontak'] = "";
        $this->json_data['NamaKontak'] = "";
        $this->json_data['DiskripsiProduk'] = "";
        $this->json_data['mapaddress'] = "";
        $this->json_data['rate'] = "";
        $this->json_data['ratediscount'] = "";
        $this->json_data['rancode'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['idpegawai'] = "";
        $this->json_data['kapasitas'] = "";
        $this->json_data['standartpemakaian'] = "";
        $this->json_data['idsatuan'] = "";
        $this->json_data['linkimage'] = "";
        $this->json_data['jmlBooking'] ="0";
        $this->load->model('modelbooking');
        $response = array();
        $this->load->model('modelproduk');
        $this->load->model('modelimagedetail');
        $this->load->model('modeldetailproduk');
        $jmlbooking  = $this->modelbooking->getJumlahBooking($xidMember);
        $xQuery = $this->modelproduk->getListprodukidbyKategori($xAwal, $xLimit,$xidkategori, $xSearch);
		// for($j=0;$j<count($output->results[0]->address_components);$j++){
                // $mapaddress .= '<b>'.$output->results[0]->address_components[$j]->types[0].': </b>  '.$output->results[0]->address_components[$j]->long_name.'<br/>';
            // }
        foreach ($xQuery->result() as $row) {
            $rowimage= $this->modelimagedetail->getDetailimagedetailbyidproduk($row->idx);
            $this->json_data['idx'] = $row->idx;
            $this->json_data['JudulProduk'] = $row->JudulProduk;
            $this->json_data['idKategoriProduk'] = $row->idKategoriProduk;
            $this->json_data['Keterangan'] = $row->Keterangan;
            $this->json_data['phonekontak'] = $row->phonekontak;
            $this->json_data['NamaKontak'] = $row->NamaKontak;
            $this->json_data['DiskripsiProduk'] = $row->DiskripsiProduk;
			// get city from latLng
//			$mapaddress = $this->getAddressProduk($row->mapaddress);
			$mapaddress = "";
            $this->json_data['mapaddress'] = $mapaddress;
            $this->json_data['rate'] = $this->modeldetailproduk->getRangeHargaproduk($row->idx);
            $this->json_data['ratediscount'] = $row->ratediscount;
            $this->json_data['rancode'] = $row->rancode;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['tglupdate'] = $row->tglupdate;
            $this->json_data['idpegawai'] = $row->idpegawai;
            $this->json_data['kapasitas'] = $row->kapasitas;
            $this->json_data['standartpemakaian'] = $row->standartpemakaian;
            $this->json_data['idsatuan'] = $row->idsatuan;
            $this->json_data['linkimage'] = $this->data_uri(@$rowimage->linkimage);
            $this->json_data['jmlBooking'] =$jmlbooking;
            
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }
    
    function data_uri($fileuri) {
        $mime = 'image/jpeg';
        $imgsrc = base_url() . 'resource/imgbtn/ic_logo_usd.png';

        if (!empty($fileuri)) {

           $fileuri= str_replace(" ", "%20", $fileuri);

            $imgsrc = base_url().'resource/uploaded/img/' . $fileuri;

            //   }
        }

        return $imgsrc;
    }
    
    function inserttableprodukAndroid() {
        $xidx = $_POST['edidx'];
        $xJudulProduk = $_POST['edJudulProduk'];
        $xidKategoriProduk = $_POST['edidKategoriProduk'];
        $xKeterangan = $_POST['edKeterangan'];
        $xphonekontak = $_POST['edphonekontak'];
        $xNamaKontak = $_POST['edNamaKontak'];
        $xDiskripsiProduk = $_POST['edDiskripsiProduk'];
        $xmapaddress = $_POST['edmapaddress'];
        $xrate = $_POST['edrate'];
        $xratediscount = $_POST['edratediscount'];
        $xrancode = $_POST['edrancode'];
        $xtglinsert = $_POST['edtglinsert'];
        $xtglupdate = $_POST['edtglupdate'];
        $xidpegawai = $_POST['edidpegawai'];
        $xkapasitas = $_POST['edkapasitas'];
        $xstandartpemakaian = $_POST['edstandartpemakaian'];
        $xidsatuan = $_POST['edidsatuan'];

        $this->load->model('modelproduk');
        if ($xidx != '0') {
            $this->modelproduk->setUpdateproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xidpegawai, $xkapasitas, $xstandartpemakaian, $xidsatuan);
        } else {
            $this->modelproduk->setInsertproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xidpegawai, $xkapasitas, $xstandartpemakaian, $xidsatuan);
        }
        $this->createform('0');
    }

	function getAddressProduk($latLng_y){
		$latLng_x = explode(" ",$latLng_y);
		$mapaddress = "-";
		if(sizeof($latLng_x) > 1){
			$latLng = $latLng_x[0].$latLng_x[1];
			$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latLng.'&sensor=false');
			$output= json_decode($geocode);
			$response = array();
			foreach($output->results[0]->address_components as $addressComponet) {
				if(in_array('political', $addressComponet->types)) {
					$response[] = $addressComponet->long_name;
				}
			}
			$Address = $response[0];
			$City = $response[1];
			$State = $response[2];
			$mapaddress =  $Address.", ".$City.", ".$State;
		}
		return $mapaddress;
	}
        
        function getprodukAndroidById() {
        $this->load->helper('json');
        //$xSearch,$xAwal,$xLimit
		
//        $xSearch = $_POST['search'];
//        $xAwal = $_POST['start'];
//        $xLimit = $_POST['limit'];
        $xidx = $_POST['idx'];
        $xidMember = $_POST['idMember'];
        // $xSearch = "";
        // $xAwal = "0";
        // $xLimit = "10";
        // $xidkategori = "2";
        // $xidMember = "0";
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['JudulProduk'] = "";
        $this->json_data['idKategoriProduk'] = "";
        $this->json_data['Keterangan'] = "";
        $this->json_data['phonekontak'] = "";
        $this->json_data['NamaKontak'] = "";
        $this->json_data['DiskripsiProduk'] = "";
        $this->json_data['mapaddress'] = "";
        $this->json_data['rate'] = "";
        $this->json_data['ratediscount'] = "";
        $this->json_data['rancode'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['idpegawai'] = "";
        $this->json_data['kapasitas'] = "";
        $this->json_data['standartpemakaian'] = "";
        $this->json_data['idsatuan'] = "";
        $this->json_data['linkimage'] = "";
        $this->json_data['jmlBooking'] ="0";
        $this->load->model('modelbooking');
        $response = array();
        $this->load->model('modelproduk');
        $this->load->model('modelimagedetail');
        $this->load->model('modeldetailproduk');
        $jmlbooking  = $this->modelbooking->getJumlahBooking($xidMember);
        $row = $this->modelproduk->getDetailproduk($xidx);
		// for($j=0;$j<count($output->results[0]->address_components);$j++){
                // $mapaddress .= '<b>'.$output->results[0]->address_components[$j]->types[0].': </b>  '.$output->results[0]->address_components[$j]->long_name.'<br/>';
            // }
//        foreach ($xQuery->result() as $row) {
            $rowimage= $this->modelimagedetail->getDetailimagedetailbyidproduk($row->idx);
            $this->json_data['idx'] = $row->idx;
            $this->json_data['JudulProduk'] = $row->JudulProduk;
            $this->json_data['idKategoriProduk'] = $row->idKategoriProduk;
            $this->json_data['Keterangan'] = $row->Keterangan;
            $this->json_data['phonekontak'] = $row->phonekontak;
            $this->json_data['NamaKontak'] = $row->NamaKontak;
            $this->json_data['DiskripsiProduk'] = $row->DiskripsiProduk;
			// get city from latLng
//			$mapaddress = $this->getAddressProduk($row->mapaddress);
			$mapaddress = "";
            $this->json_data['mapaddress'] = $mapaddress;
            $this->json_data['latlong'] = $row->mapaddress;
            $this->json_data['rate'] = $this->modeldetailproduk->getRangeHargaproduk($row->idx);
            $this->json_data['ratediscount'] = $row->ratediscount;
            $this->json_data['rancode'] = $row->rancode;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['tglupdate'] = $row->tglupdate;
            $this->json_data['idpegawai'] = $row->idpegawai;
            $this->json_data['kapasitas'] = $row->kapasitas;
            $this->json_data['standartpemakaian'] = $row->standartpemakaian;
            $this->json_data['idsatuan'] = $row->idsatuan;
            $this->json_data['linkimage'] = $this->data_uri(@$rowimage->linkimage);
            $this->json_data['jmlBooking'] =$jmlbooking;
            
            array_push($response, $this->json_data);
        
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($this->json_data);
    }
                
}

?>
