<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : jenispembayaran  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrjenispembayaran extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformjenispembayaran('0', $xAwal);
    }

    function createformjenispembayaran($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxjenispembayaran.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormjenispembayaran($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormjenispembayaran($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Jenis Pembayaran</h3><div class="garis"></div>' . form_open_multipart('ctrjenispembayaran/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edjenispembayaran', 'Jenis Pembayaran', form_input(getArrayObj('edjenispembayaran', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edjenispembayaran', 'Jenis Pembayaran (INGGRIS)', form_input(getArrayObj('edjenispembayaranING', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanjenispembayaran();"') . form_button('btNew', 'new', 'onclick="doClearjenispembayaran();"') . '<div class="spacer"></div><div id="tabledatajenispembayaran">' . $this->getlistjenispembayaran(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistjenispembayaran($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=30%') .
                tbaddcell('Jenis Pembayaran', '', 'width=60%') .
                tbaddcell('Jenis Pembayaran ING', '', 'width=60%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modeljenispembayaran');
        $xQuery = $this->modeljenispembayaran->getListjenispembayaran($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditjenispembayaran(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusjenispembayaran(\'' . $row->idx . '\',\'' . substr($row->jenispembayaran, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->jenispembayaran) .
                    tbaddcell($row->jenispembayaranING) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchjenispembayaran(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchjenispembayaran(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchjenispembayaran(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10%') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =2'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecjenispembayaran() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modeljenispembayaran');
        $row = $this->modeljenispembayaran->getDetailjenispembayaran($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['jenispembayaran'] = $row->jenispembayaran;
        $this->json_data['jenispembayaranING'] = $row->jenispembayaranING;
        echo json_encode($this->json_data);
    }

    function deletetablejenispembayaran() {
        $edidx = $_POST['edidx'];
        $this->load->model('modeljenispembayaran');
        $this->modeljenispembayaran->setDeletejenispembayaran($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchjenispembayaran() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatajenispembayaran'] = $this->getlistjenispembayaran($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function getlistjenispembayaranAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['jenispembayaran'] = "";
        $this->json_data['jenispembayaranING'] = "";
        $response = array();
        $this->load->model('modeljenispembayaran');
        $xQuery  = $this->modeljenispembayaran->getListjenispembayaran($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['jenispembayaran'] = $row->jenispembayaran;
            $this->json_data['jenispembayaranING'] = $row->jenispembayaranING;
            
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }
    
    function simpanjenispembayaran() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xjenispembayaran = $_POST['edjenispembayaran'];
        $xjenispembayaranING = $_POST['edjenispembayaranING'];
        $this->load->model('modeljenispembayaran');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modeljenispembayaran->setUpdatejenispembayaran($xidx, $xjenispembayaran,$xjenispembayaranING);
            } else {
                $xStr = $this->modeljenispembayaran->setInsertjenispembayaran($xidx, $xjenispembayaran,$xjenispembayaranING);
            }
        }
        echo json_encode(null);
    }

}

?>