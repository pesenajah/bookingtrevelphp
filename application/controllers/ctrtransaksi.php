<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : transaksi  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrtransaksi extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformtransaksi('0', $xAwal);
    }

    function createformtransaksi($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/jquery.ui.timepicker-0.0.6.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxtransaksi.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormtransaksi($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormtransaksi($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Transaksi</h3><div class="garis"></div>' . form_open_multipart('ctrtransaksi/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edidbooking', 'Id Booking', form_input(getArrayObj('edidbooking', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglbooking', 'Tanggal Booking', form_input(getArrayObj('edtglbooking', '', '100')));
        $xBufResult .= setForm('edjambooking', 'Jam Booking', form_input(getArrayObj('edjambooking', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglbatalbooking', 'Tanggal Batal Booking', form_input(getArrayObj('edtglbatalbooking', '', '100')));
        $xBufResult .= setForm('edjambatalbooking', 'Jam Batal Booking', form_input(getArrayObj('edjambatalbooking', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edketeranganbatal', 'Keterangan Batal', form_textarea(getArrayObj('edketeranganbatal', '', '600'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edharganormal', 'Harga Normal', form_input(getArrayObj('edharganormal', '', '150'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edhargadiscount', 'Harga Discount', form_input(getArrayObj('edhargadiscount', '', '150'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidvoucher', 'Kode Voucher', form_input(getArrayObj('edidvoucher', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= '<input type="hidden" id="edidmember" value="0">';
        $xBufResult .= setForm('ednama_member', 'Member', form_input(getArrayObj('ednama_member', '', '400'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edidpegawai', 'idpegawai', form_input(getArrayObj('edidpegawai', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edspesialrequest', 'Permintaan Khusus', form_textarea(getArrayObj('edspesialrequest', '', '600'))) . '<div class="spacer"></div>';
        $this->load->model('modeljenispembayaran');
//        $xBufResult .= setForm('edtglupdate', 'tglupdate', form_input(getArrayObj('edtglupdate', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidjenisbayar', 'Jenis Pembayaran', form_dropdown('edidjenisbayar', $this->modeljenispembayaran->getArrayListjenispembayaran(), '', 'id="edidjenisbayar" style = "width:150px"')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglbayar', 'Tanggal Bayar', form_input(getArrayObj('edtglbayar', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpantransaksi();"') . form_button('btNew', 'new', 'onclick="doCleartransaksi();"') . '<div class="spacer"></div><div id="tabledatatransaksi">' . $this->getlisttransaksi(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlisttransaksi($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=10%') .
                tbaddcell('Id Booking', '', 'width=10%') .
                tbaddcell('Tanggal Booking', '', 'width=10%') .
                tbaddcell('Tanggal Batal Booking', '', 'width=10%') .
                tbaddcell('Keterangan Batal', '', 'width=10%') .
                tbaddcell('Harga Normal', '', 'width=10%') .
                tbaddcell('Harga Discount', '', 'width=10%') .
                tbaddcell('Voucher', '', 'width=10%') .
                tbaddcell('Member', '', 'width=10%') .
//                tbaddcell('Id Pegawai', '', 'width=10%') .
                tbaddcell('Permintaan Khusus', '', 'width=10%') .
                tbaddcell('Tanggal Update', '', 'width=10%') .
                tbaddcell('Pembayaran', '', 'width=10%') .
                tbaddcell('Tanggal Bayar', '', 'width=10%') .
                tbaddcell('Aksi', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modeltransaksi');
        $this->load->model('modelmember');
        $this->load->model('modeljenispembayaran');
        $xQuery = $this->modeltransaksi->getListtransaksi($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $arraytgljambooking = explode(' ', $row->tglbooking);
            $tgljambooking = datetomysql($arraytgljambooking[0]) . ' ' . $arraytgljambooking[1];

            $arraytgljambatalbooking = explode(' ', $row->tglbatalbooking);
            $tgljambatalbooking = datetomysql($arraytgljambatalbooking[0]) . ' ' . $arraytgljambatalbooking[1];

            $arraytgljamupdate = explode(' ', $row->tglupdate);
            $tgljamupdate = datetomysql($arraytgljamupdate[0]) . ' ' . $arraytgljamupdate[1];

            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" title="Edit Data" onclick = "doedittransaksi(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
//            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapustransaksi(\'' . $row->idx . '\',\'' . substr($row->idbooking, 0, 20) . '\');" style="border:none;">';
            $xButtonBatal = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Batal Transaksi" title="Batal Transaksi" onclick = "dobataltransaksi(\'' . $row->idx . '\',\'' . $row->tglbooking . '\');" width="20px" height="20px" style="border:none;">';
            $xButtonLanjut = '<img src="' . base_url() . 'resource/imgbtn/correct.jpg" alt="Lanjut Transaksi" title="Lanjut Transaksi" onclick = "dolanjuttransaksi(\'' . $row->idx . '\',\'' . $row->tglbooking . '\');" width="20px" height="20px" style="border:none;">';

            if ($row->tglbatalbooking !== '0000-00-00 00:00:00') {
                $xthemestatus = "background:red";
                $xbuttonaksi = $xButtonLanjut;
            } else {
                $xthemestatus = "";
                $xbuttonaksi = $xButtonBatal;
            }

            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->idbooking) .
                    tbaddcell($tgljambooking) .
                    tbaddcell($tgljambatalbooking) .
                    tbaddcell($row->keteranganbatal) .
                    tbaddcell($row->harganormal) .
                    tbaddcell($row->hargadiscount) .
                    tbaddcell($row->idvoucher) .
                    tbaddcell($this->modelmember->getDetailmember($row->idmember)->Nama) .
//                    tbaddcell($row->idpegawai) .
                    tbaddcell($row->spesialrequest) .
                    tbaddcell($tgljamupdate) .
                    tbaddcell(@$this->modeljenispembayaran->getDetailjenispembayaran($row->idjenisbayar)->jenispembayaran) .
                    tbaddcell(datetomysql($row->tglbayar)) .
                    tbaddcell($xButtonEdit . $xbuttonaksi), $xthemestatus);
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchtransaksi(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchtransaksi(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchtransaksi(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=3') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =12'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function getlisttransaksiAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['idbooking'] = "";
        $this->json_data['tglbooking'] = "";
        $this->json_data['tglbatalbooking'] = "";
        $this->json_data['keteranganbatal'] = "";
        $this->json_data['harganormal'] = "";
        $this->json_data['hargadiscount'] = "";
        $this->json_data['idvoucher'] = "";
        $this->json_data['idmember'] = "";
        $this->json_data['idpegawai'] = "";
        $this->json_data['spesialrequest'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['idjenisbayar'] = "";
        $this->json_data['tglbayar'] = "";
        $response = array();
        $this->load->model('modeltransaksi');
        $xQuery = $this->modeltransaksi->getListtransaksi($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['idbooking'] = $row->idbooking;
            $this->json_data['tglbooking'] = $row->tglbooking;
            $this->json_data['tglbatalbooking'] = $row->tglbatalbooking;
            $this->json_data['keteranganbatal'] = $row->keteranganbatal;
            $this->json_data['harganormal'] = $row->harganormal;
            $this->json_data['hargadiscount'] = $row->hargadiscount;
            $this->json_data['idvoucher'] = $row->idvoucher;
            $this->json_data['idmember'] = $row->idmember;
            $this->json_data['idpegawai'] = $row->idpegawai;
            $this->json_data['spesialrequest'] = $row->spesialrequest;
            $this->json_data['tglupdate'] = $row->tglupdate;
            $this->json_data['idjenisbayar'] = $row->idjenisbayar;
            $this->json_data['tglbayar'] = $row->tglbayar;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($this->json_data);
    }

    function simpantransaksiAndroid() {
        $xidx = $_POST['edidx'];
        $xidbooking = $_POST['edidbooking'];
        $xtglbooking = $_POST['edtglbooking'];
        $xtglbatalbooking = $_POST['edtglbatalbooking'];
        $xketeranganbatal = $_POST['edketeranganbatal'];
        $xharganormal = $_POST['edharganormal'];
        $xhargadiscount = $_POST['edhargadiscount'];
        $xidvoucher = $_POST['edidvoucher'];
        $xidmember = $_POST['edidmember'];
        $xidpegawai = $_POST['edidpegawai'];
        $xspesialrequest = $_POST['edspesialrequest'];
        $xtglupdate = $_POST['edtglupdate'];
        $xidjenisbayar = $_POST['edidjenisbayar'];
        $xtglbayar = $_POST['edtglbayar'];
        $xNominalVoucher = $_POST['sNominalVoucher'];
        $xHarusBayar = $_POST['sHarusBayar'];

        $this->load->helper('json');
        $this->load->model('modeltransaksi');
        $this->load->model('modelvoucher');
        $this->load->model('modelbooking');
        $response = array();
        if ($xidx != '0') {
            $this->modeltransaksi->setUpdatetransaksi($xidx, $xidbooking, $xtglbooking, $xtglbatalbooking, $xketeranganbatal, str_replace(".", "", $xharganormal), str_replace(".", "", $xhargadiscount), $xidvoucher, $xidmember, $xidpegawai, $xspesialrequest, $xtglupdate, $xidjenisbayar, $xtglbayar, str_replace(".", "", $xNominalVoucher), str_replace(".", "", $xHarusBayar));
        } else {
            //echo "sdsad ".$xidvoucher;
            $this->modeltransaksi->setInserttransaksi($xidx, $xidbooking, $xtglbooking, $xtglbatalbooking, $xketeranganbatal, str_replace(".", "", $xharganormal), str_replace(".", "", $xhargadiscount), $xidvoucher, $xidmember, $xidpegawai, $xspesialrequest, $xtglupdate, $xidjenisbayar, $xtglbayar, str_replace(".", "", $xNominalVoucher), str_replace(".", "", $xHarusBayar));

            if (!empty($xidvoucher)) {
                $this->modelvoucher->setUpdatevoucherFromBooking($xidvoucher);
            }
            $this->modelbooking->setUpdatebookingFromBookingNow($xidbooking);
        }
        $row = $this->modeltransaksi->getLastIndextransaksi();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['idbooking'] = $row->idbooking;
        $this->json_data['tglbooking'] = $row->tglbooking;
        $this->json_data['tglbatalbooking'] = $row->tglbatalbooking;
        $this->json_data['keteranganbatal'] = $row->keteranganbatal;
        $this->json_data['harganormal'] = $row->harganormal;
        $this->json_data['hargadiscount'] = $row->hargadiscount;
        $this->json_data['idvoucher'] = $row->idvoucher;
        $this->json_data['idmember'] = $row->idmember;
        $this->json_data['idpegawai'] = $row->idpegawai;
        $this->json_data['spesialrequest'] = $row->spesialrequest;
        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['idjenisbayar'] = $row->idjenisbayar;
        $this->json_data['tglbayar'] = $row->tglbayar;
        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrectransaksi() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modeltransaksi');
        $row = $this->modeltransaksi->getDetailtransaksi($xIdEdit);

        $this->load->helper('json');
        $this->load->helper('common');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['idbooking'] = $row->idbooking;

        $arraytgljambooking = explode(' ', $row->tglbooking);
        $this->json_data['tglbooking'] = datetomysql($arraytgljambooking[0]);
        $this->json_data['jambooking'] = $arraytgljambooking[1];

        $arraytgljambatalbooking = explode(' ', $row->tglbatalbooking);
        $this->json_data['tglbatalbooking'] = datetomysql($arraytgljambatalbooking[0]);
        $this->json_data['jambatalbooking'] = $arraytgljambatalbooking[1];

        $this->json_data['keteranganbatal'] = $row->keteranganbatal;
        $this->json_data['harganormal'] = $row->harganormal;
        $this->json_data['hargadiscount'] = $row->hargadiscount;
        $this->json_data['idvoucher'] = $row->idvoucher;
        $this->json_data['idmember'] = $row->idmember;

        $this->load->model('modelmember');
        $rowmember = $this->modelmember->getDetailmember($row->idmember);
        $this->json_data['nama_member'] = @$rowmember->Nama;

        $this->json_data['idpegawai'] = $row->idpegawai;
        $this->json_data['spesialrequest'] = $row->spesialrequest;
//        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['idjenisbayar'] = $row->idjenisbayar;
        $this->json_data['tglbayar'] = datetomysql($row->tglbayar);
        echo json_encode($this->json_data);
    }

    function deletetabletransaksi() {
        $edidx = $_POST['edidx'];
        $this->load->model('modeltransaksi');
        $this->modeltransaksi->setDeletetransaksi($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchtransaksi() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatatransaksi'] = $this->getlisttransaksi($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpantransaksi() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xidbooking = $_POST['edidbooking'];

        $xtglbooking = datetomysql($_POST['edtglbooking']);
        $xjambooking = $_POST['edjambooking'];
        $xtgljambooking = $xtglbooking . ' ' . $xjambooking;

        $xtglbatalbooking = datetomysql($_POST['edtglbatalbooking']);
        $xjambatalbooking = $_POST['edjambatalbooking'];
        $xtgljambatalbooking = $xtglbatalbooking . ' ' . $xjambatalbooking;

        $xketeranganbatal = $_POST['edketeranganbatal'];
        $xharganormal = $_POST['edharganormal'];
        $xhargadiscount = $_POST['edhargadiscount'];
        $xidvoucher = $_POST['edidvoucher'];
        $xidmember = $_POST['edidmember'];
        $xidpegawai = $_POST['edidpegawai'];
        $xspesialrequest = $_POST['edspesialrequest'];
        $xtglupdate = $_POST['edtglupdate'];
        $xidjenisbayar = $_POST['edidjenisbayar'];
        $xtglbayar = $_POST['edtglbayar'];
        $this->load->model('modeltransaksi');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modeltransaksi->setUpdatetransaksi($xidx, $xidbooking, $xtgljambooking, $xtgljambatalbooking, $xketeranganbatal, $xharganormal, $xhargadiscount, $xidvoucher, $xidmember, $xidpegawai, $xspesialrequest, $xtglupdate, $xidjenisbayar, $xtglbayar);
            } else {
                $xStr = $this->modeltransaksi->setInserttransaksi($xidx, $xidbooking, $xtgljambooking, $xtgljambatalbooking, $xketeranganbatal, $xharganormal, $xhargadiscount, $xidvoucher, $xidmember, $xidpegawai, $xspesialrequest, $xtglupdate, $xidjenisbayar, $xtglbayar);
            }
        }
        echo json_encode(null);
    }

    function setBataltransaksi() {
        $xidx = $_POST['edidx'];
        $this->load->model('modeltransaksi');
        $xstatus = 'batal';
        $this->modeltransaksi->setUpdateProsestransaksi($xidx, $xstatus);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function setLanjuttransaksi() {
        $xidx = $_POST['edidx'];
        $this->load->model('modeltransaksi');
        $xstatus = 'lanjut';
        $this->modeltransaksi->setUpdateProsestransaksi($xidx, $xstatus);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function getTotalHarusBayar() {
        $this->load->helper('json');
        $xNominalHarusBayar = $_POST['nominalbayar'];
        $this->load->model('modeltransaksi');

        $iloop = 501;
        $isloop = true;
        while ($isloop) {
            $iloop++;

            $xNominalHarusBayar -= $iloop;
            if (!$this->modeltransaksi->getisHargaUnikAda($xNominalHarusBayar)) {
                $isloop = FALSE;
            };

            if ($iloop >= 999) {
                $isloop = FALSE;
            }
        }
        $response = array();
        $this->json_data['Status'] = "OK";
        $this->json_data['Keterangan'] = '';
        if ($xNominalHarusBayar < 0) {
            $xNominalHarusBayar = 0;
        }
        $this->json_data['Nominal'] = $xNominalHarusBayar;
        array_push($response, $this->json_data);
        echo json_encode($response);
    }

    function getTotalHarusBayarcoba($xNominalHarusBayar) {
        $this->load->helper('json');
        $this->load->model('modeltransaksi');

        $iloop = 501;
        $isloop = true;
        while ($isloop) {
            $iloop++;

            $xNominalHarusBayar -= $iloop;
            if (!$this->modeltransaksi->getisHargaUnikAda($xNominalHarusBayar)) {
                $isloop = FALSE;
            };

            if ($iloop >= 999) {
                $isloop = FALSE;
            }
        }
        $response = array();
        $this->json_data['Status'] = "OK";
        $this->json_data['Keterangan'] = '';
        if ($xNominalHarusBayar < 0) {
            $xNominalHarusBayar = 0;
        }
        $this->json_data['Nominal'] = $xNominalHarusBayar;
        array_push($response, $this->json_data);
        echo json_encode($response);
    }

    function getTransaksi($xIdTransaksi, $xTableBooking, $bhs) {
        $this->load->helper('common');
        $xBufResult = "<table class=\"tableborder\" style=\"width:100%;padding:5px;\">";

        $waktuTransaksi = "Date Of Booking";
        if ($bhs === "in") {
            $waktuTransaksi = "Tgl Booking";
        }

        $Tagihan = "Bill";
        if ($bhs === "in") {
            $Tagihan = "Tagihan";
        }

        $YgHrsdibyar = "To Be Paid";
        if ($bhs === "in") {
            $YgHrsdibyar = "Yang Harus Dibayar";
        }

        $tglbayar = "Pay of Date";
        if ($bhs === "in") {
            $tglbayar = "Tgl Bayar";
        }

        $blmbayar = "Have Not Transferred";
        if ($bhs === "in") {
            $blmbayar = "Belum Transfer";
        }

        $carabayar = "How to Pay";
        if ($bhs === "in") {
            $carabayar = "Cara Bayar";
        }
        $this->load->model('modelvoucher');
        $this->load->model('modeltransaksi');
        $this->load->model('modeljenispembayaran');
        $rowtransaksi = $this->modeltransaksi->getDetailtransaksi($xIdTransaksi);
        $rowjenispembayaran = $this->modeljenispembayaran->getDetailjenispembayaran(@$rowtransaksi->idjenisbayar);
        $xBufResult .= "<tr><td>$waktuTransaksi</td><td align=right>" . $rowtransaksi->tglbooking . "</td></tr>";
        $xBufResult .= "<tr><td colspan=2 style=\"border-bottom:1px #ddd solid\"><table width=100%>" . $xTableBooking . "</td></tr></table>";
        $xBufResult .= "<tr><td>Sub Total</td><td align=right>" . number_format($rowtransaksi->hargadiscount, 0, '.', ',') . "</td></tr>";
        $xNominalVoucher = 0;
        if (!empty($rowtransaksi->idvoucher)) {
            $rowvoucher = $this->modelvoucher->getDetailVoucherByIDVC(@$rowtransaksi->idvoucher);
            $xBufResult .= "<tr><td>Voucher (" . @$rowtransaksi->idvoucher . ")</td><td align=right>" . number_format(@$rowvoucher->nominal, 0, '.', ',') . "</td></tr>";
            $xNominalVoucher = @$rowvoucher->nominal;
        }
        $xBufResult .= "<tr><td>$Tagihan </td><td align=right>" . number_format($rowtransaksi->hargadiscount - $xNominalVoucher, 0, '.', ',') . "</td></tr>";
        $xBufResult .= "<tr><td>$YgHrsdibyar </td><td align=right>" . number_format($rowtransaksi->hargadibayar, 0, '.', ',') . "</td></tr>";
        if ($rowtransaksi->tglbayar == "0000-00-00") {
            $xBufResult .= "<tr><td>$tglbayar </td><td align=right style=\"color:red;\"><b>$blmbayar</b></td></tr>";
        } else {
            $xBufResult .= "<tr><td>$tglbayar </td><td align=right><b>" . datetomysql($rowtransaksi->tglbayar) . "</b></td></tr>";
        }
        $xBufResult .= "<tr><td>Cara Bayar </td><td align=right>" . @$rowjenispembayaran->jenispembayaran . "</td></tr>";

        $xBufResult .= "</table>";

        return $xBufResult;
    }

    function getArrListBooking($xlistidx, $bhs) {

        $xBufResult = "<tr class=\"head\"><td>No</td><td>Items Booking</td> <td>Sub Total</td></tr>";
        $this->load->model('modelbooking');
        $this->load->model('modelproduk');
        $this->load->model('modeldetailproduk');
        $xQuery = $this->modelbooking->getListbookingInIdx($xlistidx);
        $ino = 1;
        $hari = "Days";
        if ($bhs === "in") {
            $hari = "Hari";
        }
        foreach ($xQuery->result() as $row) {
            $rowproduk = $this->modelproduk->getDetailproduk($row->idproduk);
            $rowdetailproduk = $this->modeldetailproduk->getDetaildetailproduk($row->iddetailproduk);

            $xBufResult .= "<tr><td>" . ($ino++) . "</td><td>" . $rowdetailproduk->juduldetailproduk . "<br/>(" . $rowproduk->JudulProduk . ")"
                    . "<br />" . "" . number_format($row->jmlhari, 0, '.', ',') . " $hari "
                    . "<br />@Nominal :" . number_format($row->hargadiscount, 0, '.', ',') . "</td><td align=right>" . number_format($row->hargadiscount * $row->jmlhari, 0, '.', ',') . "</td></tr>";
        }

        return $xBufResult;
    }

    function getCSS() {
        $xCss = '.tablecontent{
                        border:1px solid;
              }
              .tablecontent table{
                  border-collapse: separate;
              }
              /*h3{
                  font-size:14px;
                  font-weight:bold;
              }*/

              .tableborder{
                  border-right:1px solid #C8C8C8;
                  border-bottom:1px solid #C8C8C8;
                  border-top:0px solid #C8C8C8;
                  border-left:0px solid #C8C8C8;
                  background:#F6F6EB;
              }
              .tableborder .border td{
                  border-top:1px solid #C8C8C8;
                  border-left:1px solid #C8C8C8;
              }
              .tableborder .endright td{
                  -border-right:1px solid #C8C8C8;
              }
               .tableborder .endbottom td{
                  -border-bottom:1px solid #C8C8C8;
              }
               .tableborder td{
                  padding:0 5px;

              }
               .tableborder .head{
                  /* Safari 4-5, Chrome 1-9 */ background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#F4F4F4), to(#C1C1C1));
                  /* Safari 5.1, Chrome 10+ */ background: -webkit-linear-gradient(top, #F4F4F4, #C1C1C1);
                  /* Firefox 3.6+ */ background: -moz-linear-gradient(top, #F4F4F4, #C1C1C1);
                  /* IE 10 */ background: -ms-linear-gradient(top, #F4F4F4, #C1C1C1);
                  /* Opera 11.10+ */ background: -o-linear-gradient(top, #F4F4F4, #C1C1C1);
                  text-align:center;
                  font-weight:bold;
              }
               .tableborder .head > td {
                  color: #2D2B2B;
              }';
        return $xCss;
    }

    function setDataTableTransaksiAndroid($xlistidx, $xidTransaksi, $bhs) {
        $xCss = $this->getCSS();
        $xTableBooking = $this->getArrListBooking($xlistidx, $bhs);

        $xBufResult = "<!DOCTYPE html><html><style>" . $xCss . "</style>"
                . "<body> "
                . $this->getTransaksi($xidTransaksi, $xTableBooking, $bhs)
                . "</html></body>";

        return $xBufResult;
    }

    function getlistTransaksiAndroidWeb() {
        $this->load->helper('json');
        $xIdMember = $_POST['idMember'];
        $bhs = $_POST['bhs'];
        $this->load->helper('form');
        $this->load->helper('common');

        $response = array();
        $this->load->model('modeltransaksi');
        $xQuery = $this->modeltransaksi->getListtransaksiByIdMember($xIdMember);
        $i = 1;
        foreach ($xQuery->result() as $row) {
            $xResult = $this->setDataTableTransaksiAndroid($row->idbooking, $row->idx, $bhs);
//            echo $xResult;
            $this->json_data['idx'] = $row->idx;
            $this->json_data['WebView'] = $xResult;
            array_push($response, $this->json_data);
        }

//        if (empty($response)) {
//            array_push($response, $this->json_data);
//        }
        echo json_encode($response);
    }

    function getlistTransaksiAndroidWebBelumBayar() {
        $this->load->helper('json');
        $xIdMember = $_POST['idMember'];
        $bhs = $_POST['bhs'];
        $this->load->helper('form');
        $this->load->helper('common');

        $response = array();
        $this->load->model('modeltransaksi');
        $xQuery = $this->modeltransaksi->getListtransaksiByIdMemberBelumBayar($xIdMember);
        $i = 1;
        foreach ($xQuery->result() as $row) {
            $xResult = $this->setDataTableTransaksiAndroid($row->idbooking, $row->idx, $bhs);
//            echo $xResult;
            $this->json_data['idx'] = $row->idx;
            $this->json_data['WebView'] = $xResult;
            array_push($response, $this->json_data);
        }

//        if (empty($response)) {
//            array_push($response, $this->json_data);
//        }
        echo json_encode($response);
    }

    function uploadfilefromandro() {
        $file_path = base_url() . "resource/uploaded/img/";
        $response = array();
        if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], './resource/uploaded/img/' . $_FILES['uploadfile']['name'])) {
            $this->json_data['status'] = "OK";
            array_push($response, $this->json_data);
        } else {
            $this->json_data['status'] = "NO";
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpankonfirmasi() {
        $this->load->helper('json');
        $this->load->model('modeltransaksi');
        $xidx = $_POST['edidx'];
        $xkonfirmasitext = $_POST['edketerangan'];
        $ximgkonfirmasi = $_POST['edNamaFile'];
        $this->modeltransaksi->updatetransaksi($xidx,$xkonfirmasitext,$ximgkonfirmasi);
        $this->json_data['status'] = "OK";
        $response = array();
        array_push($response, $this->json_data);
        echo json_encode($response);
    }

    function data_uri($fileuri) {
        $mime = 'image/jpeg';
        $imgsrc = base_url() . 'resource/imgbtn/ic_logo_usd.png';

        if (!empty($fileuri)) {
           $fileuri= str_replace(" ", "%20", $fileuri);
            $imgsrc = base_url().'resource/uploaded/img/' . $fileuri;
        }

        return $imgsrc;
    }
    
    function getKonfirmasiPembayaran(){
        $this->load->helper('json');
        $response = array();
        $xidx = $_POST['idx'];
        $this->load->model('modeltransaksi');
        $row = $this->modeltransaksi->getDetailtransaksi($xidx);
       if(!empty($row->idx)){         
          $this->json_data['konfirmasitext'] = $row->konfirmasitext;
          $this->json_data['imgkonfirmasi'] = $this->data_uri($row->imgkonfirmasi);
          array_push($response, $this->json_data);
       }
        echo json_encode($response);
    }
    
}


?>
