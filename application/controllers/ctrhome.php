<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');

class ctrhome extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function imageresize($width, $height, $source) {
        $this->load->library('image_lib');
        $xBufResult = '';

        $config = array();
        $config['source_image'] = $source;
        $config['dest_image'] = base_url() . $source;
        $config['width'] = $width;
        $config['height'] = $height;
        //$config['quality'] = 80;
        $config['maintain_ratio'] = true;
        $config['create_thumb'] = TRUE;
        //$this->load->library('image_lib');
        //$this->load->library('image_lib',$config);
        $this->image_lib->initialize($config);
        $this->image_lib->resize();

        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors() . base_url() . $config['source_image'];
        }
        //$this->image_lib->clear();

        $axfilegambar = explode('.', $source);
        //echo count($axfilegambar);
        $filegambar = $axfilegambar[0] . '_thumb.' . $axfilegambar[1];

//     $xBufResult .= '
//
//         <div>
//                <div id="brand-detail">
//                <a href="'.site_url('webviewer/indexl/'.$row->idbrand.'/2').'">
//                    <img src="'.base_url().'resource/uploaded/project/'.$xRowBrand->imagebrand.'" alt="'.$row->ProductItem.'" style="margin-top:-10px;height:25px;">
//                </a>
//                        </div>
//                          <div class="image" style="margin-top:15px;"><a href="'.site_url('webviewer/indexp/'.$row->idx).'"><img src="'.base_url().'resource/uploaded/project/'.$filegambar.'" alt="'.$row->ProductItem.'"></a></div>
//                            <div class="name"><a href="'.site_url('webviewer/indexp/'.$row->idx).'">'.$row->ProductItem.'</a></div>
//                            <div class="price"> Rp. '.number_format(@$row->harga,0,'',',').'</div>
//
//             </div>';
//     }
        return $filegambar;
    }

    function getimagesliding() {
        $this->load->model('modelimage');
        $query = $this->modelimage->getListimagegalerry(0, 5, '');

        $xBufResult = '';
        foreach ($query->result() as $row) {
            $xgambar = $this->imageresize(600, 253, 'resource/uploaded/' . $row->imgurl);
            $xBufResult .= '<img  src="' . base_url() . $xgambar . '" width="600" height="253" alt=""  />';
        }
        $xBufResult = '<div class="slideshow" >' . $xBufResult . '</div>';
        return $xBufResult;
    }

    function index($xidmenu = '11', $xidcontent = '', $xawal = 0, $xlimit = 3) {
        $data['idmenu'] = $xidmenu;
        $data['logo'] = $this->setviewdefault('20', '');
        $data['social'] = $this->setviewdefault('27', '');
        $data['xximgslide'] = $this->setviewdefault('21', '');
        //print_r($data['xximgslide']);
        $data['LinkWeb'] = $this->setviewdefault('22', '');
        $data['subdomain'] = $this->setviewdefault('23', '');
        if (empty($xidcontent) || ($xidmenu == '11')) {
            $data['beritautama'] = $this->setviewhome('24', '', $xawal, $xlimit);
            $data['isdetail'] = FALSE;
            //echo"bla ".$xawal;
        } elseif ($xidmenu == '17') {
            $data['beritautama'] = $this->settreearsip();
            $data['isdetail'] = TRUE;
        } elseif ($xidmenu == '18') {
            $data['beritautama'] = $this->setDetailFormcontacus();
            $data['isdetail'] = TRUE;
        } else {
            $data['beritautama'] = $this->setviewhome($xidmenu, $xidcontent, $xawal, $xlimit);
            $data['isdetail'] = TRUE;
        }

        $data['akutalitas'] = $this->setviewdefault('25', '');
        $data['kebijakan'] = $this->setviewdefault('26', '');
        $data['bawah1'] = $this->setviewdefault('28', '');
        $data['bawah2'] = $this->setviewdefault('29', '');
        $data['bawah3'] = $this->setviewhome('30', '');
        $data['menu'] = $this->getmenuview();
        $data['home'] = $this->setviewdefault($xidmenu, $xidcontent);
        $data['linkkecamatan'] = $this->setlinkkecamatan();

        $data['footer'] = $this->getfooter();
        $data['imgslide'] = $this->setviewslider($xidmenu, $xidcontent);
        $data['gallery'] = $this->setgallery($xidmenu, $xidcontent);
        $data['videogal'] = $this->setvideo($xidmenu, $xidcontent);
        $data['ticker'] = $this->setviewdefault('33', '');;
//        if(!empty ($xidxtrans)){
//           $data['posting'] = $this->getdetailtranslete($xidxtrans);
//           $data['home'] = '';
//        } else{
//         $data['posting'] = $this->getpost();
//        }
//        $data['news'] = $this->getnewsarticle();
//        $data['bank'] = $this->getbank();
//        
//        $data['office'] = $this->setviewoffice();
//        $data['noijin'] = $this->setviewnoijin();
//       // $data['copyright'] = $this->setviewtulisancopyright();
        $this->load->view('index', $data);
    }

    function search() {
        $data['idmenu'] = '18';
        //print_r($_POST);
        $xsearch = $_POST['search'];
        $data['logo'] = $this->setviewdefault('20', '');
        $data['social'] = $this->setviewdefault('27', '');
        $data['xximgslide'] = $this->setviewdefault('21', '');
        $data['LinkWeb'] = $this->setviewdefault('22', '');
        $data['subdomain'] = $this->setviewdefault('23', '');
        $data['beritautama'] = $this->settreearsip($xsearch);
        $data['isdetail'] = TRUE;
        $data['akutalitas'] = $this->setviewdefault('25', '');
        $data['kebijakan'] = $this->setviewdefault('26', '');
        $data['bawah1'] = $this->setviewdefault('28', '');
        $data['bawah2'] = $this->setviewdefault('29', '');
        $data['bawah3'] = $this->setviewhome('30', '');
        $data['menu'] = $this->getmenuview();

        $data['linkkecamatan'] = $this->setlinkkecamatan();

        $data['footer'] = $this->getfooter();


        $this->load->view('index', $data);
    }

    function setviewhome($xidmenu, $xidcontent, $xawal = 0, $xlimit = 100) {
        $this->load->model('modelcontent');
        $this->load->model('modelmenu');
        $this->load->helper('common');
        $rowmenu = $this->modelmenu->getDetailmenu($xidmenu);
        $this->load->library('pagination');
        if (!empty($xidcontent)) {
            $xQuery = $this->modelcontent->getListcontentdetail($xawal, $xlimit, $xidmenu, $xidcontent);
        } else {
            $xQuery = $this->modelcontent->getListcontent($xawal, $xlimit, $xidmenu);
        }

        $linkpagination = '';
        if ($xidmenu == '24') {
            $jmlrow = $this->modelcontent->getcountrowbyidmenu($xidmenu);
            $xicont = $xidcontent;
            if (empty($xidcontent))
                $xicont = 0;
            $config['base_url'] = base_url() . 'index.php/ctrhome/index/' . $xidmenu . '/' . $xicont . '';
            $config['total_rows'] = $jmlrow;
            $config['per_page'] = 3;
            $config['uri_segment'] = 5;
//            $config['full_tag_open'] = '<p>';
//           $config['full_tag_close'] = '</p>';        
//            $config['page'] =1;
//            $config['use_page_numbers'] = TRUE;
//        $config['uri_segment'] = 4;
            $this->pagination->initialize($config);
            $linkpagination = $this->pagination->create_links();

            //echo $linkpagination.'sdsadsad asdsad';
        }

        $xBufResult = array();
        $xarrbulan = getArrayBulan();
        $xarrhari = getArrayHari();
        $i = 0;

        foreach ($xQuery->result() as $row) {
            $hari = $xarrhari[$row->harike];
            $bulan = $xarrbulan[$row->bulanke];
            $tgl = $hari . ', ' . $row->tgl . ' ' . $bulan . ' ' . $row->tahun;
            $xArrayBug = array("â", "Å", "&rdquo;", "&ldquo;", "&amp;", "&nbsp;");
            //  echo base_url()."<br />";
            $xisi = str_replace('../../../', base_url(), str_replace($xArrayBug, ' ', stripslashes($row->isi)));
            //$xBufResult = $xisi;
            $xarraycontent = array();
            $xarraycontent['idmenu'] = $rowmenu->idmenu;
            $xarraycontent['judulmenu'] = $rowmenu->nmmenu;
            $xarraycontent['idcontent'] = $row->idx;
            $xarraycontent['judul'] = $row->judul;
            $xarraycontent['isiawal'] = $row->isiawal;
            $xarraycontent['isi'] = $xisi;
            $xarraycontent['image1'] = $row->image1;
            $xarraycontent['image2'] = $row->image2;
            $xarraycontent['image2'] = $row->image3;
            $xarraycontent['haritanggal'] = $tgl;
            $xarraycontent['isfromcontent'] = TRUE;
            $xarraycontent['linkpage'] = $linkpagination;

            $xBufResult[$i++] = $xarraycontent;
            unset($xarraycontent);
        }

        //print_r($xBufResult);
        return $xBufResult;
    }

    function setviewdefault($xidmenu, $xidcontent, $xawal = 0, $xlimit = 100) {
        $this->load->model('modelcontent');
        $this->load->model('modelmenu');
        $this->load->helper('common');
        $rowmenu = $this->modelmenu->getDetailmenu($xidmenu);
        $this->load->library('pagination');
        if (!empty($xidcontent)) {
            $xQuery = $this->modelcontent->getListcontentdetail($xawal, $xlimit, $xidmenu, $xidcontent);
        } else {
            $xQuery = $this->modelcontent->getListcontent($xawal, $xlimit, $xidmenu);
        }
        $xBufResult = array();
        $xarrbulan = getArrayBulan();
        $xarrhari = getArrayHari();
        $i = 0;

        foreach ($xQuery->result() as $row) {

            $hari = $xarrhari[$row->harike];
            $bulan = $xarrbulan[$row->bulanke];
            $tgl = $hari . ', ' . $row->tgl . ' ' . $bulan . ' ' . $row->tahun;

            $xArrayBug = array("â", "Å", "&rdquo;", "&ldquo;", "&amp;", "&nbsp;");
            //  echo base_url()."<br />";
            $xisi = str_replace('../../../', base_url(), str_replace($xArrayBug, ' ', stripslashes($row->isi)));
            //$xBufResult = $xisi;
            $xarraycontent = array();
            $xarraycontent['idmenu'] = $rowmenu->idmenu;
            $xarraycontent['judulmenu'] = $rowmenu->nmmenu;
            $xarraycontent['idcontent'] = $row->idx;
            $xarraycontent['judul'] = $row->judul;
            $xarraycontent['isiawal'] = $row->isiawal;
            $xarraycontent['isi'] = $xisi;
            $xarraycontent['image1'] = $row->image1;
            $xarraycontent['image2'] = $row->image2;
            $xarraycontent['image2'] = $row->image3;
            $xarraycontent['haritanggal'] = $tgl;

            $xBufResult[$i++] = $xarraycontent;
            unset($xarraycontent);
        }


        return $xBufResult;
    }

    function setviewslider($xidmenu, $xidcontent) {
        $this->load->model('modelcontent');
        if (!empty($xidcontent)) {
            $xQuery = $this->modelcontent->getListcontentdetail(0, 10, 21, $xidcontent);
        } else {
            $xQuery = $this->modelcontent->getListcontent(0, 10, 21);
        }
        $xBufResult = array();
        $i = 0;
        foreach ($xQuery->result() as $row) {
            $xArrayBug = array("â", "Å", "&rdquo;", "&ldquo;", "&amp;", "&nbsp;");
            $xisi = str_replace('../../../', base_url(), str_replace($xArrayBug, ' ', stripslashes($row->isi)));
            //$xBufResult = $xisi;
            $xarraycontent = array();
            $xarraycontent['idcontent'] = $row->idx;
            $xarraycontent['judul'] = $row->judul;
            $xarraycontent['isiawal'] = $row->isiawal;
            $xarraycontent['isi'] = $xisi;
            $xarraycontent['image1'] = $row->image1;
            $xarraycontent['image2'] = $row->image2;
            $xarraycontent['image2'] = $row->image3;
            $xBufResult[$i++] = $xarraycontent;
            unset($xarraycontent);
        }

        return $xBufResult;
    }

    function settreearsip($xsearch = '') {
        $this->load->model('modelcontent');
        $this->load->model('modelmenu');
        $querytahun = $this->modelcontent->getlistTahun($xsearch);

        $xbufresult = '<ul id="browser" class="filetree">';
        $i = 0;
        foreach ($querytahun->result() as $rowtahun) {
            if ($i == 0) {
                $xbufresult .= '<li><span class="folder">' . $rowtahun->tahun . '</span>';
            } else {
                $xbufresult .= '<li class="closed"> <span class="folder">' . $rowtahun->tahun . '</span>';
            }

            $querymenu = $this->modelcontent->getlistmenubytahun($rowtahun->tahun, $xsearch);
            $xbufresult .= '<ul>';
            foreach ($querymenu->result() as $rowmenu) {
                $rowmenu = $this->modelmenu->getDetailmenu($rowmenu->idmenu);
                $xbufresult .= '<li class="closed"> <span class="folder">' . $rowmenu->nmmenu . '</span>';
                $queryconten = $this->modelcontent->getlistcontentbytahunidmenu($rowtahun->tahun, $rowmenu->idmenu, $xsearch);
                $xbufresult .= '<ul>';
                foreach ($queryconten->result() as $rowcontent) {
                    $xbufresult .= '<li class="closed"> <span class="file"><div id="idfile"><a href="' . base_url() . 'index.php/ctrhome/index/' . $rowmenu->idmenu . '/' . $rowcontent->idx . '">' . $rowcontent->judul . '(' . $rowcontent->tanggal . ')' . '</a></div></span></li>';
                }
                $xbufresult .= '</ul>';
                $xbufresult .= '</li>';
            }
            $xbufresult .= '</ul>';
            $xbufresult .='</li>';
            $i++;
        }
        $xbufresult .= '</ul>';
        $rowmenu = $this->modelmenu->getDetailmenu('17');
        $xBufResult = array();
        $xarraycontent = array();
        $xarraycontent['idmenu'] = $rowmenu->idmenu;
        if (empty($xsearch)) {
            $xarraycontent['judulmenu'] = $rowmenu->nmmenu;
        } else {
            $xarraycontent['judulmenu'] = 'Hasil Pencarian : "<strong>' . $xsearch . '</strong>"';
        }
        $xarraycontent['isi'] = $xbufresult . '<script language="javascript" type="text/javascript">$("#browser").treeview();</script>';
        $xarraycontent['isfromcontent'] = FALSE;
        $xarraycontent[0] = $xarraycontent;
        return $xarraycontent;
    }
    
    function setgallery($xidmenu, $xidcontent) {
        $this->load->model('modelcontent');
        
         $xQuery = $this->modelcontent->getListcontent(0, 1000, 31);
        
        $xBufResult = array();
        $i = 0;
        $rowmenu = $this->modelmenu->getDetailmenu('31');
        foreach ($xQuery->result() as $row) {
            $xArrayBug = array("â", "Å", "&rdquo;", "&ldquo;", "&amp;", "&nbsp;");
            $xisi = str_replace('../../../', base_url(), str_replace($xArrayBug, ' ', stripslashes($row->isi)));
            //$xBufResult = $xisi;
            $xarraycontent = array();
            $xarraycontent['idcontent'] = $row->idx;
            $xarraycontent['judulmenu'] = $rowmenu->nmmenu;
            $xarraycontent['judul'] = $row->judul;
            $xarraycontent['isiawal'] = $row->isiawal;
            $xarraycontent['isi'] = $xisi;
            $xarraycontent['image1'] = $row->image1;
            $xarraycontent['image2'] = $row->image2;
            $xarraycontent['image2'] = $row->image3;
            $xBufResult[$i++] = $xarraycontent;
            unset($xarraycontent);
        }

        return $xBufResult;
    }
    function setvideo($xidmenu, $xidcontent) {
        $this->load->model('modelcontent');
        
         $xQuery = $this->modelcontent->getListcontent(0, 1000, 32);
        
        $xBufResult = array();
        $i = 0;
        $rowmenu = $this->modelmenu->getDetailmenu('32');
        foreach ($xQuery->result() as $row) {
            $xArrayBug = array("â", "Å", "&rdquo;", "&ldquo;", "&amp;", "&nbsp;");
            $xisi = str_replace('../../../', base_url(), str_replace($xArrayBug, ' ', stripslashes($row->isi)));
            //$xBufResult = $xisi;
            $xarraycontent = array();
            $xarraycontent['idcontent'] = $row->idx;
            $xarraycontent['judulmenu'] = $rowmenu->nmmenu;
            $xarraycontent['judul'] = $row->judul;
            $xarraycontent['isiawal'] = $row->isiawal;
            $xarraycontent['isi'] = $xisi;
            $xarraycontent['image1'] = $row->image1;
            $xarraycontent['image2'] = $row->image2;
            $xarraycontent['image2'] = $row->image3;
            $xBufResult[$i++] = $xarraycontent;
            unset($xarraycontent);
        }

        return $xBufResult;
    }
    
    function setlinkkecamatan() {
        $this->load->model('modelcontent');
        $xQuery = $this->modelcontent->getListcontent(0, 100, '22');
        $xbuffresult = array();
        $i = 0;
        foreach ($xQuery->result() as $row) {

            $xlink = array();
            $xlink['link'] = $row->isiawal;
            $xlink['kecamatan'] = $row->judul;
            $xbuffresult[$i++] = $xlink;
            unset($xlink);
        }
        //print_r($xbuffresult);
        return $xbuffresult;
    }

    function setDetailFormcontacus() {

        $this->load->helper('form');
        $this->load->helper('captcha');
        $xbufrandom = rand(100000, 999999);

        $vals = array(
            'word' => $xbufrandom,
            'img_path' => './resource/captcha/',
            'img_url' => base_url() . 'resource/captcha/',
            'font_path' => './path/to/fonts/texb.ttf',
            'img_width' => '150',
            'img_height' => 30,
            'expiration' => 7200
        );
        $cap = create_captcha($vals);

        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h1>Buku Tamu</h1>' . form_open_multipart('ctrhome/sendbukutamu', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="" />';
        $xBufResult .= '<input type="hidden" name="edcapcha" id="edcapcha" value="'.$xbufrandom.'" />';
        $xBufResult .= setForm('edNama', 'Pengunjung', form_input(getArrayObj('edNama', '', '300'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edemail', 'Email', form_input(getArrayObj('edemail', '', '300'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edisi', 'Pesan', form_textarea(getArrayObj('edisi', '', '300'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="capca">' . $cap['image'] . '</div><div class="spacer"></div>';
        $xBufResult .= setForm('edverified', 'Verifikasi', form_input(getArrayObj('edverified', '', '100')));
        $xBufResult .= '<div class="capca">' .form_submit('btSimpan', 'Kirim Buku Tamu', '').'</div>' . '</div><div class="spacer"></div>' . '<div class="spacer"></div>' . '<div class="garis"></div>';

        $xBufResultArr = array();
        $xarraycontent = array();
        $xarraycontent['idmenu'] = '18';
        $xarraycontent['judulmenu'] = 'Buku Tamu';
        $xarraycontent['isi'] = $xBufResult;
        $xarraycontent['isfromcontent'] = FALSE;
        $xBufResultArr[0] = $xarraycontent;
        return $xBufResultArr;
        //return $xBufResult;
    }
    
    function sendbukutamu(){
        $this->load->model('modelcontacus');
        $data['idmenu'] = '18';
        
        $xCapcha = $_POST['edcapcha'];
        $xNama = $_POST['edNama'];
        $xEmail = $_POST['edemail'];
        $xIsi = $_POST['edisi'];
        $xVerifikasi = $_POST['edverified'];
        
        if($xCapcha==$xVerifikasi){
          $xmessage = "Terimakasih Telah Mengisi Buku Tamu";    
          $this->modelcontacus->setInsertcontacus('0', $xNama, '', '', $xEmail, $xIsi, '');
        }else{
          $xmessage = "Mohon Maaf Pengisian Anda Tidak Valid";      
        }
        
        $xBufResultArr = array();
        $xarraycontent = array();
        $xarraycontent['idmenu'] = '18';
        $xarraycontent['judulmenu'] = 'Buku Tamu';
        $xarraycontent['isi'] = $xmessage;
        $xarraycontent['isfromcontent'] = FALSE;
        $xBufResultArr[0] = $xarraycontent;
        
        $data['logo'] = $this->setviewdefault('20', '');
        $data['social'] = $this->setviewdefault('27', '');
        $data['xximgslide'] = $this->setviewdefault('21', '');
        $data['LinkWeb'] = $this->setviewdefault('22', '');
        $data['subdomain'] = $this->setviewdefault('23', '');
        $data['beritautama'] = $xBufResultArr;
        $data['isdetail'] = TRUE;
        $data['akutalitas'] = $this->setviewdefault('25', '');
        $data['kebijakan'] = $this->setviewdefault('26', '');
        $data['bawah1'] = $this->setviewdefault('28', '');
        $data['bawah2'] = $this->setviewdefault('29', '');
        $data['bawah3'] = $this->setviewhome('30', '');
        $data['menu'] = $this->getmenuview();

        $data['linkkecamatan'] = $this->setlinkkecamatan();

        $data['footer'] = $this->getfooter();


        $this->load->view('index', $data);   
    }
    
    function getfooter() {
        $this->load->model('modelcontent');
        $xQuery = $this->modelcontent->getListcontent(0, 1, '12');
        $xBufResult = '';
        foreach ($xQuery->result() as $row) {

            $xArrayBug = array("â", "Å", "&rdquo;", "&ldquo;", "&amp;", "&nbsp;");
            $xisi = str_replace('../../../', base_url(), str_replace($xArrayBug, ' ', stripslashes($row->isiawal)));
//          $xBufResult = $xisi;
            $xBufResult .= $xisi;
        }
        return $xBufResult;
    }

    function getmenuview() {
        $this->load->model('modelmenu');
        $this->load->model('modelcontent');

        $xmenuarray = array();
        $xQuery = $this->modelmenu->getMenuView();
        $i = 0;
        foreach ($xQuery->result() as $row) {
            $rowcontent = $this->modelcontent->getLastIndexcontentbyidmenu($row->idmenu);
            $xmenudetail = array();
            $xmenudetail['idmenu'] = $row->idmenu;
            $xmenudetail['nmmenu'] = $row->nmmenu;
            if (!empty($rowcontent->idx)) {
                $xmenudetail['idcontent'] = $rowcontent->idx;
            } else {
                $xmenudetail['idcontent'] = 'x';
            }
            $xmenuarray[$i++] = $xmenudetail;
            unset($xmenudetail);
        }
        return $xmenuarray;
    }

    function sendmail() {
        $this->load->helper('json');
        $this->load->model('modelumum');
        $row = $this->modelumum->getDetailumum('1');
        $edProduk = $_POST['edProduk'];
        $edNama = $_POST['edNama'];
        $edAlamat = $_POST['edAlamat'];
        $edNoTelpon = $_POST['edNoTelpon'];
        $edEmail = $_POST['edEmail'];
        $edMessage = $_POST['edMessage'];


        $xMessage = $edProduk . '</br >' .
                $edNama . '</br >' .
                $edAlamat . '</br >' .
                $edNoTelpon . '</br >' .
                $edEmail . '</br >' .
                $edMessage . '</br >';

        //echo $this->email->print_debugger();
        $this->sendMessagetoowner($edMessage, $edEmail, 'kurniawanpujianto@yahoo.com', $edNama);
        //$this->sendMessagetoPengunjung($edMessage, $row->diskripsi,$edEmail , $edNama);
    }

    function sendMessagetoowner($edMessage, $sender, $reception, $edNama) {
        $email_config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => '465',
            'smtp_user' => 'diarez77@gmail.com',
            'smtp_pass' => '',
            'mailtype' => 'html',
            'starttls' => true,
            'newline' => "\r\n"
        );

        $this->load->library('email');
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);
        $this->email->from($sender, $edNama);
        $this->email->to($reception);
        $this->email->subject('Pemesanan Produk');
        $this->email->message($edMessage);
        $this->email->send();
        //   mail('kurniawanpujianto@yahoo.com', 'Pemesanan Produk', 'testtttt');
        //  echo 'sended';
    }

    function sendMessagetoPengunjung($edMessage, $sender, $reception, $edNama) {
        $this->load->library('email');
        $this->email->from($sender, $sender);
        $this->email->to($reception);
        $this->email->subject('Pemesanan Produk');
        $this->email->message($edMessage);
        $this->email->send();
    }

}

?>
