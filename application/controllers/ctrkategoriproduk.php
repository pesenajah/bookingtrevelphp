<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : kategoriproduk  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrkategoriproduk extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformkategoriproduk('0', $xAwal);
    }

    function createformkategoriproduk($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxkategoriproduk.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormkategoriproduk($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormkategoriproduk($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Kategori Produk</h3><div class="garis"></div>' . form_open_multipart('ctrkategoriproduk/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edKategori', 'Kategori', form_input(getArrayObj('edKategori', '', '500'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpankategoriproduk();"') . form_button('btNew', 'new', 'onclick="doClearkategoriproduk();"') . '<div class="spacer"></div><div id="tabledatakategoriproduk">' . $this->getlistkategoriproduk(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistkategoriproduk($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('No', '', 'width=30%') .
                tbaddcell('Kategori', '', 'width=60%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelkategoriproduk');
        $xQuery = $this->modelkategoriproduk->getListkategoriproduk($xAwal, $xLimit, $xSearch);
        $ino = 1 + $xAwal;
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditkategoriproduk(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapuskategoriproduk(\'' . $row->idx . '\',\'' . substr($row->Kategori, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($ino++) .
                    tbaddcell($row->Kategori) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchkategoriproduk(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchkategoriproduk(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchkategoriproduk(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10%') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =2'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editreckategoriproduk() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelkategoriproduk');
        $row = $this->modelkategoriproduk->getDetailkategoriproduk($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['Kategori'] = $row->Kategori;
        echo json_encode($this->json_data);
    }

    function deletetablekategoriproduk() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelkategoriproduk');
        $this->modelkategoriproduk->setDeletekategoriproduk($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchkategoriproduk() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatakategoriproduk'] = $this->getlistkategoriproduk($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpankategoriproduk() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xKategori = $_POST['edKategori'];
        $this->load->model('modelkategoriproduk');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelkategoriproduk->setUpdatekategoriproduk($xidx, $xKategori);
            } else {
                $xStr = $this->modelkategoriproduk->setInsertkategoriproduk($xidx, $xKategori);
            }
        }
        echo json_encode(null);
    }

}

?>