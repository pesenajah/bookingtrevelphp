<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ctrlaporanpromo
 *
 * @author admindatakreasi
 */
class ctrlaporanpromo extends CI_Controller {

    function __construct() {
        parent :: __construct();
    }

    function index() {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '';
        $xAddJs.= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxlaporanpromo.js"></script>';
        if (!empty($idpegawai)) {
            echo $this->modelgetmenu->SetViewAdmin($this->createviewlap(), '<div class="spacer"></div><div id="browsepdf"></div>', '', $xAddJs, '');
        } else {
            die("Anda belum Login");
        }
    }

    function createviewlap() {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelvoucher');
        $xBufResult = '';
        $xBufResult .= '<div id="stylized" class="myform">';
        $xBufResult .= '<h3>Laporan Promo</h3><div class="spacer"></div><div class="garis"></div>';
//        $xBufResult .= setForm('edTglMulai', 'Tanggal Awal', form_input(getArrayObj('edTglMulai', '', '200')));
//        $xBufResult .= setForm('edTglSelesai', 'Tanggal Akhir', form_input(getArrayObj('edTglSelesai', '', '200'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edidvoucher', 'Kode Voucher', form_dropdown('edidvoucher', $this->modelvoucher->getArrayListvoucherall(), '', 'id="edidvoucher" style = "width:150px"'));
        $xArrstatus['-'] = 'Semua';
        $xArrstatus['0'] = 'Belum Berlaku';
        $xArrstatus['1'] = 'Masih Berlaku';
        $xArrstatus['2'] = 'Sudah Tidak Berlaku';
        $xBufResult .= setForm('edisberlaku', 'Status Promo', form_dropdown('edisberlaku', $xArrstatus, '', 'id="edisberlaku" style = "width:150px"')) . '<div class="spacer"></div>';
        $xBufResult .= form_button('TampilData', '<span class="btnright">Tampil Data</span>', 'onclick="doshowlaporanvoucher();" class="btn"');
        $xBufResult .= form_button('SendToPdf', '<span class="btnright">Send To Pdf</span>', 'onclick="setpdflaporanvoucher();" class="btn"');
        $xBufResult .= form_button('ExportToExcel', '<span class="btnright">Export To Excel</span>', 'onclick="exportkeexcel();" class="btn"');
        $xBufResult .= '<div class="spacer"></div>' . '<div class="garis"></div>';
        $xBufResult .= '<div id="gbloader"><div>Proses Membaca Data </div> <img src="' . base_url() . 'resource/imgbtn/ajax-loader.gif"></div>';
        $xBufResult .= '<div id="tabledata" name="tabledata">';
        $xBufResult .= '<div id="tblaporanvoucher" name="tblaporanvoucher">';
        $xBufResult .= '</div>';
        $xBufResult .= '</div>';
        return $xBufResult;
    }

    function showtbdt($xisberlaku, $date_awal = '', $date_akhir = '') {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelvoucher');
        //$this->load->model('modelproduk');
        $xBufResult = tbaddrow(
                tbaddcell('<font color="#000">No</font>', '', 'width=5%') .
//                tbaddcell('<font color="#000">Kode Voucher</font>', '', 'width=10%') .
//                tbaddcell('<font color="#000">Nominal</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Penjelasan Voucher</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Tanggal Berlaku Dari</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Tanggal Berlaku Sampai</font>', '', 'width=10%') .
                //tbaddcell('<font color="#000">Produk</font>', '', 'width=10%') .
//                tbaddcell('<font color="#000">Jumlah Max Pengguna</font>', '', 'width=5%') .
                tbaddcell('<font color="#000">Status Promo</font>', '', 'width=5%'), 'background:#ffffff;', TRUE);
        $xResult = $this->modelvoucher->getListpromobydatestatus($xisberlaku, $date_awal, $date_akhir);
        $no = 1;
        $xBufResultdata = '';
        foreach ($xResult->result() as $row) {
            $status = '';
            $xisberlaku = $this->modelvoucher->cekStatusPromo($row->idx);
//            echo $xisberlaku;
            if ($xisberlaku === '0') {
                $status = 'Belum Berlaku';
            } elseif ($xisberlaku === '1') {
                $status = 'Masih Berlaku';
            } elseif ($xisberlaku === '2') {
                $status = 'Sudah Tidak Berlaku';
            }
            $xBufResultdata .= tbaddrow(tbaddcell($no++) .
//                    tbaddcell($row->voucher) .
//                    tbaddcell($row->nominal) .
                    tbaddcell($row->penjelasan) .
                    tbaddcell(datetomysql($row->tglberlakudari)) .
                    tbaddcell(datetomysql($row->tglberlakusampai)) .
                    //  tbaddcell(@$this->modelproduk->getDetailproduk(@$row->idproduk)->JudulProduk) .
//                    tbaddcell($row->jumlahmaxpengguna) .
                    tbaddcell($status));
        }
        if ($xBufResultdata == '') {
            $xBufResult .= tbaddrow(tbaddcell("TIDAK ADA DATA", '', 'align="center" colspan="8"'));
        } else {
            $xBufResult .= $xBufResultdata;
        }
        $xBufResult = tablegrid($xBufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xBufResult . '</div>';
    }

    function setpdf() {
        $this->load->helper('html');
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('topdf');

        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xisberlaku = $_POST['edisberlaku'];

        $html = '<html>
				<header>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" . '
				</header>
				<body>
					<p>
						<div id="report">
						<div id="tabledata">
							' . $this->showtbdt($xisberlaku, $date_awal, $date_akhir) . '
						</div>
						</div>
					</p>
				</body>
			</html>';

        $idpegawai = $this->session->userdata('idpegawai');
//        die($html);
        pdf_create($html, 'laporan_promo_' . $idpegawai);
        $xbufresult = '<object data="' . base_url() . 'resource/pdf/laporan_promo_' . $idpegawai . '.pdf" type="application/pdf" width="1200px" height = "600px" type="left:-15px;" >
                          </object>';
        $this->json_data['data'] = $xbufresult;
        echo json_encode($this->json_data);
    }

    function carilaporan_byrange() {
        $this->load->helper('common');
        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xisberlaku = $_POST['edisberlaku'];

        $strHTML = $this->showtbdt($xisberlaku, $date_awal, $date_akhir);
        $this->load->helper('json');
        $this->json_data['tblaporanvoucher'] = $strHTML;
        echo json_encode($this->json_data);
    }

    function exportkeexcel($xisberlaku, $xidvoucher, $date_awal, $date_akhir) {
        $this->load->helper('html');
        $this->load->helper('common');
        $nmfile = 'laporanpromo';
        $date_awal = datetomysql($date_awal);
        $date_akhir = datetomysql($date_akhir);
        $xhtml = $this->showtbdt($xisberlaku, $date_awal, $date_akhir);
        $xbufresult = header("Content-type: application/octet-stream") . "\n" .
                header("Content-Disposition: attachment; filename=" . $nmfile . ".xls") . "\n" .
                header("Pragma: no-cache") . "\n" .
                header("Expires: 0");
        $this->load->helper('html');
        $xbufresult .= '<html><head><style type=\'text/css\'>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" .
                '</head><body>' . $xhtml . '</body></html>';
        echo $xbufresult;
    }

}
