<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : booking  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrbooking extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformbooking('0', $xAwal);
    }

    function createformbooking($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxbooking.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormbooking($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormbooking($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $this->load->model('modeljenispembayaran');
        $this->load->model('modelkategoriproduk');
        $this->load->model('modeldetailproduk');
        $this->load->model('modelproduk');
        $xBufResult = '<div id="stylized" class="myform"><h3>Booking</h3><div class="garis"></div>' . form_open_multipart('ctrbooking/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidproduk" id="edidproduk" value="0" />';
        $xBufResult .= '<input type="hidden" name="ediddetailproduk" id="ediddetailproduk" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidkategoriproduk" id="edidkategoriproduk" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidmember" id="edidmember" value="0" />';
        $xBufResult .= setForm('edtglbooking', 'Tanggal Booking', form_input(getArrayObj('edtglbooking', '', '100')));
        $xBufResult .= setForm('edjambooking', 'Jam Booking', form_input(getArrayObj('edjambooking', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('ednamaproduk', 'Produk', form_input(getArrayObj('ednamaproduk', '', '400'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('ednamadetailproduk', 'Detail Produk', form_input(getArrayObj('ednamadetailproduk', '', '400'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('ednamakategoriproduk', 'Kategori Produk', form_input(getArrayObj('ednamakategoriproduk', '', '400'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('ednamamember', 'Member', form_input(getArrayObj('ednamamember', '', '400'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglperuntukandari', 'Tanggal Peruntukan Dari', form_input(getArrayObj('edtglperuntukandari', '', '100')));
        $xBufResult .= setForm('edtglperuntukansampai', 'Tanggal Peruntukan Sampai', form_input(getArrayObj('edtglperuntukansampai', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edjmldewasa', 'Jumlah Pengguna', form_input(getArrayObj('edjmldewasa', '', '50'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edjmlanak', 'Jumlah Anak', form_input(getArrayObj('edjmlanak', '', '50'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edjmlhewan', 'Jumlah Hewan', form_input(getArrayObj('edjmlhewan', '', '50'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edketerangantambahn', 'Keterangan Tambahan', form_textarea(getArrayObj('edketerangantambahn', '', '600'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edjmltransfer', 'Jumlah Transfer', form_input(getArrayObj('edjmltransfer', '', '150'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edidjenispembayaran', 'Jenis Pembayaran', form_dropdown('edidjenispembayaran', $this->modeljenispembayaran->getArrayListjenispembayaran(), '', 'id="edidjenispembayaran" style = "width:150px"')) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('ednomorkartu', 'Nomor Kartu', form_input(getArrayObj('ednomorkartu', '', '200'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtgltransfer', 'Tanggal Transfer', form_input(getArrayObj('edtgltransfer', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglinsert', 'tglinsert', form_input(getArrayObj('edtglinsert', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglupdate', 'tglupdate', form_input(getArrayObj('edtglupdate', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanbooking();"') . form_button('btNew', 'new', 'onclick="doClearbooking();"') . '<div class="spacer"></div><div id="tabledatabooking">' . $this->getlistbooking(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistbooking($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=5%') .
                tbaddcell('Tanggal Booking', '', 'width=10%') .
                tbaddcell('Produk', '', 'width=15%') .
                tbaddcell('Detail Produk', '', 'width=15%') .
                tbaddcell('Kategori Produk', '', 'width=10%') .
                tbaddcell('Member', '', 'width=15%') .
                tbaddcell('Tanggal Peruntukan Dari', '', 'width=10%') .
                tbaddcell('Tanggal Peruntukan Sampai', '', 'width=10%') .
                tbaddcell('Status Booking', '', 'width=10%') .
//                tbaddcell('jmldewasa', '', 'width=10%') .
//                tbaddcell('jmlanak', '', 'width=10%') .
//                tbaddcell('jmlhewan', '', 'width=10%') .
//                tbaddcell('keterangantambahn', '', 'width=10%') .
//                tbaddcell('jmltransfer', '', 'width=10%') .
//                tbaddcell('idjenispembayaran', '', 'width=10%') .
//                tbaddcell('nomorkartu', '', 'width=10%') .
//                tbaddcell('tgltransfer', '', 'width=10%') .
//                tbaddcell('tglinsert', '', 'width=10%') .
//                tbaddcell('tglupdate', '', 'width=10%') .
                tbaddcell('Aksi', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelbooking');
        $this->load->model('modelkategoriproduk');
        $this->load->model('modeldetailproduk');
        $this->load->model('modelproduk');
        $this->load->model('modelmember');
        $xQuery = $this->modelbooking->getListbooking($xAwal, $xLimit, $xSearch);
        $no = 1;
        foreach ($xQuery->result() as $row) {
            $arraytgljambooking = explode(' ', $row->tglbooking);
            $tgljambooking = datetomysql($arraytgljambooking[0]) . ' ' . $arraytgljambooking[1];

            $tglperuntukandari = datetomysql($row->tglperuntukandari);

            $tglperuntukansampai = datetomysql($row->tglperuntukansampai);

            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" title="Edit Data" onclick = "doeditbooking(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
//            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusbooking(\'' . $row->idx . '\',\'' . substr($row->tglbooking, 0, 20) . '\');" style="border:none;">';
            $xButtonBatal = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Batal Booking" title="Batal Booking" onclick = "dobatalbooking(\'' . $row->idx . '\',\'' . $row->tglbooking . '\');" width="20px" height="20px" style="border:none;">';
            $xButtonLanjut = '<img src="' . base_url() . 'resource/imgbtn/correct.jpg" alt="Lanjut Booking" title="Lanjut Booking" onclick = "dolanjutbooking(\'' . $row->idx . '\',\'' . $row->tglbooking . '\');" width="20px" height="20px" style="border:none;">';

            if ($row->status === '0') {
                $xthemestatus = "";
                $xstatus = "Booking belum diproses";
                $xbuttonaksi = $xButtonLanjut . $xButtonBatal;
            } elseif ($row->status === '1') {
                $xthemestatus = "background:yellow";
                $xstatus = "Booking belum dibayar";
                $xbuttonaksi = $xButtonBatal;
            } elseif ($row->status === '2') {
                $xthemestatus = "background:red";
                $xstatus = "Booking batal";
                $xbuttonaksi = $xButtonLanjut;
            } else {
                $xthemestatus = "background:lightgreen";
                $xstatus = "Booking sudah dibayar";
                $xbuttonaksi = "";
            }

            $xbufResult .= tbaddrow(tbaddcell($no++) .
                    tbaddcell($tgljambooking) .
                    tbaddcell($this->modelproduk->getDetailproduk($row->idproduk)->JudulProduk) .
                    tbaddcell($this->modeldetailproduk->getDetaildetailproduk($row->iddetailproduk)->juduldetailproduk) .
                    tbaddcell(@$this->modelkategoriproduk->getDetailkategoriproduk($row->idkategoriproduk)->Kategori) .
                    tbaddcell($this->modelmember->getDetailmember($row->idmember)->Nama) .
                    tbaddcell($tglperuntukandari) .
                    tbaddcell($tglperuntukansampai) .
                    tbaddcell($xstatus) .
//                    tbaddcell($row->jmldewasa) .
//                    tbaddcell($row->jmlanak) .
//                    tbaddcell($row->jmlhewan) .
//                    tbaddcell($row->keterangantambahn) .
//                    tbaddcell($row->jmltransfer) .
//                    tbaddcell($row->idjenispembayaran) .
//                    tbaddcell($row->nomorkartu) .
//                    tbaddcell($row->tgltransfer) .
//                    tbaddcell($row->tglinsert) .
//                    tbaddcell($row->tglupdate) .
                    tbaddcell($xButtonEdit . $xbuttonaksi), $xthemestatus);
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchbooking(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchbooking(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchbooking(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecbooking() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelbooking');
        $this->load->model('modelproduk');
        $this->load->model('modeldetailproduk');
        $this->load->model('modelkategoriproduk');
        $this->load->model('modelmember');
        $row = $this->modelbooking->getDetailbooking($xIdEdit);
        $rowproduk = $this->modelproduk->getDetailproduk($row->idproduk);
        $rowdetailproduk = $this->modeldetailproduk->getDetaildetailproduk($row->iddetailproduk);
        $rowkategoriproduk = $this->modelkategoriproduk->getDetailkategoriproduk($row->idkategoriproduk);
        $rowmember = $this->modelmember->getDetailmember($row->idmember);
        $this->load->helper('json');
        $this->load->helper('common');
        $this->json_data['idx'] = $row->idx;

        $arraytgljambooking = explode(' ', $row->tglbooking);
        $this->json_data['tglbooking'] = datetomysql($arraytgljambooking[0]);
        $this->json_data['jambooking'] = $arraytgljambooking[1];

        $this->json_data['tglperuntukandari'] = datetomysql($row->tglperuntukandari);

        $this->json_data['tglperuntukansampai'] = datetomysql($row->tglperuntukansampai);

        $this->json_data['idproduk'] = $row->idproduk;
        $this->json_data['iddetailproduk'] = $row->iddetailproduk;
        $this->json_data['idkategoriproduk'] = $row->idkategoriproduk;
        $this->json_data['idmember'] = $row->idmember;

        $this->json_data['namaproduk'] = $rowproduk->JudulProduk;
        $this->json_data['namadetailproduk'] = $rowdetailproduk->juduldetailproduk;
        $this->json_data['namakategoriproduk'] = $rowkategoriproduk->Kategori;
        $this->json_data['namamember'] = $rowmember->Nama;

        $this->json_data['jmldewasa'] = $row->jmldewasa;
        $this->json_data['jmlanak'] = $row->jmlanak;
        $this->json_data['jmlhewan'] = $row->jmlhewan;
        $this->json_data['keterangantambahn'] = $row->keterangantambahn;
        $this->json_data['jmltransfer'] = $row->jmltransfer;
        $this->json_data['idjenispembayaran'] = $row->idjenispembayaran;
        $this->json_data['nomorkartu'] = $row->nomorkartu;
        $this->json_data['tgltransfer'] = datetomysql($row->tgltransfer);
        $this->json_data['tglinsert'] = $row->tglinsert;
        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['status'] = $row->status;
        echo json_encode($this->json_data);
    }

    function deletetablebooking() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelbooking');
        $this->modelbooking->setDeletebooking($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function setBatalbooking() {
        $xidx = $_POST['edidx'];
        $this->load->model('modelbooking');
        $xstatus = '2';
        $this->modelbooking->setUpdateProsesbooking($xidx, $xstatus);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function setLanjutbooking() {
        $xidx = $_POST['edidx'];
        $this->load->model('modelbooking');
        $xstatus = '1';
        $this->modelbooking->setUpdateProsesbooking($xidx, $xstatus);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchbooking() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatabooking'] = $this->getlistbooking($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpanbooking() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xtglbooking = datetomysql($_POST['edtglbooking']);
        $xjambooking = $_POST['edjambooking'];
        $xtgljambooking = $xtglbooking . ' ' . $xjambooking;

        $xtglperuntukandari = datetomysql($_POST['edtglperuntukandari']);

        $xtglperuntukansampai = datetomysql($_POST['edtglperuntukansampai']);

        $xidproduk = $_POST['edidproduk'];
        $xiddetailproduk = $_POST['ediddetailproduk'];
        $xidkategoriproduk = $_POST['edidkategoriproduk'];
        $xidmember = $_POST['edidmember'];
        $xjmldewasa = $_POST['edjmldewasa'];
        $xjmlanak = $_POST['edjmlanak'];
        $xjmlhewan = $_POST['edjmlhewan'];
        $xketerangantambahn = $_POST['edketerangantambahn'];
        $xjmltransfer = $_POST['edjmltransfer'];
        $xidjenispembayaran = $_POST['edidjenispembayaran'];
        $xnomorkartu = $_POST['ednomorkartu'];
        $xtgltransfer = datetomysql($_POST['edtgltransfer']);
        $xtglinsert = $_POST['edtglinsert'];
        $xtglupdate = $_POST['edtglupdate'];
        $xstatus = $_POST['edstatus'];
        $this->load->model('modelbooking');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelbooking->setUpdatebooking($xidx, $xtgljambooking, $xidproduk, $xiddetailproduk, $xidkategoriproduk, $xidmember, $xtglperuntukandari, $xtglperuntukansampai, $xjmldewasa, $xjmlanak, $xjmlhewan, $xketerangantambahn, $xjmltransfer, $xidjenispembayaran, $xnomorkartu, $xtgltransfer, $xstatus, $xtglinsert, $xtglupdate);
            } else {
                $xStr = $this->modelbooking->setInsertbooking($xidx, $xtgljambooking, $xidproduk, $xiddetailproduk, $xidkategoriproduk, $xidmember, $xtglperuntukandari, $xtglperuntukansampai, $xjmldewasa, $xjmlanak, $xjmlhewan, $xketerangantambahn, $xjmltransfer, $xidjenispembayaran, $xnomorkartu, $xtgltransfer, $xstatus, $xtglinsert, $xtglupdate);
            }
        }
        echo json_encode(null);
    }

    function getlistbookingAndroid() {
        $this->load->helper('json');
        $xidMember = $_POST['idMember'];

        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['tglbooking'] = "";
        $this->json_data['idproduk'] = "";
        $this->json_data['iddetailproduk'] = "";
        $this->json_data['idkategoriproduk'] = "";
        $this->json_data['idmember'] = "";
        $this->json_data['tglperuntukandari'] = "";
        $this->json_data['tglperuntukansampai'] = "";
        $this->json_data['jmldewasa'] = "";
        $this->json_data['jmlanak'] = "";
        $this->json_data['jmlhewan'] = "";
        $this->json_data['keterangantambahn'] = "";
        $this->json_data['jmltransfer'] = "";
        $this->json_data['idjenispembayaran'] = "";
        $this->json_data['nomorkartu'] = "";
        $this->json_data['tgltransfer'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['isFinal'] = "";
        $this->json_data['tglFinal'] = "";
        $this->json_data['isSubmit'] = "";
        $this->json_data['tglSubmit'] = "";
        $response = array();
        $this->load->model('modelbooking');
        $xQuery = $this->modelbooking->getListbooking($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['tglbooking'] = $row->tglbooking;
            $this->json_data['idproduk'] = $row->idproduk;
            $this->json_data['iddetailproduk'] = $row->iddetailproduk;
            $this->json_data['idkategoriproduk'] = $row->idkategoriproduk;
            $this->json_data['idmember'] = $row->idmember;
            $this->json_data['tglperuntukandari'] = datetomysql($row->tglperuntukandari);
            $this->json_data['tglperuntukansampai'] = datetomysql($row->tglperuntukansampai);
            $this->json_data['jmldewasa'] = $row->jmldewasa;
            $this->json_data['jmlanak'] = $row->jmlanak;
            $this->json_data['jmlhewan'] = $row->jmlhewan;
            $this->json_data['keterangantambahn'] = $row->keterangantambahn;
            $this->json_data['jmltransfer'] = $row->jmltransfer;
            $this->json_data['idjenispembayaran'] = $row->idjenispembayaran;
            $this->json_data['nomorkartu'] = $row->nomorkartu;
            $this->json_data['tgltransfer'] = $row->tgltransfer;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['tglupdate'] = $row->tglupdate;
            $this->json_data['isFinal'] = $row->isFinal;
            $this->json_data['tglFinal'] = $row->tglFinal;
            $this->json_data['isSubmit'] = $row->isSubmit;
            $this->json_data['tglSubmit'] = $row->tglSubmit;
            ;
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($this->json_data);
    }

    function simpanbookingAndroid() {
        $this->load->helper('common');
        $xidx = $_POST['edidx'];
        $xtglbooking = $_POST['edtglbooking'];
        $xidproduk = $_POST['edidproduk'];
        $xiddetailproduk = $_POST['ediddetailproduk'];
        $xidkategoriproduk = $_POST['edidkategoriproduk'];
        $xidmember = $_POST['edidmember'];
        $xtglperuntukandari = $_POST['edtglperuntukandari'];
        $xtglperuntukansampai = $_POST['edtglperuntukansampai'];
        $xjmldewasa = $_POST['edjmldewasa'];
        $xjmlanak = $_POST['edjmlanak'];
        $xjmlhewan = $_POST['edjmlhewan'];
        $xketerangantambahn = $_POST['edketerangantambahn'];
        $xjmltransfer = $_POST['edjmltransfer'];
        $xidjenispembayaran = $_POST['edidjenispembayaran'];
        $xnomorkartu = $_POST['ednomorkartu'];
        $xtgltransfer = $_POST['edtgltransfer'];
        $xtglinsert = $_POST['edtglinsert'];
        $xtglupdate = $_POST['edtglupdate'];
//        $xisFinal = $_POST['edisFinal'];
//        $xtglFinal = $_POST['edtglFinal'];
//        $xisSubmit = $_POST['edisSubmit'];
//        $xtglSubmit = $_POST['edtglSubmit'];
//        $xidx = '17';
//        $xtglbooking = '2016-11-02 15:53:25'; 
//        $xidproduk = '1';
//        $xiddetailproduk = '81';
//        $xidkategoriproduk = '1';
//        $xidmember = '30';
//        $xtglperuntukandari = '03-11-2016'; 
//        $xtglperuntukansampai = '05-11-2016';
//        $xjmldewasa = '3';
//        $xjmlanak = '0';
//        $xjmlhewan = '0';
//        $xketerangantambahn = 'test'; 
//        $xjmltransfer = '0.00';
//        $xidjenispembayaran = '0';
//        $xnomorkartu = 'null';
//        $xtgltransfer = '0000-00-00';
//        $xtglinsert = '2016-11-02 15:53:25';
//        $xtglupdate = '2016-11-02 15:53:25';

        $this->load->model('modeldetailproduk');
        $rowdetailproduk = $this->modeldetailproduk->getDetaildetailproduk($xiddetailproduk);

        $this->load->model('modelproduk');
        $rowproduk = $this->modelproduk->getDetailproduk($xidproduk);
        $xidkategoriproduk = @$rowproduk->idKategoriProduk;

        $xharga = @$rowdetailproduk->rate;
        $xhargadiscount = @$rowdetailproduk->ratediscount + 0;

        $this->load->helper('json');
        $this->load->model('modelbooking');
        $response = array();
        if ($xidx != '0') {
            $this->modelbooking->setUpdatebooking($xidx, $xtglbooking, $xidproduk, $xiddetailproduk, $xidkategoriproduk, $xidmember, datetomysql($xtglperuntukandari), datetomysql($xtglperuntukansampai), $xjmldewasa, $xjmlanak, $xjmlhewan, $xketerangantambahn, $xjmltransfer, $xidjenispembayaran, $xnomorkartu, $xharga, $xhargadiscount);
        } else {
            $this->modelbooking->setInsertbooking($xidx, $xtglbooking, $xidproduk, $xiddetailproduk, $xidkategoriproduk, $xidmember, datetomysql($xtglperuntukandari), datetomysql($xtglperuntukansampai), $xjmldewasa, $xjmlanak, $xjmlhewan, $xketerangantambahn, $xjmltransfer, $xidjenispembayaran, $xnomorkartu, $xtgltransfer, $xharga, $xhargadiscount);
        }
        $row = $this->modelbooking->getLastIndexbooking();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['tglbooking'] = $row->tglbooking;
        $this->json_data['idproduk'] = $row->idproduk;
        $this->json_data['iddetailproduk'] = $row->iddetailproduk;
        $this->json_data['idkategoriproduk'] = @$row->idkategoriproduk;
        $this->json_data['idmember'] = $row->idmember;
        $this->json_data['tglperuntukandari'] = datetomysql($row->tglperuntukandari);
        $this->json_data['tglperuntukansampai'] = datetomysql($row->tglperuntukansampai);
        $this->json_data['jmldewasa'] = $row->jmldewasa;
        $this->json_data['jmlanak'] = $row->jmlanak;
        $this->json_data['jmlhewan'] = $row->jmlhewan;
        $this->json_data['keterangantambahn'] = $row->keterangantambahn;
        $this->json_data['jmltransfer'] = $row->jmltransfer;
        $this->json_data['idjenispembayaran'] = $row->idjenispembayaran;
        $this->json_data['nomorkartu'] = $row->nomorkartu;
        $this->json_data['tgltransfer'] = $row->tgltransfer;
        $this->json_data['tglinsert'] = $row->tglinsert;
        $this->json_data['tglupdate'] = $row->tglupdate;
//        $this->json_data['isFinal'] = $row->isFinal;
//        $this->json_data['tglFinal'] = $row->tglFinal;
//        $this->json_data['isSubmit'] = $row->isSubmit;
//        $this->json_data['tglSubmit'] = $row->tglSubmit;
        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function getBookingByIdMember() {
        //$xidproduk,$xiddetailproduk,$xidmember
        $this->load->helper('common');
        $xidproduk = $_POST['edidproduk'];
        $xiddetailproduk = $_POST['ediddetailproduk'];
        $xidmember = $_POST['edidmember'];
        $this->json_data['idx'] = "0";
        $this->json_data['tglbooking'] = "";
        $this->json_data['idproduk'] = "";
        $this->json_data['iddetailproduk'] = "";
        $this->json_data['idkategoriproduk'] = "";
        $this->json_data['idmember'] = "";
        $this->json_data['tglperuntukandari'] = "";
        $this->json_data['tglperuntukansampai'] = "";
        $this->json_data['jmldewasa'] = "";
        $this->json_data['jmlanak'] = "";
        $this->json_data['jmlhewan'] = "";
        $this->json_data['keterangantambahn'] = "";
        $this->json_data['jmltransfer'] = "";
        $this->json_data['idjenispembayaran'] = "";
        $this->json_data['nomorkartu'] = "";
        $this->json_data['tgltransfer'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['isFinal'] = "";
        $this->json_data['tglFinal'] = "";
        $this->json_data['isSubmit'] = "";
        $this->json_data['tglSubmit'] = "";
        $this->load->model('modelbooking');
        $response = array();
        $row = $this->modelbooking->getBookingDetailBookingByIdMember($xidmember, $xidproduk, $xiddetailproduk);
        if (!empty($row->idx)) {


            $this->json_data['idx'] = $row->idx;
            $this->json_data['tglbooking'] = $row->tglbooking;
            $this->json_data['idproduk'] = $row->idproduk;
            $this->json_data['iddetailproduk'] = $row->iddetailproduk;
            $this->json_data['idkategoriproduk'] = $row->idkategoriproduk;
            $this->json_data['idmember'] = $row->idmember;
            $this->json_data['tglperuntukandari'] = datetomysql($row->tglperuntukandari);
            $this->json_data['tglperuntukansampai'] = datetomysql($row->tglperuntukansampai);
            $this->json_data['jmldewasa'] = $row->jmldewasa;
            $this->json_data['jmlanak'] = $row->jmlanak;
            $this->json_data['jmlhewan'] = $row->jmlhewan;
            $this->json_data['keterangantambahn'] = $row->keterangantambahn;
            $this->json_data['jmltransfer'] = $row->jmltransfer;
            $this->json_data['idjenispembayaran'] = $row->idjenispembayaran;
            $this->json_data['nomorkartu'] = $row->nomorkartu;
            $this->json_data['tgltransfer'] = $row->tgltransfer;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['tglupdate'] = $row->tglupdate;

            array_push($response, $this->json_data);
        } else {
            array_push($response, $this->json_data);
        }
        //  print_r($response);
        echo json_encode($this->json_data);
    }

    function getlistbookingAndroidTampil() {
        $this->load->helper('json');
        $xidMember = $_POST['idMember'];

        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['JudulDetailProduk'] = "Tidak Ada Booking";
        $this->json_data['JudulProduk'] = "-";
        $this->json_data['txtHarga'] = "0";
        $this->json_data['subtotal'] = "0";

        $response = array();
        $this->load->model('modelbooking');
        $this->load->model('modelproduk');
        $this->load->model('modeldetailproduk');
        $xQuery = $this->modelbooking->getListbookingByIdMember($xidMember);
        $itotal = 0;
        foreach ($xQuery->result() as $row) {
            $rowproduk = $this->modelproduk->getDetailproduk($row->idproduk);
            $rowdetailproduk = $this->modeldetailproduk->getDetaildetailproduk($row->iddetailproduk);
            $this->json_data['idx'] = $row->idx;
            $this->json_data['JudulDetailProduk'] = @$rowdetailproduk->juduldetailproduk;
            $this->json_data['JudulProduk'] = @$rowproduk->JudulProduk;
            $this->json_data['RangeTgl'] = datetomysql(@$row->tglperuntukandari) . " S/D " . datetomysql(@$row->tglperuntukansampai);
            $this->json_data['JmlHari'] = @$row->jmlhari;
            $this->json_data['HrgPerHari'] = number_format(@$rowdetailproduk->ratediscount + 0, 0, ',', '.');
            $this->json_data['txtHarga'] = number_format((@$row->jmlhari + 0) * (@$rowdetailproduk->ratediscount + 0), 0, ',', '.');


            //<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">
            //<h4>A PHP Error was encountered</h4><p>Severity: Notice</p>
            //<p>Message:  Trying to get property of non-object</p><p>Filename: controllers/ctrbooking.php</p>
            //<p>Line Number: 503</p></div><
            //div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">
            //<h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Undefined property: stdClass::$ratediscount</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 504</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Trying to get property of non-object</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 503</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Undefined property: stdClass::$ratediscount</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 504</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Trying to get property of non-object</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 503</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Undefined property: stdClass::$ratediscount</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 504</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Trying to get property of non-object</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 503</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Undefined property: stdClass::$ratediscount</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 504</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Trying to get property of non-object</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 503</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Undefined property: stdClass::$ratediscount</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 504</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Trying to get property of non-object</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 503</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Undefined property: stdClass::$ratediscount</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 504</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Trying to get property of non-object</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 503</p></div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><h4>A PHP Error was encountered</h4><p>Severity: Notice</p><p>Message:  Undefined property: stdClass::$ratediscount</p><p>Filename: controllers/ctrbooking.php</p><p>Line Number: 504</p></div>{"idx":"12","JudulDetailProduk":"Hotel Nusa Penida","JudulProduk":null,"txtHarga":"0"}           
//            if($rowproduk->idKategoriProduk=="1"||$rowproduk->idKategoriProduk=="2"){ 
//              $this->json_data['txtHarga'] = "";
//            }else{
//               $this->json_data['txtHarga'] = ""; 
//            }
            $itotal += (@$row->jmlhari + 0) * (@$rowdetailproduk->ratediscount + 0);
            $this->json_data['subtotal'] = number_format($itotal, 0, ',', '.');
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }

        echo json_encode($response);
    }

    function getJumlahBooking(){
        $this->load->helper('json');
        $xidMember = $_POST['idmember'];
        $this->load->model('modelbooking');
        $jmlbooking = $this->modelbooking->getJumlahBooking($xidMember);  
        $this->json_data['jmlBooking'] = $jmlbooking;

        $response = array();
        array_push($response, $this->json_data);
        

        echo json_encode($response);
    }
}

?>
