<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : dataAPI  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrdataAPI extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformdataAPI('0', $xAwal);
    }

    function createformdataAPI($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxdataAPI.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormdataAPI($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormdataAPI($xidx) {
        $this->load->helper('form');
        $this->load->model('modeldataAPI');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>dataAPI</h3><div class="garis"></div>' . form_open_multipart('ctrdataAPI/inserttable', array('id' => 'form', 'name' => 'form')) . '<div class="spacer"></div>';
        $this->load->helper('common');
//        $xBufResult .= '<input type="hidden" name="linktoken" id="linktoken" value="' . getjsontoken() . '" />';
//        $xBufResult .= '<div id="dataAPI">Token : ' . getToken() . '</div><div class="garis"></div><div class="spacer"></div>';
//        $xBufResult .= '<div id="listCurrency">List Currency' . $this->getListCurrency() . '</div><div class="garis"></div><div class="spacer"></div>';
//        $xBufResult .= '<div id="listlanguage">List Language' . $this->getListLanguage() . '</div><div class="garis"></div><div class="spacer"></div>';
//        $xBufResult .= '<div id="listlanguage">List Country' . $this->getListCountry() . '</div><div class="garis"></div><div class="spacer"></div>';
//        $xBufResult .= '<div id="listlanguage">Transaction Policies' . $this->getPolicies() . '</div><div class="garis"></div><div class="spacer"></div>';
//        $xBufResult .= '<div id="listlanguage">List Hotel' . $this->getListHotel() . '</div><div class="garis"></div><div class="spacer"></div>';
//        $xBufResult .= '<div id="listairport">List Airport' . $this->getListAirport() . '</div><div class="garis"></div><div class="spacer"></div>';
//        $xBufResult .= '<div id="listairportpopulardestination">List Airport Popular Destination' . $this->getListAirportPopularDestination() . '</div><div class="garis"></div><div class="spacer"></div>';
//        $xBufResult .= '<div id="CheckFlightUpdated">Check Flight Updated' . $this->getCheckFlightUpdated() . '</div><div class="garis"></div><div class="spacer"></div>';
        $xBufResult .= '<div id="ListFlight">List Flight' . $this->getListFlight() . '</div><div class="garis"></div><div class="spacer"></div>';
        $xBufResult .= '<div id="addOrderFlight"></div><div class="garis"></div><div class="spacer"></div>';
        $xBufResult .= '<div id="listdetailhotel"></div>';
        $xBufResult .= '</div>';
        return $xBufResult;
    }

    function getListCurrency() {
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('id', '', 'width=10%') .
                tbaddcell('lang', '', 'width=10%') .
                tbaddcell('code', '', 'width=10%') .
                tbaddcell('name', '', 'width=10%'), '', TRUE);
        $this->load->model('modeldataAPI');
        $xArrList = $this->modeldataAPI->getAPIListCurrency(getToken());
        foreach ($xArrList as $row) {
            $xbufResult .= tbaddrow(tbaddcell($row->id) .
                    tbaddcell($row->lang) .
                    tbaddcell($row->code) .
                    tbaddcell($row->name));
        }
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function getListLanguage() {
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('code', '', 'width=10%') .
                tbaddcell('name_long', '', 'width=10%') .
                tbaddcell('name_short', '', 'width=10%'), '', TRUE);
        $this->load->model('modeldataAPI');
        $xArrList = $this->modeldataAPI->getAPIListLanguage(getToken());
        foreach ($xArrList as $row) {
            $xbufResult .= tbaddrow(tbaddcell($row->code) .
                    tbaddcell($row->name_long) .
                    tbaddcell($row->name_short));
        }
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
//        return $xArrList;
    }

    function getListCountry() {
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('country_id', '', 'width=10%') .
                tbaddcell('country_name', '', 'width=10%') .
                tbaddcell('country_areacode', '', 'width=10%'), '', TRUE);
        $this->load->model('modeldataAPI');
        $xArrList = $this->modeldataAPI->getAPIListCountry(getToken());
        foreach ($xArrList as $row) {
            $xbufResult .= tbaddrow(tbaddcell($row->country_id) .
                    tbaddcell($row->country_name) .
                    tbaddcell($row->country_areacode));
        }
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
//        return $xArrList;
    }

    function getPolicies() {
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('country_id', '', 'width=10%') .
                tbaddcell('country_name', '', 'width=10%') .
                tbaddcell('country_areacode', '', 'width=10%'), '', TRUE);
        $this->load->model('modeldataAPI');
        $xArrList = $this->modeldataAPI->getAPIPolicies(getToken());
//        foreach ($xArrList as $row) {
//            $xbufResult .= tbaddrow(tbaddcell($row->country_id) .
//                    tbaddcell($row->country_name) .
//                    tbaddcell($row->country_areacode));
//        }
//        $xbufResult = tablegrid($xbufResult);
//        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
        return $xArrList;
    }

    function getListHotel() {
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('province_name', '', 'width=10%') .
                tbaddcell('kecamatan_name', '', 'width=10%') .
                tbaddcell('kelurahan_name', '', 'width=10%') .
                tbaddcell('business_uri/detailhotel', '', 'width=10%') .
                tbaddcell('photo_primary', '', 'width=10%') .
                tbaddcell('star_rating', '', 'width=10%') .
                tbaddcell('id', '', 'width=10%') .
                tbaddcell('room_available', '', 'width=10%') .
                tbaddcell('latitude', '', 'width=10%') .
                tbaddcell('longitude', '', 'width=10%') .
                tbaddcell('room_max_occupancies', '', 'width=10%') .
                tbaddcell('rating', '', 'width=10%') .
                tbaddcell('tripadvisor_avg_rating', '', 'width=10%') .
                tbaddcell('room_facility_name', '', 'width=10%') .
                tbaddcell('oldprice', '', 'width=10%') .
                tbaddcell('address', '', 'width=10%') .
                tbaddcell('wifi', '', 'width=10%') .
                tbaddcell('promo_name', '', 'width=10%') .
                tbaddcell('price', '', 'width=10%') .
                tbaddcell('total_price', '', 'width=10%') .
                tbaddcell('regional', '', 'width=10%') .
                tbaddcell('name', '', 'width=10%') .
                tbaddcell('hotel_id', '', 'width=10%'), '', TRUE);
        $this->load->model('modeldataAPI');
        $country = "Yogyakarta";
        $startdate = "2016-11-29";
        $night = "1";
        $enddate = "2016-11-30";
        $room = "1";
        $adult = "2";
        $child = "0";
        $page = "1";
        $sort = "popular"; //popular/starasc/stardesc/priceasc/pricedesc
        $minprice = "";
        $maxprice = "";
        $minstar = "";
        $maxstar = "";
        $latitude = "";
        $longitude = "";
        $xArrList = $this->modeldataAPI->getAPIListHotel(getToken(), $country, $startdate, $night, $enddate, $room, $adult, $child, $page, $sort, $minprice, $maxprice, $minstar, $maxstar, $latitude, $longitude);
        foreach ($xArrList as $row) {
            $tripadvisor_avg_rating = "";
            if (!empty($row->tripadvisor_avg_rating)) {
                $tripadvisor_avg_rating = $row->tripadvisor_avg_rating->avg_rating . "</br>" .
                        "<image src='" . $row->tripadvisor_avg_rating->image_url . "'/></br>" .
                        "" . $row->tripadvisor_avg_rating->review_count . "</br>" .
                        "<a href=" . $row->tripadvisor_avg_rating->url . ">" . $row->tripadvisor_avg_rating->url . "</a></br>";
            }
//            $xArrListDetail = $row->business_uri;
//            $xArrListDetail = $this->modeldataAPI->getAPIDetailHotel($row->business_uri);
//            $listDetail = "";
//            foreach ($xArrListDetail as $rowDetail) {
//                
//            }
            $xAddImageList = '<img src="' . base_url() . 'resource/imgbtn/imglist.png" alt="Edit Data" onclick = "doshowdetailhotel(\'' . $row->business_uri . '\');" style="border:none;width:20px"/>';

            $xbufResult .= tbaddrow(
                    tbaddcell($row->province_name) .
                    tbaddcell($row->kecamatan_name) .
                    tbaddcell($row->kelurahan_name) .
                    tbaddcell($xAddImageList) .
                    tbaddcell("<img src='" . $row->photo_primary . "'/>") .
                    tbaddcell($row->star_rating) .
                    tbaddcell($row->id) .
                    tbaddcell($row->room_available) .
                    tbaddcell($row->latitude) .
                    tbaddcell($row->longitude) .
                    tbaddcell($row->room_max_occupancies) .
                    tbaddcell($row->rating) .
                    tbaddcell($tripadvisor_avg_rating) .
                    tbaddcell($row->oldprice) .
                    tbaddcell($row->room_facility_name) .
                    tbaddcell($row->address) .
                    tbaddcell($row->wifi) .
                    tbaddcell($row->promo_name) .
                    tbaddcell($row->price) .
                    tbaddcell($row->total_price) .
                    tbaddcell($row->regional) .
                    tbaddcell($row->name) .
                    tbaddcell($row->hotel_id));
        }
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
//        return $xArrList;
    }

    function getListDetailHotel($xbusiness_uri) {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modeldataAPI');
        $xobjdetail = $this->modeldataAPI->getAPIDetailHotel($xbusiness_uri);

        $xobjbreadcrumb = $this->modeldataAPI->getbreadcrumb($xobjdetail);
        $xbufResult = "<p>breadcrumb</br>" .
                "-business_id = " . $xobjbreadcrumb->business_id . "</br>" .
                "-business_uri = " . $xobjbreadcrumb->business_uri . "</br>" .
                "-business_name = " . $xobjbreadcrumb->business_name . "</br>" .
                "-area_id = " . $xobjbreadcrumb->area_id . "</br>" .
                "-area_name = " . $xobjbreadcrumb->area_name . "</br>" .
                "-kelurahan_id = " . $xobjbreadcrumb->kelurahan_id . "</br>" .
                "-kelurahan_name = " . $xobjbreadcrumb->kelurahan_name . "</br>" .
                "-kecamatan_id = " . $xobjbreadcrumb->kecamatan_id . "</br>" .
                "-kecamatan_name = " . $xobjbreadcrumb->kecamatan_name . "</br>" .
                "-city_id = " . $xobjbreadcrumb->city_id . "</br>" .
                "-province_id = " . $xobjbreadcrumb->province_name . "</br>" .
                "-country_id = " . $xobjbreadcrumb->country_id . "</br>" .
                "-country_name = " . $xobjbreadcrumb->country_name . "</br>" .
                "-continent_id = " . $xobjbreadcrumb->continent_name . "</br>" .
                "-kelurahan_uri = " . $xobjbreadcrumb->kelurahan_uri . "</br>" .
                "-kecamatan_uri = " . $xobjbreadcrumb->kecamatan_uri . "</br>" .
                "-province_uri = " . $xobjbreadcrumb->country_uri . "</br>" .
                "-continent_uri = " . $xobjbreadcrumb->continent_uri . "</br>" .
                "</p>";

        $xobjresult = $this->modeldataAPI->getresult($xobjdetail->results);
        $listroom = "<p>List Room</br>";
        $i = 1;
        foreach ($xobjresult as $rowroom) {
            $listroom .= $i . "-id = " . $rowroom->id . "</br>";
            $listroom .= $i . "-room_available = " . $rowroom->room_available . "</br>";
            $listroom .= $i . "-room_name = " . $rowroom->room_name . "</br>";
            $listroom .= $i . "-ext_source = " . $rowroom->ext_source . "</br>";
            $listroom .= $i . "-room_id = " . $rowroom->room_id . "</br>";
            $listroom .= $i . "-currency = " . $rowroom->currency . "</br>";
            $listroom .= $i . "-minimum_stays = " . $rowroom->minimum_stays . "</br>";
            $listroom .= $i . "-with_breakfasts = " . $rowroom->with_breakfasts . "</br>";
            $listphotoroom = "</br>";
            foreach ($rowroom->all_photo_room as $value) {
                $listphotoroom .= "<image src='" . $value . "'/></br>";
            }
            $listroom .= $i . "-all_photo_room = " . $listphotoroom . "</br>";
            $listroom .= $i . "-photo_url = " . $rowroom->photo_url . "</br>";
            $listroom .= $i . "-room_name = " . $rowroom->room_name . "</br>";
            $listroom .= $i . "-bookUri = " . $rowroom->bookUri . "</br>";
            $listroom_facility = "</br>";
            foreach ($rowroom->room_facility as $value) {
                $listroom_facility .= $value . "</br>";
            }
            $listroom .= $i . "-room_facility = " . $listroom_facility . "</br>";
            $listroom .= $i . "-room_description = " . $rowroom->room_description . "</br>";
            $listroom .= $i . "-additional_surcharge_currency = " . $rowroom->additional_surcharge_currency . "</br>";
            $listroom .= "</br>";
            $i++;
        }
        $listroom .= "</p>";
        $xbufResult .= $listroom;

        $xbufResult .= "<p>addinfos</br>";
        $xobjaddinfos = $this->modeldataAPI->getaddinfos($xobjdetail);
        $listaddinfos = "</br>";
        foreach ($xobjaddinfos->addinfo as $value) {
            $listaddinfos .= $value . "</br>";
        }
        $xbufResult .= $listaddinfos;

        $xobjall_photo = $this->modeldataAPI->getall_photo($xobjdetail);
        $listphoto = "List Photo</br>";
        foreach ($xobjall_photo->photo as $value) {
            $listphoto .= "<image src='" . $value->file_name . "'/> photo_type=" . $value->photo_type . "</br>";
        }
        $xbufResult .= $listphoto;

        $xobjprimaryPhotos_large = $this->modeldataAPI->getprimaryPhotos_large($xobjdetail);
        $xbufResult .= "<image src='" . $xobjprimaryPhotos_large . "'/> primaryPhotos_large</br>";

        $xobjavail_facilitiy = $this->modeldataAPI->getavail_facilitiy($xobjdetail->avail_facilities);

        $listavail_facilitiy = "avail facilitiy</br>";
        foreach ($xobjavail_facilitiy as $value) {
            $listavail_facilitiy .= "facility_type = " . $value->facility_type . ",facility_name = " . $value->facility_name . "</br>";
        }
        $xbufResult .= $listavail_facilitiy;

        $xobjnearby_attraction = $this->modeldataAPI->getnearby_attraction($xobjdetail->nearby_attractions);

        $listnearby_attraction = "nearby_attraction</br>";
        foreach ($xobjnearby_attraction as $value) {
            $listnearby_attraction .= "distance = " . $value->distance .
                    ",business_primary_photo = <image src='" . $value->business_primary_photo . "'/>" .
                    ",business_name = " . $value->business_name .
                    ",business_uri = " . $value->business_uri .
                    "</br>";
        }
        $xbufResult .= $listnearby_attraction;

        $xobjlist_internal_review = $this->modeldataAPI->getlist_internal_review($xobjdetail);

        $listlist_internal_review = "list_internal_review</br>";
        foreach ($xobjlist_internal_review as $value) {
            $listlist_internal_review .= "avatar = <image src='" . $value->avatar . "'/>" .
                    ",person = " . $value->person .
                    ",nationality_flag = <image src='" . $value->nationality_flag . "'/>" .
                    ",nationality = " . $value->nationality .
                    ",rating = " . $value->rating .
                    ",max_rating = " . $value->max_rating .
                    ",testimonial_text = " . $value->testimonial_text .
                    "</br>";
        }
        $xbufResult .= $listlist_internal_review;

        $xobjsummary_internal_review = $this->modeldataAPI->getsummary_internal_review($xobjdetail);

        $xbufResult .= "summary_internal_review</br>average = " . $xobjsummary_internal_review->average .
                ",max_rating = " . $xobjsummary_internal_review->max_rating . "</br>";

        $xobjdetailsummary_internal_review = $this->modeldataAPI->getdetailsummary_internal_review($xobjsummary_internal_review);

        $listdetailsummary_internal_review = "detailsummary_internal_review</br>";
        foreach ($xobjdetailsummary_internal_review as $value) {
            $listdetailsummary_internal_review .= "category = " . $value->category .
                    ",value = " . $value->value .
                    "</br>";
        }
        $xbufResult .= $listdetailsummary_internal_review;

        $xobjgeneral = $this->modeldataAPI->getgeneral($xobjdetail);

        $xbufResult .= "summary_internal_review</br>address = " . $xobjgeneral->address .
                ",description = " . $xobjgeneral->description .
                ",latitude = " . $xobjgeneral->latitude .
                ",longitude = " . $xobjgeneral->longitude . "</br>";

        $xbufResult .= "</p>";

        return $xbufResult;
//        return $xArrList;
    }

    function setListDetailHotel() {
        $this->load->helper("json");
        $xbusiness_uri = $_POST['business_uri'];
        $this->json_data["listdetailhotel"] = $this->getListDetailHotel($xbusiness_uri);
//        $this->load->helper('common');        
//        $this->load->model('modeldataAPI');
//        $xobjdetail = $this->modeldataAPI->getAPIDetailHotel($xbusiness_uri);
//        $this->json_data["listdetailhotel"] = $xobjdetail;

        echo json_encode($this->json_data);
    }

    function getListAirport() {
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('airport_name', '', 'width=10%') .
                tbaddcell('airport_code', '', 'width=10%') .
                tbaddcell('location_name', '', 'width=10%') .
                tbaddcell('country_id', '', 'width=10%') .
                tbaddcell('country_name', '', 'width=10%'), '', TRUE);
        $this->load->model('modeldataAPI');
        $xArrList = $this->modeldataAPI->getAPIListAirport(getToken());
        foreach ($xArrList as $row) {
            $xbufResult .= tbaddrow(tbaddcell($row->airport_name) .
                    tbaddcell($row->airport_code) .
                    tbaddcell($row->location_name) .
                    tbaddcell($row->country_id) .
                    tbaddcell($row->country_name));
        }
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
//        return $xArrList;
    }

    function getListAirportPopularDestination() {
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('arrival_city', '', 'width=10%') .
                tbaddcell('business_name', '', 'width=10%') .
                tbaddcell('province_name', '', 'width=10%') .
                tbaddcell('location_name', '', 'width=10%'), '', TRUE);
        $this->load->model('modeldataAPI');
        $xArrList = $this->modeldataAPI->getAPIListAirportPopularDestination(getToken());
        foreach ($xArrList as $row) {
            $xbufResult .= tbaddrow(tbaddcell($row->arrival_city) .
                    tbaddcell($row->business_name) .
                    tbaddcell($row->province_name) .
                    tbaddcell($row->location_name));
        }
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
//        return $xArrList;
    }

    function getCheckFlightUpdated() {
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('arrival_city', '', 'width=10%') .
                tbaddcell('business_name', '', 'width=10%') .
                tbaddcell('province_name', '', 'width=10%') .
                tbaddcell('location_name', '', 'width=10%'), '', TRUE);
        $this->load->model('modeldataAPI');
        $d = "CGK";
        $a = "DPS";
        $date = "2014-05-25";
        $adult = "1";
        $child = "0";
        $infant = "0";
        $xArrList = $this->modeldataAPI->getAPICheckFlightUpdated(getToken(), $d, $a, $date, $adult, $child, $infant);
//        foreach ($xArrList as $row) {
//            $xbufResult .= tbaddrow(tbaddcell($row->arrival_city) .
//                    tbaddcell($row->business_name) .
//                    tbaddcell($row->province_name) .
//                    tbaddcell($row->location_name));
//        }
//        $xbufResult = tablegrid($xbufResult);
//        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
        return $xArrList;
    }

    function getListFlight() {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modeldataAPI');
        $d = "CGK";
        $a = "DPS";
        $date = "2016-12-01";
//        $ret_date = "";
        $ret_date = "2016-12-02";

//        Maximal passenger per book:
//Maximal passenger Adult : 6
//Maximal passenger Child : 6
//Maximal passenger infant : 6

        $adult = 1;
        $child = 0;
        $infant = 0;
        $xArrList = $this->modeldataAPI->getAPIListFlight(getToken(), $d, $a, $date, $ret_date, $adult, $child, $infant);
        $objdep = "";
        $objret = "";
        if ($ret_date != "") {
            $objdep = $xArrList[0];
            $objret = $xArrList[1];
            $xbufResultdep = $this->getTableFlight($objdep);
            $xbufResultret = $this->getTableFlight($objret);
            $xbufResult = tbaddrow(tbaddcell($xbufResultdep) . tbaddcell($xbufResultret));
        } else {
            $objdep = $xArrList;
            $xbufResult = $this->getTableFlight($objdep);
        }
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
//        return $xArrList;
    }

    function getTableFlight($xArrList) {
        $xbufResult = tbaddrow(tbaddcell('flight_id', '', 'width=10%') .
                tbaddcell('airlines_name', '', 'width=10%') .
                tbaddcell('flight_number', '', 'width=10%') .
                tbaddcell('departure_city', '', 'width=10%') .
                tbaddcell('arrival_city', '', 'width=10%') .
                tbaddcell('stop', '', 'width=10%') .
                tbaddcell('price_value', '', 'width=10%') .
                tbaddcell('price_adult', '', 'width=10%') .
                tbaddcell('price_child', '', 'width=10%') .
                tbaddcell('price_infant', '', 'width=10%') .
                tbaddcell('timestamp', '', 'width=10%') .
                tbaddcell('has_food', '', 'width=10%') .
                tbaddcell('check_in_baggage', '', 'width=10%') .
                tbaddcell('is_promo', '', 'width=10%') .
                tbaddcell('airport_tax', '', 'width=10%') .
                tbaddcell('check_in_baggage_unit', '', 'width=10%') .
                tbaddcell('simple_departure_time', '', 'width=10%') .
                tbaddcell('long_via', '', 'width=10%') .
                tbaddcell('departure_city_name', '', 'width=10%') .
                tbaddcell('arrival_city_name', '', 'width=10%') .
                tbaddcell('full_via', '', 'width=10%') .
                tbaddcell('markup_price_string', '', 'width=10%') .
                tbaddcell('need_baggage', '', 'width=10%') .
                tbaddcell('best_deal', '', 'width=10%') .
                tbaddcell('duration', '', 'width=10%') .
                tbaddcell('image', '', 'width=10%') .
                tbaddcell('departure_flight_date', '', 'width=10%') .
                tbaddcell('departure_flight_date_str', '', 'width=10%') .
                tbaddcell('departure_flight_date_str_short', '', 'width=10%') .
                tbaddcell('arrival_flight_date', '', 'width=10%') .
                tbaddcell('arrival_flight_date_str', '', 'width=10%') .
                tbaddcell('arrival_flight_date_str_short', '', 'width=10%') .
                tbaddcell('flight_infos', '', 'width=10%'), '', TRUE);

        $adult = 1;
        $child = 0;
        $infant = 0;
        foreach ($xArrList as $row) {
            $xbufFlightInfos = "";
            $xArrListflightinfo = $this->modeldataAPI->getAPIFlightInfo($row->flight_infos);
            foreach ($xArrListflightinfo as $value) {
                $xbufFlightInfos .= "- flight_number=" . $value->flight_number . "</br>" .
                        "- class=" . $value->class . "</br>" .
                        "- departure_city=" . $value->departure_city . "</br>" .
                        "- departure_city_name=" . $value->departure_city_name . "</br>" .
                        "- arrival_city=" . $value->arrival_city . "</br>" .
                        "- arrival_city_name=" . $value->arrival_city_name . "</br>" .
                        "- airlines_name=" . $value->airlines_name . "</br>" .
                        "- departure_date_time=" . $value->departure_date_time . "</br>" .
                        "- string_departure_date=" . $value->string_departure_date . "</br>" .
                        "- string_departure_date_short=" . $value->string_departure_date_short . "</br>" .
                        "- simple_departure_time=" . $value->simple_departure_time . "</br>" .
                        "- arrival_date_time=" . $value->arrival_date_time . "</br>" .
                        "- string_arrival_date=" . $value->string_arrival_date . "</br>" .
                        "- string_arrival_date_short=" . $value->string_arrival_date_short . "</br>" .
                        "- simple_arrival_time=" . $value->simple_arrival_time . "</br>" .
                        "- img_src=" . $value->img_src . "</br>" .
                        "- duration_time=" . $value->duration_time . "</br>" .
                        "- duration_hour=" . $value->duration_hour . "</br>" .
                        "- duration_minute=" . $value->duration_minute . "</br>" .
                        "- check_in_baggage_unit=" . $value->check_in_baggage_unit . "</br>" .
                        "- terminal=" . $value->terminal . "</br>" .
                        "- transit_duration_hour=" . $value->transit_duration_hour . "</br>" .
                        "- transit_duration_minute=" . $value->transit_duration_minute . "</br>" .
                        "- transit_arrival_text_city=" . $value->transit_arrival_text_city . "</br>" .
                        "- transit_arrival_text_time=" . $value->transit_arrival_text_time . "</br>";
            }

            $xAddImageList = '<img src="' . base_url() . 'resource/imgbtn/imglist.png" alt="Edit Data" onclick = "doaddorderflight(\'' . $row->flight_id . '\',\'\',\'' . $adult . '\',\'' . $child . '\',\'' . $infant . '\');" style="border:none;width:20px"/>';
            $xbufResult .= tbaddrow(tbaddcell($row->flight_id) .
                    tbaddcell($row->airlines_name) .
                    tbaddcell($row->flight_number) .
                    tbaddcell($row->departure_city) .
                    tbaddcell($row->arrival_city) .
                    tbaddcell($row->stop) .
                    tbaddcell($row->price_value) .
                    tbaddcell($row->price_adult) .
                    tbaddcell($row->price_child) .
                    tbaddcell($row->price_infant) .
                    tbaddcell($row->timestamp) .
                    tbaddcell($row->has_food) .
                    tbaddcell($row->check_in_baggage) .
                    tbaddcell($row->is_promo) .
                    tbaddcell($row->airport_tax) .
                    tbaddcell($row->check_in_baggage_unit) .
                    tbaddcell($row->simple_departure_time) .
                    tbaddcell($row->long_via) .
                    tbaddcell($row->departure_city_name) .
                    tbaddcell($row->arrival_city_name) .
                    tbaddcell($row->full_via) .
                    tbaddcell($row->markup_price_string) .
                    tbaddcell($row->need_baggage) .
                    tbaddcell($row->best_deal) .
                    tbaddcell($row->duration) .
                    tbaddcell($row->image) .
                    tbaddcell($row->departure_flight_date) .
                    tbaddcell($row->departure_flight_date_str) .
                    tbaddcell($row->departure_flight_date_str_short) .
                    tbaddcell($row->arrival_flight_date) .
                    tbaddcell($row->arrival_flight_date_str) .
                    tbaddcell($row->arrival_flight_date_str_short) .
                    tbaddcell($xbufFlightInfos) .
                    tbaddcell($xAddImageList));
        }
        return $xbufResult;
    }

    function setDataAddOrderFlight() {
        $this->load->helper("json");
        $flight_id = $_POST["flight_id"];
        $ret_flight_id = $_POST["ret_flight_id"];
        $child = $_POST["child"];
        $adult = $_POST["adult"];
        $infant = $_POST["infant"];

        //data dibawah jg diisi dari user(pake &_POST)
        $conSalutation = "Mrs";
        $conFirstName = "budianto";
        $conLastName = "wijaya";
        $conPhone = "%2B6287880182218";
        $conEmailAddress = "you_julin@yahoo.com";
        $firstnamea = "susi";
        $lastnamea = "wijaya";
        $ida = "1116057107900001";
        $titlea = "Mr";
        $conOtherPhone = "%2B628521342534";
        $titlec = "Ms";
        $firstnamec = "carreen";
        $lastnamec = "athalia";
        $birthdatec = "2005-02-02";
        $titlei = "Mr";
        $parenti = "1";
        $firstnamei = "wendy";
        $lastnamei = "suprato";
        $birthdatei = "2011-06-29";

        $arrdataa = array();
        $arrconOtherPhone = array();
        $arrdatac = array();
        $arrdatai = array();
        if ($adult > 0) {
            $arrfirstnamea = explode(",", $firstnamea);
            $arrlastnamea = explode(",", $lastnamea);
            $arrida = explode(",", $ida);
            $arrtitlea = explode(",", $titlea);
            for ($i = 0; $i < $adult; $i++) {
                $arrdataa[] = (object) array('firstnamea' => $arrfirstnamea[$i]
                    , 'lastnamea' => $arrlastnamea[$i]
                    , 'ida' => $arrida[$i]
                    , 'titlea' => $arrtitlea[$i]);
//                $objdataa->firstnamea = $arrfirstnamea[$i];
//                $objdataa->lastnamea = $arrlastnamea[$i];
//                $objdataa->ida = $arrida[$i];
//                $objdataa->titlea = $arrtitlea[$i];
//                array_push($arrdataa, $objdataa);
            }
        }
        if ($conOtherPhone != "") {
            $arrconOtherPhone = explode(",", $conOtherPhone);
        }
        if ($child > 0) {
            $arrtitlec = explode(",", $titlec);
            $arrfirstnamec = explode(",", $firstnamec);
            $arrlastnamec = explode(",", $lastnamec);
            $arrbirthdatec = explode(",", $birthdatec);
            for ($i = 0; $i < $adult; $i++) {
                $arrdatac[] = (object) array('titlec' => $arrtitlec[$i]
                    , 'firstnamec' => $arrfirstnamec[$i]
                    , 'lastnamec' => $arrlastnamec[$i]
                    , 'birthdatec' => $arrbirthdatec[$i]);
            }
        }
        if ($infant > 0) {
            $arrtitlei = explode(",", $titlei);
            $arrparenti = explode(",", $parenti);
            $arrfirstnamei = explode(",", $firstnamei);
            $arrlastnamei = explode(",", $lastnamei);
            $arrbirthdatei = explode(",", $birthdatei);
            for ($i = 0; $i < $adult; $i++) {
                $arrdatac[] = (object) array('titlei' => $arrtitlei[$i]
                    , 'parenti' => $arrparenti[$i]
                    , 'firstnamei' => $arrfirstnamei[$i]
                    , 'lastnamei' => $arrlastnamei[$i]
                    , 'birthdatei' => $arrbirthdatei[$i]);
            }
        }
        $xResultAddOrderFlight = $this->addOrderFlight($flight_id
                , $ret_flight_id
                , $child
                , $adult
                , $infant
                , $conSalutation
                , $conFirstName
                , $conLastName
                , $conPhone
                , $conEmailAddress
                , $arrdataa
                , $arrconOtherPhone
                , $arrdatac
                , $arrdatai);
        $this->json_data["addOrderFlight"] = $xResultAddOrderFlight;
        echo json_encode($this->json_data);
    }

    function addOrderFlight($flight_id
    , $ret_flight_id
    , $child
    , $adult
    , $infant
    , $conSalutation
    , $conFirstName
    , $conLastName
    , $conPhone
    , $conEmailAddress
    , $arrdataa
    , $arrconOtherPhone
    , $arrdatac
    , $arrdatai) {
        $this->load->helper('common');
        $this->load->model('modeldataAPI');
        $xArrListAddOrderFlight = $this->modeldataAPI->getAPIAddOrderFlight(getToken(), $flight_id
                , $ret_flight_id
                , $child
                , $adult
                , $infant
                , $conSalutation
                , $conFirstName
                , $conLastName
                , $conPhone
                , $conEmailAddress
                , $arrdataa
                , $arrconOtherPhone
                , $arrdatac
                , $arrdatai);
        return $xArrListAddOrderFlight;
    }   

}

?>