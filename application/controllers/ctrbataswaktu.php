<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : bataswaktu  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrbataswaktu extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformbataswaktu('0', $xAwal);
    }

    function createformbataswaktu($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/jQuery.angka.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxbataswaktu.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormbataswaktu($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormbataswaktu($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Batas Waktu Pembayaran</h3><div class="garis"></div>' . form_open_multipart('ctrbataswaktu/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edbataswaktu', 'Batas Waktu', form_input(getArrayObj('edbataswaktu', '', '100'), '', 'class="angka"'),'satuan jam') . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanbataswaktu();"') . form_button('btNew', 'new', 'onclick="doClearbataswaktu();"') . '<div class="spacer"></div><div id="tabledatabataswaktu">' . $this->getlistbataswaktu(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistbataswaktu($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=30%') .
                tbaddcell('Batas Waktu', '', 'width=60%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelbataswaktu');
        $xQuery = $this->modelbataswaktu->getListbataswaktu($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditbataswaktu(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusbataswaktu(\'' . $row->idx . '\',\'' . substr($row->bataswaktu, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->bataswaktu) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchbataswaktu(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchbataswaktu(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchbataswaktu(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10%') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =2'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function getlistbataswaktuAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['bataswaktu'] = "";
        $response = array();
        $this->load->model('modelbataswaktu');
        $xQuery = $this->modelbataswaktu->getListbataswaktu($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['bataswaktu'] = $row->bataswaktu;
            ;
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpanbataswaktuAndroid() {
        $xidx = $_POST['edidx'];
        $xbataswaktu = $_POST['edbataswaktu'];

        $this->load->helper('json');
        $this->load->model('modelbataswaktu');
        $response = array();
        if ($xidx != '0') {
            $this->modelbataswaktu->setUpdatebataswaktu($xidx, $xbataswaktu);
        } else {
            $this->modelbataswaktu->setInsertbataswaktu($xidx, $xbataswaktu);
        }
        $row = $this->modelbataswaktu->getLastIndexbataswaktu();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['bataswaktu'] = $row->bataswaktu;
        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrecbataswaktu() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelbataswaktu');
        $row = $this->modelbataswaktu->getDetailbataswaktu($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['bataswaktu'] = $row->bataswaktu;
        echo json_encode($this->json_data);
    }

    function deletetablebataswaktu() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelbataswaktu');
        $this->modelbataswaktu->setDeletebataswaktu($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchbataswaktu() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatabataswaktu'] = $this->getlistbataswaktu($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpanbataswaktu() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xbataswaktu = $_POST['edbataswaktu'];
        $this->load->model('modelbataswaktu');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelbataswaktu->setUpdatebataswaktu($xidx, $xbataswaktu);
            } else {
                $xStr = $this->modelbataswaktu->setInsertbataswaktu($xidx, $xbataswaktu);
            }
        }
        echo json_encode(null);
    }

}

?>