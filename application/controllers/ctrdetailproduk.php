<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : detailproduk  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrdetailproduk extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xIdProduk, $xIdKatProduk) {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');

        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }

        $this->session->set_userdata('awal', 0);
        $this->createformdetailproduk($xIdProduk, $xIdKatProduk);
    }

    function createformdetailproduk($xIdProduk, $xIdKatProduk) {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/jQuery.angka.js"></script>' . "\n" .
                link_tag('resource/css/admin/upload/css/upload.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/tiny_mce/tiny_mce_src.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/tiny_mce/jquery.tinymce.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxmce2.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.knob.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.iframe-transport.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.fileupload.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/myuploadphoto.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxdetailproduk.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaximagedetail2.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormdetailproduk($xIdProduk, $xIdKatProduk), '', '', $xAddJs, '');
    }

    function setDetailFormdetailproduk($xidproduk, $xIdKatProduk) {
        $this->load->helper('form');
        $this->load->model('modelproduk');
        $this->load->model('modelsatuan');
        $row = $this->modelproduk->getDetailproduk($xidproduk);
        $this->load->model('modelkategoriproduk');
        $rowkategori = $this->modelkategoriproduk->getDetailkategoriproduk($xIdKatProduk);
        $this->load->helper('common');

        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">'
                . '<h3><img src="' . base_url() . 'resource/imgbtn/prev.png" alt="Edit Data" onclick = "dobacktoproduk(\'' . $xIdKatProduk . '\');" style="cursor:pointer;border:none;width:40px"/>'
                . 'Detail Produk "' . $row->JudulProduk . '"</h3><div class="garis"></div>';

        $xBufResult .= '<input type="hidden" name="edidxdetail" id="edidxdetail" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidproduk" id="edidproduk" value="' . $xidproduk . '" />';
        $xBufResult .= '<input type="hidden" name="edidkategoriprodukdetail" id="edidkategoriprodukdetail" value="' . $xIdKatProduk . '" />';

        $xBufResult .= setForm('edidproduk', 'Produk', form_input(getArrayObj('edNmProduk', $row->JudulProduk, '600'), '', 'disabled')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidkategoriproduk', 'Kategori Produk', form_input(getArrayObj('edNmKategoriproduk', $rowkategori->Kategori, '200'), '', 'disabled')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edjuduldetailproduk', 'Nama Detail Produk', form_input(getArrayObj('edjuduldetailproduk', '', '500'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('eddiskripsiproduk', 'Deskripsi Produk', '');
        $xBufResult .= setForm('eddiskripsiproduk', '', form_textarea(getArrayObj('eddiskripsiproduk', '', '500'), '', 'class="tinymce"')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edrate', 'Harga', form_input(getArrayObj('edrate', '', '100'), '', 'class="angka"'));
        $xBufResult .= setForm('edratediscount', 'Harga Discount', form_input(getArrayObj('edratediscount', '', '100'), '', 'class="angka"')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('edkapasitas', 'Kapasitas', form_input(getArrayObj('edkapasitas', '', '100'), '', 'class="angka"')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edstandartpemakaian', 'Standar Pemakaian', form_input(getArrayObj('edstandartpemakaian', '', '100'), '', 'class="angka"'));
        $xBufResult .= setForm('edidsatuan', 'Satuan', form_dropdown('edidsatuan', $this->modelsatuan->getArrayListsatuan(), '', 'id="edidsatuan"')) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edrancode', 'rancode', form_input(getArrayObj('edrancode', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglinsert', 'tglinsert', form_input(getArrayObj('edtglinsert', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglupdate', 'tglupdate', form_input(getArrayObj('edtglupdate', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edidpegawai', 'idpegawai', form_input(getArrayObj('edidpegawai', '', '100'))) . '<div class="spacer"></div>';

        $xBufResult .= '<div id="modalformdetail"></div>';
        $xBufResult .= '<div class="garis"></div>' .
                form_button('btSimpan', 'simpan', 'onclick="dosimpandetailproduk();"') .
                form_button('btNew', 'new', 'onclick="doCleardetailproduk();"') .
                '<div class="spacer"></div><div id="tabledatadetailproduk">' . $this->getlistdetailproduk($xidproduk, $xIdKatProduk) . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistdetailproduk($xidproduk, $xIdKatProduk) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('No', '', 'width=5%') .
//                tbaddcell('idproduk', '', 'width=10%') .
//                tbaddcell('idkategoriproduk', '', 'width=10%') .
                tbaddcell('Nama Detail Produk', '', 'width=20%') .
                tbaddcell('Deskripsi Produk', '', 'width=40%') .
                tbaddcell('Harga', '', 'width=10%') .
                tbaddcell('Harga Discount', '', 'width=10%') .
                tbaddcell('Add Image', '', 'width=10%') .
//                tbaddcell('tglinsert', '', 'width=10%') .
//                tbaddcell('tglupdate', '', 'width=10%') .
//                tbaddcell('idpegawai', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modeldetailproduk');
        $xQuery = $this->modeldetailproduk->getListtambahdetailprodukbyid($xidproduk);
        $xino = 1;
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditdetailproduk(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xAddImageList = '<img src="' . base_url() . 'resource/imgbtn/imglist.png" alt="Edit Data" onclick = "doaddimagefromdetailproduk(\'' . $row->idx . '\',\'' . $xIdKatProduk . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusdetailproduk(\'' . $row->idx . '\',\'' . $row->juduldetailproduk . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($xino++) .
//                    tbaddcell($row->idproduk) .
//                    tbaddcell($row->idkategoriproduk) .
                    tbaddcell($row->juduldetailproduk) .
                    tbaddcell($row->diskripsiproduk) .
                    tbaddcell(number_format($row->rate, 0, '', '.')) .
                    tbaddcell(number_format($row->ratediscount, 0, '', '.')) .
                    tbaddcell($xAddImageList) .
//                    tbaddcell($row->rancode) .
//                    tbaddcell($row->tglinsert) .
//                    tbaddcell($row->tglupdate) .
//                    tbaddcell($row->idpegawai) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
//        $xInput = form_input(getArrayObj('edSearch', '', '200'));
//        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchdetailproduk(0);" style="border:none;width:30px;height:30px;" />';
//        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchdetailproduk(' . ($xAwal - $xLimit) . ');"/>';
//        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchdetailproduk(' . ($xAwal + $xLimit) . ');" />';
//        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
//                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecdetailproduk() {
        $xIdEdit = $_POST['edidxdetail'];
        $this->load->model('modeldetailproduk');
        $row = $this->modeldetailproduk->getDetaildetailproduk($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['idproduk'] = $row->idproduk;
        $this->json_data['idkategoriproduk'] = $row->idkategoriproduk;
        $this->json_data['juduldetailproduk'] = $row->juduldetailproduk;
        $this->json_data['diskripsiproduk'] = $row->diskripsiproduk;
        $this->json_data['rate'] = number_format($row->rate, 0, '', '.');
        $this->json_data['ratediscount'] = number_format($row->ratediscount, 0, '', '.');
        $this->json_data['rancode'] = $row->rancode;
        $this->json_data['tglinsert'] = $row->tglinsert;
        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['kapasitas'] = $row->kapasitas;
        $this->json_data['standartpemakaian'] = $row->standartpemakaian;
        $this->json_data['idsatuan'] = $row->idsatuan;
        $this->json_data['idpegawai'] = $row->idpegawai;
        echo json_encode($this->json_data);
    }

    function deletetabledetailproduk() {
        $edidxdetail = $_POST['edidxdetail'];
        $this->load->model('modeldetailproduk');
        $this->modeldetailproduk->setDeletedetailproduk($edidxdetail);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchdetailproduk() {
        $this->load->helper('json');
        $xidproduk = $_POST['edidproduk'];
        $xIdKatProduk = $_POST['edIdKatProduk'];
        $this->json_data['tabledatadetailproduk'] = $this->getlistdetailproduk($xidproduk, $xIdKatProduk);
        echo json_encode($this->json_data);
    }

    function simpandetailproduk() {
        $this->load->helper('json');
        if (!empty($_POST['edidxdetail'])) {
            $xidx = $_POST['edidxdetail'];
        } else {
            $xidx = '0';
        }
        $xidproduk = $_POST['edidproduk'];
        $xidkategoriproduk = $_POST['edidkategoriprodukdetail'];
        $xjuduldetailproduk = $_POST['edjuduldetailproduk'];
        $xdiskripsiproduk = $_POST['eddiskripsiproduk'];
        $xrate = $_POST['edrate'];
        $xratediscount = $_POST['edratediscount'];
        $xrancode = $_POST['edrancode'];
        $xtglinsert = $_POST['edtglinsert'];
        $xtglupdate = $_POST['edtglupdate'];
        $xkapasitas = $_POST['edkapasitas'];
        $xstandartpemakaian = $_POST['edstandartpemakaian'];
        $xidsatuan = $_POST['edidsatuan'];
        $xidpegawai = $_POST['edidpegawai'];
        $this->load->model('modeldetailproduk');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modeldetailproduk->setUpdatedetailproduk($xidx, $xidproduk, $xidkategoriproduk, $xjuduldetailproduk, $xdiskripsiproduk, str_replace(".", "", $xrate), str_replace(".", "", $xratediscount), $xrancode, $xtglinsert, $xtglupdate, str_replace(".", "", $xkapasitas), str_replace(".", "", $xstandartpemakaian), $xidsatuan, $idpegawai);
            } else {
                $xStr = $this->modeldetailproduk->setInsertdetailproduk($xidx, $xidproduk, $xidkategoriproduk, $xjuduldetailproduk, $xdiskripsiproduk, str_replace(".", "", $xrate), str_replace(".", "", $xratediscount), $xrancode, $xtglinsert, $xtglupdate, str_replace(".", "", $xkapasitas), str_replace(".", "", $xstandartpemakaian), $xidsatuan, $idpegawai);
            }
        }
        echo json_encode(null);
    }

    function doShowFormdetailproduk() {
        $xIdProduk = $_POST['xIdProduk'];
        $this->load->helper('json');
        $this->json_data['modaldetailproduk'] = $this->setDetailFormdetailproduk($xIdProduk);
        echo json_encode($this->json_data);
    }

    function getlistdetailprodukAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $xidproduk = $_POST['idproduk'];
        $xidMember = $_POST['idMember'];


        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['idproduk'] = "";
        $this->json_data['idkategoriproduk'] = "";
        $this->json_data['juduldetailproduk'] = "Masih Kosong";
        $this->json_data['diskripsiproduk'] = "";
        $this->json_data['rate'] = "";
        $this->json_data['ratediscount'] = "";
        $this->json_data['rancode'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['kapasitas'] = "";
        $this->json_data['standartpemakaian'] = "";
        $this->json_data['idsatuan'] = "";
        $this->json_data['idpegawai'] = "";
        $this->json_data['NmPproduk'] = "Masih Kosong";
        $this->json_data['LinkImage'] = "";
        $this->json_data['jmlBooking'] = "0";
        $this->json_data['StatusBooking'] = "0";
        $this->json_data['idxBooking'] = "";
        $response = array();
        $this->load->model('modeldetailproduk');
        $this->load->model('modelproduk');
        $this->load->model('modelimagedetail');
        $this->load->model('modelkategoriproduk');
        $this->load->model('modelbooking');
        $jmlbooking = $this->modelbooking->getJumlahBooking($xidMember);
        $xQuery = $this->modeldetailproduk->getListdetailprodukbyid($xAwal, $xLimit, $xSearch, $xidproduk);
        foreach ($xQuery->result() as $row) {
            $rowproduk = $this->modelproduk->getDetailproduk($row->idproduk);
//        $rowimage = $this->modelimagedetail->getDetailimagedetailbyidproduk($row->idx);
            $rowkategori = $this->modelkategoriproduk->getDetailkategoriproduk($row->idkategoriproduk);
            $rowbooking = $this->modelbooking->getStatusBooking($xidMember, $xidproduk, $row->idx);

            $xQueryimage = $this->modelimagedetail->getListimagedetailbyiddetailproduk($row->idx, "N");
            $this->json_data['idx'] = $row->idx;
            $this->json_data['idproduk'] = $row->idproduk;
            $this->json_data['idkategoriproduk'] = $row->idkategoriproduk;
            $this->json_data['juduldetailproduk'] = $row->juduldetailproduk;
            $this->json_data['diskripsiproduk'] = $row->diskripsiproduk;
            $this->json_data['rate'] = number_format($row->rate, 0, '.', ',');
            $this->json_data['ratediscount'] = number_format($row->ratediscount, 0, '.', ',');
            $this->json_data['rancode'] = $row->rancode;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['tglupdate'] = $row->tglupdate;
            $this->json_data['kapasitas'] = $row->kapasitas;
            $this->json_data['standartpemakaian'] = $row->standartpemakaian;
            $this->json_data['idsatuan'] = $row->idsatuan;
            $this->json_data['idpegawai'] = $row->idpegawai;
            $this->json_data['NmProduk'] = @$rowproduk->JudulProduk;
            $this->json_data['nmkategoriproduk'] = @$rowkategori->Kategori;
            $this->json_data['LinkImage'] = ""; //not used
            $this->json_data['jmlBooking'] = $jmlbooking;
            $this->json_data['StatusBooking'] = @$rowbooking->status;
            $this->json_data['idxBooking'] = @$rowbooking->idx;
            $arrlinkimage = array();
            foreach ($xQueryimage->result() as $rowimage) {
                array_push($arrlinkimage, $this->data_uri(@$rowimage->linkimage));
            }
            $this->json_data['arrlinkimage'] = $arrlinkimage;
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function data_uri($fileuri) {
        $mime = 'image/jpeg';
        $imgsrc = base_url() . 'resource/imgbtn/ic_logo_usd.png';

        if (!empty($fileuri)) {

            $fileuri = str_replace(" ", "%20", $fileuri);

            $imgsrc = base_url() . 'resource/uploaded/img/' . $fileuri;

            //   }
        }

        return $imgsrc;
    }

}

?>
