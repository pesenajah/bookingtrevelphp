<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ctrlaporanbookingproduk
 *
 * @author admindatakreasi
 */
class ctrlaporanbookingproduk extends CI_Controller {

    function __construct() {
        parent :: __construct();
    }

    function index() {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '';
        $xAddJs.= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxlaporanbookingproduk.js"></script>';
        if (!empty($idpegawai)) {
            echo $this->modelgetmenu->SetViewAdmin($this->createviewlap(), '<div class="spacer"></div><div id="browsepdf"></div>', '', $xAddJs, '');
        } else {
            die("Anda belum Login");
        }
    }

    function createviewlap() {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelkategoriproduk');
        $xBufResult = '';
        $xBufResult .= '<div id="stylized" class="myform">';
        $xBufResult .= '<h3>Laporan Booking Produk</h3><div class="spacer"></div><div class="garis"></div>';
        $xBufResult .= setForm('edTglMulai', 'Tanggal Awal', form_input(getArrayObj('edTglMulai', '', '200')));
        $xBufResult .= setForm('edTglSelesai', 'Tanggal Akhir', form_input(getArrayObj('edTglSelesai', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidkategoriproduk', 'Kategori Produk', form_dropdown('edidkategoriproduk', $this->modelkategoriproduk->getArrayListkategoriprodukall(), '', 'onchange="doshowdropdownproduk();" id="edidkategoriproduk" style = "width:150px"')) . '<div class="spacer"></div>';
        $xBufResult .= '<div id="dropdownproduk" name="dropdownproduk"></div>' . '<div class="spacer"></div>';
        $xBufResult .= '<div id="dropdowndetailproduk" name="dropdowndetailproduk"></div>' . '<div class="spacer"></div>';
        $xBufResult .= form_button('TampilData', '<span class="btnright">Tampil Data</span>', 'onclick="doshowlaporanbookingproduk();" class="btn"');
        $xBufResult .= form_button('SendToPdf', '<span class="btnright">Send To Pdf</span>', 'onclick="setpdflaporanbookingproduk();" class="btn"');
        $xBufResult .= form_button('ExportToExcel', '<span class="btnright">Export To Excel</span>', 'onclick="exportkeexcel();" class="btn"');
        $xBufResult .= '<div class="spacer"></div>' . '<div class="garis"></div>';
        $xBufResult .= '<div id="gbloader"><div>Proses Membaca Data </div> <img src="' . base_url() . 'resource/imgbtn/ajax-loader.gif"></div>';
        $xBufResult .= '<div id="tabledata" name="tabledata">';
        $xBufResult .= '<div id="tblaporanbookingproduk" name="tblaporanbookingproduk">';
        $xBufResult .= '</div>';
        $xBufResult .= '</div>';
        return $xBufResult;
    }

    function showtbdt($xidkategoriproduk, $xidproduk = '', $xiddetailproduk = '', $date_awal = '', $date_akhir = '') {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelbooking');
        $this->load->model('modelkategoriproduk');
        $xBufResult = tbaddrow(
                tbaddcell('<font color="#000">No</font>', '', 'width=5%') .
                tbaddcell('<font color="#000">Kategori Produk</font>', '', 'width=20%') .
                tbaddcell('<font color="#000">Data Booking</font>', '', 'width=75%'), 'background:#ffffff;', TRUE);
        $xResult = $this->modelkategoriproduk->getListkategoriprodukbyid($xidkategoriproduk);
        $no = 1;
        $xBufResultdata = '';
        foreach ($xResult->result() as $row) {
            $xBufResultdata .= tbaddrow(tbaddcell($no++) .
                    tbaddcell($row->Kategori) .
                    tbaddcell($this->getExplodeProduk($row->idx, $xidproduk, $xiddetailproduk, $date_awal, $date_akhir)));
        }
        if ($xBufResultdata == '') {
            $xBufResult .= tbaddrow(tbaddcell("TIDAK ADA DATA", '', 'align="center" colspan="8"'));
        } else {
            $xBufResult .= $xBufResultdata;
        }
        $xBufResult = tablegrid($xBufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xBufResult . '</div>';
    }

    function showdropdownproduk($xidkategoriproduk) {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelproduk');

        $xBufResult = setForm('edidproduk', 'Produk', form_dropdown('edidproduk', $this->modelproduk->getArrayListprodukall($xidkategoriproduk), '', 'onchange="doshowdropdowndetailproduk();" id="edidproduk" style = "width:150px"'));

        return '<div class="dropdownproduk"  style="width:100%;left:-12px;">' . $xBufResult . '</div>';
    }

    function showdropdowndetailproduk($xidproduk) {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modeldetailproduk');

        $xBufResult = setForm('ediddetailproduk', 'Detail Produk', form_dropdown('ediddetailproduk', $this->modeldetailproduk->getArrayListdetailprodukall($xidproduk), '', 'id="ediddetailproduk" style = "width:150px"')) . '<div class="spacer"></div>';

        return '<div class="dropdowndetailproduk"  style="width:100%;left:-12px;">' . $xBufResult . '</div>';
    }

    function getExplodeProduk($xidkategoriproduk, $xidproduk, $xiddetailproduk, $date_awal, $date_akhir) {
        $this->load->model('modelproduk');
        $this->load->helper('common');

        $hasilPerintah = $this->modelproduk->getListprodukbyidkategori($xidkategoriproduk, $xidproduk);

        $xBufResult = '<table width="100%">';
        $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="5%" align="center">No</td>';
        $xBufResult .= '<td style="border-top:none;border-left:none;" width="40%" align="center">Nama Produk</td>';
        $xBufResult .= '<td style="border-top:none;border-left:none;" width="55%" align="center">Detail Produk</td></tr>';

        $i = 1;
        foreach ($hasilPerintah->result() as $row) {
            $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="5%" align="left">' . '' . $i . '</td>';
            $xBufResult .= '<td style="border-top:none;border-left:none;" width="40%" align="left">' . '' . $row->JudulProduk . '</td>';
            $xBufResult .= '<td style="border-top:none;border-left:none;" width="55%" align="left">' . '' . $this->getExplodeDetailProduk($row->idx, $xiddetailproduk, $date_awal, $date_akhir, $i) . '</td></tr>';
            $i++;
        }

        return $xBufResult . '</table>';
    }

    function getExplodeDetailProduk($xidproduk, $xiddetailproduk, $date_awal, $date_akhir, $x) {
        $this->load->model('modeldetailproduk');
        $this->load->model('modelbooking');
        $this->load->helper('common');

        $hasilPerintah = $this->modeldetailproduk->getListdetailprodukbyidproduk($xidproduk, $xiddetailproduk);

        $xBufResult = '<table width="100%">';
        if ($x === 1) {
            $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="5%" align="center">No</td>';
            $xBufResult .= '<td style="border-top:none;border-left:none;" width="75%" align="center">Nama Detail Produk</td>';
            $xBufResult .= '<td style="border-top:none;border-left:none;" width="20%" align="center">Jumlah Booking</td></tr>';
        }

        $i = 1;
        foreach ($hasilPerintah->result() as $row) {

            $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="5%" align="left">' . '' . $i++ . '</td>';
            $xBufResult .= '<td style="border-top:none;border-left:none;" width="75%" align="left">' . '' . $row->juduldetailproduk . '</td>';
            $xBufResult .= '<td style="border-top:none;border-left:none;" width="20%" align="center">' . $this->modelbooking->getJumlahBookingbyIddetproduk($row->idx, $date_awal, $date_akhir) . '</td></tr>';
        }
        return $xBufResult . '</table>';
    }

    function setpdf() {
        $this->load->helper('html');
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('topdf');

        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xidkategoriproduk = $_POST['edidkategoriproduk'];
        $xidproduk = $_POST['edidproduk'];
        $xiddetailproduk = $_POST['ediddetailproduk'];

        $html = '<html>
				<header>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" . '
				</header>
				<body>
					<p>
						<div id="report">
						<div id="tabledata">
							' . $this->showtbdt($xidkategoriproduk, $xidproduk, $xiddetailproduk, $date_awal, $date_akhir) . '
						</div>
						</div>
					</p>
				</body>
			</html>';

        $idpegawai = $this->session->userdata('idpegawai');
//        die($html);
        pdf_create($html, 'laporan_bookingproduk_' . $idpegawai);
        $xbufresult = '<object data="' . base_url() . 'resource/pdf/laporan_bookingproduk_' . $idpegawai . '.pdf" type="application/pdf" width="1200px" height = "600px" type="left:-15px;" >
                          </object>';
        $this->json_data['data'] = $xbufresult;
        echo json_encode($this->json_data);
    }

    function carilaporan_byrange() {
        $this->load->helper('common');
        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xidkategoriproduk = $_POST['edidkategoriproduk'];
        $xidproduk = $_POST['edidproduk'];
        $xiddetailproduk = $_POST['ediddetailproduk'];

        $strHTML = $this->showtbdt($xidkategoriproduk, $xidproduk, $xiddetailproduk, $date_awal, $date_akhir);
        $this->load->helper('json');
        $this->json_data['tblaporanbookingproduk'] = $strHTML;
        echo json_encode($this->json_data);
    }

    function tampil_dropdownproduk() {
        $this->load->helper('common');
        $xidkategoriproduk = $_POST['edidkategoriproduk'];

        $strHTML = $this->showdropdownproduk($xidkategoriproduk);
        $this->load->helper('json');
        $this->json_data['dropdownproduk'] = $strHTML;
        echo json_encode($this->json_data);
    }

    function tampil_dropdowndetailproduk() {
        $this->load->helper('common');
        $xidproduk = $_POST['edidproduk'];

        $strHTML = $this->showdropdowndetailproduk($xidproduk);
        $this->load->helper('json');
        $this->json_data['dropdowndetailproduk'] = $strHTML;
        echo json_encode($this->json_data);
    }

    function exportkeexcel($xisblokir, $xidjenisbookingproduk, $date_awal, $date_akhir) {
        $this->load->helper('html');
        $this->load->helper('common');
        $nmfile = 'laporanbookingproduk';
        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xidkategoriproduk = $_POST['edidkategoriproduk'];
        $xidproduk = $_POST['edidproduk'];
        $xiddetailproduk = $_POST['ediddetailproduk'];
        $xhtml = $this->showtbdt($xidkategoriproduk, $xidproduk, $xiddetailproduk, $date_awal, $date_akhir);
        $xbufresult = header("Content-type: application/octet-stream") . "\n" .
                header("Content-Disposition: attachment; filename=" . $nmfile . ".xls") . "\n" .
                header("Pragma: no-cache") . "\n" .
                header("Expires: 0");
        $this->load->helper('html');
        $xbufresult .= '<html><head><style type=\'text/css\'>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" .
                '</head><body>' . $xhtml . '</body></html>';
        echo $xbufresult;
    }

}
