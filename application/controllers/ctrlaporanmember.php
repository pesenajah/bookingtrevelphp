<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ctrlaporanmember
 *
 * @author admindatakreasi
 */
class ctrlaporanmember extends CI_Controller {

    function __construct() {
        parent :: __construct();
    }

    function index() {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '';
        $xAddJs.= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxlaporanmember.js"></script>';
        if (!empty($idpegawai)) {
            echo $this->modelgetmenu->SetViewAdmin($this->createviewlap(), '<div class="spacer"></div><div id="browsepdf"></div>', '', $xAddJs, '');
        } else {
            die("Anda belum Login");
        }
    }

    function createviewlap() {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modeljenismember');
        $xBufResult = '';
        $xBufResult .= '<div id="stylized" class="myform">';
        $xBufResult .= '<h3>Laporan Data Member</h3><div class="spacer"></div><div class="garis"></div>';
        $xBufResult .= setForm('edTglMulai', 'Tanggal Awal', form_input(getArrayObj('edTglMulai', '', '200')));
        $xBufResult .= setForm('edTglSelesai', 'Tanggal Akhir', form_input(getArrayObj('edTglSelesai', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidjenismember', 'Jenis Member', form_dropdown('edidjenismember', $this->modeljenismember->getArrayListjenismemberall(), '', 'id="edidjenismember" style = "width:100px"'));
        $xArrstatus['-'] = 'Semua';
        $xArrstatus['N'] = 'Tidak';
        $xArrstatus['Y'] = 'Ya';
        $xBufResult .= setForm('edisblokir', 'Status Blokir', form_dropdown('edisblokir', $xArrstatus, '', 'id="edisblokir" style = "width:100px"')) . '<div class="spacer"></div>';
        $xBufResult .= form_button('TampilData', '<span class="btnright">Tampil Data</span>', 'onclick="doshowlaporanmember();" class="btn"');
        $xBufResult .= form_button('SendToPdf', '<span class="btnright">Send To Pdf</span>', 'onclick="setpdflaporanmember();" class="btn"');
        $xBufResult .= form_button('ExportToExcel', '<span class="btnright">Export To Excel</span>', 'onclick="exportkeexcel();" class="btn"');
        $xBufResult .= '<div class="spacer"></div>' . '<div class="garis"></div>';
        $xBufResult .= '<div id="gbloader"><div>Proses Membaca Data </div> <img src="' . base_url() . 'resource/imgbtn/ajax-loader.gif"></div>';
        $xBufResult .= '<div id="tabledata" name="tabledata">';
        $xBufResult .= '<div id="tblaporanmember" name="tblaporanmember">';
        $xBufResult .= '</div>';
        $xBufResult .= '</div>';
        return $xBufResult;
    }

    function showtbdt($xisblokir, $xidjenismember, $date_awal = '', $date_akhir = '') {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelmember');
        $this->load->model('modeljenismember');
        $xBufResult = tbaddrow(
                tbaddcell('<font color="#000">No</font>', '', 'width=5%') .
                tbaddcell('<font color="#000">Nama Member</font>', '', 'width=20%') .
                tbaddcell('<font color="#000">Alamat</font>', '', 'width=30%') .
                tbaddcell('<font color="#000">Nomor Telepon</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Email</font>', '', 'width=15%') .
                tbaddcell('<font color="#000">Tanggal Daftar</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Jenis Member</font>', '', 'width=5%') .
                tbaddcell('<font color="#000">Status Blokir</font>', '', 'width=5%'), 'background:#ffffff;', TRUE);
        $xResult = $this->modelmember->getListmemberbydatestatus($xisblokir, $xidjenismember, $date_awal, $date_akhir);
        $no = 1;
        $xBufResultdata = '';
        foreach ($xResult->result() as $row) {
            $arraytgljamdaftar = explode(' ', $row->tglinsert);
            $tgljamdaftar = datetomysql($arraytgljamdaftar[0]) . ' ' . $arraytgljamdaftar[1];

            $rowjenismember = $this->modeljenismember->getDetailjenismember($row->idjenismember);
            $xblokir = 'Tidak';
            if (strcmp($row->isblokir, 'Y') === 0) {
                $xblokir = 'Ya';
            }

            $xBufResultdata .= tbaddrow(tbaddcell($no++) .
                    tbaddcell($row->Nama) .
                    tbaddcell($row->Alamat) .
                    tbaddcell($row->NoTelpon) .
                    tbaddcell($row->email) .
                    tbaddcell($tgljamdaftar) .
                    tbaddcell(@$rowjenismember->JenisMember) .
                    tbaddcell($xblokir));
        }
        if ($xBufResultdata == '') {
            $xBufResult .= tbaddrow(tbaddcell("TIDAK ADA DATA", '', 'align="center" colspan="8"'));
        } else {
            $xBufResult .= $xBufResultdata;
        }
        $xBufResult = tablegrid($xBufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xBufResult . '</div>';
    }

    function setpdf() {
        $this->load->helper('html');
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('topdf');

        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xisblokir = $_POST['edisblokir'];
        $xidjenismember = $_POST['edidjenismember'];

        $html = '<html>
				<header>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" . '
				</header>
				<body>
					<p>
						<div id="report">
						<div id="tabledata">
							' . $this->showtbdt($xisblokir, $xidjenismember, $date_awal, $date_akhir) . '
						</div>
						</div>
					</p>
				</body>
			</html>';

        $idpegawai = $this->session->userdata('idpegawai');
//        die($html);
        pdf_create($html, 'laporan_member_' . $idpegawai);
        $xbufresult = '<object data="' . base_url() . 'resource/pdf/laporan_member_' . $idpegawai . '.pdf" type="application/pdf" width="1200px" height = "600px" type="left:-15px;" >
                          </object>';
        $this->json_data['data'] = $xbufresult;
        echo json_encode($this->json_data);
    }

    function carilaporan_byrange() {
        $this->load->helper('common');
        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xisblokir = $_POST['edisblokir'];
        $xidjenismember = $_POST['edidjenismember'];

        $strHTML = $this->showtbdt($xisblokir, $xidjenismember, $date_awal, $date_akhir);
        $this->load->helper('json');
        $this->json_data['tblaporanmember'] = $strHTML;
        echo json_encode($this->json_data);
    }

    function exportkeexcel($xisblokir, $xidjenismember, $date_awal, $date_akhir) {
        $this->load->helper('html');
        $this->load->helper('common');
        $nmfile = 'laporanmember';
        $date_awal = datetomysql($date_awal);
        $date_akhir = datetomysql($date_akhir);
        $xhtml = $this->showtbdt($xisblokir, $xidjenismember, $date_awal, $date_akhir);
        $xbufresult = header("Content-type: application/octet-stream") . "\n" .
                header("Content-Disposition: attachment; filename=" . $nmfile . ".xls") . "\n" .
                header("Pragma: no-cache") . "\n" .
                header("Expires: 0");
        $this->load->helper('html');
        $xbufresult .= '<html><head><style type=\'text/css\'>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" .
                '</head><body>' . $xhtml . '</body></html>';
        echo $xbufresult;
    }

}
