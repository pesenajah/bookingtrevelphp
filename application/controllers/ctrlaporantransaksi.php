<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ctrlaporantransaksi
 *
 * @author admindatakreasi
 */
class ctrlaporantransaksi extends CI_Controller {

    function __construct() {
        parent :: __construct();
    }

    function index() {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '';
        $xAddJs.= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxlaporantransaksi.js"></script>';
        if (!empty($idpegawai)) {
            echo $this->modelgetmenu->SetViewAdmin($this->createviewlap(), '<div class="spacer"></div><div id="browsepdf"></div>', '', $xAddJs, '');
        } else {
            die("Anda belum Login");
        }
    }

    function createviewlap() {
        $this->load->helper('form');
        $this->load->helper('common');
        $xBufResult = '';
        $xBufResult .= '<div id="stylized" class="myform">';
        $xBufResult .= '<h3>Laporan Transaksi</h3><div class="spacer"></div><div class="garis"></div>';
        $xBufResult .= setForm('edTglMulai', 'Tanggal Awal', form_input(getArrayObj('edTglMulai', '', '200')));
        $xBufResult .= setForm('edTglSelesai', 'Tanggal Akhir', form_input(getArrayObj('edTglSelesai', '', '200'))) . '<div class="spacer"></div>';
        $xArrstatustransaksi[0] = 'Semua';
        $xArrstatustransaksi[1] = 'Belum dibayar';
        $xArrstatustransaksi[2] = 'Sudah dibayar';
        $xArrstatustransaksi[3] = 'Batal';
        $xBufResult .= setForm('edidstatus', 'Status Transaksi', form_dropdown('edidstatus', $xArrstatustransaksi, '', 'id="edidstatus" style = "width:200px"')) . '<div class="spacer"></div>';
        $xBufResult .= form_button('TampilData', '<span class="btnright">Tampil Data</span>', 'onclick="doshowlaporantransaksi();" class="btn"');
        $xBufResult .= form_button('SendToPdf', '<span class="btnright">Send To Pdf</span>', 'onclick="setpdflaporantransaksi();" class="btn"');
        $xBufResult .= form_button('ExportToExcel', '<span class="btnright">Export To Excel</span>', 'onclick="exportkeexcel();" class="btn"');
        $xBufResult .= '<div class="spacer"></div>' . '<div class="garis"></div>';
        $xBufResult .= '<div id="gbloader"><div>Proses Membaca Data </div> <img src="' . base_url() . 'resource/imgbtn/ajax-loader.gif"></div>';
        $xBufResult .= '<div id="tabledata" name="tabledata">';
        $xBufResult .= '<div id="tblaporantransaksi" name="tblaporantransaksi">';
        $xBufResult .= '</div>';
        $xBufResult .= '</div>';
        return $xBufResult;
    }

    function showtbdt($xidstatus, $date_awal = '', $date_akhir = '') {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modeltransaksi');
        $this->load->model('modelmember');
        $xBufResult = tbaddrow(
                tbaddcell('<font color="#000">No</font>', '', 'width=5% rowspan=2') .
                tbaddcell('<font color="#000">Tanggal Booking</font>', '', 'width=5% rowspan=2') .
                tbaddcell('<font color="#000">Member</font>', '', 'width=15% rowspan=2') .
                tbaddcell('<font color="#000">Detail Booking</font>', '', 'width=70% colspan=5') .
                tbaddcell('<font color="#000">Status Transaksi</font>', '', 'width=5% rowspan=2'), 'background:#ffffff;', TRUE);
        $xBufResult .= tbaddrow(
                tbaddcell('<font color="#000">Produk</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Detail Produk</font>', '', 'width=15%') .
                tbaddcell('<font color="#000">Tanggal Dari</font>', '', 'width=15%') .
                tbaddcell('<font color="#000">Tanggal Sampai</font>', '', 'width=15%') .
                tbaddcell('<font color="#000">Status Booking</font>', '', 'width=15%'), 'background:#ffffff;', TRUE);
        $xResult = $this->modeltransaksi->getListtransaksibydatestatus($xidstatus, $date_awal, $date_akhir);
        $no = 1;
        $xBufResultdata = '';
        foreach ($xResult->result() as $row) {
            $arraytgljambooking = explode(' ', $row->tglbooking);
            $tgljambooking = datetomysql($arraytgljambooking[0]) . ' ' . $arraytgljambooking[1];

            if ($row->tglbatalbooking !== '0000-00-00 00:00:00') {
                $xstatus = "Transaksi Batal";
            } else {
                if ($row->tglbayar !== '0000-00-00') {
                    $xstatus = "Sudah Bayar";
                } else {
                    $xstatus = "Belum Bayar";
                }
            }

            $xBufResultdata .= tbaddrow(tbaddcell($no++) .
                    tbaddcell($tgljambooking) .
                    tbaddcell($this->modelmember->getDetailmember($row->idmember)->Nama) .
                    tbaddcell($this->getExplodeProduk($row->idx)) .
                    tbaddcell($this->getExplodeDetailProduk($row->idx)) .
                    tbaddcell($this->getExplodeTanggalDari($row->idx)) .
                    tbaddcell($this->getExplodeTanggalSampai($row->idx)) .
                    tbaddcell($this->getExplodeStatusBooking($row->idx)) .
                    tbaddcell($xstatus));
        }
        if ($xBufResultdata == '') {
            $xBufResult .= tbaddrow(tbaddcell("TIDAK ADA DATA", '', 'align="center" colspan="9"'));
        } else {
            $xBufResult .= $xBufResultdata;
        }
        $xBufResult = tablegrid($xBufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xBufResult . '</div>';
    }

    function getExplodeProduk($xidtransaksi) {
        $this->load->model('modeltransaksi');
        $this->load->model('modelproduk');
        $this->load->helper('common');

        $hasilPerintah = $this->modeltransaksi->getListbooking($xidtransaksi);

        $xBufResult = '<table width="100%">';
        $i = 1;
        foreach ($hasilPerintah->result() as $row) {
            $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="70%" align="left">' . '' . @$this->modelproduk->getDetailproduk($row->idproduk)->JudulProduk . '</td></tr>';
        }
        return $xBufResult . '</table>';
    }

    function getExplodeDetailProduk($xidtransaksi) {
        $this->load->model('modeltransaksi');
        $this->load->model('modeldetailproduk');
        $this->load->helper('common');

        $hasilPerintah = $this->modeltransaksi->getListbooking($xidtransaksi);

        $xBufResult = '<table width="100%">';
        $i = 1;
        foreach ($hasilPerintah->result() as $row) {
            $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="70%" align="left">' . '' . @$this->modeldetailproduk->getDetaildetailproduk($row->iddetailproduk)->juduldetailproduk . '</td></tr>';
        }
        return $xBufResult . '</table>';
    }

    function getExplodeTanggalDari($xidtransaksi) {
        $this->load->model('modeltransaksi');
        $this->load->helper('common');

        $hasilPerintah = $this->modeltransaksi->getListbooking($xidtransaksi);

        $xBufResult = '<table width="100%">';
        $i = 1;
        foreach ($hasilPerintah->result() as $row) {
            $tgl = datetomysql($row->tglperuntukandari);

            $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="70%" align="left">' . '' . $tgl . '</td></tr>';
        }
        return $xBufResult . '</table>';
    }

    function getExplodeTanggalSampai($xidtransaksi) {
        $this->load->model('modeltransaksi');
        $this->load->helper('common');

        $hasilPerintah = $this->modeltransaksi->getListbooking($xidtransaksi);

        $xBufResult = '<table width="100%">';
        $i = 1;
        foreach ($hasilPerintah->result() as $row) {
            $tgl = datetomysql($row->tglperuntukansampai);

            $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="70%" align="left">' . '' . $tgl . '</td></tr>';
        }
        return $xBufResult . '</table>';
    }

    function getExplodeStatusBooking($xidtransaksi) {
        $this->load->model('modeltransaksi');
        $this->load->helper('common');

        $hasilPerintah = $this->modeltransaksi->getListbooking($xidtransaksi);

        $xBufResult = '<table width="100%">';
        $i = 1;
        foreach ($hasilPerintah->result() as $row) {
            if ($row->status === '0') {
                $status = "Tanpa konfirmasi";
            } else if ($row->status === '1') {
                $status = "Dengan konfirmasi";
            } else if ($row->status === '2') {
                $status = "Dibatalkan";
            } else if ($row->status === '3') {
                $status = "Sudah dibayar";
            }
            $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="70%" align="left">' . '' . $status . '</td></tr>';
        }
        return $xBufResult . '</table>';
    }

    function setpdf() {
        $this->load->helper('html');
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('topdf');

        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xstatusin = $_POST['edidstatus'];

        $html = '<html>
				<header>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" . '
				</header>
				<body>
					<p>
						<div id="report">
						<div id="tabledata">
							' . $this->showtbdt($xstatusin, $date_awal, $date_akhir) . '
						</div>
						</div>
					</p>
				</body>
			</html>';

        $idpegawai = $this->session->userdata('idpegawai');
//        die($html);
        pdf_create($html, 'laporan_transaksi_' . $idpegawai);
        $xbufresult = '<object data="' . base_url() . 'resource/pdf/laporan_transaksi_' . $idpegawai . '.pdf" type="application/pdf" width="1200px" height = "600px" type="left:-15px;" >
                          </object>';


        $this->json_data['data'] = $xbufresult;
        echo json_encode($this->json_data);
    }

    function carilaporan_byrange() {
        $this->load->helper('common');
        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xstatusin = $_POST['edidstatus'];

        $strHTML = $this->showtbdt($xstatusin, $date_awal, $date_akhir);
        $this->load->helper('json');
        $this->json_data['tblaporantransaksi'] = $strHTML;
        echo json_encode($this->json_data);
    }

    function exportkeexcel($xstatusin, $date_awal, $date_akhir) {
        $this->load->helper('html');
        $this->load->helper('common');
        $nmfile = 'laporantransaksi';
        if (!empty($date_awal) && !empty($date_akhir)) {
            $date_awal = datetomysql($date_awal);
            $date_akhir = datetomysql($date_akhir);
            $xhtml = $this->showtbdt($xstatusin, $date_awal, $date_akhir);
            $xbufresult = header("Content-type: application/octet-stream") . "\n" .
                    header("Content-Disposition: attachment; filename=" . $nmfile . ".xls") . "\n" .
                    header("Pragma: no-cache") . "\n" .
                    header("Expires: 0");
            $this->load->helper('html');
            $xbufresult .= '<html><head><style type=\'text/css\'>' .
                    link_tag('resource/css/admin/frmlayout.css') . "\n" .
                    '</head><body>' . $xhtml . '</body></html>';
            echo $xbufresult;
        } else {
            echo null;
        }
    }

}
