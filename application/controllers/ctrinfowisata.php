<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : infowisata  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrinfowisata extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createforminfowisata('0', $xAwal);
    }

    function createforminfowisata($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/css/admin/gmap.css') . "\n" .
                link_tag('resource/css/admin/upload/css/upload.css') . "\n" .
                link_tag('resource/js/jquery/themes/base/jquery.ui.all.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/jQuery.angka.js"></script>' . "\n" .
//                '<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>'.
                '<script src="http://maps.google.com/maps/api/js?libraries=places"></script>' .
//                '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGc4vt-6RtxAEmsuxBNldxm4YgYO9qvXk&callback=initMap" async defer></script>'.
                '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-7YB22OHr5YyQwY_zNk7OtZfSpnizcsw&callback=initMap" async defer></script>' .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/gmapv3.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/tiny_mce/tiny_mce_src.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/tiny_mce/jquery.tinymce.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxmce2.js"></script>' . "\n" .
                '<script language=javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/baseurl.js"></script>' .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.knob.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.iframe-transport.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.fileupload.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/myuploadphoto.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxinfowisata.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailForminfowisata($xidx), '', '', $xAddJs, '');
    }

    function setDetailForminfowisata($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Info Wisata</h3><div class="garis"></div>' . form_open_multipart('ctrinfowisata/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edjudulinfo', 'Nama Objek Wisata', form_input(getArrayObj('edjudulinfo', '', '600'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('eddeskripsihtml', 'Deskripsi Wisata', '');
        $xBufResult .= setForm('eddeskripsihtml', '', form_textarea(getArrayObj('eddeskripsihtml', '', '600'), '', 'class="tinymce"')) . '<div class="spacer"></div><br>';

        $xBufResult .= '<div id="uploadcover" style="position:relative;left:150px;">';
        $xBufResult .= '  <input type="input" name="edimgutama" id="edimgutama" alt="upload image"/>';
        $xBufResult .= '</div>' . '<div class="spacer"></div><br>';

//        $xBufResult .= setForm('edimgutama', 'imgutama', form_input(getArrayObj('edimgutama', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div id="mapkoordinat" class="maparea">'
                . '</div>' . '<div class="spacer"></div><br>';
        $xBufResult .= setForm('edmapadress', 'Map Address', form_input(getArrayObj('edmapaddress', '', '400'))) . '<div class="spacer"></div>';


//        $xBufResult .= setForm('edtglinsert', 'tglinsert', form_input(getArrayObj('edtglinsert', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglupdate', 'tglupdate', form_input(getArrayObj('edtglupdate', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edidpegawai', 'idpegawai', form_input(getArrayObj('edidpegawai', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpaninfowisata();"') . form_button('btNew', 'new', 'onclick="doClearinfowisata();"') . '<div class="spacer"></div><div id="tabledatainfowisata">' . $this->getlistinfowisata(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistinfowisata($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('No', '', 'width=3%') .
                tbaddcell('Nama Objek Wisata', '', 'width=20%') .
                tbaddcell('Deskripsi Wisata', '', 'width=40%') .
                tbaddcell('Gambar', '', 'width=10%') .
                tbaddcell('Map Address', '', 'width=10%') .
//                tbaddcell('tglinsert', '', 'width=10%') .
//                tbaddcell('tglupdate', '', 'width=10%') .
//                tbaddcell('idpegawai', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelinfowisata');
        $xQuery = $this->modelinfowisata->getListinfowisata($xAwal, $xLimit, $xSearch);
        $ino = 1 + $xAwal;
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditinfowisata(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusinfowisata(\'' . $row->idx . '\',\'' . substr($row->judulinfo, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($ino++) .
                    tbaddcell($row->judulinfo) .
                    tbaddcell($row->deskripsihtml) .
                    tbaddcell($row->imgutama) .
                    tbaddcell($row->mapadress) .
//                    tbaddcell($row->tglinsert) .
//                    tbaddcell($row->tglupdate) .
//                    tbaddcell($row->idpegawai) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchinfowisata(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchinfowisata(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchinfowisata(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function getlistinfowisataAndroid() {
        $this->load->helper('json');
//        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['judulinfo'] = "";
        $this->json_data['deskripsihtml'] = "";
        $this->json_data['imgutama'] = "";
        $this->json_data['mapadress'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['idpegawai'] = "";
        $response = array();
        $this->load->model('modelinfowisata');
        $xQuery = $this->modelinfowisata->getListinfowisata($xAwal, $xLimit);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['judulinfo'] = $row->judulinfo;
            $this->json_data['deskripsihtml'] = $row->deskripsihtml;
            $this->json_data['imgutama'] = $this->data_uri($row->imgutama);
            $this->json_data['mapadress'] = $row->mapadress;
//            $mapaddress = $this->getAddressProduk($row->mapaddress);
//            $this->json_data['alamat'] = $mapaddress;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['tglupdate'] = $row->tglupdate;
            $this->json_data['idpegawai'] = $row->idpegawai;
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpaninfowisataAndroid() {
        $xidx = $_POST['edidx'];
        $xjudulinfo = $_POST['edjudulinfo'];
        $xdeskripsihtml = $_POST['eddeskripsihtml'];
        $ximgutama = $_POST['edimgutama'];
        $xmapadress = $_POST['edmapadress'];
        $xtglinsert = $_POST['edtglinsert'];
        $xtglupdate = $_POST['edtglupdate'];
        $xidpegawai = $_POST['edidpegawai'];

        $this->load->helper('json');
        $this->load->model('modelinfowisata');
        $response = array();
        if ($xidx != '0') {
            $this->modelinfowisata->setUpdateinfowisata($xidx, $xjudulinfo, $xdeskripsihtml, $ximgutama, $xmapadress, $xtglinsert, $xtglupdate, $xidpegawai);
        } else {
            $this->modelinfowisata->setInsertinfowisata($xidx, $xjudulinfo, $xdeskripsihtml, $ximgutama, $xmapadress, $xtglinsert, $xtglupdate, $xidpegawai);
        }
        $row = $this->modelinfowisata->getLastIndexinfowisata();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['judulinfo'] = $row->judulinfo;
        $this->json_data['deskripsihtml'] = $row->deskripsihtml;
        $this->json_data['imgutama'] = $row->imgutama;
        $this->json_data['mapadress'] = $row->mapadress;
        $this->json_data['tglinsert'] = $row->tglinsert;
        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['idpegawai'] = $row->idpegawai;
        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrecinfowisata() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelinfowisata');
        $row = $this->modelinfowisata->getDetailinfowisata($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['judulinfo'] = $row->judulinfo;
        $this->json_data['deskripsihtml'] = $row->deskripsihtml;
        $this->json_data['imgutama'] = $row->imgutama;
        $this->json_data['mapadress'] = $row->mapadress;
        $this->json_data['tglinsert'] = $row->tglinsert;
        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['idpegawai'] = $row->idpegawai;
        echo json_encode($this->json_data);
    }

    function deletetableinfowisata() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelinfowisata');
        $this->modelinfowisata->setDeleteinfowisata($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchinfowisata() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatainfowisata'] = $this->getlistinfowisata($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpaninfowisata() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xjudulinfo = $_POST['edjudulinfo'];
        $xdeskripsihtml = $_POST['eddeskripsihtml'];
        $ximgutama = $_POST['edimgutama'];
        $xmapadress = $_POST['edmapadress'];
        $xtglinsert = $_POST['edtglinsert'];
        $xtglupdate = $_POST['edtglupdate'];
        $xidpegawai = $_POST['edidpegawai'];
        $this->load->model('modelinfowisata');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelinfowisata->setUpdateinfowisata($xidx, $xjudulinfo, $xdeskripsihtml, $ximgutama, $xmapadress, $xtglinsert, $xtglupdate, $xidpegawai);
            } else {
                $xStr = $this->modelinfowisata->setInsertinfowisata($xidx, $xjudulinfo, $xdeskripsihtml, $ximgutama, $xmapadress, $xtglinsert, $xtglupdate, $xidpegawai);
            }
        }
        echo json_encode(null);
    }

    function data_uri($fileuri) {
        $mime = 'image/jpeg';
        $imgsrc = base_url() . 'resource/imgbtn/ic_logo_usd.png';

        if (!empty($fileuri)) {

            $fileuri = str_replace(" ", "%20", $fileuri);

            $imgsrc = base_url() . 'resource/uploaded/img/' . $fileuri;

            //   }
        }

        return $imgsrc;
    }

    function getAddressProduk($latLng_y) {
        $latLng_x = explode(" ", $latLng_y);
        $mapaddress = "-";
        if (sizeof($latLng_x) > 1) {
            $latLng = $latLng_x[0] . $latLng_x[1];
            $geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latLng . '&sensor=false');
            $output = json_decode($geocode);
            $response = array();
            foreach ($output->results[0]->address_components as $addressComponet) {
                if (in_array('political', $addressComponet->types)) {
                    $response[] = $addressComponet->long_name;
                }
            }
            $Address = $response[0];
            $City = $response[1];
            $State = $response[2];
            $mapaddress = $Address . ", " . $City . ", " . $State;
        }
        return $mapaddress;
    }

        function getinfowisataAndroid() {
        $this->load->helper('json');
//        $xSearch = $_POST['search'];
        $xidx = $_POST['idx'];
//        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['judulinfo'] = "";
        $this->json_data['deskripsihtml'] = "";
        $this->json_data['imgutama'] = "";
        $this->json_data['mapadress'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['idpegawai'] = "";
        $response = array();
        $this->load->model('modelinfowisata');
        $row = $this->modelinfowisata->getDetailinfowisata($xidx);;
        
            $this->json_data['idx'] = $row->idx;
            $this->json_data['judulinfo'] = $row->judulinfo;
            $this->json_data['deskripsihtml'] = $row->deskripsihtml;
            $this->json_data['imgutama'] = $this->data_uri($row->imgutama);
            $this->json_data['mapadress'] = $row->mapadress;
//            $mapaddress = $this->getAddressProduk($row->mapaddress);
//            $this->json_data['alamat'] = $mapaddress;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['tglupdate'] = $row->tglupdate;
            $this->json_data['idpegawai'] = $row->idpegawai;
            array_push($response, $this->json_data);
        
//        if (empty($response)) {
//            array_push($response, $this->json_data);
//        }
        echo json_encode($response);
}

//    function getgambarAndroid() {
//        $this->load->helper('json');
////        $xSearch = $_POST['search'];
//        $xjudulinfo = $_POST['judulinfo'];
////        $xLimit = $_POST['limit'];
//        $this->load->helper('form');
//        $this->load->helper('common');
//        
//        $this->json_data['imgutama'] = "";
//        
//        $response = array();
//        $this->load->model('modelinfowisata');
//        $row = $this->modelinfowisata->getDetailimagedetailbytitle($xjudulinfo);
//        
//            
//            $this->json_data['imgutama'] = $this->data_uri($row->imgutama);
//            
//            array_push($response, $this->json_data);
//        
////        if (empty($response)) {
////            array_push($response, $this->json_data);
////        }
//        echo json_encode($response);
//    }
    
    function getgambarinfowisataAndroid() {
        $this->load->helper('json');
//        $xSearch = $_POST['search'];
        $xjudulinfo = $_POST['judulinfo'];
//        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['judulinfo'] = "";
        $this->json_data['deskripsihtml'] = "";
        $this->json_data['imgutama'] = "";
        $this->json_data['mapadress'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['idpegawai'] = "";
        $response = array();
        $this->load->model('modelinfowisata');
        $row = $this->modelinfowisata-> getgambarinfowisata($xjudulinfo);
        
            $this->json_data['idx'] = $row->idx;
            $this->json_data['judulinfo'] = $row->judulinfo;
            $this->json_data['deskripsihtml'] = $row->deskripsihtml;
            $this->json_data['imgutama'] = $this->data_uri($row->imgutama);
            $this->json_data['mapadress'] = $row->mapadress;
//            $mapaddress = $this->getAddressProduk($row->mapaddress);
//            $this->json_data['alamat'] = $mapaddress;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['tglupdate'] = $row->tglupdate;
            $this->json_data['idpegawai'] = $row->idpegawai;
            array_push($response, $this->json_data);
        
//        if (empty($response)) {
//            array_push($response, $this->json_data);
//        }
        echo json_encode($response);
    }

}
?>