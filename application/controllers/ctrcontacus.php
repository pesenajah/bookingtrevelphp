<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : contacus  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrcontacus extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformcontacus('0', $xAwal);
    }

    function createformcontacus($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxcontacus.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormcontacus($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormcontacus($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>contacus</h3><div class="garis"></div>' . form_open_multipart('ctrcontacus/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edNama', 'Nama', form_input(getArrayObj('edNama', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edalamat', 'alamat', form_input(getArrayObj('edalamat', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('ednotelpon', 'notelpon', form_input(getArrayObj('ednotelpon', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edemail', 'email', form_input(getArrayObj('edemail', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edisi', 'isi', form_input(getArrayObj('edisi', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglisi', 'tglisi', form_input(getArrayObj('edtglisi', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpancontacus();"') . form_button('btNew', 'new', 'onclick="doClearcontacus();"') . '<div class="spacer"></div><div id="tabledatacontacus">' . $this->getlistcontacus(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistcontacus($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=10%') .
                tbaddcell('Nama', '', 'width=10%') .
                tbaddcell('alamat', '', 'width=10%') .
                tbaddcell('notelpon', '', 'width=10%') .
                tbaddcell('email', '', 'width=10%') .
                tbaddcell('isi', '', 'width=10%') .
                tbaddcell('tglisi', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelcontacus');
        $xQuery = $this->modelcontacus->getListcontacus($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditcontacus(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapuscontacus(\'' . $row->idx . '\',\'' . substr($row->Nama, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->Nama) .
                    tbaddcell($row->alamat) .
                    tbaddcell($row->notelpon) .
                    tbaddcell($row->email) .
                    tbaddcell($row->isi) .
                    tbaddcell($row->tglisi) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchcontacus(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchcontacus(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchcontacus(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editreccontacus() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelcontacus');
        $row = $this->modelcontacus->getDetailcontacus($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['Nama'] = $row->Nama;
        $this->json_data['alamat'] = $row->alamat;
        $this->json_data['notelpon'] = $row->notelpon;
        $this->json_data['email'] = $row->email;
        $this->json_data['isi'] = $row->isi;
        $this->json_data['tglisi'] = $row->tglisi;
        echo json_encode($this->json_data);
    }

    function deletetablecontacus() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelcontacus');
        $this->modelcontacus->setDeletecontacus($edidx);
    }

    function searchcontacus() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatacontacus'] = $this->getlistcontacus($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpancontacus() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xNama = $_POST['edNama'];
        $xalamat = $_POST['edalamat'];
        $xnotelpon = $_POST['ednotelpon'];
        $xemail = $_POST['edemail'];
        $xisi = $_POST['edisi'];
        $xtglisi = $_POST['edtglisi'];
        $this->load->model('modelcontacus');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelcontacus->setUpdatecontacus($xidx, $xNama, $xalamat, $xnotelpon, $xemail, $xisi, $xtglisi);
            } else {
                $xStr = $this->modelcontacus->setInsertcontacus($xidx, $xNama, $xalamat, $xnotelpon, $xemail, $xisi, $xtglisi);
            }
        }
    }

}

?>