<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : satuan  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrsatuan extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformsatuan('0', $xAwal);
    }

    function createformsatuan($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxsatuan.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormsatuan($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormsatuan($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Satuan</h3><div class="garis"></div>' . form_open_multipart('ctrsatuan/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edsatuan', 'Satuan', form_input(getArrayObj('edsatuan', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpansatuan();"') . form_button('btNew', 'new', 'onclick="doClearsatuan();"') . '<div class="spacer"></div><div id="tabledatasatuan">' . $this->getlistsatuan(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistsatuan($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=30%') .
                tbaddcell('Satuan', '', 'width=60%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelsatuan');
        $xQuery = $this->modelsatuan->getListsatuan($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditsatuan(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapussatuan(\'' . $row->idx . '\',\'' . substr($row->satuan, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->satuan) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchsatuan(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchsatuan(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchsatuan(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=20%') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =2'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecsatuan() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelsatuan');
        $row = $this->modelsatuan->getDetailsatuan($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['satuan'] = $row->satuan;
        echo json_encode($this->json_data);
    }

    function deletetablesatuan() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelsatuan');
        $this->modelsatuan->setDeletesatuan($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchsatuan() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatasatuan'] = $this->getlistsatuan($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpansatuan() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xsatuan = $_POST['edsatuan'];
        $this->load->model('modelsatuan');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelsatuan->setUpdatesatuan($xidx, $xsatuan);
            } else {
                $xStr = $this->modelsatuan->setInsertsatuan($xidx, $xsatuan);
            }
        }
        echo json_encode(null);
    }

}

?>