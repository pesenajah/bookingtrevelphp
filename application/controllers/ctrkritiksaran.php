<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ctrkritiksaran
 *
 * @author admindatakreasi
 */
class ctrkritiksaran extends CI_Controller {

    function __construct() {
        parent :: __construct();
    }

    function index() {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '';
        $xAddJs.= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxkritiksaran.js"></script>';
        if (!empty($idpegawai)) {
            echo $this->modelgetmenu->SetViewAdmin($this->createviewlap(), '<div class="spacer"></div><div id="browsepdf"></div>', '', $xAddJs, '');
        } else {
            die("Anda belum Login");
        }
    }

    function createviewlap() {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelkritiksaran');
        $xBufResult = '';
        $xBufResult .= '<div id="stylized" class="myform">';
        $xBufResult .= '<h3>Laporan Kritik Saran</h3><div class="spacer"></div><div class="garis"></div>';
        $xBufResult .= setForm('edTglMulai', 'Tanggal Awal', form_input(getArrayObj('edTglMulai', '', '200')));
        $xBufResult .= setForm('edTglSelesai', 'Tanggal Akhir', form_input(getArrayObj('edTglSelesai', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= form_button('TampilData', '<span class="btnright">Tampil Data</span>', 'onclick="doshowlaporankritiksaran();" class="btn"');
        $xBufResult .= form_button('SendToPdf', '<span class="btnright">Send To Pdf</span>', 'onclick="setpdflaporankritiksaran();" class="btn"');
        $xBufResult .= form_button('ExportToExcel', '<span class="btnright">Export To Excel</span>', 'onclick="exportkeexcel();" class="btn"');
        $xBufResult .= '<div class="spacer"></div>' . '<div class="garis"></div>';
        $xBufResult .= '<div id="gbloader"><div >Proses Membaca Data </div> <img src="' . base_url() . 'resource/imgbtn/ajax-loader.gif"></div>';
        $xBufResult .= '<div id="tabledata" name ="tabledata">';
        $xBufResult .= '<div id="tblaporankritiksaran" name ="tblaporankritiksaran">';
        $xBufResult .= '</div>';
        $xBufResult .= '</div>';
        return $xBufResult;
    }

    function showtbdt($date_awal = '', $date_akhir = '') {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelgetmenu');
        $this->load->model('modelmember');
        $this->load->model('modelkritiksaran');
        $xBufResult = tbaddrow(
                tbaddcell('<font color="#000">No</font>', '', 'width=5% rowspan=2') .
                tbaddcell('<font color="#000">Tanggal</font>', '', 'width=15% rowspan=2') .
                tbaddcell('<font color="#000">Member</font>', '', 'width=40% colspan=2') .
                tbaddcell('<font color="#000">Kritik / Saran</font>', '', 'width=40% rowspan=2'), 'background:#ffffff;', TRUE);
        $xBufResult .= tbaddrow(
                tbaddcell('<font color="#000">No Member</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Nama Member</font>', '', 'width=30%'), 'background:#ffffff;', TRUE);
        $xResult = $this->modelkritiksaran->getListkritiksaranbydate($date_awal, $date_akhir);
        $no = 1;
        $xBufResultdata = '';
        foreach ($xResult->result() as $row) {
            $rowmember = $this->modelmember->getDetailmember($row->idmember);
            $xtglsaran = explode(' ', $row->tglsaran);
            $xtgljamsaran = datetomysql($xtglsaran[0]) . ' ' . $xtglsaran[1];
            $xBufResultdata .= tbaddrow(tbaddcell($no++) .
                    tbaddcell($xtgljamsaran) .
                    tbaddcell($rowmember->idx) .
                    tbaddcell($rowmember->Nama) .
                    tbaddcell($row->Saran));
        }
        if ($xBufResultdata == '') {
            $xBufResult .= tbaddrow(tbaddcell("TIDAK ADA DATA", '', 'align="center" colspan="7"'));
        } else {
            $xBufResult .= $xBufResultdata;
        }
        $xBufResult = tablegrid($xBufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xBufResult . '</div>';
    }

    function setpdf() {
        $this->load->helper('html');
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('topdf');

        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);

        $html = '<html>
				<header>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" . '
				</header>
				<body>
					<p>
						<div id="report">
						<div id="tabledata">
							' . $this->showtbdt($date_awal, $date_akhir) . '
						</div>
						</div>
					</p>
				</body>
			</html>';

        $idpegawai = $this->session->userdata('idpegawai');
        //die($html);
        pdf_create($html, 'laporan_kritik_saran' . $idpegawai);
        $xbufresult = '<object data="' . base_url() . 'resource/pdf/laporan_kritik_saran' . $idpegawai . '.pdf" type="application/pdf" width="1200px" height = "600px" type="left:-15px;" >
                          </object>';

        $this->json_data['data'] = $xbufresult;
        echo json_encode($this->json_data);
    }

    function carilaporan_byrange() {
        $this->load->helper('common');
        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);

        $strHTML = $this->showtbdt($date_awal, $date_akhir);
        $this->load->helper('json');
        $this->json_data['tblaporankritiksaran'] = $strHTML;
        echo json_encode($this->json_data);
    }

    function exportkeexcel($date_awal, $date_akhir) {
        $this->load->helper('html');
        $this->load->helper('common');
        $nmfile = 'laporankritiksaran';
        if (!empty($date_awal) && !empty($date_akhir)) {
            $date_awal = datetomysql($date_awal);
            $date_akhir = datetomysql($date_akhir);
            $xhtml = $this->showtbdt($date_awal, $date_akhir);
            $xbufresult = header("Content-type: application/octet-stream") . "\n" .
                    header("Content-Disposition: attachment; filename=" . $nmfile . ".xls") . "\n" .
                    header("Pragma: no-cache") . "\n" .
                    header("Expires: 0");
            $this->load->helper('html');
            $xbufresult .= '<html><head><style type=\'text/css\'>' .
                    link_tag('resource/css/admin/frmlayout.css') . "\n" .
                    '</head><body>' . $xhtml . '</body></html>';
            echo $xbufresult;
        } else {
            echo null;
        }
    }
	
	function simpankritiksaranAndro() {
        $this->load->helper('json');
        $xidx = $_POST['edidx'];
        $xidmember = $_POST['edidmember'];
        $xSaran = $_POST['edSaran'];
        $xtglsaran = $_POST['edtglsaran'];

        $this->load->model('modelkritiksaran');
        if ($xidx != '0') {
            $this->modelkritiksaran->setUpdatekritiksaran($xidx, $xidmember, $xSaran, $xtglsaran);
        } else {
            $this->modelkritiksaran->setInsertkritiksaran($xidx, $xidmember, $xSaran, $xtglsaran);
        }
        $row = $this->modelkritiksaran->getLastIndexkritiksaran();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['idmember'] = $row->idmember;
        $this->json_data['Saran'] = $row->Saran;
        $this->json_data['tglsaran'] = $row->tglsaran;
        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }
}
