<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ctrlaporanpembayaran
 *
 * @author admindatakreasi
 */
class ctrlaporanpembayaran extends CI_Controller {

    function __construct() {
        parent :: __construct();
    }

    function index() {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '';
        $xAddJs.= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxlaporanpembayaran.js"></script>';
        if (!empty($idpegawai)) {
            echo $this->modelgetmenu->SetViewAdmin($this->createviewlap(), '<div class="spacer"></div><div id="browsepdf"></div>', '', $xAddJs, '');
        } else {
            die("Anda belum Login");
        }
    }

    function createviewlap() {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modeljenispembayaran');
        $xBufResult = '';
        $xBufResult .= '<div id="stylized" class="myform">';
        $xBufResult .= '<h3>Laporan Pembayaran</h3><div class="spacer"></div><div class="garis"></div>';
        $xBufResult .= setForm('edTglMulai', 'Tanggal Awal', form_input(getArrayObj('edTglMulai', '', '200')));
        $xBufResult .= setForm('edTglSelesai', 'Tanggal Akhir', form_input(getArrayObj('edTglSelesai', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidjenispembayaran', 'Jenis Pembayaran', form_dropdown('edidjenispembayaran', $this->modeljenispembayaran->getArrayListjenispembayaranall(), '', 'id="edidjenispembayaran" style = "width:150px"')) . '<div class="spacer"></div>';
        $xBufResult .= form_button('TampilData', '<span class="btnright">Tampil Data</span>', 'onclick="doshowlaporanpembayaran();" class="btn"');
        $xBufResult .= form_button('SendToPdf', '<span class="btnright">Send To Pdf</span>', 'onclick="setpdflaporanpembayaran();" class="btn"');
        $xBufResult .= form_button('ExportToExcel', '<span class="btnright">Export To Excel</span>', 'onclick="exportkeexcel();" class="btn"');
        $xBufResult .= '<div class="spacer"></div>' . '<div class="garis"></div>';
        $xBufResult .= '<div id="gbloader"><div>Proses Membaca Data </div> <img src="' . base_url() . 'resource/imgbtn/ajax-loader.gif"></div>';
        $xBufResult .= '<div id="tabledata" name="tabledata">';
        $xBufResult .= '<div id="tblaporanpembayaran" name="tblaporanpembayaran">';
        $xBufResult .= '</div>';
        $xBufResult .= '</div>';
        return $xBufResult;
    }

    function showtbdt($xidjenispembayaran, $date_awal = '', $date_akhir = '') {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modeljenispembayaran');
        $this->load->model('modeltransaksi');
        $this->load->model('modelproduk');
        $this->load->model('modelmember');
        $xBufResult = tbaddrow(
                tbaddcell('<font color="#000">No</font>', '', 'width=5%') .
                tbaddcell('<font color="#000">Member</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Tanggal Booking</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Detail Produk</font>', '', 'width=20%') .
                tbaddcell('<font color="#000">Tanggal Bayar</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Jenis Pembayaran</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Harga</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Nominal Voucher</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Harga Setelah Voucher</font>', '', 'width=10%') .
                tbaddcell('<font color="#000">Harga Bayar</font>', '', 'width=10%'), 'background:#ffffff;', TRUE);
        $xResult = $this->modeltransaksi->getListtransaksibydatejenisbayar($xidjenispembayaran, $date_awal, $date_akhir);
        $no = 1;
        $xBufResultdata = '';
        foreach ($xResult->result() as $row) {
            $arraytgljambooking = explode(' ', $row->tglbooking);
            $tgljambooking = datetomysql($arraytgljambooking[0]) . ' ' . $arraytgljambooking[1];

            $xBufResultdata .= tbaddrow(tbaddcell($no++) .
                    tbaddcell($this->modelmember->getDetailmember($row->idmember)->Nama) .
                    tbaddcell($tgljambooking) .
                    tbaddcell($this->getExplodeDetailProduk($row->idbooking)) .
                    tbaddcell(datetomysql($row->tglbayar)) .
                    tbaddcell($this->modeljenispembayaran->getDetailjenispembayaran($row->idjenisbayar)->jenispembayaran) .
                    tbaddcell(number_format($row->hargadiscount, 0, '', '.')) .
                    tbaddcell(number_format($row->nominalvoucher, 0, '', '.')) .
                    tbaddcell(number_format((int)$row->hargadiscount-(int)$row->nominalvoucher, 0, '', '.')) .
                    tbaddcell(number_format($row->hargadibayar, 0, '', '.')));
        }
        if ($xBufResultdata == '') {
            $xBufResult .= tbaddrow(tbaddcell("TIDAK ADA DATA", '', 'align="center" colspan="10"'));
        } else {
            $xBufResult .= $xBufResultdata;
        }
        $xBufResult = tablegrid($xBufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xBufResult . '</div>';
    }

    function getExplodeDetailProduk($xidbooking) {
        $this->load->model('modeldetailproduk');
        $this->load->model('modelproduk');
        $this->load->model('modelbooking');
        $this->load->helper('common');

        $hasilPerintah = $this->modelbooking->getListdetailprodukbyidbooking($xidbooking);

        $xBufResult = '<table width="100%">';
        $i = 1;
        foreach ($hasilPerintah->result() as $row) {
            $xBufResult .= '<tr><td style="border-top:none;border-left:none;" width="5%" align="left">' . $i++ . '.</td>';
            $xBufResult .= '<td style="border-top:none;border-left:none;" width="45%" align="left">' . $this->modeldetailproduk->getDetaildetailproduk($row->iddetailproduk)->juduldetailproduk . '</td>';
            $xBufResult .= '<td style="border-top:none;border-left:none;" width="40%" align="left">' . $this->modelproduk->getDetailproduk($this->modeldetailproduk->getDetaildetailproduk($row->iddetailproduk)->idproduk)->JudulProduk . '</td>';
        }
        return $xBufResult . '</table>';
    }

    function setpdf() {
        $this->load->helper('html');
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('topdf');

        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xidjenispembayaran = $_POST['edidjenispembayaran'];

        $html = '<html>
				<header>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" . '
				</header>
				<body>
					<p>
						<div id="report">
						<div id="tabledata">
							' . $this->showtbdt($xidjenispembayaran, $date_awal, $date_akhir) . '
						</div>
						</div>
					</p>
				</body>
			</html>';

        $idpegawai = $this->session->userdata('idpegawai');
//        die($html);
        pdf_create($html, 'laporan_pembayaran_' . $idpegawai);
        $xbufresult = '<object data="' . base_url() . 'resource/pdf/laporan_pembayaran_' . $idpegawai . '.pdf" type="application/pdf" width="1200px" height = "600px" type="left:-15px;" >
                          </object>';
        $this->json_data['data'] = $xbufresult;
        echo json_encode($this->json_data);
    }

    function carilaporan_byrange() {
        $this->load->helper('common');
        $date_awal = datetomysql($_POST['edTglMulai']);
        $date_akhir = datetomysql($_POST['edTglSelesai']);
        $xidjenispembayaran = $_POST['edidjenispembayaran'];

        $strHTML = $this->showtbdt($xidjenispembayaran, $date_awal, $date_akhir);
        $this->load->helper('json');
        $this->json_data['tblaporanpembayaran'] = $strHTML;
        echo json_encode($this->json_data);
    }

    function exportkeexcel($xidjenispembayaran, $date_awal, $date_akhir) {
        $this->load->helper('html');
        $this->load->helper('common');
        $nmfile = 'laporanpembayaran';
        $date_awal = datetomysql($date_awal);
        $date_akhir = datetomysql($date_akhir);
        $xhtml = $this->showtbdt($xidjenispembayaran, $date_awal, $date_akhir);
        $xbufresult = header("Content-type: application/octet-stream") . "\n" .
                header("Content-Disposition: attachment; filename=" . $nmfile . ".xls") . "\n" .
                header("Pragma: no-cache") . "\n" .
                header("Expires: 0");
        $this->load->helper('html');
        $xbufresult .= '<html><head><style type=\'text/css\'>' .
                link_tag('resource/css/admin/frmlayout.css') . "\n" .
                '</head><body>' . $xhtml . '</body></html>';
        echo $xbufresult;
    }

}
