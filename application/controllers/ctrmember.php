<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : member  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrmember extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformmember('0', $xAwal);
    }

    function createformmember($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxmember.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormmember($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormmember($xidx) {
        $this->load->helper('form');
        $this->load->model('modeljenismember');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Member</h3><div class="garis"></div>' . form_open_multipart('ctrmember/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edNama', 'Nama', form_input(getArrayObj('edNama', '', '400'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edAlamat', 'Alamat', form_textarea(getArrayObj('edAlamat', '', '600'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edNoTelpon', 'Nomor Telepon', form_input(getArrayObj('edNoTelpon', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edemail', 'Email', form_input(getArrayObj('edemail', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edpassword', 'Password', form_input(getArrayObj('edpassword', '', '200'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edidtoken', 'Id Token', form_input(getArrayObj('edidtoken', '', '200'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglinsert', 'Tanggal Daftar', form_input(getArrayObj('edtglinsert', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidjenismember', 'Jenis Member', form_dropdown('edidjenismember', $this->modeljenismember->getArrayListjenismember(), '', 'id="edidjenismember" style = "width:100px"')) . '<div class="spacer"></div>';
        $xArrstatus['N'] = 'Tidak';
        $xArrstatus['Y'] = 'Ya';
        $xBufResult .= setForm('edisblokir', 'Status Blokir', form_dropdown('edisblokir', $xArrstatus, '', 'id="edisblokir" style = "width:100px"')) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanmember();"') . form_button('btNew', 'new', 'onclick="doClearmember();"') . '<div class="spacer"></div><div id="tabledatamember">' . $this->getlistmember(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistmember($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=5%') .
                tbaddcell('Nama', '', 'width=20%') .
                tbaddcell('Alamat', '', 'width=20%') .
                tbaddcell('No Telepon', '', 'width=10%') .
                tbaddcell('Email', '', 'width=10%') .
                tbaddcell('Password', '', 'width=10%') .
//                tbaddcell('Id Token', '', 'width=10%') .
                tbaddcell('Tanggal Daftar', '', 'width=10%') .
                tbaddcell('Jenis Member', '', 'width=5%') .
                tbaddcell('Status Blokir', '', 'width=5%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:5%;text-align:center;'), '', TRUE);
        $this->load->model('modelmember');
        $this->load->model('modeljenismember');
        $xQuery = $this->modelmember->getListmember($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditmember(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusmember(\'' . $row->idx . '\',\'' . substr($row->Nama, 0, 100) . '\');" style="border:none;">';
            $rowjenismember = $this->modeljenismember->getDetailjenismember($row->idjenismember);
            $xblokir = 'Tidak';
            if (strcmp($row->isblokir, 'Y') === 0) {
                $xblokir = 'Ya';
            }
            $arraytgljam = explode(' ', $row->tglinsert);
            $tgljaminsert = datetomysql($arraytgljam[0]) . ' ' . $arraytgljam[1];

            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->Nama) .
                    tbaddcell($row->Alamat) .
                    tbaddcell($row->NoTelpon) .
                    tbaddcell($row->email) .
                    tbaddcell(@$row->password) .
//                    tbaddcell($row->idtoken) .
                    tbaddcell($tgljaminsert) .
                    tbaddcell(@$rowjenismember->JenisMember) .
                    tbaddcell($xblokir) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchmember(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchmember(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchmember(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecmember() {
        $xIdEdit = $_POST['edidx'];
        $this->load->helper('common');
        $this->load->model('modelmember');
        $row = $this->modelmember->getDetailmember($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['Nama'] = $row->Nama;
        $this->json_data['Alamat'] = $row->Alamat;
        $this->json_data['NoTelpon'] = $row->NoTelpon;
        $this->json_data['idtoken'] = $row->idtoken;
        $this->json_data['email'] = $row->email;
        $this->json_data['password'] = $row->password;
        $this->json_data['tglinsert'] = datetomysql($row->tglinsert);
        $this->json_data['idjenismember'] = $row->idjenismember;
        $this->json_data['isblokir'] = $row->isblokir;
        echo json_encode($this->json_data);
    }

    function deletetablemember() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelmember');
        $this->modelmember->setDeletemember($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchmember() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatamember'] = $this->getlistmember($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function getlistmemberAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['Nama'] = "";
        $this->json_data['Alamat'] = "";
        $this->json_data['NoTelpon'] = "";
        $this->json_data['idtoken'] = "";
        $this->json_data['email'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['isblokir'] = "";
        $this->json_data['idjenismember'] = "";
        $response = array();
        $this->load->model('modelmember');
        $xQuery = $this->modelmember->getListmember($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['Nama'] = $row->Nama;
            $this->json_data['Alamat'] = $row->Alamat;
            $this->json_data['NoTelpon'] = $row->NoTelpon;
            $this->json_data['idtoken'] = $row->idtoken;
            $this->json_data['email'] = $row->email;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['isblokir'] = $row->isblokir;
            $this->json_data['idjenismember'] = $row->idjenismember;
            ;
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($this->json_data);
    }

    function getDetailMemberAndroid() {
        $this->load->helper('json');
        $xIdMember = $_POST['idmember'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['Nama'] = "";
        $this->json_data['Alamat'] = "";
        $this->json_data['NoTelpon'] = "";
        $this->json_data['idtoken'] = "";
        $this->json_data['email'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['isblokir'] = "";
        $this->json_data['idjenismember'] = "";
        $this->json_data['photoUrl'] = "";


        $response = array();
        $this->load->model('modelmember');
        if (!empty($xIdMember)) {
            $row = $this->modelmember->getDetailmember($xIdMember);
            if (!empty($row->idx)) {
                $this->json_data['idx'] = $row->idx;
                $this->json_data['Nama'] = $row->Nama;
                $this->json_data['Alamat'] = $row->Alamat;
                $this->json_data['NoTelpon'] = $row->NoTelpon;
                $this->json_data['idtoken'] = $row->idtoken;
                $this->json_data['email'] = $row->email;
                $this->json_data['tglinsert'] = $row->tglinsert;
                $this->json_data['isblokir'] = $row->isblokir;
                $this->json_data['idjenismember'] = $row->idjenismember;
                $this->json_data['photoUrl'] = $row->photoUrl;

                array_push($response, $this->json_data);
            }
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    /*
      function simpanmemberAndroid() {

      $xidx = $_POST['edidx'];
      $xNama = $_POST['edNama'];
      $xAlamat = $_POST['edAlamat'];
      $xNoTelpon = $_POST['edNoTelpon'];
      $xidtoken = $_POST['edidtoken'];
      $xemail = $_POST['edemail'];
      $xtglinsert = $_POST['edtglinsert'];
      $xisblokir = $_POST['edisblokir'];
      $xidjenismember = $_POST['edidjenismember'];
      $xpassword = $_POST['edpassword'];

      $this->load->helper('json');
      $this->load->model('modelmember');
      // echo "Nama ".$xNama."-->".$xidtoken;

      $response = array();
      if ($xidx != '0') {
      $this->modelmember->setUpdatemember($xidx, $xNama, $xAlamat, $xNoTelpon, $xidtoken, $xemail, $xpassword, $xtglinsert, $xidjenismember, $xisblokir);
      } else {
      $this->modelmember->setInsertmember($xidx, $xNama, $xAlamat, $xNoTelpon, $xidtoken, $xemail, $xpassword, $xtglinsert, $xidjenismember, $xisblokir);
      }
      $row = $this->modelmember->getDetailByToken($xidtoken);
      $this->json_data['idx'] = $row->idx;
      $this->json_data['Nama'] = $row->Nama;
      $this->json_data['Alamat'] = $row->Alamat;
      $this->json_data['NoTelpon'] = $row->NoTelpon;
      $this->json_data['idtoken'] = $row->idtoken;
      $this->json_data['email'] = $row->email;
      $this->json_data['tglinsert'] = $row->tglinsert;
      $this->json_data['isblokir'] = $row->isblokir;
      $this->json_data['idjenismember'] = $row->idjenismember;
      $this->json_data['password'] = $row->password;

      array_push($response, $this->json_data);

      echo json_encode($response);

      } */

    function simpanmemberAndroid() {

        $xidx = $_POST['edidx'];
        $xNama = $_POST['edNama'];
        $xAlamat = $_POST['edAlamat'];
        $xNoTelpon = $_POST['edNoTelpon'];
        $xidtoken = $_POST['edidtoken'];
        $xemail = $_POST['edemail'];
        $xtglinsert = $_POST['edtglinsert'];
        $xisblokir = $_POST['edisblokir'];
        $xidjenismember = $_POST['edidjenismember'];
        $xpassword = $_POST['edpassword'];
        $xphotoUrl = $_POST['edphotoUrl'];

        $this->load->helper('json');
        $this->load->model('modelmember');
        // echo "Nama ".$xNama."-->".$xidtoken;

        $response = array();
        if ($xidx != '0') {
            $this->modelmember->setUpdatemember($xidx, $xNama, $xAlamat, $xNoTelpon, $xidtoken, $xemail, $xpassword, $xtglinsert, $xidjenismember, $xisblokir);
        } else {
            $isaktif = $this->modelmember->getIsEksis($xemail);
            if ($isaktif) {
                $this->modelmember->setInsertmember($xidx, $xNama, $xAlamat, $xNoTelpon, $xidtoken, $xemail, $xpassword, $xtglinsert, $xidjenismember, $xisblokir, $xphotoUrl);
            } else {
                $xidtoken = '0';
            }
        }
        $row = $this->modelmember->getDetailByToken($xidtoken);
        if (!empty($row)) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['Nama'] = $row->Nama;
            $this->json_data['Alamat'] = $row->Alamat;
            $this->json_data['NoTelpon'] = $row->NoTelpon;
            $this->json_data['idtoken'] = $row->idtoken;
            $this->json_data['email'] = $row->email;
            $this->json_data['tglinsert'] = $row->tglinsert;
            $this->json_data['isblokir'] = $row->isblokir;
            $this->json_data['idjenismember'] = $row->idjenismember;
            $this->json_data['password'] = $row->password;
            array_push($response, $this->json_data);
            echo json_encode($response);
        } else {
            echo null;
        }
    }

    function simpanmember() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xNama = $_POST['edNama'];
        $xAlamat = $_POST['edAlamat'];
        $xNoTelpon = $_POST['edNoTelpon'];
        $xidtoken = $_POST['edidtoken'];
        $xemail = $_POST['edemail'];
        $xpassword = "";
        $xtglinsert = datetomysql($_POST['edtglinsert']);
        $xidjenismember = $_POST['edidjenismember'];
        $xisblokir = $_POST['edisblokir'];
        $this->load->model('modelmember');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelmember->setUpdatemember($xidx, $xNama, $xAlamat, $xNoTelpon, $xidtoken, $xemail, $xpassword, $xtglinsert, $xidjenismember, $xisblokir);
            } else {
                $xStr = $this->modelmember->setInsertmember($xidx, $xNama, $xAlamat, $xNoTelpon, $xidtoken, $xemail, $xpassword, $xtglinsert, $xidjenismember, $xisblokir);
            }
        }
        echo json_encode(null);
    }

    function updatetokenmember() {

        $xidx = $_POST['edidx'];
        $xidtoken = $_POST['edidtoken'];
        $this->load->model('modelmember');
        $this->modelmember->setUpdateToken($xidx, $xidtoken);
    }

    function setAutoComplitemember() {
        $this->load->helper('json');
        $this->load->model('modelmember');
        $edSearchmember = $_POST['edSearchmember'];

        $query = $this->modelmember->getautocompletemember($edSearchmember);

        $xdata = array();
        foreach ($query->result() as $row) {
            $idx = substr($row->idx, 3);
            $xdata[] = array('label' => $row->Nama . ' | ' . $row->email . ' | ' . $row->NoTelpon, 'value' => $row->idx);
        }
        $this->json_data['data'] = $xdata;
        echo json_encode($this->json_data);
    }

}

?>
