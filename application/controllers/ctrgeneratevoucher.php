<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : voucher  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrgeneratevoucher extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformvoucher('0', $xAwal);
    }

    function createformvoucher($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/jquery.ui.timepicker-0.0.6.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxvoucher.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormvoucher($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormvoucher($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Generate Voucher</h3><div class="garis"></div>' . form_open_multipart('ctrvoucher/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
//        $xBufResult .= setForm('edvoucher', 'Kode Voucher', form_input(getArrayObj('edvoucher', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edjumlahgenerate', 'Jumlah Generate', form_input(getArrayObj('edjumlahgenerate', '', '150'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('ednominal', 'Nominal', form_input(getArrayObj('ednominal', '', '150'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglberlakudari', 'Tanggal Berlaku Dari', form_input(getArrayObj('edtglberlakudari', '', '100')));
        $xBufResult .= setForm('edjamberlakudari', 'Jam Berlaku Dari', form_input(getArrayObj('edjamberlakudari', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglberlakusampai', 'Tanggal Berlaku Sampai', form_input(getArrayObj('edtglberlakusampai', '', '100')));
        $xBufResult .= setForm('edjamberlakusampai', 'Jam Berlaku Sampai', form_input(getArrayObj('edjamberlakusampai', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edidmember', 'Member', form_input(getArrayObj('edidmember', '', '400'))) . '<div class="spacer"></div>';
//        $xArrstatus['N'] = 'Tidak';
//        $xArrstatus['Y'] = 'Ya';
//        $xBufResult .= setForm('edisterpakai', 'Terpakai', form_dropdown('edisterpakai', $xArrstatus, '', 'id="edisterpakai" style = "width:100px"')) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglpakai', 'Tanggal Pakai', form_input(getArrayObj('edtglpakai', '', '100')));
//        $xBufResult .= setForm('edjampakai', 'Jam Pakai', form_input(getArrayObj('edjampakai', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btGenerate', 'generate', 'onclick="dogeneratevoucher();"')
//                . form_button('btNew', 'new', 'onclick="doClearvoucher();"') 
                . '<div class="spacer"></div><div id="tabledatavoucher">' . $this->getlistvoucher(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistvoucher($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=5%') .
                tbaddcell('Kode Voucher', '', 'width=10%') .
                tbaddcell('Nominal', '', 'width=10%') .
                tbaddcell('Tgl Berlaku Dari', '', 'width=10%') .
                tbaddcell('Tgl Berlaku Sampai', '', 'width=10%') .
                tbaddcell('Member', '', 'width=20%') .
                tbaddcell('Terpakai', '', 'width=10%') .
                tbaddcell('Tgl Pakai', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:5%;text-align:center;'), '', TRUE);
        $this->load->model('modelvoucher');
        $xQuery = $this->modelvoucher->getListvoucher($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $arraytgljamberlakudari = explode(' ', $row->tglberlakudari);
            $tgljamberlakudari = datetomysql($arraytgljamberlakudari[0]) . ' ' . $arraytgljamberlakudari[1];

            $arraytgljamberlakusampai = explode(' ', $row->tglberlakusampai);
            $tgljamberlakusampai = datetomysql($arraytgljamberlakusampai[0]) . ' ' . $arraytgljamberlakusampai[1];

            $arraytgljampakai = explode(' ', $row->tglpakai);
            $tgljampakai = datetomysql($arraytgljampakai[0]) . ' ' . $arraytgljampakai[1];

            $xterpakai = 'Tidak';
            if (strcmp($row->isterpakai, 'Y') === 0) {
                $xterpakai = 'Ya';
            }

            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditvoucher(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusvoucher(\'' . $row->idx . '\',\'' . substr($row->voucher, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->voucher) .
                    tbaddcell($row->nominal) .
                    tbaddcell($tgljamberlakudari) .
                    tbaddcell($tgljamberlakusampai) .
                    tbaddcell($row->idmember) .
                    tbaddcell($xterpakai) .
                    tbaddcell($tgljampakai) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchvoucher(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchvoucher(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchvoucher(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecvoucher() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelvoucher');
        $row = $this->modelvoucher->getDetailvoucher($xIdEdit);
        $this->load->helper('json');
        $this->load->helper('common');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['voucher'] = $row->voucher;
        $this->json_data['nominal'] = $row->nominal;
        $this->json_data['idmember'] = $row->idmember;
        $this->json_data['isterpakai'] = $row->isterpakai;

        $arraytgljamberlakudari = explode(' ', $row->tglberlakudari);
        $this->json_data['tglberlakudari'] = datetomysql($arraytgljamberlakudari[0]);
        $this->json_data['jamberlakudari'] = $arraytgljamberlakudari[1];

        $arraytgljamberlakusampai = explode(' ', $row->tglberlakusampai);
        $this->json_data['tglberlakusampai'] = datetomysql($arraytgljamberlakusampai[0]);
        $this->json_data['jamberlakusampai'] = $arraytgljamberlakusampai[1];

        $arraytgljampakai = explode(' ', $row->tglpakai);
        $this->json_data['tglpakai'] = datetomysql($arraytgljampakai[0]);
        $this->json_data['jampakai'] = $arraytgljampakai[1];

        echo json_encode($this->json_data);
    }

    function deletetablevoucher() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelvoucher');
        $this->modelvoucher->setDeletevoucher($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchvoucher() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatavoucher'] = $this->getlistvoucher($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpanvoucher() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }

        $xtglberlakudari = datetomysql($_POST['edtglberlakudari']);
        $xjamberlakudari = $_POST['edjamberlakudari'];
        $xtgljamberlakudari = $xtglberlakudari . ' ' . $xjamberlakudari;

        $xtglberlakusampai = datetomysql($_POST['edtglberlakusampai']);
        $xjamberlakusampai = $_POST['edjamberlakusampai'];
        $xtgljamberlakusampai = $xtglberlakusampai . ' ' . $xjamberlakusampai;

        $xtglpakai = datetomysql($_POST['edtglpakai']);
        $xjampakai = $_POST['edjampakai'];
        $xtgljampakai = $xtglpakai . ' ' . $xjampakai;

        $xvoucher = $_POST['edvoucher'];
        $xnominal = $_POST['ednominal'];
        $xidmember = $_POST['edidmember'];
        $xisterpakai = $_POST['edisterpakai'];
        $this->load->model('modelvoucher');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelvoucher->setUpdatevoucher($xidx, $xvoucher, $xnominal, $xtgljamberlakudari, $xtgljamberlakusampai, $xidmember, $xisterpakai, $xtgljampakai);
            } else {
                $xStr = $this->modelvoucher->setInsertvoucher($xidx, $xvoucher, $xnominal, $xtgljamberlakudari, $xtgljamberlakusampai, $xidmember, $xisterpakai, $xtgljampakai);
            }
        }
        echo json_encode(null);
    }

    function generatevoucher() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }

        $xjumlahgenerate = $_POST['edjumlahgenerate'];
        $i = 0;
        while ($i < $xjumlahgenerate) {
            $xtglberlakudari = datetomysql($_POST['edtglberlakudari']);
            $xjamberlakudari = $_POST['edjamberlakudari'];
            $xtgljamberlakudari = $xtglberlakudari . ' ' . $xjamberlakudari;

            $xtglberlakusampai = datetomysql($_POST['edtglberlakusampai']);
            $xjamberlakusampai = $_POST['edjamberlakusampai'];
            $xtgljamberlakusampai = $xtglberlakusampai . ' ' . $xjamberlakusampai;

//        $xtglpakai = datetomysql($_POST['edtglpakai']);
//        $xjampakai = $_POST['edjampakai'];
//        $xtgljampakai = $xtglpakai . ' ' . $xjampakai;

            $xtgljampakai = '';

            $xvoucher = substr(md5(uniqid()), 0, 10);
            $xnominal = $_POST['ednominal'];
//        $xidmember = $_POST['edidmember'];
//        $xisterpakai = $_POST['edisterpakai'];
            $xidmember = '';
            $xisterpakai = 'N';
            $this->load->model('modelvoucher');
            $rowcek = $this->modelvoucher->cekKesamaanvoucher($xvoucher);
            $idpegawai = $this->session->userdata('idpegawai');
            if (!empty($idpegawai)) {
                if (!empty($rowcek->idx)) {
                    $xvoucher = substr(md5(uniqid()), 0, 10);
                    $xStr = $this->modelvoucher->setInsertvoucher($xidx, $xvoucher, $xnominal, $xtgljamberlakudari, $xtgljamberlakusampai, $xidmember, $xisterpakai, $xtgljampakai);
                } else {
                    $xStr = $this->modelvoucher->setInsertvoucher($xidx, $xvoucher, $xnominal, $xtgljamberlakudari, $xtgljamberlakusampai, $xidmember, $xisterpakai, $xtgljampakai);
                }
            }
            $i++;
        }

        echo json_encode(null);
    }

}

?>