<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : transaksi  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrcektransaksi extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformtransaksi('0', $xAwal);
    }

    function createformtransaksi($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxcektransaksi.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormtransaksi($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormtransaksi($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Cek Transaksi Pembayaran</h3><div class="garis"></div>' . form_open_multipart('ctrcektransaksi/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= setForm('edtglawal', 'Tanggal Awal', form_input(getArrayObj('edtglawal', '', '100')));
        $xBufResult .= setForm('edtglakhir', 'Tanggal Akhir', form_input(getArrayObj('edtglakhir', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edNama', 'Nama', form_input(getArrayObj('edNama', '', '400'))) . '<div class="spacer"></div>';
        $xArrstatuskonfirmasi['0'] = 'Semua';
        $xArrstatuskonfirmasi['1'] = 'Belum Konfirmasi';
        $xArrstatuskonfirmasi['2'] = 'Sudah Konfirmasi';
        $xBufResult .= setForm('ediskonfirmasi', 'Status Konfirmasi', form_dropdown('ediskonfirmasi', $xArrstatuskonfirmasi, '', 'id="ediskonfirmasi" style = "width:200px"')) . '<div class="spacer"></div>';
        $xBufResult .= '<div id="modalformdetail"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSearch', 'Tampil Data', 'onclick="dotampiltransaksi();"') . '<div class="spacer"></div><div id="tabledatatransaksi"></div><div class="spacer"></div>';
        $xBufResult .= '<div id="tabledatabooking"></div>';
        return $xBufResult;
    }

    function getlisttransaksi($xAwal, $xtglawal, $xtglakhir, $xSearch, $xiskonfirmasi) {
        $xLimit = 20;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('No', '', 'width=5%') .
                tbaddcell('Tanggal Booking', '', 'width=10%') .
                tbaddcell('Member', '', 'width=10%') .
                tbaddcell('Permintaan Khusus', '', 'width=10%') .
                tbaddcell('Status Bayar', '', 'width=10%') .
                tbaddcell('Tanggal Bayar', '', 'width=10%') .
                tbaddcell('Tagihan', '', 'width=10%') .
                tbaddcell('Nominal Bayar', '', 'width=10%') .
                tbaddcell('Tanggal Konfirmasi', '', 'width=10%') .
                tbaddcell('Image Konfirmasi', '', 'width=10%') .
                tbaddcell('Keterangan Konfirmasi', '', 'width=10%') .
                tbaddcell('Aksi', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modeltransaksi');
        $this->load->model('modelmember');
        $this->load->model('modeljenispembayaran');
        $this->cekKeterlambatan();
        $xQuery = $this->modeltransaksi->getListtransaksibydate($xAwal, $xLimit, $xtglawal, $xtglakhir, $xSearch, $xiskonfirmasi);
        $no = 1;
        foreach ($xQuery->result() as $row) {
            $arraytgljambooking = explode(' ', $row->tglbooking);
            $tgljambooking = datetomysql($arraytgljambooking[0]) . ' ' . $arraytgljambooking[1];

            $tgljamkonfirmasi = '';
            if ($row->tglkonfirmasi !== null) {
                $arraytgljamkonfirmasi = explode(' ', $row->tglkonfirmasi);
                $tgljamkonfirmasi = datetomysql($arraytgljamkonfirmasi[0]) . ' ' . $arraytgljamkonfirmasi[1];
            }

            $xButtonBatal = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Tandai Belum Bayar" title="Tandai Belum Bayar" onclick = "dobelumbayar(\'' . $row->idx . '\',\'' . $row->tglbooking . '\',\'' . $row->idmember . '\');" width="20px" height="20px" style="border:none;">';
            $xButtonLanjut = '<img src="' . base_url() . 'resource/imgbtn/correct.jpg" alt="Tandai Sudah Bayar" title="Tandai Sudah Bayar" onclick = "dotandaibayar(\'' . $row->idx . '\',\'' . $row->tglbooking . '\',\'' . $row->idmember . '\');" width="20px" height="20px" style="border:none;">';

            if ($row->tglbayar == '0000-00-00') {
                if ($row->isfinal === 'Y') {
                    $xthemestatus = "background:red";
                    $xstatus = "Terlambat Bayar";
                    $xbuttonaksi = '';
                } else {
                    $xthemestatus = "background:yellow";
                    $xstatus = "Belum Bayar";
                    $xbuttonaksi = $xButtonLanjut;
                }
            } else {
                $xthemestatus = "background:lightgreen";
                $xstatus = "Sudah Bayar";
                $xbuttonaksi = $xButtonBatal;
            }
            
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" title="Detail Booking" onclick = "dotampildetailbooking(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
//            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapustransaksi(\'' . $row->idx . '\',\'' . substr($row->idbooking, 0, 20) . '\');" style="border:none;">';
            $xImgKonfirmasi = '<img src="' . base_url() . 'resource/uploaded/img/' . $row->imgkonfirmasi . '" onclick="doshowimage(' . $row->idx . ')" style="border:none;width:20px"/>';
            $xbufResult .= tbaddrow(tbaddcell($no++) .
                    tbaddcell($tgljambooking) .
                    tbaddcell($this->modelmember->getDetailmember($row->idmember)->Nama) .
                    tbaddcell(@$row->spesialrequest) .
                    tbaddcell($xstatus) .
                    tbaddcell(datetomysql($row->tglbayar)) .
                    tbaddcell(number_format($row->hargadiscount, 0, '', '.')) .
                    tbaddcell(number_format($row->hargadibayar, 0, '', '.')) .
                    tbaddcell($tgljamkonfirmasi) .
                    tbaddcell($xImgKonfirmasi) .
                    tbaddcell($row->konfirmasitext) .
                    tbaddcell($xButtonEdit . $xbuttonaksi), $xthemestatus);
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchtransaksi(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchtransaksi(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchtransaksi(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=3') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =9'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrectransaksi() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modeltransaksi');
        $row = $this->modeltransaksi->getDetailtransaksi($xIdEdit);

        $this->load->helper('json');
        $this->load->helper('common');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['idbooking'] = $row->idbooking;

        $arraytgljambooking = explode(' ', $row->tglbooking);
        $this->json_data['tglbooking'] = datetomysql($arraytgljambooking[0]);
        $this->json_data['jambooking'] = $arraytgljambooking[1];

        $arraytgljambatalbooking = explode(' ', $row->tglbatalbooking);
        $this->json_data['tglbatalbooking'] = datetomysql($arraytgljambatalbooking[0]);
        $this->json_data['jambatalbooking'] = $arraytgljambatalbooking[1];

        $this->json_data['keteranganbatal'] = $row->keteranganbatal;
        $this->json_data['harganormal'] = $row->harganormal;
        $this->json_data['hargadiscount'] = $row->hargadiscount;
        $this->json_data['idvoucher'] = $row->idvoucher;
        $this->json_data['idmember'] = $row->idmember;

        $this->load->model('modelmember');
        $rowmember = $this->modelmember->getDetailmember($row->idmember);
        $this->json_data['nama_member'] = @$rowmember->Nama;

        $this->json_data['idpegawai'] = $row->idpegawai;
        $this->json_data['spesialrequest'] = $row->spesialrequest;
//        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['idjenisbayar'] = $row->idjenisbayar;
        $this->json_data['tglbayar'] = datetomysql($row->tglbayar);
        echo json_encode($this->json_data);
    }

    function deletetabletransaksi() {
        $edidx = $_POST['edidx'];
        $this->load->model('modeltransaksi');
        $this->modeltransaksi->setDeletetransaksi($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchtransaksi() {
        $this->load->helper('json');
        $this->load->helper('common');
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];

        $xtglawal = '';
        $xtglakhir = '';
        if (!empty($_POST['xtglawal']) && !empty($_POST['xtglakhir'])) {
            $xtglawal = datetomysql($_POST['xtglawal']);
            $xtglakhir = datetomysql($_POST['xtglakhir']);
        }
        $xiskonfirmasi = $_POST['xiskonfirmasi'];

        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatatransaksi'] = $this->getlisttransaksi($xAwal, $xtglawal, $xtglakhir, $xSearch, $xiskonfirmasi);
        echo json_encode($this->json_data);
    }

    function tampildetailbooking() {
        $xidtransaksi = $_POST["idtransaksi"];
        $this->load->helper('json');
        $this->json_data['tabledatabooking'] = $this->getlistbooking($xidtransaksi);
        echo json_encode($this->json_data);
    }

    function getlistbooking($xidtransaksi) {
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('No', '', 'width=5%') .
                tbaddcell('Tanggal Booking', '', 'width=10%') .
                tbaddcell('Produk', '', 'width=15%') .
                tbaddcell('Detail Produk', '', 'width=15%') .
                tbaddcell('Kategori Produk', '', 'width=10%') .
                tbaddcell('Member', '', 'width=15%') .
                tbaddcell('Tanggal Peruntukan Dari', '', 'width=10%') .
                tbaddcell('Tanggal Peruntukan Sampai', '', 'width=10%'), '', TRUE);
        $this->load->model('modelbooking');
        $this->load->model('modelkategoriproduk');
        $this->load->model('modeldetailproduk');
        $this->load->model('modelproduk');
        $this->load->model('modelmember');
        $this->load->model('modeltransaksi');
        $xQuery = $this->modeltransaksi->getListbooking($xidtransaksi);
        $no = 1;
        foreach ($xQuery->result() as $row) {
            $arraytgljambooking = explode(' ', $row->tglbooking);
            $tgljambooking = datetomysql($arraytgljambooking[0]) . ' ' . $arraytgljambooking[1];

            $tglperuntukandari = datetomysql($row->tglperuntukandari);
            $tglperuntukansampai = datetomysql($row->tglperuntukansampai);

            if ($row->status === '0') {
                $xthemestatus = "";
                $xstatus = "Booking belum diproses";
            } elseif ($row->status === '1') {
                $xthemestatus = "background:yellow";
                $xstatus = "Booking belum dibayar";
            } elseif ($row->status === '2') {
                $xthemestatus = "background:red";
                $xstatus = "Booking batal";
            } else {
                $xthemestatus = "background:lightgreen";
                $xstatus = "Booking sudah dibayar";
            }

            $xbufResult .= tbaddrow(tbaddcell($no++) .
                    tbaddcell($tgljambooking) .
                    tbaddcell($this->modelproduk->getDetailproduk($row->idproduk)->JudulProduk) .
                    tbaddcell($this->modeldetailproduk->getDetaildetailproduk($row->iddetailproduk)->juduldetailproduk) .
                    tbaddcell($this->modelkategoriproduk->getDetailkategoriproduk($row->idkategoriproduk)->Kategori) .
                    tbaddcell($this->modelmember->getDetailmember($row->idmember)->Nama) .
                    tbaddcell($tglperuntukandari) .
                    tbaddcell($tglperuntukansampai), $xthemestatus);
        }
        $xbufResult .= tbaddrow(tbaddcell('', '', 'width=10% colspan=2') .
                tbaddcell('', '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function setBelumBayar() {
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            $xidx = $_POST['edidx'];
            $xidmember = $_POST['edidmember'];
            $this->load->model('modeltransaksi');
            $xstatus = 'belum';
            $this->modeltransaksi->setUpdatePembayaranbooking($xidx, $xstatus);
            $xstatusawal = '3';
            $xstatusakhir = '1';
            $this->modeltransaksi->setUpdateFinalBooking($xidx, $xstatusawal, $xstatusakhir);
        }
        $this->load->helper('json');
        echo json_encode(null);
    }

    function setSudahBayar() {
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            $xidx = $_POST['edidx'];
            $xidmember = $_POST['edidmember'];
            $this->load->model('modeltransaksi');
            $xstatus = 'sudah';
            $this->modeltransaksi->setUpdatePembayaranbooking($xidx, $xstatus);
            $xstatusawal = '1';
            $xstatusakhir = '3';
            $this->modeltransaksi->setUpdateFinalBooking($xidx, $xstatusawal, $xstatusakhir);

            $rowtransaksi = $this->modeltransaksi->getTransaksiSudahBayar($xidmember);
            if (!empty($rowtransaksi->idx)) {
                $this->load->model('modelandroidsend');

                $xStatus = 'Terima kasih, Anda telah melakukan pembayaran ID transaksi : ' . $xidx;

                $this->modelandroidsend->pushNotificationAndroid($xidmember, "Konfirmasi Pembayaran", $xStatus, "cektransaksi");
            }
        }
        $this->load->helper('json');
        echo json_encode(null);
    }

    function cekKeterlambatan() {
        $this->load->model('modeltransaksi');
        $xQuery = $this->modeltransaksi->getListtransaksiAll();
        $xidmember = '';
        foreach ($xQuery->result() as $row) {
            if ($row->tglbayar === '0000-00-00' && $row->isfinal === 'N') {
                $this->doBatalkanTransaksi($row->idx);
                $xidmember = $row->idmember;

                $rowtransaksi = $this->modeltransaksi->getTransaksiTerlambatBayar($xidmember);
                if (!empty($rowtransaksi->idx)) {
                    $this->load->model('modelandroidsend');
                    $this->load->model('modelinboxfcm');

                    $xStatus = 'Maaf, Transaksi booking Anda dengan ID : ' . $rowtransaksi->idx . ' dibatalkan karena keterlambatan pembayaran.';

                    if (!$this->modelinboxfcm->isSudahDikirim($xidmember, $xStatus)) {
                        $this->modelandroidsend->pushNotificationAndroid($xidmember, "Pembatalan Transaksi", $xStatus, "cektransaksi");
                    }
                }
            }
        }
    }

    function doBatalkanTransaksi($xidx) {
        $this->load->model('modeltransaksi');
        $this->load->model('modelbooking');
        $xketerangan = 'Terlambat bayar';
        $this->modeltransaksi->setTerlambattransaksi($xidx, $xketerangan);
        $this->modelbooking->setBatalbooking($xidx);
    }

    function doShowFormImage() {
        $this->load->helper('json');
        $this->load->model('modeltransaksi');
        $xidx = $_POST['xidx'];
        $row = $this->modeltransaksi->getDetailtransaksi($xidx);
        $this->json_data['imgkonfirmasi'] = '<img src="' . base_url() . 'resource/uploaded/img/' . $row->imgkonfirmasi . '" style="border:none;width:100%;height:100%">';
        echo json_encode($this->json_data);
    }

}

?>
