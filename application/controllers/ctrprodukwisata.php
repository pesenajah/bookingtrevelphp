<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : produk  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrprodukwisata extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformproduk('0', $xAwal);
    }

    function createformproduk($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/css/admin/gmap.css') . "\n" .
                link_tag('resource/css/admin/upload/css/upload.css') . "\n" .
                link_tag('resource/js/jquery/themes/base/jquery.ui.all.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/jQuery.angka.js"></script>' . "\n" .
//                '<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>'.
                '<script src="http://maps.google.com/maps/api/js?libraries=places"></script>' .
//                '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGc4vt-6RtxAEmsuxBNldxm4YgYO9qvXk&callback=initMap" async defer></script>'.
                '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-7YB22OHr5YyQwY_zNk7OtZfSpnizcsw&callback=initMap" async defer></script>' .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/gmapv3.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaximagedetail.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxdetailproduk.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxprodukwisata.js"></script>' . "\n" .
                '<script language="javasc" type="text/javascript" src="' . base_url() . 'resource/ajax/baseurl.js"></script>' .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/tiny_mce/tiny_mce_src.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/tiny_mce/jquery.tinymce.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxmce2.js"></script>' . "\n" .
                link_tag('resource/css/admin/upload/css/upload.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.knob.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.iframe-transport.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.fileupload.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/myuploadphoto.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/myupload.js"></script>' . "\n";

        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormproduk($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormproduk($xidx) {
        $this->load->helper('form');
        $this->load->model('modelsatuan');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Paket Wisata</h3><div class="garis"></div>' . form_open_multipart('ctrproduk/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidKategoriProdukx" id="edidKategoriProdukx" value="3" />';
        $xBufResult .= setForm('edJudulProduk', 'Nama Produk', form_input(getArrayObj('edJudulProduk', '', '500'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edDiskripsiProduk', 'Deskripsi Produk', '');
        $xBufResult .= setForm('edDiskripsiProduk', '', form_textarea(getArrayObj('edDiskripsiProduk', '', '600'), '', 'class="tinymce"')) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edidKategoriProduk', 'idKategoriProduk', form_input(getArrayObj('edidKategoriProduk', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edKeterangan', 'Keterangan', form_input(getArrayObj('edKeterangan', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edNamaKontak', 'Nama Kontak', form_input(getArrayObj('edNamaKontak', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edphonekontak', 'Phone Kontak', form_input(getArrayObj('edphonekontak', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div id="mapkoordinat" class="maparea">'
                . '</div>' . '<div class="spacer"></div>';
        $xBufResult .= setForm('edmapaddress', 'Map Address', form_input(getArrayObj('edmapaddress', '', '500'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edrate', 'Rate', form_input(getArrayObj('edrate', '', '100')));
//        $xBufResult .= setForm('edratediscount', 'Rate Discount', form_input(getArrayObj('edratediscount', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edkapasitas', 'Kapasitas', form_input(getArrayObj('edkapasitas', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edstandartpemakaian', 'Standar Pemakaian', form_input(getArrayObj('edstandartpemakaian', '', '100')));
//        $xBufResult .= setForm('edidsatuan', 'Satuan', form_dropdown('edidsatuan', $this->modelsatuan->getArrayListsatuan(), '', 'id="edidsatuan"')) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edrancode', 'rancode', form_input(getArrayObj('edrancode', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglinsert', 'tglinsert', form_input(getArrayObj('edtglinsert', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglupdate', 'tglupdate', form_input(getArrayObj('edtglupdate', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div id="modalform"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanproduk();"') . form_button('btNew', 'new', 'onclick="doClearproduk();"') . '<div class="spacer"></div><div id="tabledataproduk">' . $this->getlistproduk(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistproduk($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('No', '', 'width=5%') .
                tbaddcell('Nama Produk', '', 'width=20%') .
//                tbaddcell('idKategoriProduk', '', 'width=10%') .
//                tbaddcell('Keterangan', '', 'width=10%') .                
                tbaddcell('Deskripsi Produk', '', 'width=30%') .
                tbaddcell('Nama Kontak', '', 'width=10%') .
                tbaddcell('Phone Kontak', '', 'width=10%') .
                tbaddcell('Map Address', '', 'width=10%') .
//                tbaddcell('Rate', '', 'width=10%') .
//                tbaddcell('Rate Discount', '', 'width=10%') .
                tbaddcell('Tambah Image', '', 'width=10%') .
                tbaddcell('Detail Produk', '', 'width=10%') .
//                tbaddcell('rancode', '', 'width=10%') .
//                tbaddcell('tglinsert', '', 'width=10%') .
//                tbaddcell('tglupdate', '', 'width=10%') .
//                tbaddcell('idpegawai', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelprodukwisata');
        $xQuery = $this->modelprodukwisata->getListproduk($xAwal, $xLimit, $xSearch);
        $ino = 1 + $xAwal;
        foreach ($xQuery->result() as $row) {

            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditproduk(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xAddImageList = '<img src="' . base_url() . 'resource/imgbtn/imglist.png" alt="Edit Data" onclick = "doDetailImage(\'' . $row->idx . '\');" style="border:none;width:30px"/>';
            $xDetailProduk = '<img src="' . base_url() . 'resource/imgbtn/detailproduk.png" alt="Edit Data" onclick = "doDetailProduk(\'' . $row->idx . '\',\'' . $row->idKategoriProduk . '\');" style="border:none;width:30px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusproduk(\'' . $row->idx . '\',\'' . substr($row->JudulProduk, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($ino++) .
                    tbaddcell($row->JudulProduk) .
//                    tbaddcell($row->idKategoriProduk) .
//                    tbaddcell($row->Keterangan) .
                    tbaddcell($row->DiskripsiProduk) .
                    tbaddcell($row->NamaKontak) .
                    tbaddcell($row->phonekontak) .
                    tbaddcell($row->mapaddress) .
//                    tbaddcell($row->rate) .
//                    tbaddcell($row->ratediscount) .
                    tbaddcell($xAddImageList) .
                    tbaddcell($xDetailProduk) .
//                    tbaddcell($row->rancode) .
//                    tbaddcell($row->tglinsert) .
//                    tbaddcell($row->tglupdate) .
//                    tbaddcell($row->idpegawai) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchproduk(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchproduk(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchproduk(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecproduk() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelprodukwisata');
        $row = $this->modelprodukwisata->getDetailproduk($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['JudulProduk'] = $row->JudulProduk;
        $this->json_data['idKategoriProduk'] = $row->idKategoriProduk;
        $this->json_data['Keterangan'] = $row->Keterangan;
        $this->json_data['phonekontak'] = $row->phonekontak;
        $this->json_data['NamaKontak'] = $row->NamaKontak;
        $this->json_data['DiskripsiProduk'] = $row->DiskripsiProduk;
        $this->json_data['mapaddress'] = $row->mapaddress;
        $this->json_data['rate'] = $row->rate;
        $this->json_data['ratediscount'] = $row->ratediscount;
        $this->json_data['rancode'] = $row->rancode;
        $this->json_data['tglinsert'] = $row->tglinsert;
        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['kapasitas'] = $row->kapasitas;
        $this->json_data['standartpemakaian'] = $row->standartpemakaian;
        $this->json_data['idsatuan'] = $row->idsatuan;
        $this->json_data['idpegawai'] = $row->idpegawai;
        echo json_encode($this->json_data);
    }

    function deletetableproduk() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelprodukwisata');
        $this->modelprodukwisata->setDeleteproduk($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchproduk() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledataproduk'] = $this->getlistproduk($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpanproduk() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xJudulProduk = $_POST['edJudulProduk'];
        $xidKategoriProduk = $_POST['edidKategoriProduk'];
        $xKeterangan = $_POST['edKeterangan'];
        $xphonekontak = $_POST['edphonekontak'];
        $xNamaKontak = $_POST['edNamaKontak'];
        $xDiskripsiProduk = $_POST['edDiskripsiProduk'];
        $xmapaddress = $_POST['edmapaddress'];
        $xrate = $_POST['edrate'];
        $xratediscount = $_POST['edratediscount'];
        $xrancode = $_POST['edrancode'];
        $xtglinsert = $_POST['edtglinsert'];
        $xtglupdate = $_POST['edtglupdate'];
        $xkapasitas = $_POST['edkapasitas'];
        $xstandartpemakaian = $_POST['edstandartpemakaian'];
        $xidsatuan = $_POST['edidsatuan'];
        $xidpegawai = $_POST['edidpegawai'];
        $this->load->model('modelprodukwisata');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelprodukwisata->setUpdateproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xkapasitas, $xstandartpemakaian, $xidsatuan, $xidpegawai);
            } else {
                $xStr = $this->modelprodukwisata->setInsertproduk($xidx, $xJudulProduk, $xidKategoriProduk, $xKeterangan, $xphonekontak, $xNamaKontak, $xDiskripsiProduk, $xmapaddress, $xrate, $xratediscount, $xrancode, $xtglinsert, $xtglupdate, $xkapasitas, $xstandartpemakaian, $xidsatuan, $xidpegawai);
            }
        }
        echo json_encode(null);
    }

}

?>