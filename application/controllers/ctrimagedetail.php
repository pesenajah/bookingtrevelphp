<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : imagedetail  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrimagedetail extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformimagedetail('0', $xAwal);
    }

    function createformimagedetail($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaximagedetail.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormimagedetail($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormimagedetail($xidproduk,$xisfromproduk) {
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelproduk');
        $this->load->model('modeldetailproduk');
        $this->load->model('modelkategoriproduk');
        $xidkategoriproduk  = '';
        $xiddetailproduk  = '';
        $xJudulProduk = '';
        $xKategori = '';
//        echo $xidproduk;
//        echo $xisfromproduk;
        if(strcmp($xisfromproduk,'Y')){
            $rowproduk = $this->modelproduk->getDetailproduk($xidproduk);
            if(!empty($rowproduk->idx)){
               $xidkategoriproduk  = $rowproduk->idKategoriProduk;  
               $xJudulProduk=$rowproduk->JudulProduk;
            }
        }
//        echo $xidkategoriproduk;
        
        $rowkategori = $this->modelkategoriproduk->getDetailkategoriproduk($xidkategoriproduk);
        $xKategori=$rowkategori->Kategori;
        
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Image Produk "'.$xJudulProduk.'"</h3><div class="garis"></div>' . form_open_multipart('ctrimagedetail/inserttable', array('id' => 'form', 'name' => 'form'));
        
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= '<input type="hidden" name="idproduk" id="idproduk" value="'.$xidproduk.'" />';
        $xBufResult .= '<input type="hidden" name="isproduk" id="isproduk" value="'.$xisfromproduk.'" />';
        $xBufResult .= '<input type="hidden" name="iddetailproduk" id="iddetailproduk" value="'.$xiddetailproduk.'" />';
        $xBufResult .= '<input type="hidden" name="idkategoriproduk" id="idkategoriproduk" value="'.$xidkategoriproduk.'" />';
        
        $xBufResult .= setForm('ediddetailproduk', 'Nama Produk', form_input(getArrayObj('edNamaProduk', $xJudulProduk, '600'),'','disabled')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidkategoriproduk', 'Kategori Produk', form_input(getArrayObj('edKategoriProduk', $xKategori, '300'),'','disabled')) . '<div class="spacer"></div>';
        //$xBufResult .= setForm('edlinkimage', 'Link Image', form_input(getArrayObj('edlinkimage', '', '100'),'','alt="Upload Image"')) . '<div class="spacer"></div>';
//        $xBufResultImg  = '<div id="uploadcover" style="">';
//        $xBufResultImg .= '  <input type="input" name="edlinkimage" id="edlinkimage" alt="upload image"/>';
//        $xBufResultImg .= '</div>'. '<div class="spacer"></div>';
        
        $xBufResult .= '<div id="uploadcover" style="position:relative;left:150px;">';
        $xBufResult .= '  <input type="input" name="edlinkimage" id="edlinkimage" alt="upload image"/>';
        $xBufResult .= '</div>'. '<div class="spacer"></div>';
        
        $xBufResult .= setForm('edketeranganimage', 'Keterangan Image', form_textarea(getArrayObj('edketeranganimage', '', '500'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edrancode', 'rancode', form_input(getArrayObj('edrancode', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglinsert', 'tglinsert', form_input(getArrayObj('edtglinsert', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edtglupdate', 'tglupdate', form_input(getArrayObj('edtglupdate', '', '100'))) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('edidpegawai', 'idpegawai', form_input(getArrayObj('edidpegawai', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'Simpan', 'onclick="dosimpanimagedetail();"') . 
                                                     form_button('btNew', 'new', 'onclick="doClearimagedetail();"') . 
                        '<div class="spacer"></div><div id="tabledataimagedetail">' . $this->getlistimagedetail($xidproduk,$xisfromproduk) . '</div><div class="spacer"></div>';
        return '<div>'.$xBufResult.'</div>';
//        return '<div style="float:left;width:65%">'.$xBufResult.'</div><div style="float:left;width:30%">'.$xBufResultImg.'</div>';
    }

    function getlistimagedetail($xidproduk,$xisfromproduk) {
        
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('No', '', 'width=5%') .
//                tbaddcell('iddetailproduk', '', 'width=10%') .
//                tbaddcell('idkategoriproduk', '', 'width=10%') .
                tbaddcell('Keterangan Image', '', 'width=35%') .
                tbaddcell('Link Image', '', 'width=40%') .
                
//                tbaddcell('rancode', '', 'width=10%') .
//                tbaddcell('tglinsert', '', 'width=10%') .
//                tbaddcell('tglupdate', '', 'width=10%') .
//                tbaddcell('idpegawai', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:5%;text-align:center;'), '', TRUE);
        $this->load->model('modelimagedetail');
        $ino = 1;
        if(strcmp($xisfromproduk,'Y')){
          $xQuery = $this->modelimagedetail->getListimagedetailbyidproduk($xidproduk,$xisfromproduk);
        }else{
          $xQuery = $this->modelimagedetail->getListimagedetailbyiddetailproduk($xidproduk,$xisfromproduk);  
        }
        foreach ($xQuery->result() as $row) {
            if ($row->iddetailproduk==0) {
                $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditimagedetail(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusimagedetail(\'' . $row->idx . '\',\'' . substr($row->iddetailproduk, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($ino++) .
//                    tbaddcell($row->iddetailproduk) .
//                    tbaddcell($row->idkategoriproduk) .
                    tbaddcell($row->keteranganimage) .
                    tbaddcell($row->linkimage) .                    
//                    tbaddcell($row->rancode) .
//                    tbaddcell($row->tglinsert) .
//                    tbaddcell($row->tglupdate) .
//                    tbaddcell($row->idpegawai) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
            }
            
        }
//        $xInput = form_input(getArrayObj('edSearch', '', '200'));
//        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchimagedetail(0);" style="border:none;width:30px;height:30px;" />';
//        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchimagedetail(' . ($xAwal - $xLimit) . ');"/>';
//        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchimagedetail(' . ($xAwal + $xLimit) . ');" />';
//        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
//                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecimagedetail() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelimagedetail');
        $row = $this->modelimagedetail->getDetailimagedetail($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['iddetailproduk'] = $row->iddetailproduk;
        $this->json_data['idkategoriproduk'] = $row->idkategoriproduk;
        $this->json_data['linkimage'] = $row->linkimage;
        $this->json_data['keteranganimage'] = $row->keteranganimage;
        $this->json_data['rancode'] = $row->rancode;
        $this->json_data['tglinsert'] = $row->tglinsert;
        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['idpegawai'] = $row->idpegawai;
        echo json_encode($this->json_data);
    }

    function deletetableimagedetail() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelimagedetail');
        $this->modelimagedetail->setDeleteimagedetail($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchimagedetail() {
        $xidproduk = $_POST['idproduk'];
        $xisproduk = $_POST['isproduk'];
        $this->load->helper('json');
        $this->json_data['tabledataimagedetail'] = $this->getlistimagedetail($xidproduk, $xisproduk);
        echo json_encode($this->json_data);
    }

    function simpanimagedetail() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xidproduk = $_POST['idproduk'];
        $xisproduk = $_POST['isproduk'];
        $xiddetailproduk = $_POST['iddetailproduk'];
        $xidkategoriproduk = $_POST['idkategoriproduk'];
        $xlinkimage = $_POST['edlinkimage'];
        $xketeranganimage = $_POST['edketeranganimage'];
        $xrancode = $_POST['edrancode'];
        $xtglinsert = $_POST['edtglinsert'];
        $xtglupdate = $_POST['edtglupdate'];
        $xidpegawai = $_POST['edidpegawai'];
        $this->load->model('modelimagedetail');
        
        if(strcmp($xisproduk, "Y")){
                $xiddetailproduk ='0';                
                
        }else{ 
                $xidproduk ='0';
        }     
           
       //echo "bla-bala ".$xisproduk;         
       
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {                  
                   $xStr = $this->modelimagedetail->setUpdateimagedetail($xidx, $xiddetailproduk, $xidkategoriproduk, $xlinkimage, $xketeranganimage, $xrancode, $xtglinsert, $xtglupdate, $idpegawai,$xidproduk);                 
            } else {                
                  $xStr = $this->modelimagedetail->setInsertimagedetail($xidx, $xiddetailproduk, $xidkategoriproduk, $xlinkimage, $xketeranganimage, $xrancode, $xtglinsert, $xtglupdate, $idpegawai,$xidproduk);
                
            }
        }
        echo json_encode(null);
    }

    function doShowFormDetailImage(){
        $xIdProduk = $_POST['xIdProduk'];        
        $xisfromproduk = $_POST['isfromproduk'];        
        $this->load->helper('json');        
        $this->json_data['modaldetailproduk'] = $this->setDetailFormimagedetail($xIdProduk,$xisfromproduk);
        echo json_encode($this->json_data);
    }
	
	function getlistimagedetailAndroidbyiddetailproduk() {
        $this->load->helper('json');
        $xiddetailproduk = $_POST['iddetailproduk'];
        // $xiddetailproduk = 43;
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['iddetailproduk'] = "";
        $this->json_data['idkategoriproduk'] = "";
        $this->json_data['linkimage'] = "";
        $this->json_data['keteranganimage'] = "";
        $this->json_data['rancode'] = "";
        $this->json_data['tglinsert'] = "";
        $this->json_data['tglupdate'] = "";
        $this->json_data['idpegawai'] = "";
        $response = array();
        $this->load->model('modelimagedetail');
        $xQuery= $this->modelimagedetail->getListimagedetailbyiddetailproduk($xiddetailproduk, 'N');
        foreach ($xQuery->result() as $row) {
        $this->json_data['idx'] = $row->idx;
        $this->json_data['iddetailproduk'] = $row->iddetailproduk;
        $this->json_data['idkategoriproduk'] = $row->idkategoriproduk;
        $this->json_data['linkimage'] = $this->data_uri(@$row->linkimage);
        $this->json_data['keteranganimage'] = $row->keteranganimage;
        $this->json_data['rancode'] = $row->rancode;
        $this->json_data['tglinsert'] = $row->tglinsert;
        $this->json_data['tglupdate'] = $row->tglupdate;
        $this->json_data['idpegawai'] = $row->idpegawai;            
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }
	
    function data_uri($fileuri) {
        $mime = 'image/jpeg';
        $imgsrc = base_url() . 'resource/imgbtn/ic_logo_usd.png';

        if (!empty($fileuri)) {

           $fileuri= str_replace(" ", "%20", $fileuri);

            $imgsrc = base_url().'resource/uploaded/img/' . $fileuri;

            //   }
        }

        return $imgsrc;
    }
}

?>