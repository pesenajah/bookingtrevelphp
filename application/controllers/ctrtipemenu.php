<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : tipemenu  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrtipemenu extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformtipemenu('0', $xAwal);
    }

    function createformtipemenu($xidTipeMenu, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxtipemenu.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormtipemenu($xidTipeMenu), '', '', $xAddJs, '');
    }

    function setDetailFormtipemenu($xidTipeMenu) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>tipemenu</h3><div class="garis"></div>' . form_open_multipart('ctrtipemenu/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidTipeMenu" id="edidTipeMenu" value="0" />';
        $xBufResult .= setForm('edNmTipeMenu', 'NmTipeMenu', form_input(getArrayObj('edNmTipeMenu', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpantipemenu();"') . form_button('btNew', 'new', 'onclick="doCleartipemenu();"') . '<div class="spacer"></div><div id="tabledatatipemenu">' . $this->getlisttipemenu(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlisttipemenu($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idTipeMenu', '', 'width=10%') .
                tbaddcell('NmTipeMenu', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modeltipemenu');
        $xQuery = $this->modeltipemenu->getListtipemenu($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doedittipemenu(\'' . $row->idTipeMenu . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapustipemenu(\'' . $row->idTipeMenu . '\',\'' . substr($row->NmTipeMenu, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idTipeMenu) .
                    tbaddcell($row->NmTipeMenu) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchtipemenu(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchtipemenu(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchtipemenu(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrectipemenu() {
        $xIdEdit = $_POST['edidTipeMenu'];
        $this->load->model('modeltipemenu');
        $row = $this->modeltipemenu->getDetailtipemenu($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idTipeMenu'] = $row->idTipeMenu;
        $this->json_data['NmTipeMenu'] = $row->NmTipeMenu;
        echo json_encode($this->json_data);
    }

    function deletetabletipemenu() {
        $edidTipeMenu = $_POST['edidTipeMenu'];
        $this->load->model('modeltipemenu');
        $this->modeltipemenu->setDeletetipemenu($edidTipeMenu);
    }

    function searchtipemenu() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatatipemenu'] = $this->getlisttipemenu($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpantipemenu() {
        $this->load->helper('json');
        if (!empty($_POST['edidTipeMenu'])) {
            $xidTipeMenu = $_POST['edidTipeMenu'];
        } else {
            $xidTipeMenu = '0';
        }
        $xNmTipeMenu = $_POST['edNmTipeMenu'];
        $this->load->model('modeltipemenu');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidTipeMenu != '0') {
                $xStr = $this->modeltipemenu->setUpdatetipemenu($xidTipeMenu, $xNmTipeMenu);
            } else {
                $xStr = $this->modeltipemenu->setInserttipemenu($xidTipeMenu, $xNmTipeMenu);
            }
        }
    }

}

?>