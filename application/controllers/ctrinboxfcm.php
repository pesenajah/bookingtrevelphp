<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : inboxfcm  * di Buat oleh Diar PHP Generator  *  By Diar */

class ctrinboxfcm extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
//  $this->load->view('test.php');
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createforminboxfcm('0', $xAwal);
    }

    function createforminboxfcm($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxinboxfcm.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailForminboxfcm($xidx), '', '', $xAddJs, '');
    }

    function setDetailForminboxfcm($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>inboxfcm</h3><div class="garis"></div>' . form_open_multipart('ctrinboxfcm/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= setForm('edidmember', 'idmember', form_input(getArrayObj('edidmember', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edjudul', 'judul', form_input(getArrayObj('edjudul', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edmessage', 'message', form_input(getArrayObj('edmessage', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edtglmessage', 'tglmessage', form_input(getArrayObj('edtglmessage', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edisterbaca', 'isterbaca', form_input(getArrayObj('edisterbaca', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpaninboxfcm();"') . form_button('btNew', 'new', 'onclick="doClearinboxfcm();"') . '<div class="spacer"></div><div id="tabledatainboxfcm">' . $this->getlistinboxfcm(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistinboxfcm($xAwal, $xSearch) {
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult = tbaddrow(tbaddcell('idx', '', 'width=10%') .
                tbaddcell('idmember', '', 'width=10%') .
                tbaddcell('judul', '', 'width=10%') .
                tbaddcell('message', '', 'width=10%') .
                tbaddcell('tglmessage', '', 'width=10%') .
                tbaddcell('isterbaca', '', 'width=10%') .
                tbaddcell('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelinboxfcm');
        $xQuery = $this->modelinboxfcm->getListinboxfcm($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditinboxfcm(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusinboxfcm(\'' . $row->idx . '\',\'' . substr($row->idmember, 0, 20) . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->idmember) .
                    tbaddcell($row->judul) .
                    tbaddcell($row->message) .
                    tbaddcell($row->tglmessage) .
                    tbaddcell($row->isterbaca) .
                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
        }
        $xInput = form_input(getArrayObj('edSearch', '', '200'));
        $xButtonSearch = '<img src="' . base_url() . 'resource/imgbtn/b_view.png" alt="Search Data" onclick = "dosearchinboxfcm(0);" style="border:none;width:30px;height:30px;" />';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchinboxfcm(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchinboxfcm(' . ($xAwal + $xLimit) . ');" />';
        $xbufResult .= tbaddrow(tbaddcell($xInput . $xButtonSearch, '', 'width=10% colspan=2') .
                tbaddcell($xButtonPrev . '&nbsp&nbsp' . $xButtonNext, '', 'width=40% colspan =10'), '', TRUE);
        $xbufResult = tablegrid($xbufResult);
        return '<div class="tabledata"  style="width:100%;left:-12px;">' . $xbufResult . '</div>';
    }

    function editrecinboxfcm() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelinboxfcm');
        $row = $this->modelinboxfcm->getDetailinboxfcm($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['idmember'] = $row->idmember;
        $this->json_data['judul'] = $row->judul;
        $this->json_data['message'] = $row->message;
        $this->json_data['tglmessage'] = $row->tglmessage;
        $this->json_data['isterbaca'] = $row->isterbaca;
        echo json_encode($this->json_data);
    }

    function deletetableinboxfcm() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelinboxfcm');
        $this->modelinboxfcm->setDeleteinboxfcm($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchinboxfcm() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatainboxfcm'] = $this->getlistinboxfcm($xAwal, $xSearch);
        echo json_encode($this->json_data);
    }

    function simpaninboxfcm() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xidmember = $_POST['edidmember'];
        $xjudul = $_POST['edjudul'];
        $xmessage = $_POST['edmessage'];
        $xtglmessage = $_POST['edtglmessage'];
        $xisterbaca = $_POST['edisterbaca'];
        $this->load->model('modelinboxfcm');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelinboxfcm->setUpdateinboxfcm($xidx, $xidmember, $xjudul, $xmessage, $xtglmessage, $xisterbaca, $xidmenuandroid);
            } else {
                $xStr = $this->modelinboxfcm->setInsertinboxfcm($xidx, $xidmember, $xjudul, $xmessage, $xtglmessage, $xisterbaca, $xidmenuandroid);
            }
        }
        echo json_encode(null);
    }

    function getlistinboxfcmandroid() {
        $xidmember = $_POST['idmember'];
        $this->load->helper('json');
        $this->load->model('modelinboxfcm');
        $response = array();
        $json_data["idx"] = "0";
        $json_data["idmember"] = "";
        $json_data["judul"] = "Belum Ada Inbox";
        $json_data["message"] = "masih kosong";
        $json_data["tglmessage"] = "";
        $json_data["isterbaca"] = "";
        $json_data["idmenuandroid"] = "";

        $xQuery = $this->modelinboxfcm->getListinboxfcmandro($xidmember);
                
            foreach ($xQuery->result() as $row) {
                $json_data["idx"] = $row->idx;
                $json_data["idmember"] = $row->idmember;
                $json_data["judul"] = $row->judul;
                $json_data["message"] = $row->message;
                $json_data["tglmessage"] = $row->tglmessage;
                $json_data["isterbaca"] = $row->isterbaca;
                $json_data["idmenuandroid"] = $row->idmenuandroid;
                array_push($response,$json_data);
            }
        
            if (empty($response))
               array_push($response, $json_data); 
        
        
        echo json_encode($response);
    }
    
    function isAdapesanAnggota(){
      $xidmember = $_POST['idmember'];
      $this->load->helper('json');
      $this->load->model('modelinboxfcm');  
      
        $rowinbox = $this->modelinboxfcm->getDetailinboxfcmIsAdapesan($xidmember);
        $this->json_data["idx"] = "0";
        $this->json_data["idmember"] = "";
        $this->json_data["judul"] = "Belum Ada Inbox";
        $this->json_data["message"] = "masih kosong";
        $this->json_data["tglmessage"] = "";
        $this->json_data["isterbaca"] = "";
        $this->json_data["idmenuandroid"] = "";
        
      if(!empty($rowinbox->idx)){
        $this->json_data["idx"] = $rowinbox->idx;
        $this->json_data["idmember"] = $rowinbox->idmember;
        $this->json_data["judul"] = $rowinbox->judul;
        $this->json_data["message"] = $rowinbox->message;
        $this->json_data["tglmessage"] = $rowinbox->tglmessage;
        $this->json_data["isterbaca"] = $rowinbox->isterbaca;        
        $this->json_data["idmenuandroid"] = $rowinbox->idmenuandroid;
      }
     echo json_encode($this->json_data);       
    }
    
function setinboxfcmandroidterbaca() {
        $this->load->helper('json');
        $xidx = $_POST['idx'];
        $this->load->model('modelinboxfcm');  
        $this->modelinboxfcm->setUpdateinboxfcmTerbaca($xidx);
        $this->json_data["isterbaca"] = "Y";        
        echo json_encode($this->json_data);       
        }
}

?>