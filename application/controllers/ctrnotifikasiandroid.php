<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : notifikasiandroid  * di Buat oleh Diar PHP Generator * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class ctrnotifikasiandroid extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        } $this->session->set_userdata('awal', $xAwal);
        $this->createformnotifikasiandroid('0', $xAwal);
    }

    function createformnotifikasiandroid($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxnotifikasiandroid.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormnotifikasiandroid($xidx), '', '', $xAddJs, '');
    }

    function setDetailFormnotifikasiandroid($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform"><h3>Notifikasi Android</h3><div class="garis"></div>' . form_open_multipart('ctrnotifikasiandroid/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
//        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= '<div id="gbloader"><div >Mengirim Notifikasi</div> <img src="' . base_url() . 'resource/imgbtn/ajax-loader.gif"></div>';

        $xBufResult .= '<input type="hidden" name="edidmember" id="edidmember" value="0" />';
        $xBufResult .= setForm('edSearchmember', 'Member', form_input(getArrayObj('edSearchmember', '', '300')), 'Auto Search Member');
        $xBufResult .= setForm('edSelectallmember', 'Broadcast', '<input type=checkbox id="selectallmember"/>') . '<div class="spacer"></div>';
        $xBufResult .= setForm('edjudulnotifikasi', 'Judul Notifikasi', form_input(getArrayObj('edjudulnotifikasi', '', '200'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edmessage', 'Message', form_textarea(getArrayObj('edmessage', '', '300'))) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edmenuandroid', 'Menu Android', form_input(getArrayObj('edmenuandroid', '', '100'))) . '<div class="spacer"></div>';
        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'Kirim', 'onclick="dosimpannotifikasiandroid();"') . '<div class="spacer"></div>';
//        $xBufResult .= '<div class="spacer"></div><div id="tabledata">' . $this->getlistnotifikasiandroid(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function simpannotifikasiandroid() {
        $this->load->helper('json');
        $xidmember = $_POST['edidmember'];
        $xjudulnotifikasi = $_POST['edjudulnotifikasi'];
        $xmessage = $_POST['edmessage'];
        $xmenuandroid = $_POST['edmenuandroid'];
        $xselectallmember = $_POST['selectallmember'];
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            $this->load->model('modelandroidsend');
            if ($xselectallmember == 'Y') {
                $this->modelandroidsend->pushNotificationAndroidBroadcast($xjudulnotifikasi, $xmessage, $xmenuandroid);
            } else {
                $this->modelandroidsend->pushNotificationAndroid($xidmember, $xjudulnotifikasi, $xmessage, $xmenuandroid);
            }
        } else {
            echo "idpegawai = " . $idpegawai;
        }
        $this->load->helper('json');
        echo json_encode(null);
    }

}

?>
