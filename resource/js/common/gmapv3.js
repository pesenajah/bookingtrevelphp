/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var geocoder = new google.maps.Geocoder();

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus(str) {
 // document.getElementById('edmapaddress').value = str;
}

function updateMarkerPosition(latLng) {
  document.getElementById('edmapaddress').value = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
}

function updateMarkerAddress(str) {
  //document.getElementById('address').innerHTML = str;
}

function initialize(latitudex,longitudex) {
  
  //var geocoder = new google.maps.Geocoder();
//  var address = "Yogyakarta";
//  var latitudex = 0;
//  var longitudex = 0;

/*
geocoder.geocode( { 'address': address}, function(results, status) {
  if (status == google.maps.GeocoderStatus.OK) {
    var  latitude = results[0].geometry.location.lat();
    var   longitude = results[0].geometry.location.lng();    
    latitudex = latitude;
    longitudex = longitude;
  } 
});
*/
  var latLng = new google.maps.LatLng(latitudex, longitudex);
  
  var map = new google.maps.Map(document.getElementById('mapkoordinat'), {
    zoom: 8,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'Koordinat',
    map: map,
    draggable: true
  });

  // Update current position info.
  updateMarkerPosition(latLng);
  geocodePosition(latLng);

  // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Dragging...');
  });

  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerStatus('Dragging...');
    updateMarkerPosition(marker.getPosition());
  });

  google.maps.event.addListener(marker, 'dragend', function() {
    updateMarkerStatus(marker.getPosition());
    geocodePosition(marker.getPosition());
  });
}

// Onload handler to fire off the app.
//google.maps.event.addDomListener(window, 'load', initialize);

//function loadFromSearch(address)
//{
//  //midpoint = getLatLong(address);
//  midpoint = LatLngalamat(address);
//  alert(midpoint+" sdkkasldksa"); 
//}
//
//function LatLngalamat(address)
//{
//   var returnValue;
//   var geocoder = new google.maps.Geocoder();
//   geocoder.geocode({'address': address}, function(results, status) { returnValue = results[0].geometry.location;});
//   //alert("raiso"+returnValue);
//   return returnValue;
//}
//
//function getLatLong(address)
//{
//  var geocoder = new google.maps.Geocoder();
//  var result = "";
//  geocoder.geocode({ 'address': address, 'region': 'uk' }, function (results, status) {
//     if (status == google.maps.GeocoderStatus.OK)
//     {
//        result = results[0].geometry.location;
//        latLongCallback(result);
//          
//     }
//     else
//     {
//        result = "Unable to find address: " + status;
//        
//     }
//  });
//  alert("raiso"+result);
//  return result;
//}
//
//
////loadFromSearch("Yogyakarta");

function codeAddress(address) {
var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var loc=[]; // no need to define it in outer function now
        loc[0]=results[0].geometry.location.lat();
        loc[1]=results[0].geometry.location.lng();
        
        display(loc); 

      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    }); 
    
  }

  function display(long_lat ){
     
     google.maps.event.addDomListener(window, 'load', initialize(long_lat[0],long_lat[1]));
    // alert(long_lat);
     //updateMarkerPosition(long_lat)
  }
  
 codeAddress("Yogyakarta");