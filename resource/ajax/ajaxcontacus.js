function dosearchcontacus(xAwal){ 
    xSearch ="";
    try 
    {
        if ($("#edSearch").val()!=""){
            xSearch = $("#edSearch").val();
        } 
    }catch(err){
        xSearch ="";
    }
    if (typeof(xSearch) =="undefined"){
        xSearch ="";
    } 
    $(document).ready(function(){
        $.ajax({
            url: getBaseURL()+"index.php/ctrcontacus/searchcontacus/",
            data: "xAwal="+xAwal+"&xSearch="+xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function(json){
                $("#tabledatacontacus").html(json.tabledatacontacus);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                start = xmlHttpRequest.responseText.search("<title>") + 7;
                end  = xmlHttpRequest.responseText.search("</title>");
                errorMsg = " error on search contacus "+xmlHttpRequest.responseText;
                if (start > 0 && end > 0)
                    alert("Rangerti "+errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                else
                    alert("Error juga"+errorMsg);
            }
        });
    });
} 

function doeditcontacus(edidx){ 
    $(document).ready(function(){
        $.ajax({
            url: getBaseURL()+"index.php/ctrcontacus/editreccontacus/",
            data: "edidx="+edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function(json){
                $("#edidx").val(json.idx);
                $("#edNama").val(json.Nama);
                $("#edalamat").val(json.alamat);
                $("#ednotelpon").val(json.notelpon);
                $("#edemail").val(json.email);
                $("#edisi").val(json.isi);
                $("#edtglisi").val(json.tglisi);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                start = xmlHttpRequest.responseText.search("<title>") + 7;
                end = xmlHttpRequest.responseText.search("</title>");
                errorMsg = "OnEdit contacus "+xmlHttpRequest.responseText;
                if (start > 0 && end > 0)  alert("On Edit "+errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                else
                    alert("Error juga "+errorMsg);
            }
        });
    });
} 

function doClearcontacus(){ 
    $(document).ready(function(){
        $("#edidx").val("0");
        $("#edNama").val("");
        $("#edalamat").val("");
        $("#ednotelpon").val("");
        $("#edemail").val("");
        $("#edisi").val("");
        $("#edtglisi").val("");
    });
} 

function dosimpancontacus(){ 
    $(document).ready(function(){
        $.ajax({
            url: getBaseURL()+"index.php/ctrcontacus/simpancontacus/",
            data: "edidx="+$("#edidx").val()+"&edNama="+$("#edNama").val()+"&edalamat="+$("#edalamat").val()+"&ednotelpon="+$("#ednotelpon").val()+"&edemail="+$("#edemail").val()+"&edisi="+$("#edisi").val()+"&edtglisi="+$("#edtglisi").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function(msg){
                doClearcontacus();
                dosearchcontacus('-99');
                alert("Data Has Been Saved.... ");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                start = xmlHttpRequest.responseText.search("<title>") + 7;
                end = xmlHttpRequest.responseText.search("</title>");
                errorMsg =  " On Simpan contacus "+xmlHttpRequest.responseText;
                if (start > 0 && end > 0)
                    alert("Rangerti "+errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                else
                    alert("Error juga "+errorMsg);
            }
        });
    });
} 

function dohapuscontacus(edidx,edNama){ 
    if (confirm("Anda yakin Akan menghapus data "+edNama+"?"))
    {
        $(document).ready(function(){
            $.ajax({
                url: getBaseURL()+"index.php/ctrcontacus/deletetablecontacus/",
                data: "edidx="+edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function(json){
                    doClearcontacus(); 
                    dosearchcontacus('-99'); 
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    start = xmlHttpRequest.responseText.search("<title>") + 7;
                    end = xmlHttpRequest.responseText.search("</title>");
                    errorMsg = " HAPUS contacus "+xmlHttpRequest.responseText;
                    if (start > 0 && end > 0)
                        alert("Rangerti "+errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                    else
                        alert("Error juga "+errorMsg);
                }
            });
        });
    }
} 


dosearchcontacus(0); 


