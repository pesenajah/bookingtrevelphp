function dosearchnotifikasi(xAwal) {
    xSearch = "";
    try {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL_notif() + "index.php/ctrnotifikasi/searchnotifikasi/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledata").html(json.tabledata);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga on search" + xmlHttpRequest.responseText);
            }
        });
    });
}
function doeditnotifikasi(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL_notif() + "index.php/ctrnotifikasi/editrecnotifikasi/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edid_pengirim").val(json.id_pengirim);
                $("#edid_penerima").val(json.id_penerima);
                $("#edketerangan").val(json.keterangan);
                $("#edtgl_kirim").val(json.tgl_kirim);
                $("#edis_clicked").val(json.is_clicked);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga on edit" + xmlHttpRequest.responseText);
            }
        });
    });
}
function doClearnotifikasi() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edid_pengirim").val("");
        $("#edid_penerima").val("");
        $("#edketerangan").val("");
        $("#edtgl_kirim").val("");
        $("#edis_clicked").val("");
    });
}
function dosimpannotifikasi() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL_notif() + "index.php/ctrnotifikasi/simpannotifikasi/",
            data: "edidx=" + $("#edidx").val() + "&edid_pengirim=" + $("#edid_pengirim").val() + "&edid_penerima=" + $("#edid_penerima").val() + "&edketerangan=" + $("#edketerangan").val() + "&edtgl_kirim=" + $("#edtgl_kirim").val() + "&edis_clicked=" + $("#edis_clicked").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (msg) {
                doClearnotifikasi();
                dosearchnotifikasi('-99');
                //alert("Data Berhasil Disimpan.... ");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga do simpan" + xmlHttpRequest.responseText);
            }
        });
    });
}
function dohapusnotifikasi(edidx, edid_pengirim) {
    if (confirm("Anda yakin Akan menghapus data " + edid_pengirim + "?")) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL_notif() + "index.php/ctrnotifikasi/deletetablenotifikasi/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearnotifikasi();
                    dosearchnotifikasi('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga on hapus" + xmlHttpRequest.responseText);
                }
            });
        });
    }
}
//dosearchnotifikasi(0);
//$("#div_notifikasi").pop

//$(document).ready(function () {
//    $("#div_notifikasi").dialog({
//        title: "PEMBERITAHUAN",
//        dialogClass: "no-close",
//        autoOpen: false,
//        height: 250,
//        width: 500,
//    });
//});

function checkNotifikasi_json() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL_notif() + "index.php/ctrnotifikasi/checkNotifikasi_json/",
            data: "",
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                //$("#tabledatanotifikasi").html(json.tabledatanotifikasi);
                if (!json) {
                    $("#div_notifikasi").html("");
                    $("#div_notifikasi").dialog('close');
                }
                else {
                    $("#div_notifikasi").html(json.div_notifikasi);
                    $("#div_notifikasi").dialog('open');
                }

            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga on check notifikasi" + xmlHttpRequest.responseText);
            }
        });
    });
}

//$(document).ready(function () {
//    setInterval(checkNotifikasi_json, 120000);		// setiap 2 menit.
//    checkNotifikasi_json();
//});

function doclosenotif_json(idx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL_notif() + "index.php/ctrnotifikasi/doclosenotif_json/",
            data: "idx=" + idx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (msg) {
                checkNotifikasi_json();
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga on close notifikasi" + xmlHttpRequest.responseText);
            }
        });
    });
}

function isfunction(nama_fungsi) {
    //alert (typeof(nama_fungsi) );
    if (typeof (nama_fungsi) == typeof (isfunction)) {

    }
}
isfunction(dosearchnotifikasi);

function getBaseURL_notif() {
    var url = location.href;  // entire url including querystring - also: window.location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));


    if (baseURL.indexOf('http://localhost') != -1) {
        // Base Url for localhost
        var url = location.href;  // window.location.href;
        var pathname = location.pathname;  // window.location.pathname;
        var index1 = url.indexOf(pathname);
        var index2 = url.indexOf("/", index1 + 1);
        var baseLocalUrl = url.substr(0, index2);

        return "http://localhost/spartaci/";
//        return 'http://192.168.0.104/spartaci/'
    }
    else {
        // Root Url for domain name
        return "http://localhost/spartaci/";
//        return 'http://192.168.0.104/spartaci/'
    }

}
