$(document).ready(function () {
    $("input#edtglbooking").datepicker({
        dateFormat: 'dd-mm-yy',
        onSelect: function () {

        }
    });
    $("#edjambooking").timepicker({
        timeOnly: true,
        showHour: true,
        showMinute: true,
        showSecond: true,
        controlType: 'select',
        timeFormat: 'HH:mm:ss'
    });

    $("input#edtglbayar").datepicker({
        dateFormat: 'dd-mm-yy',
        onSelect: function () {

        }
    });

    $("input#edtglbatalbooking").datepicker({
        dateFormat: 'dd-mm-yy',
        onSelect: function () {

        }
    });
    $("#edjambatalbooking").timepicker({
        timeOnly: true,
        showHour: true,
        showMinute: true,
        showSecond: true,
        controlType: 'select',
        timeFormat: 'HH:mm:ss'
    });
});


function dosearchtransaksi(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrtransaksi/searchtransaksi/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatatransaksi").html(json.tabledatatransaksi);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doedittransaksi(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrtransaksi/editrectransaksi/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edidbooking").val(json.idbooking);
                $("#edtglbooking").val(json.tglbooking);
                $("#edtglbatalbooking").val(json.tglbatalbooking);
                $("#edjambooking").val(json.jambooking);
                $("#edjambatalbooking").val(json.jambatalbooking);
                $("#edketeranganbatal").val(json.keteranganbatal);
                $("#edharganormal").val(json.harganormal);
                $("#edhargadiscount").val(json.hargadiscount);
                $("#edidvoucher").val(json.idvoucher);
                $("#edidmember").val(json.idmember);
                $("#ednama_member").val(json.nama_member);
                $("#edidpegawai").val(json.idpegawai);
                $("#edspesialrequest").val(json.spesialrequest);
                $("#edtglupdate").val(json.tglupdate);
                $("#edidjenisbayar").val(json.idjenisbayar);
                $("#edtglbayar").val(json.tglbayar);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doCleartransaksi() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edidbooking").val("");
        $("#edtglbooking").val("");
        $("#edtglbatalbooking").val("");
        $("#edjambooking").val("");
        $("#edjambatalbooking").val("");
        $("#edketeranganbatal").val("");
        $("#edharganormal").val("");
        $("#edhargadiscount").val("");
        $("#edidvoucher").val("");
        $("#edidmember").val("");
        $("#ednama_member").val("");
        $("#edidpegawai").val("");
        $("#edspesialrequest").val("");
        $("#edtglupdate").val("");
        $("#edidjenisbayar").val("");
        $("#edtglbayar").val("");
    });
}

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function empty2(e) {
    switch (e) {
        case "":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function dosimpantransaksi() {
    if (!empty($("#edtglbooking").val()) && !empty($("#edjambooking").val()) && !empty($("#edidbooking").val())
            && !empty($("#edidmember").val()) && !empty($("#edtglperuntukanmulai").val()) && !empty($("#edtglperuntukanselasai").val())
            && !empty2($("#edharganormal").val()) && !empty2($("#edhargadiscount").val()) && !empty($("#edidmember").val())
            ) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrtransaksi/simpantransaksi/",
                data: "edidx=" + $("#edidx").val() + "&edidbooking=" + $("#edidbooking").val() +
                        "&edtglbooking=" + $("#edtglbooking").val() + "&edtglbatalbooking=" + $("#edtglbatalbooking").val() +
                        "&edjambooking=" + $("#edjambooking").val() + "&edjambatalbooking=" + $("#edjambatalbooking").val() +
                        "&edketeranganbatal=" + $("#edketeranganbatal").val() +
                        "&edharganormal=" + $("#edharganormal").val() + "&edhargadiscount=" + $("#edhargadiscount").val() +
                        "&edidvoucher=" + $("#edidvoucher").val() + "&edidmember=" + $("#edidmember").val() +
                        "&edidpegawai=" + $("#edidpegawai").val() + "&edspesialrequest=" + $("#edspesialrequest").val() +
                        "&edtglupdate=" + $("#edtglupdate").val() + "&edidjenisbayar=" + $("#edidjenisbayar").val() +
                        "&edtglbayar=" + $("#edtglbayar").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doCleartransaksi();
                    dosearchtransaksi('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum lengkap");
    }
}

function dohapustransaksi(edidx, edidbooking) {
    if (confirm("Anda yakin Akan menghapus data " + edidbooking + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrtransaksi/deletetabletransaksi/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doCleartransaksi();
                    dosearchtransaksi('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchtransaksi(0);

function dobataltransaksi(edidx, edtglbooking) {
    if (confirm("Anda yakin akan membatalkan transaksi " + edtglbooking + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrtransaksi/setBataltransaksi/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doCleartransaksi();
                    dosearchtransaksi('-99');
                    doedittransaksi(edidx);
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}

function dolanjuttransaksi(edidx, edtglbooking) {
    if (confirm("Anda yakin akan melanjutkan proses transaksi " + edtglbooking + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrtransaksi/setLanjuttransaksi/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doCleartransaksi();
                    dosearchtransaksi('-99');
                    doedittransaksi(edidx);
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}

