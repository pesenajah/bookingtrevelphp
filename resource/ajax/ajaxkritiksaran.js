$(document).ready(function () {
    $("#edTglMulai").datepicker({
        dateFormat: 'dd-mm-yy',
    });

    $("#edTglSelesai").datepicker({
        dateFormat: 'dd-mm-yy',
    });
    $("#browsepdf").dialog({
        closeOnEscape: true,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").show();
        },
        autoOpen: false,
        height: 700,
        width: 1240,
        modal: true
    });
});

$(document).ready(function () {
    $("#gbloader").hide();
});

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}


function doshowlaporankritiksaran() {
    $(document).ready(function () {
        if (empty($("#edTglMulai").val()) || empty($("#edTglSelesai").val())) {
            alert("Masukkan Tanggal Awal dan Tanggal Akhir!!");
        } else {
            $("#gbloader").show();
            $.ajax({
                url: getBaseURL() + "index.php/ctrkritiksaran/carilaporan_byrange/",
                data: 'edTglMulai=' + $("#edTglMulai").val() + '&edTglSelesai=' + $("#edTglSelesai").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    $("#tblaporankritiksaran").html(json.tblaporankritiksaran);
                    $("#tblaporankritiksaran").show();
                    $("#gbloader").hide();
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
//                    start = xmlHttpRequest.responseText.search("<title>") + 7;
//                    end = xmlHttpRequest.responseText.search("</title>");
//                    errorMsg = " error on search " + xmlHttpRequest.responseText;
//                    if (start > 0 && end > 0)
//                        alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
//                    else
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        }
    });
}

//
function setpdflaporankritiksaran() {
    if (empty($("#edTglMulai").val()) || empty($("#edTglSelesai").val())) {
        alert("Masukkan Tanggal Awal dan Tanggal Akhir!!");
    } else {
        $(document).ready(function () {
            $("#gbloader").show();
            $.ajax({
                url: getBaseURL() + "index.php/ctrkritiksaran/setpdf/",
                //data: "NoDokumen="+$("#edNoDokumen").val(),
                data: "edTglMulai=" + $("#edTglMulai").val() + "&edTglSelesai=" + $("#edTglSelesai").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    //alert(json.data);
                    $("#browsepdf").html("");
                    $("#browsepdf").html(json.data);
                    $("#browsepdf").dialog("open");
                    $("#gbloader").hide();
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    start = xmlHttpRequest.responseText.search("<title>") + 7;
                    end = xmlHttpRequest.responseText.search("</title>");
                    errorMsg = "On Open PDF " + xmlHttpRequest.responseText;
                    if (start > 0 && end > 0)
                        alert("On Open PDF " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                    else
                        alert("Error juga " + errorMsg);
                }
            });
        });
    }
}
//
function exportkeexcel() {
    if (empty($("#edTglMulai").val()) || empty($("#edTglSelesai").val())) {
        alert("Masukkan Tanggal Awal dan Tanggal Akhir!!");
    } else {
        $("#gbloader").show();
        $(document).ready(function () {
            document.location = getBaseURL() + "index.php/ctrkritiksaran/exportkeexcel/"
                    + $("#edTglMulai").val() + "/"
                    + $("#edTglSelesai").val();
            $("#gbloader").hide();
        });
    }
}



