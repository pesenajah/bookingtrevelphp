function dosearchinboxfcm(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrinboxfcm/searchinboxfcm/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatainboxfcm").html(json.tabledatainboxfcm);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditinboxfcm(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrinboxfcm/editrecinboxfcm/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edidmember").val(json.idmember);
                $("#edjudul").val(json.judul);
                $("#edmessage").val(json.message);
                $("#edtglmessage").val(json.tglmessage);
                $("#edisterbaca").val(json.isterbaca);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearinboxfcm() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edidmember").val("");
        $("#edjudul").val("");
        $("#edmessage").val("");
        $("#edtglmessage").val("");
        $("#edisterbaca").val("");
    });
}

function dosimpaninboxfcm() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrinboxfcm/simpaninboxfcm/",
            data: "edidx=" + $("#edidx").val() + "&edidmember=" + $("#edidmember").val() + "&edjudul=" + $("#edjudul").val() + "&edmessage=" + $("#edmessage").val() + "&edtglmessage=" + $("#edtglmessage").val() + "&edisterbaca=" + $("#edisterbaca").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (msg) {
                doClearinboxfcm();
                dosearchinboxfcm('-99');
                alert("Data Berhasil Disimpan.... ");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dohapusinboxfcm(edidx, edidmember) {
    if (confirm("Anda yakin Akan menghapus data " + edidmember + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrinboxfcm/deletetableinboxfcm/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearinboxfcm();
                    dosearchinboxfcm('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchinboxfcm(0);


