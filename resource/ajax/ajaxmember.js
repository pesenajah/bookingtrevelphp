function dosearchmember(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrmember/searchmember/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatamember").html(json.tabledatamember);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditmember(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrmember/editrecmember/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edNama").val(json.Nama);
                $("#edAlamat").val(json.Alamat);
                $("#edNoTelpon").val(json.NoTelpon);
                $("#edidtoken").val(json.idtoken);
                $("#edemail").val(json.email);
                $("#edpassword").val(json.password);
                $("#edtglinsert").val(json.tglinsert);
                $("#edidjenismember").val(json.idjenismember);
                $("#edisblokir").val(json.isblokir);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearmember() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edNama").val("");
        $("#edAlamat").val("");
        $("#edNoTelpon").val("");
        $("#edidtoken").val("");
        $("#edemail").val("");
        $("#edpassword").val("");
        $("#edtglinsert").val("");
        $("#edidjenismember").val("");
        $("#edisblokir").val("");
    });
}

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function dosimpanmember() {
    if (!empty($("#edNama").val()) && !empty($("#edAlamat").val()) && !empty($("#edNoTelpon").val()) && !empty($("#edemail").val())
            && !empty($("#edtglinsert").val()) && !empty($("#edidtoken").val()) && !empty($("#edpassword").val())) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrmember/simpanmember/",
                data: "edidx=" + $("#edidx").val() + "&edNama=" + $("#edNama").val() +
                        "&edAlamat=" + $("#edAlamat").val() + "&edNoTelpon=" + $("#edNoTelpon").val() +
                        "&edidtoken=" + $("#edidtoken").val() + "&edemail=" + $("#edemail").val() + "&edtglinsert=" + $("#edtglinsert").val() +
                        "&edidjenismember=" + $("#edidjenismember").val() + "&edisblokir=" + $("#edisblokir").val() + "&edpassword=" + $("#edpassword").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doClearmember();
                    dosearchmember('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum lengkap");
    }
}

function dohapusmember(edidx, edNama) {
    if (confirm("Anda yakin Akan menghapus data " + edNama + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrmember/deletetablemember/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearmember();
                    dosearchmember('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchmember(0);


