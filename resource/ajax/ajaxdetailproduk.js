function dosearchdetailproduk() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrdetailproduk/searchdetailproduk/",
            data: "edidproduk=" + $("#edidproduk").val()+"&edIdKatProduk=" + $("#edidkategoriprodukdetail").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatadetailproduk").html(json.tabledatadetailproduk);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditdetailproduk(edidxdetail) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrdetailproduk/editrecdetailproduk/",
            data: "edidxdetail=" + edidxdetail,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidxdetail").val(json.idx);
                $("#edidproduk").val(json.idproduk);
                $("#edidkategoriprodukdetail").val(json.idkategoriproduk);
                $("#edjuduldetailproduk").val(json.juduldetailproduk);
                $("#eddiskripsiproduk").val(json.diskripsiproduk);
                $("#edrate").val(json.rate);
                $("#edratediscount").val(json.ratediscount);
                $("#edrancode").val(json.rancode);
                $("#edtglinsert").val(json.tglinsert);
                $("#edtglupdate").val(json.tglupdate);
                $("#edkapasitas").val(json.kapasitas);
                $("#edstandartpemakaian").val(json.standartpemakaian);
                $("#edidsatuan").val(json.idsatuan);
                $("#edidpegawai").val(json.idpegawai);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doCleardetailproduk() {
    $(document).ready(function () {
        $("#edidxdetail").val("0");
//        $("#edidproduk").val("");
        $("#edidkategoriprodukdetail").val("");
        $("#edjuduldetailproduk").val("");
        $("#eddiskripsiproduk").val("");
        $("#edrate").val("");
        $("#edratediscount").val("");
        $("#edrancode").val("");
        $("#edtglinsert").val("");
        $("#edtglupdate").val("");
        $("#edkapasitas").val("");
        $("#edstandartpemakaian").val("");
        $("#edidsatuan").val("");
        $("#edidpegawai").val("");
    });
}

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}


function dosimpandetailproduk() {
//    alert($("#edidkategoriprodukdetail").val());
    if (!empty($("#edjuduldetailproduk").val()) && !empty($("#eddiskripsiproduk").val()) && !empty($("#edrate").val()) &&
            !empty($("#edratediscount").val()) && !empty($("#edkapasitas").val()) && !empty($("#edstandartpemakaian").val()) &&
            !empty($("#edidsatuan").val())) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrdetailproduk/simpandetailproduk/",
                data: "edidxdetail=" + $("#edidxdetail").val() + "&edidproduk=" + $("#edidproduk").val() +
                        "&edidkategoriprodukdetail=" + $("#edidkategoriprodukdetail").val() + "&edjuduldetailproduk=" +
                        $("#edjuduldetailproduk").val() + "&eddiskripsiproduk=" + $("#eddiskripsiproduk").val() +
                        "&edrate=" + $("#edrate").val() + "&edratediscount=" + $("#edratediscount").val() +
                        "&edrancode=" + $("#edrancode").val() + "&edtglinsert=" + $("#edtglinsert").val() +
                        "&edtglupdate=" + $("#edtglupdate").val() + "&edkapasitas=" + $("#edkapasitas").val() +
                        "&edstandartpemakaian=" + $("#edstandartpemakaian").val() + "&edidsatuan=" + $("#edidsatuan").val() +
                        "&edidpegawai=" + $("#edidpegawai").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doCleardetailproduk();
                    dosearchdetailproduk();
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum lengkap");
    }
}

function dohapusdetailproduk(edidxdetail, edjuduldetail) {
    if (confirm("Anda yakin Akan menghapus data " + edjuduldetail + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrdetailproduk/deletetabledetailproduk/",
                data: "edidxdetail=" + edidxdetail,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doCleardetailproduk();
                    dosearchdetailproduk();
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


//dosearchdetailproduk();

function setpopup() {
    $(document).ready(function () {
        $("#modalformdetail").dialog({
            autoOpen: false,
            top: 150,
            height: 600,
            width: 1100,
            modal: true
        })
    });
}
function doaddimagefromdetailproduk(xIdProdukdetail, $xIdKatProduk) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrimagedetail2/doShowFormDetailImage/",
            data: "xIdProduk=" + xIdProdukdetail + "&xIdKatProduk=" + $xIdKatProduk + "&isfromproduk='N'",
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#modalformdetail").html("");
                $("#modalformdetail").html(json.modaldetailproduk);

                setpopup();
                $(".formupload").html("");
                $("#modalformdetail").dialog("open");
                $("#edlinkimage2").myuploadphoto();

                setkelasangka();
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dobacktoproduk($xidKat) {
    $(document).ready(function () {
        if ($xidKat == '1') {
            location = getBaseURL() + "index.php/ctrprodukhotel/index/";
        }
        if ($xidKat == '2') {
            location = getBaseURL() + "index.php/ctrproduktravel/index/";
        }
        if ($xidKat == '3') {
            location = getBaseURL() + "index.php/ctrprodukwisata/index/";
        }
    });

}

function setkelasangka() {
    $(document).ready(function () {
        $('.angka').priceFormat({
            limit: 30,
            centsLimit: 0,
            prefix: '',
            centsSeparator: '',
            thousandsSeparator: '.'
        });
    });

}
setkelasangka();