$(document).ready(function () {
    $("#modalform").dialog({
        autoOpen: false,
        top: 150,
        height: 600,
        width: 1100,
        modal: true
    })
});

function dosearchproduk(xAwal) {
    doClearproduk();
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrproduktravel/searchproduk/",
            data: "xAwal=" + xAwal + "&xSearch=" + $("#edSearch").val() + "&xKategoriProduk=" + $("#idKategoriProduk").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledataproduk").html(json.tabledataproduk);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditproduk(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrproduktravel/editrecproduk/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edJudulProduk").val(json.JudulProduk);
                //$("#edidKategoriProduk").val(json.idKategoriProduk);
                $("#edKeterangan").val(json.Keterangan);
                $("#edphonekontak").val(json.phonekontak);
                $("#edNamaKontak").val(json.NamaKontak);
                $("#edDiskripsiProduk").val(json.DiskripsiProduk);
                $("#edmapaddress").val(json.mapaddress);
                $("#edrate").val(json.rate);
                $("#edratediscount").val(json.ratediscount);
                $("#edrancode").val(json.rancode);
                $("#edtglinsert").val(json.tglinsert);
                $("#edtglupdate").val(json.tglupdate);
                $("#edkapasitas").val(json.kapasitas);
                $("#edstandartpemakaian").val(json.standartpemakaian);
                $("#edidsatuan").val(json.idsatuan);
                $("#edidpegawai").val(json.idpegawai);
                var res = json.mapaddress.split(",");
                initialize(res[0], res[1]);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearproduk() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edJudulProduk").val("");
//        $("#edidKategoriProduk").val("");
        $("#edKeterangan").val("");
        $("#edphonekontak").val("");
        $("#edNamaKontak").val("");
        $("#edDiskripsiProduk").val("");
        $("#edmapaddress").val("");
        $("#edrate").val("");
        $("#edratediscount").val("");
        $("#edrancode").val("");
        $("#edtglinsert").val("");
        $("#edtglupdate").val("");
        $("#edkapasitas").val("");
        $("#edstandartpemakaian").val("");
        $("#edidsatuan").val("");

        $("#edidpegawai").val("");
        codeAddress("Denpasar");
    });
}

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function dosimpanproduk() {
    if (!empty($("#edJudulProduk").val()) && !empty($("#edidKategoriProdukx").val()) && !empty($("#edKeterangan").val()) && !empty($("#edphonekontak").val())
            && !empty($("#edNamaKontak").val()) && !empty($("#edDiskripsiProduk").val()) && !empty($("#edmapaddress").val())
//            && !empty($("#edrate").val()) && !empty($("#edratediscount").val()) && !empty($("#edkapasitas").val())
//            && !empty($("#edstandartpemakaian").val())
            ) {
        $(document).ready(function () {
            //alert($("#edidKategoriProdukx").val() + '-->'+$("#edidx").val());
            $.ajax({
                url: getBaseURL() + "index.php/ctrprodukhotel/simpanproduk/",
                data: "edidx=" + $("#edidx").val() + "&edJudulProduk=" + $("#edJudulProduk").val() +
                        "&edidKategoriProduk=" + $("#edidKategoriProdukx").val() + "&edKeterangan=" +
                        $("#edKeterangan").val() + "&edphonekontak=" + $("#edphonekontak").val() +
                        "&edNamaKontak=" + $("#edNamaKontak").val() + "&edDiskripsiProduk=" +
                        $("#edDiskripsiProduk").val() + "&edmapaddress=" + $("#edmapaddress").val() +
                        "&edrate=" + $("#edrate").val() + "&edratediscount=" + $("#edratediscount").val() +
                        "&edrancode=" + $("#edrancode").val() + "&edtglinsert=" + $("#edtglinsert").val() +
                        "&edtglupdate=" + $("#edtglupdate").val() + "&edkapasitas=" + $("#edkapasitas").val() +
                        "&edstandartpemakaian=" + $("#edstandartpemakaian").val() + "&edidsatuan=" + $("#edidsatuan").val() +
                        "&edidpegawai=" + $("#edidpegawai").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doClearproduk();
                    dosearchproduk('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum lengkap");
    }
}

function dohapusproduk(edidx, edJudulProduk) {
    if (confirm("Anda yakin Akan menghapus data " + edJudulProduk + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrproduktravel/deletetableproduk/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearproduk();
                    dosearchproduk('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchproduk(0);

function setkelasangka() {
    $(document).ready(function () {
        $('.angka').priceFormat({
            limit: 30,
            centsLimit: 0,
            prefix: '',
            centsSeparator: '',
            thousandsSeparator: ','
        });
    });

}
setkelasangka();

function doDetailProduk(xIdProduk, xIdKatProduk) {
    $(document).ready(function () {
        location = getBaseURL() + "index.php/ctrdetailproduk/index/" + xIdProduk + "/" + xIdKatProduk;
        /* $.ajax({
         url: getBaseURL() + "index.php/ctrdetailproduk/doShowFormdetailproduk/",
         data: "xIdProduk=" + xIdProduk,
         cache: false,
         dataType: 'json',
         type: 'POST',
         success: function (json) {
         $("#modalform").html(json.modaldetailproduk);
         $("#modalform").dialog("open");                    
         setkelasangka();
         },
         error: function (xmlHttpRequest, textStatus, errorThrown) {
         alert("Error juga " + xmlHttpRequest.responseText);
         }
         });
         */
    });
}

//$(document).keypress(function(event) {
//   $("#edmapaddress").on( "keydown", function(event) {
//      if(event.which == 13) 
//         codeAddress($("#edmapaddress").val());
//    });
//});


function doDetailImage(xIdProduk) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrimagedetail/doShowFormDetailImage/",
            data: "xIdProduk=" + xIdProduk + "&isfromproduk='Y'",
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
//                    location =  json.modaldetailproduk;
                $("#modalform").html(json.modaldetailproduk);
                $("#modalform").dialog("open");
                $("#edlinkimage").myuploadphoto();

                //setkelasangka();
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}