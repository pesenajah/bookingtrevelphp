function dosearchimagedetail(xAwal) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrimagedetail/searchimagedetail/",
            data: "idproduk=" + $("#idproduk").val() + "&isproduk=" + $("#isproduk").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledataimagedetail").html(json.tabledataimagedetail);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditimagedetail(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrimagedetail/editrecimagedetail/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);

//                $("#ediddetailproduk").val(json.iddetailproduk);
//                        $("#ediddetailproduk").val(json.iddetailproduk);
//                $("#ediddetailproduk").val(json.iddetailproduk);
//                $("#edidkategoriproduk").val(json.idkategoriproduk);
                $("#edlinkimage").val(json.linkimage);

                $("#edketeranganimage").val(json.keteranganimage);
                $("#edrancode").val(json.rancode);
                $("#edtglinsert").val(json.tglinsert);
                $("#edtglupdate").val(json.tglupdate);
                $("#edidpegawai").val(json.idpegawai);
                $("#edlinkimage").val(json.linkimage).trigger('change');
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearimagedetail() {
    $(document).ready(function () {
        $("#edidx").val("0");

//        $("#ediddetailproduk").val("");
//        $("#edidkategoriproduk").val("");
        $("#edlinkimage").val("");
        $("#edketeranganimage").val("");
        $("#edrancode").val("");
        $("#edtglinsert").val("");
        $("#edtglupdate").val("");
        $("#edidpegawai").val("");
    });
}

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}


function dosimpanimagedetail() {
    if (!empty($("#edlinkimage").val()) && !empty($("#edketeranganimage").val()) ) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrimagedetail/simpanimagedetail/",
                data: "edidx=" + $("#edidx").val() +
                        "&idproduk=" + $("#idproduk").val() +
                        "&isproduk=" + $("#isproduk").val() +
                        "&iddetailproduk=" + $("#iddetailproduk").val() +
                        "&idkategoriproduk=" + $("#idkategoriproduk").val() +
                        "&edlinkimage=" + $("#edlinkimage").val() +
                        "&edketeranganimage=" + $("#edketeranganimage").val() +
                        "&edrancode=" + $("#edrancode").val() +
                        "&edtglinsert=" + $("#edtglinsert").val() +
                        "&edtglupdate=" + $("#edtglupdate").val() +
                        "&edidpegawai=" + $("#edidpegawai").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doClearimagedetail();
                    dosearchimagedetail('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum diisi");
    }
}

function dohapusimagedetail(edidx, ediddetailproduk) {
    if (confirm("Anda yakin Akan menghapus data " + ediddetailproduk + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrimagedetail/deletetableimagedetail/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearimagedetail();
                    dosearchimagedetail('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchimagedetail(0);


