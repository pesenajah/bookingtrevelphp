$(document).ready(function () {
    $("input#edtglberlakudari").datepicker({
        dateFormat: 'dd-mm-yy',
    });

    $("input#edtglberlakusampai").datepicker({
        dateFormat: 'dd-mm-yy',
    });

    $(".formupload").html("");
    $("#edlinkimage").myuploadphoto();
});

function dosearchvoucher(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrvoucher/searchvoucher/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatavoucher").html(json.tabledatavoucher);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditvoucher(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrvoucher/editrecvoucher/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edvoucher").val(json.voucher);
                $("#ednominal").val(json.nominal);
                $("#edjumlahmaxpengguna").val(json.jumlahmaxpengguna);
                $("#edtglberlakudari").val(json.tglberlakudari);
                $("#edtglberlakusampai").val(json.tglberlakusampai);
                $("#edidproduk").val(json.idproduk);
                $("#edpenjelasan").val(json.penjelasan);
                $("#edlinkimage").val(json.linkimage);
                $("#edlinkimage").val(json.linkimage).trigger('change');
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearvoucher() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edvoucher").val("");
        $("#ednominal").val("");
        $("#edjumlahmaxpengguna").val("");
        $("#edtglberlakudari").val("");
        $("#edtglberlakusampai").val("");
        $("#edidproduk").val("");
        $("#edpenjelasan").val("");
        $("#edlinkimage").val("");
    });
}

//function doCleargeneratevoucher() {
//    $(document).ready(function () {
//        $("#edidx").val("0");
//        $("#edvoucher").val("");
//        $("#edjumlahgenerate").val("");
//        $("#ednominal").val("");
//        $("#edtglberlakudari").val("");
//        $("#edtglberlakusampai").val("");
//        $("#edjamberlakudari").val("");
//        $("#edjamberlakusampai").val("");
//        $("#edidmember").val("");
//        $("#edisterpakai").val("");
//        $("#edtglpakai").val("");
//        $("#edjampakai").val("");
//    });
//}

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function dosimpanvoucher() {
//    alert($("#edidproduk").val());
    if (!empty($("#edvoucher").val()) && !empty($("#ednominal").val()) && !empty($("#edjumlahmaxpengguna").val()) && !empty($("#edlinkimage").val()) && !empty($("#edpenjelasan").val())
            && !empty($("#edtglberlakudari").val()) && !empty($("#edtglberlakusampai").val())) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrvoucher/simpanvoucher/",
                data: "edidx=" + $("#edidx").val() + "&edvoucher=" + $("#edvoucher").val() + "&ednominal=" + $("#ednominal").val() +
                        "&edjumlahmaxpengguna=" + $("#edjumlahmaxpengguna").val() +
                        "&edtglberlakudari=" + $("#edtglberlakudari").val() + "&edtglberlakusampai=" + $("#edtglberlakusampai").val() +
                        "&edidproduk=" + $("#edidproduk").val() + "&edpenjelasan=" + $("#edpenjelasan").val() + "&edlinkimage=" + $("#edlinkimage").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doClearvoucher();
                    dosearchvoucher('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum lengkap");
    }
}

function dohapusvoucher(edidx, edvoucher) {
    if (confirm("Anda yakin Akan menghapus data " + edvoucher + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrvoucher/deletetablevoucher/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearvoucher();
                    dosearchvoucher('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchvoucher(0);

function setkelasangka() {
    $(document).ready(function () {
        $('.angka').priceFormat({
            limit: 30,
            centsLimit: 0,
            prefix: '',
            centsSeparator: '',
            thousandsSeparator: '.'
        });
    });

}
setkelasangka();


//function dogeneratevoucher() {
//    if (!empty($("#edjumlahgenerate").val()) && !empty($("#ednominal").val()) && !empty($("#edtglberlakudari").val()) && !empty($("#edtglberlakusampai").val())) {
//        $(document).ready(function () {
//            $.ajax({
//                url: getBaseURL() + "index.php/ctrgeneratevoucher/generatevoucher/",
//                data: "edidx=" + $("#edidx").val() + "&edjumlahgenerate=" + $("#edjumlahgenerate").val() + "&ednominal=" + $("#ednominal").val() +
//                        "&edtglberlakudari=" + $("#edtglberlakudari").val() + "&edtglberlakusampai=" + $("#edtglberlakusampai").val() +
//                        "&edjamberlakudari=" + $("#edjamberlakudari").val() + "&edjamberlakusampai=" + $("#edjamberlakusampai").val(),
//                cache: false,
//                dataType: 'json',
//                type: 'POST',
//                success: function (msg) {
//                    doCleargeneratevoucher();
//                    dosearchvoucher('-99');
//                    alert("Data Berhasil Disimpan.... ");
//                },
//                error: function (xmlHttpRequest, textStatus, errorThrown) {
//                    alert("Error juga " + xmlHttpRequest.responseText);
//                }
//            });
//        });
//    } else {
//        alert("Data belum lengkap");
//    }
//}
//
//function setAutoCompleteMember() {
//    $(document).ready(function () {
//        $('#ednama_member').autocomplete({
//            multiple: true,
//            minLength: 3,
//            multipleSeparator: ",",
//            source: function (request, response) {
//                $.ajax({
//                    url: getBaseURL() + "index.php/ctrvoucher/setAutoCompleteMember/",
//                    data: "ednama_member=" + $("#ednama_member").val(),
//                    dataType: "json",
//                    cache: false,
//                    type: "POST",
//                    success: function (json) {
//                        response($.map(json.data, function (item) {
//                            return {
//                                label: item.label,
//                                value: item.label,
//                                idx: item.value
//                            }
//                        }))
//                    },
//                    error: function (xmlHttpRequest, textStatus, errorThrown) {
//                        start = xmlHttpRequest.responseText.search("<title>") + 7;
//                        end = xmlHttpRequest.responseText.search("</title>");
//                        errorMsg = " ON CARI " + xmlHttpRequest.responseText;
//                        if (start > 0 && end > 0)
//                            alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
//                        else
//                            alert("Error juga " + errorMsg);
//                    }
//                });
//            },
//            width: 100,
//            max: 10,
//            select: function (event, ui) {
////                txt = ui.item.value;
////                atxt = txt.split("-");
//                $('#edidmember').val(ui.item.idx);
//                $('#ednama_member').val(ui.item.label);
//            },
//            change: function (event, ui) {
//            }
//        });
//    })
//}
//
//$(document).ready(function () {
//    setAutoCompleteMember();
//});