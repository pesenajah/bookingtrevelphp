$(document).ready(function () {
    $("#edtgltransfer").datepicker({
        dateFormat: 'dd-mm-yy',
    });
});

$(document).ready(function () {
    $("input#edtglbooking").datepicker({
        dateFormat: 'dd-mm-yy',
    });

    $("input#edtglperuntukandari").datepicker({
        dateFormat: 'dd-mm-yy',
    });

    $("input#edtglperuntukansampai").datepicker({
        dateFormat: 'dd-mm-yy',
        onSelect: function () {

        }
    });

});


function dosearchbooking(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrbooking/searchbooking/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatabooking").html(json.tabledatabooking);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditbooking(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrbooking/editrecbooking/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edtglbooking").val(json.tglbooking);
                $("#edjambooking").val(json.jambooking);
                $("#edidproduk").val(json.idproduk);
                $("#ediddetailproduk").val(json.iddetailproduk);
                $("#edidkategoriproduk").val(json.idkategoriproduk);
                $("#edidmember").val(json.idmember);
                $("#ednamaproduk").val(json.namaproduk);
                $("#ednamadetailproduk").val(json.namadetailproduk);
                $("#ednamakategoriproduk").val(json.namakategoriproduk);
                $("#ednamamember").val(json.namamember);
                $("#edtglperuntukandari").val(json.tglperuntukandari);
                $("#edtglperuntukansampai").val(json.tglperuntukansampai);
                $("#edjmldewasa").val(json.jmldewasa);
                $("#edjmlanak").val(json.jmlanak);
                $("#edjmlhewan").val(json.jmlhewan);
                $("#edketerangantambahn").val(json.keterangantambahn);
                $("#edjmltransfer").val(json.jmltransfer);
                $("#edidjenispembayaran").val(json.idjenispembayaran);
                $("#ednomorkartu").val(json.nomorkartu);
                $("#edstatus").val(json.status);
                $("#edtgltransfer").val(json.tgltransfer);
                $("#edtglinsert").val(json.tglinsert);
                $("#edtglupdate").val(json.tglupdate);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearbooking() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edtglbooking").val("");
        $("#edjambooking").val("");
        $("#edidproduk").val("");
        $("#ediddetailproduk").val("");
        $("#edidkategoriproduk").val("");
        $("#edidmember").val("");
        $("#ednamaproduk").val("");
        $("#ednamadetailproduk").val("");
        $("#ednamakategoriproduk").val("");
        $("#ednamamember").val("");
        $("#edtglperuntukandari").val("");
        $("#edtglperuntukansampai").val("");
        $("#edjmldewasa").val("");
        $("#edjmlanak").val("");
        $("#edjmlhewan").val("");
        $("#edketerangantambahn").val("");
        $("#edjmltransfer").val("");
        $("#edidjenispembayaran").val("");
        $("#ednomorkartu").val("");
        $("#edstatus").val("");
        $("#edtgltransfer").val("");
        $("#edtglinsert").val("");
        $("#edtglupdate").val("");
    });
}

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function empty2(e) {
    switch (e) {
        case "":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function dosimpanbooking() {
    if (!empty($("#edtglbooking").val()) && !empty($("#edjambooking").val()) && !empty($("#edidproduk").val()) && !empty($("#ediddetailproduk").val()) && !empty($("#edidkategoriproduk").val())
            && !empty($("#edidmember").val()) && !empty($("#edtglperuntukandari").val()) && !empty($("#edtglperuntukansampai").val())
            && !empty2($("#edjmldewasa").val()) && !empty2($("#edjmlanak").val()) && !empty2($("#edjmlhewan").val())) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrbooking/simpanbooking/",
                data: "edidx=" + $("#edidx").val() + "&edtglbooking=" + $("#edtglbooking").val() + "&edjambooking=" + $("#edjambooking").val() +
                        "&edidproduk=" + $("#edidproduk").val() + "&ediddetailproduk=" + $("#ediddetailproduk").val() +
                        "&edidkategoriproduk=" + $("#edidkategoriproduk").val() + "&edidmember=" + $("#edidmember").val() +
                        "&edtglperuntukandari=" + $("#edtglperuntukandari").val() + "&edtglperuntukansampai=" + $("#edtglperuntukansampai").val() +
                        "&edjmldewasa=" + $("#edjmldewasa").val() + "&edjmlanak=" + $("#edjmlanak").val() +
                        "&edjmlhewan=" + $("#edjmlhewan").val() + "&edketerangantambahn=" + $("#edketerangantambahn").val() +
                        "&edjmltransfer=" + $("#edjmltransfer").val() + "&edidjenispembayaran=" + $("#edidjenispembayaran").val() +
                        "&ednomorkartu=" + $("#ednomorkartu").val() + "&edtgltransfer=" + $("#edtgltransfer").val() +
                        "&edtglinsert=" + $("#edtglinsert").val() + "&edtglupdate=" + $("#edtglupdate").val()+ "&edstatus=" + $("#edstatus").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doClearbooking();
                    dosearchbooking('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum lengkap");
    }
}

function dohapusbooking(edidx, edtglbooking) {
    if (confirm("Anda yakin akan menghapus data " + edtglbooking + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrbooking/deletetablebooking/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearbooking();
                    dosearchbooking('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}

function dobatalbooking(edidx, edtglbooking) {
    if (confirm("Anda yakin akan membatalkan booking " + edtglbooking + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrbooking/setBatalbooking/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearbooking();
                    dosearchbooking('-99');
                    doeditbooking(edidx);
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}

function dolanjutbooking(edidx, edtglbooking) {
    if (confirm("Anda yakin akan melanjutkan proses booking " + edtglbooking + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrbooking/setLanjutbooking/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearbooking();
                    dosearchbooking('-99');
                    doeditbooking(edidx);
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchbooking(0);


