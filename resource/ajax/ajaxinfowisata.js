
function dosearchinfowisata(xAwal) {
    doClearinfowisata();
    $(document).ready(function () {        
        $("#edimgutama").myuploadphoto();
        $.ajax({
            url: getBaseURL() + "index.php/ctrinfowisata/searchinfowisata/",
            data: "xAwal=" + xAwal + "&xSearch=" + $("#edSearch").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatainfowisata").html(json.tabledatainfowisata);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditinfowisata(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrinfowisata/editrecinfowisata/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edjudulinfo").val(json.judulinfo);
                $("#eddeskripsihtml").val(json.deskripsihtml);
                //$("#edimgutama").val(json.imgutama);
                $("#edimgutama").val(json.imgutama).trigger('change');
                $("#edmapaddress").val(json.mapadress);
                
                $("#edtglinsert").val(json.tglinsert);
                $("#edtglupdate").val(json.tglupdate);
                $("#edidpegawai").val(json.idpegawai);
                var res = json.mapadress.split(",");
                initialize(res[0], res[1]);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearinfowisata() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edjudulinfo").val("");
        $("#eddeskripsihtml").val("");
        $("#edimgutama").val("");
        $("#edmapaddress").val("");
        $("#edtglinsert").val("");
        $("#edtglupdate").val("");
        $("#edidpegawai").val("");
        $("#edimgutama").val("").trigger('change');
        codeAddress("Denpasar");
    });
}

function dosimpaninfowisata() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrinfowisata/simpaninfowisata/",
            data: "edidx=" + $("#edidx").val() + "&edjudulinfo=" + $("#edjudulinfo").val() + "&eddeskripsihtml=" + $("#eddeskripsihtml").val() + "&edimgutama=" + $("#edimgutama").val() + "&edmapadress=" + $("#edmapaddress").val() + "&edtglinsert=" + $("#edtglinsert").val() + "&edtglupdate=" + $("#edtglupdate").val() + "&edidpegawai=" + $("#edidpegawai").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (msg) {
                doClearinfowisata();
                dosearchinfowisata('-99');
                alert("Data Berhasil Disimpan.... ");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dohapusinfowisata(edidx, edjudulinfo) {
    if (confirm("Anda yakin Akan menghapus data " + edjudulinfo + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrinfowisata/deletetableinfowisata/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearinfowisata();
                    dosearchinfowisata('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchinfowisata(0);


