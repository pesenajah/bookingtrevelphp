function dosearchjenispembayaran(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrjenispembayaran/searchjenispembayaran/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatajenispembayaran").html(json.tabledatajenispembayaran);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditjenispembayaran(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrjenispembayaran/editrecjenispembayaran/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edjenispembayaran").val(json.jenispembayaran);
                $("#edjenispembayaranING").val(json.jenispembayaranING);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearjenispembayaran() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edjenispembayaranING").val("");
    });
}

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}


function dosimpanjenispembayaran() {
    if (!empty($("#edjenispembayaran").val())) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrjenispembayaran/simpanjenispembayaran/",
                data: "edidx=" + $("#edidx").val() + "&edjenispembayaran=" + $("#edjenispembayaran").val()+ "&edjenispembayaranING=" + $("#edjenispembayaranING").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doClearjenispembayaran();
                    dosearchjenispembayaran('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum diisi");
    }

}

function dohapusjenispembayaran(edidx, edjenispembayaran) {
    if (confirm("Anda yakin Akan menghapus data " + edjenispembayaran + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrjenispembayaran/deletetablejenispembayaran/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearjenispembayaran();
                    dosearchjenispembayaran('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchjenispembayaran(0);


