//function dosearchnotifikasiandroid(xAwal) {
//    xSearch = "";
//    try
//    {
//        if ($("#edSearch").val() != "") {
//            xSearch = $("#edSearch").val();
//        }
//    } catch (err) {
//        xSearch = "";
//    }
//    if (typeof (xSearch) == "undefined") {
//        xSearch = "";
//    }
//    $(document).ready(function () {
//        $.ajax({
//            url: getBaseURL() + "index.php/ctrnotifikasiandroid/searchnotifikasiandroid/",
//            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
//            cache: false,
//            dataType: 'json',
//            type: 'POST',
//            success: function (json) {
//                $("#tabledata").html(json.tabledata);
//            },
//            error: function (xmlHttpRequest, textStatus, errorThrown) {
//                start = xmlHttpRequest.responseText.search("<title>") + 7;
//                end = xmlHttpRequest.responseText.search("</title>");
//                errorMsg = " error on search ";
//                if (start > 0 && end > 0)
//                    alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
//                else
//                    alert("Error juga" + errorMsg);
//            }
//        });
//    });
//}
//
//function doeditnotifikasiandroid(edidmember) {
//    $(document).ready(function () {
//        $.ajax({
//            url: getBaseURL() + "index.php/ctrnotifikasiandroid/editrecnotifikasiandroid/",
//            data: "edidmember=" + edidmember,
//            cache: false,
//            dataType: 'json',
//            type: 'POST',
//            success: function (json) {
//                $("#edidmember").val(json.idx);
//                $("#edjudulnotifikasi").val(json.Nmnotifikasiandroid);
//                $("#edmessage").val(json.Kodenotifikasiandroid);
//                $("#edmenuandroid").val(json.keterangan);
//            },
//            error: function (xmlHttpRequest, textStatus, errorThrown) {
//                start = xmlHttpRequest.responseText.search("<title>") + 7;
//                end = xmlHttpRequest.responseText.search("</title>");
//                errorMsg = "OnEdit ";
//                if (start > 0 && end > 0)
//                    alert("On Edit " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
//                else
//                    alert("Error juga " + errorMsg);
//            }
//        });
//    });
//}
//
//function doClearnotifikasiandroid() {
//    $(document).ready(function () {
//        $("#edidmember").val("0");
//        $("#edjudulnotifikasi").val("");
//        $("#edmessage").val("");
//        $("#edmenuandroid").val("");
//    });
//}

function dosimpannotifikasiandroid() {
    $(document).ready(function () {
        
        $("#gbloader").show();
		var selectallmember = 'N';
		if($("#selectallmember").is(":checked")){
			selectallmember = 'Y';
		}
        $.ajax({
            url: getBaseURL() + "index.php/ctrnotifikasiandroid/simpannotifikasiandroid/",
            data: "edidmember=" + $("#edidmember").val() + "&edjudulnotifikasi=" + $("#edjudulnotifikasi").val() + "&edmessage=" + $("#edmessage").val() 
			+ "&edmenuandroid=" + $("#edmenuandroid").val()
			+ "&selectallmember=" + selectallmember,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (msg) {
//                doClearnotifikasiandroid();
//                dosearchnotifikasiandroid('-99');

        $("#gbloader").hide();
                alert("Notifikasi Terkirim");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                start = xmlHttpRequest.responseText.search("<title>") + 7;
                end = xmlHttpRequest.responseText.search("</title>");
                errorMsg = " On Simpan " +xmlHttpRequest.responseText;
                if (start > 0 && end > 0)
                    alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                else
                    alert("Error juga " + errorMsg);
            }
        });
    });
}

//function dohapusnotifikasiandroid(edidmember, edjudulnotifikasi) {
//    if (confirm("Anda yakin Akan menghapus data " + edjudulnotifikasi + "?"))
//    {
//        $(document).ready(function () {
//            $.ajax({
//                url: getBaseURL() + "index.php/ctrnotifikasiandroid/deletetablenotifikasiandroid/",
//                data: "edidmember=" + edidmember,
//                cache: false,
//                dataType: 'json',
//                type: 'POST',
//                success: function (json) {
//                    doClearnotifikasiandroid();
//                    dosearchnotifikasiandroid('-99');
//                },
//                error: function (xmlHttpRequest, textStatus, errorThrown) {
//                    start = xmlHttpRequest.responseText.search("<title>") + 7;
//                    end = xmlHttpRequest.responseText.search("</title>");
//                    errorMsg = " HAPUS ";
//                    if (start > 0 && end > 0)
//                        alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
//                    else
//                        alert("Error juga " + errorMsg);
//                }
//            });
//        });
//    }
//}
//
//
//dosearchnotifikasiandroid(0);
//

function setsearchmember() {
    $(document).ready(function () {
        
        $("#gbloader").hide();
        $('#edSearchmember').autocomplete({
            multiple: true,
            minLength: 2,
            multipleSeparator: ",",
            source: function (request, response) {
                $.ajax({
                    url: getBaseURL() + "index.php/ctrmember/setAutoComplitemember/",
                    data: "edSearchmember=" + $("#edSearchmember").val(),
                    dataType: "json",
                    cache: false,
                    type: "POST",
                    success: function (json) {
                        response($.map(json.data, function (item) {

                            return {
                                label: item.label,
                                value: item.label,
                                idx: item.value
                            }
                        }))
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("Error juga setsearchmember" + xmlHttpRequest.responseText);
                    }
                });
            },
            width: 100,
            max: 10,
            select: function (event, ui) {

                $('#edSearchmember').val(ui.item.value);
                $('#edidmember').val(ui.item.idx);

            },
            change: function (event, ui) {
            }
        });
    });
}


setsearchmember();

