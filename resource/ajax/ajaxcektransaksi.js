$(document).ready(function () {
    $("input#edtglawal").datepicker({
        dateFormat: 'dd-mm-yy',
        onSelect: function () {

        }
    });
    $("input#edtglakhir").datepicker({
        dateFormat: 'dd-mm-yy',
        onSelect: function () {

        }
    });

    $("#tabledatabooking").dialog({
        closeOnEscape: true,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").show();
        },
        autoOpen: false,
        height: 600,
        width: 1200,
        modal: true
    });
});

function setpopup() {
    $(document).ready(function () {
        $("#modalformdetail").dialog({
            autoOpen: false,
            top: 150,
            height: 600,
            width: 600,
            modal: true
        })
    });
}

function doshowimage(idx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrcektransaksi/doShowFormImage/",
            data: "xidx=" + idx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#modalformdetail").html("");
                $("#modalformdetail").html(json.imgkonfirmasi);

                setpopup();
                $("#modalformdetail").dialog("open");

            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dosearchtransaksi(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrcektransaksi/searchtransaksi/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch + "&xiskonfirmasi=" + $("#ediskonfirmasi").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatatransaksi").html(json.tabledatatransaksi);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function dotampildetailbooking(idtransaksi) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrcektransaksi/tampildetailbooking/",
            data: "idtransaksi=" + idtransaksi,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatabooking").html(json.tabledatabooking);
                $("#tabledatabooking").dialog('open');
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function dotampiltransaksi() {
//    alert($("#edNama").val());
    xSearch = "";
    xAwal = 0;
    try
    {
        if ($("#edNama").val() != "") {
            xSearch = $("#edNama").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrcektransaksi/searchtransaksi/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch + "&xtglawal=" + $("#edtglawal").val() + "&xtglakhir=" + $("#edtglakhir").val()
                    + "&xiskonfirmasi=" + $("#ediskonfirmasi").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatatransaksi").html(json.tabledatatransaksi);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doedittransaksi(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrtransaksi/editrectransaksi/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edidbooking").val(json.idbooking);
                $("#edtglbooking").val(json.tglbooking);
                $("#edtglbatalbooking").val(json.tglbatalbooking);
                $("#edjambooking").val(json.jambooking);
                $("#edjambatalbooking").val(json.jambatalbooking);
                $("#edketeranganbatal").val(json.keteranganbatal);
                $("#edharganormal").val(json.harganormal);
                $("#edhargadiscount").val(json.hargadiscount);
                $("#edidvoucher").val(json.idvoucher);
                $("#edidmember").val(json.idmember);
                $("#ednama_member").val(json.nama_member);
                $("#edidpegawai").val(json.idpegawai);
                $("#edspesialrequest").val(json.spesialrequest);
                $("#edtglupdate").val(json.tglupdate);
                $("#edidjenisbayar").val(json.idjenisbayar);
                $("#edtglbayar").val(json.tglbayar);
                $("#ediskonfirmasi").val(json.iskonfirmasi);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doCleartransaksi() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edidbooking").val("");
        $("#edtglbooking").val("");
        $("#edtglbatalbooking").val("");
        $("#edjambooking").val("");
        $("#edjambatalbooking").val("");
        $("#edketeranganbatal").val("");
        $("#edharganormal").val("");
        $("#edhargadiscount").val("");
        $("#edidvoucher").val("");
        $("#edidmember").val("");
        $("#ednama_member").val("");
        $("#edidpegawai").val("");
        $("#edspesialrequest").val("");
        $("#edtglupdate").val("");
        $("#edidjenisbayar").val("");
        $("#edtglbayar").val("");
        $("#ediskonfirmsasi").val("0");
    });
}

function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function empty2(e) {
    switch (e) {
        case "":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function dosimpantransaksi() {
    if (!empty($("#edtglbooking").val()) && !empty($("#edjambooking").val()) && !empty($("#edidbooking").val())
            && !empty($("#edidmember").val()) && !empty($("#edtglperuntukanmulai").val()) && !empty($("#edtglperuntukanselasai").val())
            && !empty($("#edharganormal").val()) && !empty($("#edhargadiscount").val()) && !empty($("#edidmember").val())
            ) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrtransaksi/simpantransaksi/",
                data: "edidx=" + $("#edidx").val() + "&edidbooking=" + $("#edidbooking").val() +
                        "&edtglbooking=" + $("#edtglbooking").val() + "&edtglbatalbooking=" + $("#edtglbatalbooking").val() +
                        "&edjambooking=" + $("#edjambooking").val() + "&edjambatalbooking=" + $("#edjambatalbooking").val() +
                        "&edketeranganbatal=" + $("#edketeranganbatal").val() +
                        "&edharganormal=" + $("#edharganormal").val() + "&edhargadiscount=" + $("#edhargadiscount").val() +
                        "&edidvoucher=" + $("#edidvoucher").val() + "&edidmember=" + $("#edidmember").val() +
                        "&edidpegawai=" + $("#edidpegawai").val() + "&edspesialrequest=" + $("#edspesialrequest").val() +
                        "&edtglupdate=" + $("#edtglupdate").val() + "&edidjenisbayar=" + $("#edidjenisbayar").val() +
                        "&edtglbayar=" + $("#edtglbayar").val() + "&ediskonfirmasi=" + $("#ediskonfirmasi").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doCleartransaksi();
                    dosearchtransaksi('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum lengkap");
    }
}

function dohapustransaksi(edidx, edidbooking) {
    if (confirm("Anda yakin Akan menghapus data " + edidbooking + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrtransaksi/deletetabletransaksi/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doCleartransaksi();
                    dosearchtransaksi('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}

dosearchtransaksi(0);

function dobelumbayar(edidx, edtglbooking, edidmember) {
    if (confirm("Anda menandai booking " + edtglbooking + " belum dibayar ?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrcektransaksi/setBelumBayar/",
                data: "edidx=" + edidx + "&edidmember=" + edidmember,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    dosearchtransaksi(0);
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}

function dotandaibayar(edidx, edtglbooking, edidmember) {
    if (confirm("Anda menandai booking " + edtglbooking + " sudah dibayar ?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrcektransaksi/setSudahBayar/",
                data: "edidx=" + edidx + "&edidmember=" + edidmember,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    dosearchtransaksi(0);
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}

