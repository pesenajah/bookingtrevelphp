
$(document).ready(function () {
    $("#listdetailhotel").dialog({
        closeOnEscape: true,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").show();
        },
        autoOpen: false,
        height: 600,
        width: 1200,
        modal: true
    });
    $("#addOrderFlight").dialog({
        closeOnEscape: true,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").show();
        },
        autoOpen: false,
        height: 600,
        width: 1200,
        modal: true
    });
});

function doshowdetailhotel(edbusiness_uri) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrdataAPI/setListDetailHotel/",
            data: "business_uri=" + edbusiness_uri,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#listdetailhotel").html("");
                $("#listdetailhotel").html("List Detail Hotel<p>" + json.listdetailhotel + "</p>");
                $("#listdetailhotel").dialog("open");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doaddorderflight(flight_id, ret_flight_id, adult,child, infant) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrdataAPI/setDataAddOrderFlight/",
            data: "flight_id=" + flight_id +
                    "&ret_flight_id=" + ret_flight_id +
                    "&adult=" + adult +
                    "&child=" + child +
                    "&infant=" + infant,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#addOrderFlight").html("");
                $("#addOrderFlight").html("Add Order Flight<p>" + json.addOrderFlight + "</p>");
                $("#addOrderFlight").dialog("open");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doCleardataAPI() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edJenisMember").val("");
    });
}

function dosimpandataAPI() {
    if (!empty($("#edJenisMember").val())) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrdataAPI/simpandataAPI/",
                data: "edidx=" + $("#edidx").val() + "&edJenisMember=" + $("#edJenisMember").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doCleardataAPI();
                    dosearchdataAPI('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum diisi");
    }

}

function dohapusdataAPI(edidx, edJenisMember) {
    if (confirm("Anda yakin Akan menghapus data " + edJenisMember + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrdataAPI/deletetabledataAPI/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doCleardataAPI();
                    dosearchdataAPI('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


