function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default :
            return false;
    }
}

function dosearchjenismember(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrjenismember/searchjenismember/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatajenismember").html(json.tabledatajenismember);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditjenismember(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrjenismember/editrecjenismember/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edJenisMember").val(json.JenisMember);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearjenismember() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edJenisMember").val("");
    });
}

function dosimpanjenismember() {
    if (!empty($("#edJenisMember").val())) {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrjenismember/simpanjenismember/",
                data: "edidx=" + $("#edidx").val() + "&edJenisMember=" + $("#edJenisMember").val(),
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (msg) {
                    doClearjenismember();
                    dosearchjenismember('-99');
                    alert("Data Berhasil Disimpan.... ");
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    } else {
        alert("Data belum diisi");
    }

}

function dohapusjenismember(edidx, edJenisMember) {
    if (confirm("Anda yakin Akan menghapus data " + edJenisMember + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrjenismember/deletetablejenismember/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearjenismember();
                    dosearchjenismember('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchjenismember(0);


