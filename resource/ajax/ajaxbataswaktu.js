function dosearchbataswaktu(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrbataswaktu/searchbataswaktu/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatabataswaktu").html(json.tabledatabataswaktu);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditbataswaktu(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrbataswaktu/editrecbataswaktu/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edbataswaktu").val(json.bataswaktu);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearbataswaktu() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edbataswaktu").val("");
    });
}

function dosimpanbataswaktu() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrbataswaktu/simpanbataswaktu/",
            data: "edidx=" + $("#edidx").val() + "&edbataswaktu=" + $("#edbataswaktu").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (msg) {
                doClearbataswaktu();
                dosearchbataswaktu('-99');
                alert("Data Berhasil Disimpan.... ");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dohapusbataswaktu(edidx, edbataswaktu) {
    if (confirm("Anda yakin Akan menghapus data " + edbataswaktu + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrbataswaktu/deletetablebataswaktu/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearbataswaktu();
                    dosearchbataswaktu('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchbataswaktu(0);

function setkelasangka() {
    $(document).ready(function () {
        $('.angka').priceFormat({
            limit: 30,
            centsLimit: 0,
            prefix: '',
            centsSeparator: '',
            thousandsSeparator: '.'
        });
    });

}
setkelasangka();


