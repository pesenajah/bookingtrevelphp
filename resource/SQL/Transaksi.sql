/*
SQLyog Ultimate v10.42 
MySQL - 5.6.12-log : Database - bookingtravel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `bookingtravel`;

/*Table structure for table `booking` */

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `tglbooking` datetime DEFAULT NULL,
  `idproduk` int(10) DEFAULT NULL,
  `iddetailproduk` int(10) DEFAULT NULL,
  `idkategoriproduk` int(10) DEFAULT NULL,
  `idmember` int(10) DEFAULT NULL,
  `tglperuntukandari` date DEFAULT NULL,
  `tglperuntukansampai` date DEFAULT NULL,
  `jmldewasa` int(10) DEFAULT NULL,
  `jmlanak` int(10) DEFAULT NULL,
  `jmlhewan` int(10) DEFAULT NULL,
  `keterangantambahn` text,
  `jmltransfer` float(15,2) DEFAULT NULL,
  `idjenispembayaran` int(10) DEFAULT NULL,
  `nomorkartu` varchar(100) DEFAULT NULL,
  `tgltransfer` date DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  `status` enum('0','1','2') DEFAULT '0' COMMENT '0: awal,1:confirm Booking,2:Final',
  `harga` float(15,2) DEFAULT '0.00',
  `hargadiscount` float(15,2) DEFAULT '0.00',
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `booking` */

insert  into `booking`(`idx`,`tglbooking`,`idproduk`,`iddetailproduk`,`idkategoriproduk`,`idmember`,`tglperuntukandari`,`tglperuntukansampai`,`jmldewasa`,`jmlanak`,`jmlhewan`,`keterangantambahn`,`jmltransfer`,`idjenispembayaran`,`nomorkartu`,`tgltransfer`,`tglinsert`,`tglupdate`,`status`,`harga`,`hargadiscount`) values (2,'2016-10-08 09:45:13',10,16,1,1,'2016-10-11','2016-10-13',2,1,0,'',0.00,2,'','0000-00-00','2016-10-08 09:46:19','2016-10-10 10:20:09','2',NULL,NULL),(3,'2016-10-08 09:45:13',10,16,1,1,'2016-10-11','2016-10-13',2,1,0,'',0.00,2,'','0000-00-00','2016-10-08 09:46:19','2016-10-10 09:51:10','1',NULL,NULL),(4,'2016-10-08 09:45:13',10,16,1,1,'2016-10-11','2016-10-13',2,1,0,'',0.00,2,'','0000-00-00','2016-10-08 09:46:19','2016-10-10 09:44:29','0',NULL,NULL),(5,'2016-10-10 09:55:30',10,16,1,1,'2016-10-11','2016-10-13',2,1,0,'',0.00,2,'','0000-00-00','2016-10-08 09:46:19','2016-10-10 09:44:29','0',NULL,NULL),(6,'0000-00-00 00:00:00',16,47,0,24,'2016-10-19','2016-10-19',10,10,0,'ddghhjj',0.00,0,'null','0000-00-00','2016-10-19 10:25:54','2016-10-23 23:54:36','1',NULL,NULL),(7,'0000-00-00 00:00:00',16,38,0,24,'0000-00-00','0000-00-00',10,10,0,'',0.00,0,'null','0000-00-00','2016-10-19 10:27:56','2016-10-23 23:54:36','1',NULL,NULL),(8,'2016-10-19 11:35:04',20,33,0,24,'2016-10-19','2016-10-19',10,10,0,'',0.00,0,'null','0000-00-00','2016-10-19 11:35:04','2016-10-23 23:54:36','1',NULL,NULL),(9,'2016-10-19 11:36:05',16,29,0,24,'2016-10-19','2016-10-19',10,0,0,'',0.00,0,'null','0000-00-00','2016-10-19 11:36:05','2016-10-23 23:54:36','1',NULL,NULL),(10,'2016-10-19 11:37:34',15,46,0,24,'2016-10-19','2016-10-19',1,0,0,'',0.00,0,'null','0000-00-00','2016-10-19 11:37:34','2016-10-23 23:54:36','1',NULL,NULL),(11,'2016-10-19 11:44:14',15,37,0,24,'2016-10-19','2016-10-19',1,0,0,'',0.00,0,'null','0000-00-00','2016-10-19 11:44:14','2016-10-23 23:54:36','1',NULL,NULL),(12,'2016-10-19 12:29:13',19,50,0,24,'2016-10-19','2016-10-19',1,0,0,'',0.00,0,'null','0000-00-00','2016-10-19 12:29:13','2016-10-23 23:54:36','1',NULL,NULL);

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idbooking` text,
  `tglbooking` datetime DEFAULT NULL,
  `tglbatalbooking` datetime DEFAULT NULL,
  `keteranganbatal` text,
  `harganormal` float DEFAULT NULL,
  `hargadiscount` float DEFAULT NULL,
  `idvoucher` text,
  `idmember` int(10) DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `spesialrequest` text,
  `tglupdate` datetime DEFAULT NULL,
  `idjenisbayar` int(10) DEFAULT NULL,
  `tglbayar` date DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `transaksi` */

insert  into `transaksi`(`idx`,`idbooking`,`tglbooking`,`tglbatalbooking`,`keteranganbatal`,`harganormal`,`hargadiscount`,`idvoucher`,`idmember`,`idpegawai`,`spesialrequest`,`tglupdate`,`idjenisbayar`,`tglbayar`) values (3,'12','2016-10-23 22:52:28','0000-00-00 00:00:00','',36910000,0,'',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(4,'6,7,8,9,10,11,12','2016-10-23 22:59:18','0000-00-00 00:00:00','',36910000,300000,'akhdayfgj,akhdayfg1,',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(5,'6,7,8,9,10,11,12','2016-10-23 23:12:24','0000-00-00 00:00:00','',36910000,100000,'akhdayfgj',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(6,'6,7,8,9,10,11,12','2016-10-23 23:13:18','0000-00-00 00:00:00','',36910000,300000,'akhdayfgj,akhdayfg1',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(7,'6,7,8,9,10,11,12','2016-10-23 23:15:00','0000-00-00 00:00:00','',36910000,300000,'akhdayfgj,akhdayfg1',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(8,'6,7,8,9,10,11,12','2016-10-23 23:16:40','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(9,'6,7,8,9,10,11,12','2016-10-23 23:17:30','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(10,'6,7,8,9,10,11,12','2016-10-23 23:17:50','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(11,'6,7,8,9,10,11,12','2016-10-23 23:18:41','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(12,'6,7,8,9,10,11,12','2016-10-23 23:20:18','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(13,'6,7,8,9,10,11,12','2016-10-23 23:35:57','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(14,'6,7,8,9,10,11,12','2016-10-23 23:36:58','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(15,'6,7,8,9,10,11,12','2016-10-23 23:37:36','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(16,'6,7,8,9,10,11,12','2016-10-23 23:42:47','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(17,'6,7,8,9,10,11,12','2016-10-23 23:46:25','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(18,'6,7,8,9,10,11,12','2016-10-23 23:46:43','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00'),(19,'6,7,8,9,10,11,12','2016-10-23 23:52:31','0000-00-00 00:00:00','',36910000,0,'null',24,0,'','0000-00-00 00:00:00',0,'0000-00-00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
