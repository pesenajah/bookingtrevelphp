/*
SQLyog Ultimate v10.42 
MySQL - 5.6.12-log : Database - bookingtravel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `bookingtravel`;

/*Table structure for table `booking` */

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `tglbooking` datetime DEFAULT NULL,
  `idproduk` int(10) DEFAULT NULL,
  `iddetailproduk` int(10) DEFAULT NULL,
  `idkategoriproduk` int(10) DEFAULT NULL,
  `idmember` int(10) DEFAULT NULL,
  `tglperuntukandari` date DEFAULT NULL,
  `tglperuntukansampai` date DEFAULT NULL,
  `jmldewasa` int(10) DEFAULT NULL,
  `jmlanak` int(10) DEFAULT NULL,
  `jmlhewan` int(10) DEFAULT NULL,
  `keterangantambahn` text,
  `jmltransfer` float(15,2) DEFAULT NULL,
  `idjenispembayaran` int(10) DEFAULT NULL,
  `nomorkartu` varchar(100) DEFAULT NULL,
  `tgltransfer` date DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `booking` */

/*Table structure for table `detailproduk` */

DROP TABLE IF EXISTS `detailproduk`;

CREATE TABLE `detailproduk` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idproduk` int(10) DEFAULT NULL,
  `idkategoriproduk` int(10) DEFAULT NULL,
  `juduldetailproduk` varchar(30) DEFAULT NULL,
  `diskripsiproduk` text,
  `rate` float(15,2) DEFAULT NULL,
  `ratediscount` float(15,2) DEFAULT NULL,
  `rancode` varchar(50) DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `kapasitas` int(10) DEFAULT NULL COMMENT 'orang',
  `standartpemakaian` int(10) DEFAULT NULL,
  `standart` int(10) DEFAULT NULL,
  `idsatuan` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `detailproduk` */

insert  into `detailproduk`(`idx`,`idproduk`,`idkategoriproduk`,`juduldetailproduk`,`diskripsiproduk`,`rate`,`ratediscount`,`rancode`,`tglinsert`,`tglupdate`,`idpegawai`,`kapasitas`,`standartpemakaian`,`standart`,`idsatuan`) values (1,2,1,'test 2','uji coba',20000.00,1000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,NULL,NULL,NULL,NULL),(3,1,1,'uji detail 1','uji detail 1',0.00,0.00,'undefined','2016-05-28 10:11:58','2016-05-28 10:16:04',1,NULL,NULL,NULL,NULL),(4,1,1,'test detail 1 2','test detail 1 2',0.00,0.00,'undefined','2016-05-28 10:14:44','2016-05-28 10:14:44',1,NULL,NULL,NULL,NULL),(5,1,1,'test detail 1 3','test detail 1 3',0.00,0.00,'undefined','2016-05-28 10:15:36','2016-05-28 10:15:36',1,NULL,NULL,NULL,NULL);

/*Table structure for table `imagedetail` */

DROP TABLE IF EXISTS `imagedetail`;

CREATE TABLE `imagedetail` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `iddetailproduk` int(10) DEFAULT NULL,
  `idkategoriproduk` int(10) DEFAULT NULL COMMENT '1:produk,2:detail',
  `linkimage` text,
  `keteranganimage` text,
  `rancode` varchar(50) DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `idproduk` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `imagedetail` */

insert  into `imagedetail`(`idx`,`iddetailproduk`,`idkategoriproduk`,`linkimage`,`keteranganimage`,`rancode`,`tglinsert`,`tglupdate`,`idpegawai`,`idproduk`) values (1,0,1,'engrang.jpg','egrang','undefined','2016-05-25 13:54:47','2016-05-31 09:12:10',1,1);

/*Table structure for table `jenismember` */

DROP TABLE IF EXISTS `jenismember`;

CREATE TABLE `jenismember` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `JenisMember` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `jenismember` */

/*Table structure for table `jenispembayaran` */

DROP TABLE IF EXISTS `jenispembayaran`;

CREATE TABLE `jenispembayaran` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `jenispembayaran` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `jenispembayaran` */

/*Table structure for table `kategoriproduk` */

DROP TABLE IF EXISTS `kategoriproduk`;

CREATE TABLE `kategoriproduk` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `Kategori` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `kategoriproduk` */

insert  into `kategoriproduk`(`idx`,`Kategori`) values (1,'Hotel'),(2,'Travel'),(3,'Paket Wisa');

/*Table structure for table `komponen` */

DROP TABLE IF EXISTS `komponen`;

CREATE TABLE `komponen` (
  `idkomponen` int(10) NOT NULL AUTO_INCREMENT COMMENT 'header,menu,iklan,isi,keterangan filed form',
  `NmKomponen` varchar(40) DEFAULT NULL,
  `isshow` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idkomponen`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `komponen` */

insert  into `komponen`(`idkomponen`,`NmKomponen`,`isshow`) values (1,'Menu View','T'),(2,'Menu Admin','T');

/*Table structure for table `logdelrecord` */

DROP TABLE IF EXISTS `logdelrecord`;

CREATE TABLE `logdelrecord` (
  `idx` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `idxhapus` varchar(10) DEFAULT NULL,
  `keterangan` text,
  `nmtable` varchar(100) DEFAULT NULL,
  `tgllog` datetime DEFAULT NULL,
  `ideksekusi` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `logdelrecord` */

insert  into `logdelrecord`(`idx`,`idxhapus`,`keterangan`,`nmtable`,`tgllog`,`ideksekusi`) values (1,'4',NULL,'jeniskelamin','2014-02-13 09:47:34',1),(2,'5',NULL,'jeniskelamin','2014-02-13 09:49:55',1),(3,'1',NULL,'loguploadfile','2014-02-27 10:38:51',1),(4,'3',NULL,'pejabat','2014-03-12 10:12:48',1),(5,'4',NULL,'pejabat','2014-03-12 10:19:30',1),(6,'5',NULL,'pejabat','2014-03-24 15:06:43',1),(7,'1',NULL,'notausulanpembelian','2014-04-18 08:04:28',4),(8,'3',NULL,'orderpembelian','2014-04-28 10:48:46',4),(9,'4',NULL,'orderpembelian','2014-04-28 11:17:47',4),(10,'1',NULL,'orderpembelian','2014-04-28 11:18:31',4),(11,'5',NULL,'orderpembelian','2014-04-28 11:22:40',4),(12,'7',NULL,'orderpembelian','2014-04-28 11:31:34',4),(13,'6',NULL,'orderpembelian','2014-04-28 11:31:46',4),(14,'9',NULL,'orderpembelian','2014-04-28 11:40:06',4),(15,'8',NULL,'orderpembelian','2014-04-28 11:40:25',4),(16,'10',NULL,'orderpembelian','2014-04-28 11:44:18',4),(17,'11',NULL,'orderpembelian','2014-04-28 13:37:29',4),(18,'16',NULL,'orderpembelian','2014-05-02 14:50:18',4),(19,'1',NULL,'catatanpulangcepat','2014-06-27 10:05:25',1),(20,'2',NULL,'catatanpulangcepat','2014-06-27 10:08:30',1),(21,'2',NULL,'groupshiftkaryawan','2014-07-16 09:27:49',1),(22,'5',NULL,'groupshiftkaryawan','2014-07-16 09:29:35',1),(23,'2',NULL,'detailproduk','2016-05-28 10:16:10',1);

/*Table structure for table `member` */

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(100) DEFAULT NULL,
  `Alamat` text,
  `NoTelpon` varchar(100) DEFAULT NULL,
  `idtoken` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `isblokir` enum('Y','N') DEFAULT 'N',
  `idjenismember` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `member` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `idmenu` int(10) NOT NULL AUTO_INCREMENT,
  `nmmenu` varchar(100) DEFAULT NULL,
  `tipemenu` int(1) DEFAULT NULL COMMENT '1 : page singgle 2: Page list',
  `idkomponen` int(10) DEFAULT NULL,
  `iduser` int(10) DEFAULT '0',
  `parentmenu` int(10) DEFAULT NULL,
  `urlci` varchar(100) DEFAULT NULL,
  `urut` int(10) DEFAULT NULL,
  `jmlgambar` int(1) DEFAULT '0',
  `settingform` text,
  `idaplikasi` int(10) DEFAULT NULL,
  `isumum` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`idmenu`)
) ENGINE=MyISAM AUTO_INCREMENT=207 DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`idmenu`,`nmmenu`,`tipemenu`,`idkomponen`,`iduser`,`parentmenu`,`urlci`,`urut`,`jmlgambar`,`settingform`,`idaplikasi`,`isumum`) values (34,'UMUM',1,1,0,0,'',10,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),(2,'Setting Menu',1,2,0,34,'ctrmenu',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),(6,'LOGOUT',1,2,0,0,'webadmindo/logout',21,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),(35,'Hak User',1,2,0,34,'ctrusermenu',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),(36,'Setting User Group',1,2,0,34,'ctrusergroup',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),(203,'KAMUS',1,2,0,0,'',6,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),(204,'Katergori Produk',1,2,0,203,'ctrkategoriproduk',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),(205,'PRODUK',1,2,0,0,'',4,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),(206,'Hotel',1,2,0,205,'ctrprodukhotel',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N');

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `JudulProduk` varchar(100) DEFAULT NULL,
  `idKategoriProduk` int(10) DEFAULT NULL,
  `Keterangan` text,
  `phonekontak` varchar(50) DEFAULT NULL,
  `NamaKontak` varchar(50) DEFAULT NULL,
  `DiskripsiProduk` text,
  `mapaddress` varchar(100) DEFAULT NULL,
  `rate` float(15,2) DEFAULT NULL,
  `ratediscount` float(15,2) DEFAULT NULL,
  `rancode` varchar(50) DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `kapasitas` int(10) DEFAULT NULL COMMENT 'jml orang',
  `standartpemakaian` int(10) DEFAULT NULL COMMENT 'jml hari/jam',
  `idsatuan` int(10) DEFAULT NULL COMMENT 'hari jam',
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `produk` */

insert  into `produk`(`idx`,`JudulProduk`,`idKategoriProduk`,`Keterangan`,`phonekontak`,`NamaKontak`,`DiskripsiProduk`,`mapaddress`,`rate`,`ratediscount`,`rancode`,`tglinsert`,`tglupdate`,`idpegawai`,`kapasitas`,`standartpemakaian`,`idsatuan`) values (1,'test',1,'undefined','test','test','test','-7.665282232790957, 110.47351639443355',20000.00,1000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,NULL,NULL,NULL),(2,'uji 2',1,'undefined','','uuu','uji 2','-7.272790587238387, 110.44639389687495',100.00,0.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,NULL,NULL,NULL);

/*Table structure for table `satuan` */

DROP TABLE IF EXISTS `satuan`;

CREATE TABLE `satuan` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `satuan` */

/*Table structure for table `tipemenu` */

DROP TABLE IF EXISTS `tipemenu`;

CREATE TABLE `tipemenu` (
  `idTipeMenu` varchar(1) DEFAULT NULL,
  `NmTipeMenu` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tipemenu` */

insert  into `tipemenu`(`idTipeMenu`,`NmTipeMenu`) values ('1','Singgle'),('2','Berbentuk Daftar');

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idproduk` int(10) DEFAULT NULL,
  `iddetailproduk` int(10) DEFAULT NULL,
  `tglbooking` datetime DEFAULT NULL,
  `tglperuntukanmulai` datetime DEFAULT NULL,
  `tglperuntukanselasai` datetime DEFAULT NULL,
  `tglbatalbooking` datetime DEFAULT NULL,
  `keteranganbatal` text,
  `harganormal` float DEFAULT NULL,
  `hargadiscount` float DEFAULT NULL,
  `idvoucher` text,
  `idmember` int(10) DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `spesialrequest` text,
  `tglupdate` datetime DEFAULT NULL,
  `jmlorangdewasa` int(10) DEFAULT NULL,
  `jmlanak` int(10) DEFAULT NULL,
  `idjenisbayar` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `transaksi` */

/*Table structure for table `usergroup` */

DROP TABLE IF EXISTS `usergroup`;

CREATE TABLE `usergroup` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `NmUserGroup` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `usergroup` */

insert  into `usergroup`(`idx`,`NmUserGroup`) values (1,'Admin'),(2,'Admin 2'),(3,'Reviewer');

/*Table structure for table `usermenu` */

DROP TABLE IF EXISTS `usermenu`;

CREATE TABLE `usermenu` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `iduser` int(10) DEFAULT NULL,
  `idmenu` int(10) DEFAULT NULL,
  `idaplikasi` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=6133 DEFAULT CHARSET=latin1;

/*Data for the table `usermenu` */

insert  into `usermenu`(`idx`,`iduser`,`idmenu`,`idaplikasi`) values (6132,1,6,NULL),(6131,1,34,NULL),(6130,1,203,NULL),(6129,1,205,NULL),(6128,1,36,NULL),(6127,1,35,NULL),(6126,1,206,NULL),(6125,1,204,NULL),(6124,1,2,NULL);

/*Table structure for table `usersistem` */

DROP TABLE IF EXISTS `usersistem`;

CREATE TABLE `usersistem` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `npp` varchar(20) DEFAULT NULL,
  `Nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL COMMENT 'Organisasi',
  `NoTelpon` varchar(50) DEFAULT NULL,
  `user` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `statuspeg` int(10) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `ym` varchar(100) DEFAULT NULL,
  `isaktif` enum('Y','N') DEFAULT 'N',
  `idusergroup` int(10) DEFAULT NULL,
  `idkabupaten` int(10) DEFAULT NULL,
  `idpropinsi` int(10) DEFAULT NULL,
  `imehp` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `usersistem` */

insert  into `usersistem`(`idx`,`npp`,`Nama`,`alamat`,`NoTelpon`,`user`,`password`,`statuspeg`,`photo`,`email`,`ym`,`isaktif`,`idusergroup`,`idkabupaten`,`idpropinsi`,`imehp`) values (1,'undefined','User Demo','0','0274747474','demo','demo',0,'undefined','','','',1,173,12,'860205025197033'),(2,'undefined','pengguna lain','0','04532','test','test',0,'undefined','','','',2,100,9,NULL),(3,'undefined','moses','rinjani','0271-765279','moses','moses',0,'undefined','w.moses@yahoo.co.id','w.moses','',1,0,10,'0856625542556');

/*Table structure for table `voucher` */

DROP TABLE IF EXISTS `voucher`;

CREATE TABLE `voucher` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `voucher` text,
  `nominal` float(10,2) DEFAULT NULL,
  `tglberlakudari` datetime DEFAULT NULL,
  `tglberlakusampai` datetime DEFAULT NULL,
  `idmember` int(10) DEFAULT NULL,
  `isterpakai` enum('Y','N') DEFAULT 'N',
  `tglpakai` datetime DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `voucher` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
