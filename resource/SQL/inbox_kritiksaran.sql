/*
SQLyog Enterprise v10.42 
MySQL - 5.6.17 : Database - bookingtravel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bookingtravel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bookingtravel`;

/*Table structure for table `inboxfcm` */

DROP TABLE IF EXISTS `inboxfcm`;

CREATE TABLE `inboxfcm` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idmember` varchar(20) DEFAULT NULL,
  `judul` text,
  `message` text,
  `tglmessage` datetime DEFAULT NULL,
  `isterbaca` enum('Y','N') DEFAULT NULL,
  `idmenuandroid` text,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `inboxfcm` */

/*Table structure for table `kritiksaran` */

DROP TABLE IF EXISTS `kritiksaran`;

CREATE TABLE `kritiksaran` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idmember` int(10) DEFAULT NULL,
  `Saran` text,
  `tglsaran` datetime DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `kritiksaran` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
