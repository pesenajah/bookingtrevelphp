/*
SQLyog Community v12.2.2 (64 bit)
MySQL - 10.1.9-MariaDB : Database - bookingtravel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bookingtravel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bookingtravel`;

/*Table structure for table `booking` */

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `tglbooking` datetime DEFAULT NULL,
  `idproduk` int(10) DEFAULT NULL,
  `iddetailproduk` int(10) DEFAULT NULL,
  `idkategoriproduk` int(10) DEFAULT NULL,
  `idmember` int(10) DEFAULT NULL,
  `tglperuntukandari` datetime DEFAULT NULL,
  `tglperuntukansampai` datetime DEFAULT NULL,
  `jmldewasa` int(10) DEFAULT NULL,
  `jmlanak` int(10) DEFAULT NULL,
  `jmlhewan` int(10) DEFAULT NULL,
  `keterangantambahn` text,
  `jmltransfer` float(15,2) DEFAULT NULL,
  `idjenispembayaran` int(10) DEFAULT NULL,
  `nomorkartu` varchar(100) DEFAULT NULL,
  `tgltransfer` date DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  `status` enum('0','1','2') DEFAULT '0',
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `booking` */

insert  into `booking`(`idx`,`tglbooking`,`idproduk`,`iddetailproduk`,`idkategoriproduk`,`idmember`,`tglperuntukandari`,`tglperuntukansampai`,`jmldewasa`,`jmlanak`,`jmlhewan`,`keterangantambahn`,`jmltransfer`,`idjenispembayaran`,`nomorkartu`,`tgltransfer`,`tglinsert`,`tglupdate`,`status`) values 
(2,'2016-10-08 09:45:13',10,16,1,1,'2016-10-11 09:45:47','2016-10-13 09:45:56',2,1,0,'',0.00,2,'','0000-00-00','2016-10-08 09:46:19','2016-10-10 10:20:09','2'),
(3,'2016-10-08 09:45:13',10,16,1,1,'2016-10-11 09:45:47','2016-10-13 09:45:56',2,1,0,'',0.00,2,'','0000-00-00','2016-10-08 09:46:19','2016-10-10 09:51:10','1'),
(4,'2016-10-08 09:45:13',10,16,1,1,'2016-10-11 09:45:47','2016-10-13 09:45:56',2,1,0,'',0.00,2,'','0000-00-00','2016-10-08 09:46:19','2016-10-10 09:44:29','0'),
(5,'2016-10-10 09:55:30',10,16,1,1,'2016-10-11 09:45:47','2016-10-13 09:45:56',2,1,0,'',0.00,2,'','0000-00-00','2016-10-08 09:46:19','2016-10-10 09:44:29','0');

/*Table structure for table `detailproduk` */

DROP TABLE IF EXISTS `detailproduk`;

CREATE TABLE `detailproduk` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idproduk` int(10) DEFAULT NULL,
  `idkategoriproduk` int(10) DEFAULT NULL,
  `juduldetailproduk` varchar(30) DEFAULT NULL,
  `diskripsiproduk` text,
  `rate` float(15,2) DEFAULT NULL,
  `ratediscount` float(15,2) DEFAULT NULL,
  `rancode` varchar(50) DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `kapasitas` int(10) DEFAULT NULL COMMENT 'orang',
  `standartpemakaian` int(10) DEFAULT NULL,
  `standart` int(10) DEFAULT NULL,
  `idsatuan` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

/*Data for the table `detailproduk` */

insert  into `detailproduk`(`idx`,`idproduk`,`idkategoriproduk`,`juduldetailproduk`,`diskripsiproduk`,`rate`,`ratediscount`,`rancode`,`tglinsert`,`tglupdate`,`idpegawai`,`kapasitas`,`standartpemakaian`,`standart`,`idsatuan`) values 
(24,11,2,'Mobil Travel','Mobil angkutan travel yg nyaman',200000.00,190000.00,'undefined','2016-10-07 11:28:49','2016-10-07 11:28:49',1,10,1,NULL,1),
(22,10,1,'Rooftop','area rooftop hotel',5000000.00,4900000.00,'undefined','2016-10-07 11:07:39','2016-10-07 11:18:18',1,30,1,NULL,1),
(23,10,1,'Ballroom','Area ballroom yang luas dan nyaman',10000000.00,9900000.00,'undefined','2016-10-07 11:14:19','2016-10-07 11:14:19',1,500,1,NULL,1),
(16,10,1,'Kamar Romantis','Kamar dengan nuansa romantis',450000.00,449000.00,'undefined','2016-10-07 10:09:41','2016-10-07 10:09:41',1,2,1,NULL,1),
(25,12,3,'Paket 1','Paket wisata kawasan Bali',1000000.00,990000.00,'undefined','2016-10-07 11:39:00','2016-10-07 11:39:00',1,10,3,NULL,1),
(26,13,1,'Kamar Happy','Kamar Happy',450000.00,445000.00,'undefined','2016-10-10 12:58:35','2016-10-10 12:58:35',1,2,1,NULL,1),
(27,14,1,'Kamar yahud','Kamar yahud',340000.00,330000.00,'undefined','2016-10-10 12:59:34','2016-10-10 12:59:34',1,2,1,NULL,1),
(28,15,1,'Kamar Sehat','Kamar Sehat',240000.00,230000.00,'undefined','2016-10-10 13:00:19','2016-10-10 13:00:19',1,2,1,NULL,1),
(29,16,1,'Kamar Melati','Kamar Melati',300000.00,290000.00,'undefined','2016-10-10 13:01:32','2016-10-10 13:01:32',1,2,1,NULL,1),
(30,17,1,'Kamar Cruiser','Kamar Cruiser',400000.00,390000.00,'undefined','2016-10-10 13:02:20','2016-10-10 13:02:20',1,2,1,NULL,1),
(31,18,1,'Hotel Mentari','Hotel Mentari',300000.00,299000.00,'undefined','2016-10-10 13:03:26','2016-10-10 13:03:26',1,2,1,NULL,1),
(32,19,1,'Kamar Heboh','Hotel Heboh',310000.00,300000.00,'undefined','2016-10-10 13:12:40','2016-10-10 13:12:40',1,2,1,NULL,1),
(33,20,1,'Kamar Ahay','ahay',340000.00,330000.00,'undefined','2016-10-10 13:14:39','2016-10-10 13:14:39',1,2,1,NULL,1),
(34,21,1,'Kamar vegeta','vegeta',370000.00,367000.00,'undefined','2016-10-10 13:15:38','2016-10-11 10:25:38',1,2,1,NULL,1),
(35,13,1,'rooftop','rooftop',12000000.00,11900000.00,'undefined','2016-10-10 13:23:20','2016-10-10 13:23:20',1,30,1,NULL,1),
(36,14,1,'rooftop','rooftop',10000000.00,9800000.00,'undefined','2016-10-10 13:24:32','2016-10-10 13:24:32',1,20,1,NULL,1),
(37,15,1,'rooftop','rooftop',13000000.00,12600000.00,'undefined','2016-10-10 13:26:44','2016-10-10 13:26:44',1,20,1,NULL,1),
(38,16,1,'rooftop','rooftop',9000000.00,8700000.00,'undefined','2016-10-10 13:27:52','2016-10-10 13:27:52',1,15,1,NULL,1),
(39,17,1,'rooftop','rooftop',14000000.00,13800000.00,'undefined','2016-10-10 13:29:16','2016-10-10 13:29:16',1,25,1,NULL,1),
(40,18,1,'rooftop','rooftop',9500000.00,9300000.00,'undefined','2016-10-10 13:30:45','2016-10-10 13:30:45',1,20,1,NULL,1),
(41,19,1,'rooftop','rooftop',7000000.00,6750000.00,'undefined','2016-10-10 13:31:40','2016-10-10 13:31:40',1,15,1,NULL,1),
(42,20,1,'rooftop','rooftop',8000000.00,7800000.00,'undefined','2016-10-10 13:36:26','2016-10-10 13:36:26',1,15,1,NULL,1),
(43,21,1,'rooftop','rooftop',7000000.00,6500000.00,'undefined','2016-10-10 13:37:49','2016-10-10 13:37:49',1,20,1,NULL,1),
(44,13,1,'meeting room','meeting room',7000000.00,6800000.00,'undefined','2016-10-10 13:52:05','2016-10-10 13:52:05',1,20,12,NULL,2),
(45,14,1,'meeting room','meeting room',4000000.00,3900000.00,'undefined','2016-10-10 13:54:19','2016-10-10 13:54:19',1,20,12,NULL,2),
(46,15,1,'meeting room','meeting room',5000000.00,4500000.00,'undefined','2016-10-10 13:56:44','2016-10-10 13:56:44',1,20,12,NULL,2),
(47,16,1,'meeting room','meeting room',6000000.00,5600000.00,'undefined','2016-10-10 13:57:52','2016-10-10 13:57:52',1,20,12,NULL,2),
(48,17,1,'meeting room','meeting room',4000000.00,3800000.00,'undefined','2016-10-10 14:00:14','2016-10-10 14:00:14',1,20,12,NULL,2),
(49,18,1,'meeting room','meeting room',5000000.00,4500000.00,'undefined','2016-10-10 14:01:21','2016-10-10 14:01:21',1,20,12,NULL,2),
(50,19,1,'meeting room','meeting room',5000000.00,4890000.00,'undefined','2016-10-11 08:31:18','2016-10-11 08:31:18',1,20,12,NULL,2),
(51,20,1,'meeting room','meeting room',4000000.00,3960000.00,'undefined','2016-10-11 08:32:51','2016-10-11 08:32:51',1,20,12,NULL,2),
(52,21,1,'meeting room','meeting room',8000000.00,7980000.00,'undefined','2016-10-11 08:33:41','2016-10-11 08:33:41',1,25,12,NULL,2),
(53,11,2,'mobil travel','mobil travel',200000.00,190000.00,'undefined','2016-10-12 09:58:16','2016-10-12 09:58:16',1,10,1,NULL,1),
(58,22,2,'mobil travel','mobil travel',200000.00,189000.00,'undefined','2016-10-12 10:28:42','2016-10-12 10:28:42',1,10,1,NULL,1),
(67,22,2,'mobil','mobil',200000.00,190000.00,'undefined','2016-10-12 13:04:36','2016-10-12 13:11:36',1,10,1,NULL,1),
(71,11,2,'mobil','mobil',180000.00,170000.00,'undefined','2016-10-12 13:25:52','2016-10-12 13:25:52',1,10,1,NULL,1),
(72,23,2,'mobil','mobil',240000.00,230000.00,'undefined','2016-10-12 13:45:08','2016-10-12 13:45:08',1,10,1,NULL,1),
(73,24,2,'mobil','mobil',160000.00,150000.00,'undefined','2016-10-12 13:48:41','2016-10-12 13:48:41',1,10,1,NULL,1),
(74,25,2,'mobil','mobil',180000.00,170000.00,'undefined','2016-10-12 14:00:50','2016-10-12 14:00:50',1,10,1,NULL,1),
(75,26,2,'mobil','mobil',110000.00,110000.00,'undefined','2016-10-12 14:08:21','2016-10-12 14:08:21',1,10,1,NULL,1),
(76,27,2,'mobil','mobil',200000.00,190000.00,'undefined','2016-10-12 14:10:46','2016-10-12 14:10:46',1,10,1,NULL,1),
(77,28,2,'mobil','mobil',200000.00,190000.00,'undefined','2016-10-12 14:12:07','2016-10-12 14:12:07',1,10,1,NULL,1),
(78,29,2,'mobil','mobil',200000.00,200000.00,'undefined','2016-10-12 14:13:14','2016-10-12 14:13:14',1,10,1,NULL,1),
(79,30,2,'mobil','mobil',200000.00,180000.00,'undefined','2016-10-12 14:13:55','2016-10-12 14:13:55',1,10,1,NULL,1);

/*Table structure for table `imagedetail` */

DROP TABLE IF EXISTS `imagedetail`;

CREATE TABLE `imagedetail` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `iddetailproduk` int(10) DEFAULT NULL,
  `idkategoriproduk` int(10) DEFAULT NULL COMMENT '1:produk,2:detail',
  `linkimage` text,
  `keteranganimage` text,
  `rancode` varchar(50) DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `idproduk` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;

/*Data for the table `imagedetail` */

insert  into `imagedetail`(`idx`,`iddetailproduk`,`idkategoriproduk`,`linkimage`,`keteranganimage`,`rancode`,`tglinsert`,`tglupdate`,`idpegawai`,`idproduk`) values 
(23,0,3,'bali.jpg','Paket Wisata Bali','undefined','2016-10-07 11:39:36','2016-10-07 11:39:36',1,12),
(22,24,2,'mobil.jpg','Mobil 1','undefined','2016-10-07 11:31:33','2016-10-07 11:31:33',1,11),
(21,0,2,'travel.jpg','Travel Hidayah','undefined','2016-10-07 11:30:50','2016-10-07 11:30:50',1,11),
(19,22,1,'rooftop.jpg','rooftop hotel','undefined','2016-10-07 11:24:40','2016-10-07 11:24:40',1,10),
(20,16,1,'kamar1.jpg','kamar romantis','undefined','2016-10-07 11:25:02','2016-10-07 11:25:02',1,10),
(17,0,1,'1922_ho_00_p_346x260.jpg','Wijaya','undefined','2016-10-07 11:20:51','2016-10-10 11:44:28',1,13),
(18,23,1,'ballroom.jpg','Area ballroom','undefined','2016-10-07 11:24:16','2016-10-07 11:24:16',1,10),
(24,25,3,'paket1.jpg','Paket 1','undefined','2016-10-07 11:40:06','2016-10-07 11:40:06',1,12),
(25,0,1,'bali.jpg','Bali','undefined','2016-10-08 11:01:34','2016-10-08 11:01:34',1,10),
(26,0,1,'393775_15092210170036251500.jpg','Blitz','undefined','2016-10-10 11:44:50','2016-10-10 11:44:50',1,14),
(27,0,1,'27035161.jpg','Yusro','undefined','2016-10-10 11:45:53','2016-10-10 11:45:53',1,15),
(28,0,1,'bale-kambang.jpg','Kuta Selatan','undefined','2016-10-10 11:46:14','2016-10-10 11:46:14',1,16),
(29,0,1,'ded08ad691a05d8ca060670f49e6ce23.jpg','Karangasem','undefined','2016-10-10 11:46:43','2016-10-10 11:48:00',1,18),
(30,0,1,'home-sunsetbalihotel-1.jpg','Nusa Penida','undefined','2016-10-10 11:49:28','2016-10-10 11:49:28',1,19),
(31,0,1,'medplaya-hotel-bali-12.jpg','Ubud','undefined','2016-10-10 11:50:29','2016-10-10 11:50:29',1,20),
(32,0,1,'Zodiak-At-Seminyak-Bali-Hotel-1.jpg','Klungkung','undefined','2016-10-10 11:51:11','2016-10-10 11:51:11',1,21),
(33,0,1,'Medplaya-Hotel-Bali-photos-Exterior.JPEG','Singaraja','undefined','2016-10-10 12:52:14','2016-10-10 12:52:14',1,17),
(34,26,1,'7537_ro_00_p_1024x768.jpg','happy','undefined','2016-10-10 12:58:52','2016-10-10 12:58:52',1,13),
(35,27,1,'1449507Untitled-51780x390.jpg','yahud','undefined','2016-10-10 13:04:15','2016-10-10 13:04:15',1,14),
(36,29,1,'bali_gareden_-_superior.jpg','melati','undefined','2016-10-10 13:10:27','2016-10-10 13:10:27',1,16),
(37,30,1,'hotel-di-bali.jpg','cruiser','undefined','2016-10-10 13:11:06','2016-10-10 13:11:06',1,17),
(38,31,1,'Kamar-di-B-Hotel-Bali-Spa.jpg','mentari','undefined','2016-10-10 13:11:46','2016-10-10 13:11:46',1,18),
(39,32,1,'Kamar-hotel1.jpg','kamar heboh','undefined','2016-10-10 13:13:52','2016-10-10 13:13:52',1,19),
(40,33,1,'Kamar-Hotel-Sun-Island-Kuta.jpg','ahay','undefined','2016-10-10 13:14:54','2016-10-10 13:14:54',1,20),
(41,35,1,'360_tga_bali_0623.jpg','rooftop','undefined','2016-10-10 13:23:35','2016-10-10 13:23:35',1,13),
(42,36,1,'1414644517951.jpg','rooftop','undefined','2016-10-10 13:24:49','2016-10-10 13:24:49',1,14),
(43,37,1,'dwp-main-pic.w.jpg','rooftop','undefined','2016-10-10 13:27:10','2016-10-10 13:27:10',1,15),
(44,38,1,'image_350368-l-hotel-bali-luna-bar.jpg','rooftop','undefined','2016-10-10 13:28:08','2016-10-10 13:28:08',1,16),
(45,39,1,'love-f-hotel-rooftop-in-bali.jpg','rooftop','undefined','2016-10-10 13:29:31','2016-10-10 13:29:31',1,17),
(46,40,1,'luna-bar.jpg','rooftop','undefined','2016-10-10 13:31:00','2016-10-10 13:31:00',1,18),
(47,41,1,'Sky-Lounge.jpg','rooftop','undefined','2016-10-10 13:31:53','2016-10-10 13:31:53',1,19),
(48,42,1,'view4b-1412242948.jpg','rooftop','undefined','2016-10-10 13:36:52','2016-10-10 13:36:52',1,20),
(49,43,1,'image11.jpg','rooftop','undefined','2016-10-10 13:38:16','2016-10-10 13:38:16',1,21),
(50,44,1,'3693.jpg','meeting room','undefined','2016-10-10 13:52:35','2016-10-10 13:52:35',1,13),
(51,45,1,'Ayana-Resort-Spa-9.JPG','meeting room','undefined','2016-10-10 13:54:46','2016-10-10 13:54:46',1,14),
(52,46,1,'bali-dynasty-resort-meeting-room.43.jpg','meeting room','undefined','2016-10-10 13:57:00','2016-10-10 13:57:00',1,15),
(53,47,1,'CN_meetingroom01.jpg','meeting room','undefined','2016-10-10 13:58:17','2016-10-10 13:58:17',1,16),
(54,48,1,'index.jpg','meeting room','undefined','2016-10-10 14:00:27','2016-10-10 14:00:27',1,17),
(55,49,1,'xGrand-Hyatt-Bali-Boardroom-Thumbnail.jpg.pagespeed.ic._B7f34pymD.jpg','meeting room','undefined','2016-10-11 08:30:41','2016-10-11 08:30:41',1,18),
(56,50,1,'slide-meeting.jpg','meeting room','undefined','2016-10-11 08:31:37','2016-10-11 08:31:37',1,19),
(57,51,1,'meetings_venues_masthead.jpg','meeting room','undefined','2016-10-11 08:33:09','2016-10-11 08:33:09',1,20),
(58,52,1,'kuta-lagoon-resort-meeting-room.jpg','meeting room','undefined','2016-10-11 08:33:56','2016-10-11 08:33:56',1,21),
(59,34,1,'hotel-bali-sorgawi-1.jpg','vegeta','undefined','2016-10-11 10:26:50','2016-10-11 10:26:50',1,21),
(60,16,1,'kamar2.jpg','kamar','undefined','2016-10-11 12:11:44','2016-10-11 12:11:44',1,10),
(61,16,1,'kamar5.jpg','kamar','undefined','2016-10-11 12:13:18','2016-10-11 12:13:18',1,10),
(62,26,1,'kamar2.jpg','kamar','undefined','2016-10-11 12:14:02','2016-10-11 12:14:02',1,13),
(63,26,1,'kamar5.jpg','kamar','undefined','2016-10-11 12:14:12','2016-10-11 12:14:12',1,13),
(64,27,1,'kamar2.jpg','kmar','undefined','2016-10-11 12:15:03','2016-10-11 12:15:03',1,14),
(65,27,1,'kamar5.jpg','kamar','undefined','2016-10-11 12:15:12','2016-10-11 12:15:12',1,14),
(66,28,1,'kamar2.jpg','kamar','undefined','2016-10-11 12:15:37','2016-10-11 12:15:37',1,15),
(67,28,1,'kamar5.jpg','kamar','undefined','2016-10-11 12:15:46','2016-10-11 12:15:46',1,15),
(68,29,1,'kamar2.jpg','kamar','undefined','2016-10-11 12:16:20','2016-10-11 12:16:20',1,16),
(69,30,1,'kamar2.jpg','kamar','undefined','2016-10-11 12:18:06','2016-10-11 12:18:06',1,17),
(70,30,1,'kamar5.jpg','kamar','undefined','2016-10-11 12:18:18','2016-10-11 12:18:18',1,17),
(71,31,1,'kamar2.jpg','kamaar','undefined','2016-10-11 12:18:42','2016-10-11 12:18:42',1,18),
(72,31,1,'kamar5.jpg','kmar','undefined','2016-10-11 12:18:50','2016-10-11 12:18:50',1,18),
(73,32,1,'kamar2.jpg','kamar','undefined','2016-10-11 12:34:26','2016-10-11 12:34:26',1,19),
(74,32,1,'kamar5.jpg','kamar','undefined','2016-10-11 12:34:37','2016-10-11 12:34:37',1,19),
(75,33,1,'kamar2.jpg','kamar','undefined','2016-10-11 12:35:10','2016-10-11 12:35:10',1,20),
(76,33,1,'kamar5.jpg','kamar','undefined','2016-10-11 12:35:25','2016-10-11 12:35:25',1,20),
(77,34,1,'kamar2.jpg','kamar','undefined','2016-10-11 12:35:52','2016-10-11 12:35:52',1,21),
(78,34,1,'kamar5.jpg','kamar','undefined','2016-10-11 12:36:01','2016-10-11 12:36:01',1,21),
(79,0,2,'blog-agen-travel-surabaya-ke-bali-terpercaya.jpg','travel','undefined','2016-10-11 12:45:00','2016-10-11 12:45:00',1,22),
(80,0,2,'header web shareefa tour.png','Sana','undefined','2016-10-11 12:47:03','2016-10-11 12:47:03',1,23),
(81,0,2,'pti-travel.jpg','pandawa','undefined','2016-10-11 12:54:27','2016-10-11 12:54:27',1,24),
(82,0,2,'travel bali prima.png','travel','undefined','2016-10-11 12:54:48','2016-10-11 12:54:48',1,25),
(83,0,2,'logo antik 1.png','antik','undefined','2016-10-12 09:06:45','2016-10-12 09:06:45',1,26),
(84,0,2,'Wira-Tour-Travel-Bali-Logo.jpg','wira','undefined','2016-10-12 09:32:43','2016-10-12 09:32:43',1,29),
(85,0,2,'iwata.gif','iwata','undefined','2016-10-12 09:35:30','2016-10-12 09:35:30',1,30),
(86,24,2,'mobiltravel1.png','mobil travel','undefined','2016-10-12 09:55:19','2016-10-12 09:55:19',1,11),
(87,24,2,'mobiltravel1.png','mobil travel','undefined','2016-10-12 09:55:46','2016-10-12 09:55:46',1,11),
(88,53,2,'mobiltravel2.jpg','mobil travel','undefined','2016-10-12 09:58:33','2016-10-12 09:58:33',1,11),
(89,53,2,'mobiltravel2.jpg','mobil travel','undefined','2016-10-12 09:58:49','2016-10-12 09:58:49',1,11),
(90,53,2,'mobiltravel2.jpg','mobil travel','undefined','2016-10-12 09:58:57','2016-10-12 09:58:57',1,11),
(91,58,2,'mobiltravel1.png','mobil travel','undefined','2016-10-12 10:29:29','2016-10-12 10:29:29',1,22),
(92,58,2,'mobiltravel1.png','mobil travel','undefined','2016-10-12 10:29:37','2016-10-12 10:29:37',1,22),
(93,58,2,'mobiltravel1.png','mobil travel','undefined','2016-10-12 10:29:45','2016-10-12 10:29:45',1,22),
(94,71,2,'mobiltravel3.jpg','mobil','undefined','2016-10-12 13:33:03','2016-10-12 13:33:03',1,11),
(95,71,2,'mobiltravel3.jpg','mobil','undefined','2016-10-12 13:33:18','2016-10-12 13:33:18',1,11),
(96,71,2,'mobiltravel3.jpg','mobil','undefined','2016-10-12 13:33:41','2016-10-12 13:33:41',1,11),
(97,67,2,'mobiltravel2.jpg','mobil','undefined','2016-10-12 13:43:58','2016-10-12 13:43:58',1,22),
(98,67,2,'mobiltravel2.jpg','mobil','undefined','2016-10-12 13:44:16','2016-10-12 13:44:16',1,22),
(99,72,2,'mobiltravel1.png','mobil','undefined','2016-10-12 13:45:30','2016-10-12 13:45:30',1,23),
(100,72,2,'mobiltravel2.jpg','mobil','undefined','2016-10-12 13:45:41','2016-10-12 13:45:41',1,23),
(101,72,2,'mobiltravel3.jpg','mobil','undefined','2016-10-12 13:45:52','2016-10-12 13:45:52',1,23),
(102,73,2,'mobiltravel3.jpg','mobil','undefined','2016-10-12 13:49:36','2016-10-12 13:49:36',1,24),
(103,73,2,'mobiltravel2.jpg','mobil','undefined','2016-10-12 13:50:20','2016-10-12 13:50:20',1,24),
(104,73,2,'mobiltravel1.png','mobil','undefined','2016-10-12 13:57:46','2016-10-12 13:57:46',1,24),
(105,74,2,'mobiltravel1.png','mobil','undefined','2016-10-12 14:01:06','2016-10-12 14:01:06',1,25),
(106,74,2,'mobiltravel1.png','mobil','undefined','2016-10-12 14:01:37','2016-10-12 14:01:37',1,25),
(107,74,2,'mobiltravel1.png','mobil','undefined','2016-10-12 14:02:17','2016-10-12 14:02:17',1,25),
(108,74,2,'mobiltravel2.jpg','mobil','undefined','2016-10-12 14:02:28','2016-10-12 14:02:28',1,25),
(109,74,2,'mobiltravel2.jpg','mobil','undefined','2016-10-12 14:05:04','2016-10-12 14:05:04',1,25),
(110,74,2,'mobiltravel3.jpg','mobil','undefined','2016-10-12 14:05:15','2016-10-12 14:05:15',1,25),
(111,74,2,'mobiltravel3.jpg','mobil','undefined','2016-10-12 14:05:28','2016-10-12 14:05:28',1,25),
(112,75,2,'mobiltravel2.jpg','mobil','undefined','2016-10-12 14:08:35','2016-10-12 14:08:35',1,26),
(113,76,2,'mobiltravel2.jpg','mobil','undefined','2016-10-12 14:10:58','2016-10-12 14:10:58',1,27),
(114,77,2,'mobiltravel3.jpg','mobil','undefined','2016-10-12 14:12:28','2016-10-12 14:12:28',1,28),
(115,78,2,'mobiltravel1.png','mobil','undefined','2016-10-12 14:13:28','2016-10-12 14:13:28',1,29),
(116,79,2,'mobiltravel3.jpg','mobil','undefined','2016-10-12 14:14:07','2016-10-12 14:14:07',1,30),
(117,79,2,'mobiltravel1.png','mobil','undefined','2016-10-12 14:14:25','2016-10-12 14:14:25',1,30);

/*Table structure for table `jenismember` */

DROP TABLE IF EXISTS `jenismember`;

CREATE TABLE `jenismember` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `JenisMember` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `jenismember` */

insert  into `jenismember`(`idx`,`JenisMember`) values 
(2,'Silver'),
(3,'Gold'),
(4,'Platinum');

/*Table structure for table `jenispembayaran` */

DROP TABLE IF EXISTS `jenispembayaran`;

CREATE TABLE `jenispembayaran` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `jenispembayaran` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `jenispembayaran` */

insert  into `jenispembayaran`(`idx`,`jenispembayaran`) values 
(2,'Setor Tunai'),
(3,'Transfer'),
(4,'Tunai');

/*Table structure for table `kategoriproduk` */

DROP TABLE IF EXISTS `kategoriproduk`;

CREATE TABLE `kategoriproduk` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `Kategori` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `kategoriproduk` */

insert  into `kategoriproduk`(`idx`,`Kategori`) values 
(1,'Hotel'),
(2,'Travel'),
(3,'Paket Wisata');

/*Table structure for table `komponen` */

DROP TABLE IF EXISTS `komponen`;

CREATE TABLE `komponen` (
  `idkomponen` int(10) NOT NULL AUTO_INCREMENT COMMENT 'header,menu,iklan,isi,keterangan filed form',
  `NmKomponen` varchar(40) DEFAULT NULL,
  `isshow` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idkomponen`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `komponen` */

insert  into `komponen`(`idkomponen`,`NmKomponen`,`isshow`) values 
(1,'Menu View','T'),
(2,'Menu Admin','T');

/*Table structure for table `logdelrecord` */

DROP TABLE IF EXISTS `logdelrecord`;

CREATE TABLE `logdelrecord` (
  `idx` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `idxhapus` varchar(10) DEFAULT NULL,
  `keterangan` text,
  `nmtable` varchar(100) DEFAULT NULL,
  `tgllog` datetime DEFAULT NULL,
  `ideksekusi` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

/*Data for the table `logdelrecord` */

insert  into `logdelrecord`(`idx`,`idxhapus`,`keterangan`,`nmtable`,`tgllog`,`ideksekusi`) values 
(1,'4',NULL,'jeniskelamin','2014-02-13 09:47:34',1),
(2,'5',NULL,'jeniskelamin','2014-02-13 09:49:55',1),
(3,'1',NULL,'loguploadfile','2014-02-27 10:38:51',1),
(4,'3',NULL,'pejabat','2014-03-12 10:12:48',1),
(5,'4',NULL,'pejabat','2014-03-12 10:19:30',1),
(6,'5',NULL,'pejabat','2014-03-24 15:06:43',1),
(7,'1',NULL,'notausulanpembelian','2014-04-18 08:04:28',4),
(8,'3',NULL,'orderpembelian','2014-04-28 10:48:46',4),
(9,'4',NULL,'orderpembelian','2014-04-28 11:17:47',4),
(10,'1',NULL,'orderpembelian','2014-04-28 11:18:31',4),
(11,'5',NULL,'orderpembelian','2014-04-28 11:22:40',4),
(12,'7',NULL,'orderpembelian','2014-04-28 11:31:34',4),
(13,'6',NULL,'orderpembelian','2014-04-28 11:31:46',4),
(14,'9',NULL,'orderpembelian','2014-04-28 11:40:06',4),
(15,'8',NULL,'orderpembelian','2014-04-28 11:40:25',4),
(16,'10',NULL,'orderpembelian','2014-04-28 11:44:18',4),
(17,'11',NULL,'orderpembelian','2014-04-28 13:37:29',4),
(18,'16',NULL,'orderpembelian','2014-05-02 14:50:18',4),
(19,'1',NULL,'catatanpulangcepat','2014-06-27 10:05:25',1),
(20,'2',NULL,'catatanpulangcepat','2014-06-27 10:08:30',1),
(21,'2',NULL,'groupshiftkaryawan','2014-07-16 09:27:49',1),
(22,'5',NULL,'groupshiftkaryawan','2014-07-16 09:29:35',1),
(23,'2',NULL,'detailproduk','2016-05-28 10:16:10',1),
(24,'4',NULL,'produk','2016-09-21 11:14:07',1),
(25,'7',NULL,'imagedetail','2016-09-21 15:47:22',1),
(26,'1',NULL,'jenismember','2016-09-22 12:57:05',1),
(27,'2',NULL,'member','2016-10-01 16:55:17',1),
(28,'3',NULL,'member','2016-10-01 16:55:43',1),
(29,'4',NULL,'member','2016-10-01 17:03:14',1),
(30,'6',NULL,'produk','2016-10-01 17:12:33',1),
(31,'5',NULL,'member','2016-10-01 17:18:13',1),
(32,'6',NULL,'member','2016-10-01 17:21:53',1),
(33,'7',NULL,'member','2016-10-01 17:22:35',1),
(34,'7',NULL,'produk','2016-10-01 17:32:02',1),
(35,'8',NULL,'produk','2016-10-03 11:14:28',1),
(36,'1',NULL,'jenispembayaran','2016-10-03 11:56:05',1),
(37,'5',NULL,'jenispembayaran','2016-10-05 13:36:36',1),
(38,'5',NULL,'jenismember','2016-10-05 13:39:47',1),
(39,'3',NULL,'satuan','2016-10-05 13:42:13',1),
(40,'4',NULL,'kategoriproduk','2016-10-05 13:47:34',1),
(41,'5',NULL,'kategoriproduk','2016-10-05 13:48:59',1),
(42,'12',NULL,'imagedetail','2016-10-05 13:52:57',1),
(43,'9',NULL,'detailproduk','2016-10-05 14:10:57',1),
(44,'13',NULL,'imagedetail','2016-10-05 14:42:45',1),
(45,'1',NULL,'transaksi','2016-10-06 10:52:16',1),
(46,'13',NULL,'detailproduk','2016-10-07 09:48:12',1),
(47,'18',NULL,'detailproduk','2016-10-07 10:58:16',1),
(48,'19',NULL,'detailproduk','2016-10-07 11:00:55',1),
(49,'20',NULL,'detailproduk','2016-10-07 11:04:51',1),
(50,'21',NULL,'detailproduk','2016-10-07 11:07:14',1),
(51,'54',NULL,'detailproduk','2016-10-12 10:00:38',1),
(52,'55',NULL,'detailproduk','2016-10-12 10:19:44',1),
(53,'56',NULL,'detailproduk','2016-10-12 10:24:34',1),
(54,'57',NULL,'detailproduk','2016-10-12 10:27:47',1),
(55,'60',NULL,'detailproduk','2016-10-12 10:40:00',1),
(56,'61',NULL,'detailproduk','2016-10-12 10:59:28',1),
(57,'62',NULL,'detailproduk','2016-10-12 12:38:46',1),
(58,'66',NULL,'detailproduk','2016-10-12 12:39:56',1),
(59,'68',NULL,'detailproduk','2016-10-12 13:15:56',1),
(60,'69',NULL,'detailproduk','2016-10-12 13:19:08',1),
(61,'70',NULL,'detailproduk','2016-10-12 13:25:30',1);

/*Table structure for table `member` */

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(100) DEFAULT NULL,
  `Alamat` text,
  `NoTelpon` varchar(100) DEFAULT NULL,
  `idtoken` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `isblokir` enum('Y','N') DEFAULT 'N',
  `idjenismember` int(10) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `member` */

insert  into `member`(`idx`,`Nama`,`Alamat`,`NoTelpon`,`idtoken`,`email`,`tglinsert`,`isblokir`,`idjenismember`,`password`) values 
(1,'Gregorius Agung Purwanto Nugroho','Sedayu','085643266799','uhfushff','greigo.mail@gmail.com','2016-10-01 00:00:00','N',4,'hahaha');

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `idmenu` int(10) NOT NULL AUTO_INCREMENT,
  `nmmenu` varchar(100) DEFAULT NULL,
  `tipemenu` int(1) DEFAULT NULL COMMENT '1 : page singgle 2: Page list',
  `idkomponen` int(10) DEFAULT NULL,
  `iduser` int(10) DEFAULT '0',
  `parentmenu` int(10) DEFAULT NULL,
  `urlci` varchar(100) DEFAULT NULL,
  `urut` int(10) DEFAULT NULL,
  `jmlgambar` int(1) DEFAULT '0',
  `settingform` text,
  `idaplikasi` int(10) DEFAULT NULL,
  `isumum` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`idmenu`)
) ENGINE=MyISAM AUTO_INCREMENT=218 DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`idmenu`,`nmmenu`,`tipemenu`,`idkomponen`,`iduser`,`parentmenu`,`urlci`,`urut`,`jmlgambar`,`settingform`,`idaplikasi`,`isumum`) values 
(34,'UMUM',1,1,0,0,'',10,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),
(2,'Setting Menu',1,2,0,34,'ctrmenu',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),
(6,'LOGOUT',1,2,0,0,'webadmindo/logout',21,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),
(35,'Hak User',1,2,0,34,'ctrusermenu',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),
(36,'Setting User Group',1,2,0,34,'ctrusergroup',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',1,'Y'),
(203,'KAMUS',1,2,0,0,'',6,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(204,'Kategori Produk',1,2,0,203,'ctrkategoriproduk',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(205,'PRODUK',1,2,0,0,'',4,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(206,'Hotel',1,2,0,205,'ctrprodukhotel',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(207,'Travel',1,2,0,205,'ctrproduktravel',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(208,'Paket Wisata',1,2,0,205,'ctrprodukwisata',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(209,'Satuan',1,2,0,203,'ctrsatuan',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(210,'Jenis Member',1,2,0,203,'ctrjenismember',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(211,'Jenis Pembayaran',1,2,0,203,'ctrjenispembayaran',4,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(212,'MEMBER',1,2,0,0,'',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(213,'Data Member',1,2,0,212,'ctrmember',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(214,'TRANSAKSI',1,2,0,0,'',0,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(215,'Booking',1,2,0,214,'ctrbooking',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(216,'Transaksi',1,2,0,214,'ctrtransaksi',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N'),
(217,'Voucher',1,2,0,214,'ctrvoucher',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;',NULL,'N');

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `JudulProduk` varchar(100) DEFAULT NULL,
  `idKategoriProduk` int(10) DEFAULT NULL,
  `Keterangan` text,
  `phonekontak` varchar(50) DEFAULT NULL,
  `NamaKontak` varchar(50) DEFAULT NULL,
  `DiskripsiProduk` text,
  `mapaddress` varchar(100) DEFAULT NULL,
  `rate` float(15,2) DEFAULT NULL,
  `ratediscount` float(15,2) DEFAULT NULL,
  `rancode` varchar(50) DEFAULT NULL,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `kapasitas` int(10) DEFAULT NULL COMMENT 'jml orang',
  `standartpemakaian` int(10) DEFAULT NULL COMMENT 'jml hari/jam',
  `idsatuan` int(10) DEFAULT NULL COMMENT 'hari jam',
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `produk` */

insert  into `produk`(`idx`,`JudulProduk`,`idKategoriProduk`,`Keterangan`,`phonekontak`,`NamaKontak`,`DiskripsiProduk`,`mapaddress`,`rate`,`ratediscount`,`rancode`,`tglinsert`,`tglupdate`,`idpegawai`,`kapasitas`,`standartpemakaian`,`idsatuan`) values 
(13,'Hotel Wijaya',1,'undefined','0818027377373','Endra','hotel di kawasan Bali','-8.670458199999999, 115.2126293',400000.00,395000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(12,'Paket Wisata Bali',3,'undefined','08822778811','Bagus','Paket Wisata kawasan Bali','-8.670458199999999, 115.2126293',1000000.00,990000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,3,1),
(10,'Hotel Bali',1,'undefined','0888222111','Agung','Hotel di Kawasan Bali','-8.670458199999999, 115.2126293',400000.00,390000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(11,'Travel Hidayah',2,'undefined','088811332244','Bagus','Travel kawasan Bali','-8.670458199999999, 115.2126293',200000.00,190000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,1,1),
(14,'Hotel Blitz',1,'undefined','0875523231','Truno','Hotel di kawasan Gianyar','-8.545538641056806, 115.333478909375',300000.00,298000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(15,'Hotel Yusro',1,'undefined','08654363773','Yusro','Hotel di kawasan Negara','-8.366234870145178, 114.630353909375',200000.00,197000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(16,'Hotel Kuta Selatan',1,'undefined','087877666','Husni','Hotel di kawasan Kuta Selatan','-8.795336240018491, 115.201642971875',300000.00,265000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(17,'Hotel Singaraja',1,'undefined','087463363','Eri','Hotel Singaraja','-8.129754036691093, 115.07804678046875',200000.00,198000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(18,'Hotel Karangasem',1,'undefined','086736536','Yuni','Hotel Karangasem','-8.453180448454152, 115.59440420234375',230000.00,220000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(19,'Hotel Nusa Penida',1,'undefined','087237274','Gungun','Hotel Nusa Penida','-8.727472925540136, 115.531232815625',350000.00,345000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(20,'Hotel Ubud',1,'undefined','08140583736','Wencim','Hotel Ubud','-8.495287677550863, 115.26069448554688',400000.00,398000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(21,'Hotel Klungkung',1,'undefined','0876355262','Evi','Hotel Klungkung','-8.540106423018635, 115.4048900421875',450000.00,440000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,1,1),
(22,'Travel Oka',2,'undefined','0874534525','Oka','Travel Oka','-8.366234870145178, 114.61936758125',250000.00,230000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,12,1,1),
(23,'Travel Sana',2,'undefined','087356774','Sana','Travel Sana','-8.534674127639144, 115.32249258125',190000.00,185000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,1,1),
(24,'Travel Gitu',2,'undefined','0876356373','Gini','Travel Gitu','-8.121597014216784, 115.0753001984375',200000.00,191000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,1,1),
(25,'Travel Yuk',2,'undefined','08357282653','Yuki','Travel Yuk','-8.670458199999999, 115.2126293',250000.00,240000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,1,1),
(26,'Travel Antik',2,'undefined','0876212762','Anti','Travel Antik','-8.789907631218597, 115.2071361359375',200000.00,198000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,1,1),
(27,'Travel ADA',2,'undefined','082627273','Adis','Travel ADA','-8.670458199999999, 115.2126293',230000.00,225000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,1,1),
(28,'Travel Adie',2,'undefined','08364253642','Adie','Travel Adie','-8.670458199999999, 115.2126293',240000.00,220000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,1,1),
(29,'Travel Wira',2,'undefined','0875235462','Wira','Travel Wira','-8.417861160518434, 114.8171214875',220000.00,215000.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,1,1),
(30,'Travel Iwata',2,'undefined','082654264','Iwa','Travel Iwata','-8.495287677550863, 115.26069448554688',210000.00,200500.00,'undefined','0000-00-00 00:00:00','0000-00-00 00:00:00',0,10,1,1);

/*Table structure for table `satuan` */

DROP TABLE IF EXISTS `satuan`;

CREATE TABLE `satuan` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `satuan` */

insert  into `satuan`(`idx`,`satuan`) values 
(1,'hari'),
(2,'jam');

/*Table structure for table `tipemenu` */

DROP TABLE IF EXISTS `tipemenu`;

CREATE TABLE `tipemenu` (
  `idTipeMenu` varchar(1) DEFAULT NULL,
  `NmTipeMenu` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tipemenu` */

insert  into `tipemenu`(`idTipeMenu`,`NmTipeMenu`) values 
('1','Singgle'),
('2','Berbentuk Daftar');

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idbooking` int(10) DEFAULT NULL,
  `idproduk` int(10) DEFAULT NULL,
  `iddetailproduk` int(10) DEFAULT NULL,
  `tglbooking` datetime DEFAULT NULL,
  `tglperuntukanmulai` datetime DEFAULT NULL,
  `tglperuntukanselasai` datetime DEFAULT NULL,
  `tglbatalbooking` datetime DEFAULT NULL,
  `keteranganbatal` text,
  `harganormal` float DEFAULT NULL,
  `hargadiscount` float DEFAULT NULL,
  `idvoucher` text,
  `idmember` int(10) DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `spesialrequest` text,
  `tglupdate` datetime DEFAULT NULL,
  `jmlorangdewasa` int(10) DEFAULT NULL,
  `jmlanak` int(10) DEFAULT NULL,
  `idjenisbayar` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `transaksi` */

/*Table structure for table `usergroup` */

DROP TABLE IF EXISTS `usergroup`;

CREATE TABLE `usergroup` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `NmUserGroup` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `usergroup` */

insert  into `usergroup`(`idx`,`NmUserGroup`) values 
(1,'Admin'),
(2,'Admin 2'),
(3,'Reviewer');

/*Table structure for table `usermenu` */

DROP TABLE IF EXISTS `usermenu`;

CREATE TABLE `usermenu` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `iduser` int(10) DEFAULT NULL,
  `idmenu` int(10) DEFAULT NULL,
  `idaplikasi` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=6288 DEFAULT CHARSET=latin1;

/*Data for the table `usermenu` */

insert  into `usermenu`(`idx`,`iduser`,`idmenu`,`idaplikasi`) values 
(6286,1,34,NULL),
(6285,1,203,NULL),
(6284,1,211,NULL),
(6283,1,205,NULL),
(6282,1,217,NULL),
(6281,1,210,NULL),
(6280,1,208,NULL),
(6279,1,36,NULL),
(6278,1,216,NULL),
(6277,1,209,NULL),
(6276,1,207,NULL),
(6275,1,35,NULL),
(6274,1,215,NULL),
(6273,1,213,NULL),
(6272,1,212,NULL),
(6271,1,206,NULL),
(6270,1,204,NULL),
(6269,1,2,NULL),
(6268,1,214,NULL),
(6287,1,6,NULL);

/*Table structure for table `usersistem` */

DROP TABLE IF EXISTS `usersistem`;

CREATE TABLE `usersistem` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `npp` varchar(20) DEFAULT NULL,
  `Nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL COMMENT 'Organisasi',
  `NoTelpon` varchar(50) DEFAULT NULL,
  `user` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `statuspeg` int(10) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `ym` varchar(100) DEFAULT NULL,
  `isaktif` enum('Y','N') DEFAULT 'N',
  `idusergroup` int(10) DEFAULT NULL,
  `idkabupaten` int(10) DEFAULT NULL,
  `idpropinsi` int(10) DEFAULT NULL,
  `imehp` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `usersistem` */

insert  into `usersistem`(`idx`,`npp`,`Nama`,`alamat`,`NoTelpon`,`user`,`password`,`statuspeg`,`photo`,`email`,`ym`,`isaktif`,`idusergroup`,`idkabupaten`,`idpropinsi`,`imehp`) values 
(1,'undefined','User Demo','0','0274747474','demo','demo',0,'undefined','','','',1,173,12,'860205025197033'),
(2,'undefined','pengguna lain','0','04532','test','test',0,'undefined','','','',2,100,9,NULL),
(3,'undefined','moses','rinjani','0271-765279','moses','moses',0,'undefined','w.moses@yahoo.co.id','w.moses','',1,0,10,'0856625542556');

/*Table structure for table `voucher` */

DROP TABLE IF EXISTS `voucher`;

CREATE TABLE `voucher` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `voucher` text,
  `nominal` float(10,2) DEFAULT NULL,
  `tglberlakudari` datetime DEFAULT NULL,
  `tglberlakusampai` datetime DEFAULT NULL,
  `idmember` int(10) DEFAULT NULL,
  `isterpakai` enum('Y','N') DEFAULT 'N',
  `tglpakai` datetime DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `voucher` */

insert  into `voucher`(`idx`,`voucher`,`nominal`,`tglberlakudari`,`tglberlakusampai`,`idmember`,`isterpakai`,`tglpakai`) values 
(1,'akhdayfgj',100000.00,'2016-10-07 06:04:04','2016-11-07 06:04:12',0,'N','0000-00-00 00:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
