/*
SQLyog - Free MySQL GUI v5.02
Host - 5.1.41-community : Database - kementrans
*********************************************************************
Server version : 5.1.41-community
*/


create database if not exists `kementrans`;

USE `kementrans`;

/*Table structure for table `bahasa` */

DROP TABLE IF EXISTS `bahasa`;

CREATE TABLE `bahasa` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `bahasa` varchar(40) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `lokasi` text,
  `urut` int(10) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `bahasa` */

insert into `bahasa` values 
(1,'Indonesia','','',0,0),
(2,'Inggris','','',0,0);

/*Table structure for table `contacus` */

DROP TABLE IF EXISTS `contacus`;

CREATE TABLE `contacus` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(100) DEFAULT NULL,
  `alamat` text,
  `notelpon` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `isi` text,
  `tglisi` datetime DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `contacus` */

insert into `contacus` values 
(1,'uji coba','','','uji coab','ssadsad','2013-07-10 00:00:00');

/*Table structure for table `content` */

DROP TABLE IF EXISTS `content`;

CREATE TABLE `content` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `judul` text,
  `isiawal` text,
  `isi` text,
  `idbahasa` int(10) DEFAULT NULL,
  `idmenu` int(10) DEFAULT NULL,
  `idkomponen` int(10) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jam` time DEFAULT NULL,
  `idadmin` int(11) DEFAULT NULL,
  `urut` int(10) DEFAULT NULL,
  `image1` text,
  `image2` text,
  `image3` text,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

/*Data for the table `content` */

insert into `content` values 
(8,'1',NULL,'<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum. Typi non habent claritatem  insitam; est usus legentis in iis qui facit eorum claritatem.  Investigationes demonstraverunt lectores legere me lius quod ii legunt  saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc  putamus parum claram, anteposuerit litterarum formas humanitatis per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis  videntur parum clari, fiant sollemnes in futurum.</p>',1,123,5,'2012-06-29','20:35:38',1,0,NULL,NULL,NULL),
(9,'2',NULL,'<p>orem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>\n<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum.</p>',1,360,2,'2012-11-23','15:34:57',1,1,NULL,NULL,NULL),
(10,'3',NULL,'<p>Typi non habent claritatem insitam; est usus legentis in iis qui facit  eorum claritatem. Investigationes demonstraverunt lectores legere me  lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui  sequitur mutationem consuetudium lectorum. Mirum est notare quam  littera gothica,</p>',1,360,2,'2012-11-24','16:44:44',1,2,NULL,NULL,NULL),
(11,'4',NULL,'<p>nem consuetudium lectorum. Mirum est notare quam littera gothica, quam  nunc putamus parum claram, anteposuerit litterarum formas humanitatis  per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc  nobis videntur parum clari, fiant sollemnes in futurum.</p>',1,360,2,'2012-11-24','16:41:46',1,2,NULL,NULL,NULL),
(15,'undefined','<p>Awal isi awal isi</p>','<p>ini dalam home&nbsp; ini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dala<img src=\"../../../resource/uploaded/img/Logo_05.png\" alt=\"\" width=\"39\" height=\"25\" />m homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam homeini dalam home</p>',1,11,1,'2013-06-14','23:53:44',1,0,'undefined','undefined','undefined'),
(16,'Ini Judulku yang pertama','<p>Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertamaIni Judulku yang pertama Ini Judulku yang pertama</p>','<p>Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertamaIni Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertamaIni Judulku yang pertama Ini Judulku yang pertama</p>\n<p>Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertama Ini Judulku yang pertamaIni Judulku yang pertama Ini Judulku yang pertama</p>',1,12,2,'2013-06-08','03:01:45',1,0,'undefined','','undefined'),
(17,'Inilah per cobaaa judul 2','<p>Inilah per cobaaa judul 2Inilah per cobaaa judul 2Inilah per cobaaa judul 2Inilah per cobaaa judul 2Inilah per cobaaa judul 2</p>','<p>Inilah per cobaaa judul 2 Inilah per cobaaa judul 2 Inilah per cobaaa judul 2 Inilah per cobaaa judul 2</p>',1,12,2,'2013-06-08','03:11:48',1,0,'undefined','','undefined'),
(18,'Kab Suka raja','#testt','undefined',0,22,1,'2013-07-08','07:29:02',1,1,'undefined','undefined','undefined'),
(19,'undefined','<h3>kalteng.go.id</h3>\n<h4>Situs resmi Pemerintah Provinsi Kalimantan Tengah</h4>','undefined',0,20,2,'2013-07-07','16:17:46',1,0,'logo.png','undefined','undefined'),
(20,'Visit me on Facebook','# Facebook','undefined',0,27,2,'2013-07-08','10:23:05',1,1,'Facebook.png','undefined','undefined'),
(21,'Visit me On Twiter','# Isikan Link Twiter','undefined',0,27,2,'2013-07-08','10:22:08',1,2,'Twitter.png','undefined','undefined'),
(22,'Visit me on Facebook','# Isikan link','undefined',0,27,2,'2013-07-08','10:21:48',1,3,'LinkedIn.png','undefined','undefined'),
(23,'undefined','','',1,21,2,'2013-07-08','06:13:09',1,0,'slide.jpg','undefined','undefined'),
(24,'undefined','www.google.com','undefined',0,23,2,'2013-07-08','10:55:17',1,1,'subdomain1.jpg','undefined','undefined'),
(25,'undefined','# ini contoh Link sub domain','undefined',0,23,2,'2013-07-08','06:36:28',1,2,'subdomain2.jpg','undefined','undefined'),
(26,'Berita Satu','undefined','<p>Berita Satu nanti di isi ya ndess&nbsp; Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess&nbsp; Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess&nbsp; Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess&nbsp; Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess</p>\n<p>&nbsp;<iframe src=\"http://www.youtube.com/embed/COEjUXCHTw8\" frameborder=\"0\" width=\"425\" height=\"350\"></iframe></p>\n<p>Berita Satu nanti di isi ya ndess&nbsp; Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess&nbsp; Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndessBerita Satu nanti di isi ya ndess&nbsp; Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess&nbsp; Berita Satu nanti di isi ya ndess Berita Satu nanti di isi ya ndess</p>',0,24,2,'2013-07-29','22:53:05',1,1,'berita1.jpg','undefined','undefined'),
(27,'Berita Judul 2','Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kampanye yang melibatkan seluruh stakeholder terkait di wilayah provinsi Kalimantan Tengah untuk mendorong peningkatan konsumsi ikan','<p>Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2</p>\n<p>Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2 Berita Judul ke 2</p>\n<p>&nbsp;</p>',0,24,2,'2013-07-08','11:47:03',1,2,'berita1.jpg','undefined','undefined'),
(28,'Kegiatan Dinas Kelautan & Perikanan','undefined','<p>Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kampanye yang melibatkan seluruh stakeholder terkait di wilayah provinsi Kalimantan Tengah untuk mendorong peningkatan konsumsi ikan Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kampanye yang melibatkan seluruh stakeholder terkait di wilayah provinsi Kalimantan Tengah untuk mendorong peningkatan konsumsi ikan</p>\n<p>&nbsp;</p>\n<p>Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kampanye yang melibatkan seluruh stakeholder terkait di wilayah provinsi Kalimantan Tengah untuk mendorong peningkatan konsumsi ikan</p>',0,25,1,'2013-07-08','07:22:47',1,0,'berita1.jpg','undefined','undefined'),
(29,'Kegiatan Dinas Kelautan & Perikanan 2','undefined','<p>Kegiatan Dinas Kelautan &amp; Perikanan Kegiatan Dinas Kelautan &amp; Perikanan Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kampanye yang melibatkan seluruh stakeholder terkait di wilayah provinsi Kalimantan Tengah untuk mendorong peningkatan konsumsi ikan Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kam Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kampanye yang melibatkan seluruh stakeholder terkait di wilayah provinsi Kalimantan Tengah untuk mendorong peningkatan konsumsi ikan Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kam</p>\n<p>Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kampanye yang melibatkan seluruh stakeholder terkait di wilayah provinsi Kalimantan Tengah untuk mendorong peningkatan konsumsi ikan Gemarikan adalah kegiatan yang dilaksanakan melalui berbagai kegiatan promosi dan kam</p>',0,25,1,'2013-07-08','07:23:45',1,1,'berita1.jpg','undefined','undefined'),
(30,'Kab 1','#Kab 1','undefined',0,22,1,'2013-07-08','07:29:21',1,2,'undefined','undefined','undefined'),
(31,'Kab 2','#Kab 1','undefined',0,22,1,'2013-07-08','07:30:49',1,2,'undefined','undefined','undefined'),
(32,'#Kecmatan 1','#Kecmatan 1','undefined',0,22,1,'2013-07-08','07:31:09',1,4,'undefined','undefined','undefined'),
(33,'Kecmatan 2','#Kecamatan 2','undefined',0,22,1,'2013-07-08','07:31:30',1,0,'undefined','undefined','undefined'),
(34,'Kecamatan 3','#Kecmatan 3','undefined',0,22,1,'2013-07-08','07:32:03',1,5,'undefined','undefined','undefined'),
(35,'Kecamatan 4','#Kecamatan 4','undefined',0,22,1,'2013-07-08','07:32:34',1,6,'undefined','undefined','undefined'),
(36,'Kecamatan 7','#Kecamatan 7','undefined',0,22,1,'2013-07-08','07:33:00',1,7,'undefined','undefined','undefined'),
(37,'Kecamatan 8','Kecamatan 8','undefined',0,22,1,'2013-07-08','07:33:15',1,8,'undefined','undefined','undefined'),
(38,'Kecamatan 9','#Kecamatan 9','undefined',0,22,1,'2013-07-08','07:33:50',1,9,'undefined','undefined','undefined'),
(39,'undefined','# Kebijakan 1','undefined',0,26,1,'2013-07-08','07:51:59',1,1,'subdomain3.jpg','undefined','undefined'),
(40,'undefined','Kebijakan 2','undefined',0,26,1,'2013-07-08','07:52:17',1,1,'subdomain4.jpg','undefined','undefined'),
(41,'Passing GradeTes CPNS 2012','undefined','<p>Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012</p>\n<p>&nbsp;</p>\n<p>Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012</p>\n<p>Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012</p>',0,28,2,'2013-07-08','12:30:07',1,1,'undefined','undefined','undefined'),
(42,'Menyorot peran dan fungsi wakil kepala daerah dalam tata pemerintahan NKRI','undefined','<p>Menyorot peran dan fungsi wakil kepala daerah dalam tata pemerintahan NKRI Menyorot peran dan fungsi wakil kepala daerah dalam tata pemerintahan NKRI Menyorot peran dan fungsi wakil kepala daerah dalam tata pemerintahan NKRI Menyorot peran dan fungsi wakil kepala daerah dalam tata pemerintahan NKRI</p>\n<p>Menyorot peran dan fungsi wakil kepala daerah dalam tata pemerintahan NKRI Menyorot peran dan fungsi wakil kepala daerah dalam tata pemerintahan NKRI Menyorot peran dan fungsi wakil kepala daerah dalam tata pemerintahan NKRI</p>\n<p>Menyorot peran dan fungsi wakil kepala daerah dalam tata pemerintahan NKRI</p>\n<p>&nbsp;</p>',0,28,2,'2013-07-08','12:30:58',1,2,'undefined','undefined','undefined'),
(43,'Passing GradeTes CPNS 2012','undefined','<p>Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012</p>\n<p>&nbsp;</p>\n<p>Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012 Passing GradeTes CPNS 2012</p>',0,29,2,'2013-07-08','12:32:13',1,0,'undefined','undefined','undefined'),
(44,'Membangkitkan jiwa wirausaha di Kalteng','undefined','<p>Membangkitkan jiwa wirausaha di Kalteng Membangkitkan jiwa wirausaha di Kalteng Membangkitkan jiwa wirausaha di Kalteng Membangkitkan jiwa wirausaha di Kalteng Membangkitkan jiwa wirausaha di Kalteng Membangkitkan jiwa wirausaha di Kalteng Membangkitkan jiwa wirausaha di Kalteng</p>\n<p>Membangkitkan jiwa wirausaha di Kalteng</p>\n<p>Membangkitkan jiwa wirausaha di KaltengMembangkitkan jiwa wirausaha di KaltengMembangkitkan jiwa wirausaha di KaltengMembangkitkan jiwa wirausaha di KaltengMembangkitkan jiwa wirausaha di Kalteng</p>',0,29,2,'2013-07-08','12:32:49',1,2,'undefined','undefined','undefined'),
(45,'Kamus Bahasa Dayak Ma\'anyan','undefined','<p>Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan</p>\n<p>Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan</p>',0,29,2,'2013-07-08','12:33:22',1,1,'undefined','undefined','undefined'),
(46,'Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan','undefined','<p>Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan</p>\n<p>Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan</p>\n<p>Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan</p>\n<p>Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan Kamus Bahasa Dayak Ma\'anyan</p>\n<p>&nbsp;</p>',0,30,2,'2013-07-08','12:33:42',1,1,'undefined','undefined','undefined'),
(47,'Judul 3','undefined','<p>test Judul 3&nbsp; test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3test Judul 3</p>\n<p>test Judul 3test Judul 3test Judul 3test Judul 3</p>',0,24,2,'2013-07-08','15:20:45',1,3,'detail.jpg','undefined','undefined'),
(48,'Judul 4','undefined','<p>Judul 4 Judul 4 Judul 4 &nbsp;Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4</p>\n<p>Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4Judul 4 Judul 4 Judul 4</p>',0,24,2,'2013-07-08','15:38:08',1,4,'berita1.jpg','undefined','undefined'),
(49,'undefined','undefined','<p>isikan profile</p>',0,14,1,'2013-07-09','15:20:17',1,0,'undefined','undefined','undefined'),
(50,'undefined','undefined','<p>isikan instansi</p>',0,15,1,'2013-07-09','15:20:35',1,0,'undefined','undefined','undefined'),
(51,'undefined','undefined','<p>isikan layanan</p>',0,16,1,'2013-07-09','15:20:51',1,0,'undefined','undefined','undefined'),
(52,'undefined','undefined','<p>isikan kontak</p>',0,19,1,'2013-07-09','15:21:04',1,0,'undefined','undefined','undefined'),
(53,'undefined','uji coba test','undefined',0,31,2,'2013-07-26','05:56:11',1,1,'Ampitheater kecil.jpg','undefined','undefined'),
(54,'undefined','uji coba 2','undefined',0,31,2,'2013-07-26','05:56:45',1,2,'Deluxe Luar kecil.jpg','undefined','undefined'),
(55,'undefined','ujicoba 3','undefined',0,31,2,'2013-07-26','05:57:13',1,3,'Deluxe ke kolam renang kecil.jpg','undefined','undefined'),
(56,'undefined','uji coba 4','undefined',0,31,2,'2013-07-26','05:57:49',1,4,'Dining Hall Ceiling kecil.jpg','undefined','undefined'),
(57,'undefined','Uji coba 5','undefined',0,31,2,'2013-07-26','05:58:12',1,3,'Dining Hall Ceiling kecil.jpg','undefined','undefined'),
(58,'undefined','Uji coba 6','undefined',0,31,2,'2013-07-26','05:58:57',1,6,'Dining Hall Terrace 3 kecil.jpg','undefined','undefined'),
(64,'','undefined','<p><iframe src=\"http://www.youtube.com/embed/COEjUXCHTw8\" frameborder=\"0\" width=\"425\" height=\"350\"></iframe></p>',0,32,2,'2013-07-30','01:21:57',1,2,'undefined','undefined','undefined'),
(61,'Profile Pangkalan Bun','undefined','<p><iframe src=\"http://www.youtube.com/embed/NKJY6DnOF2E\" frameborder=\"0\" width=\"425\" height=\"350\" data=\"http://www.youtube.com/embed/NKJY6DnOF2E\"></iframe></p>',0,32,2,'2013-07-30','01:11:39',1,0,'undefined','undefined','undefined'),
(62,'undefined','Coba News 1','undefined',0,33,2,'2013-07-30','01:09:34',1,1,'undefined','undefined','undefined'),
(63,'undefined','Coba news ke 2','undefined',0,33,2,'2013-07-30','01:09:45',1,3,'undefined','undefined','undefined');

/*Table structure for table `detailsp` */

DROP TABLE IF EXISTS `detailsp`;

CREATE TABLE `detailsp` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idperusahaan` int(10) DEFAULT NULL,
  `idserikatpekerja` int(10) DEFAULT NULL,
  `idpegawaiin` int(10) DEFAULT NULL,
  `idpegawaiupdate` int(10) DEFAULT NULL,
  `tglin` date DEFAULT NULL,
  `tglupdate` date DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `detailsp` */

insert into `detailsp` values 
(1,1,1,1,1,'2013-12-04','2013-12-04'),
(3,1,2,1,1,'2013-12-04','2013-12-04'),
(4,4,1,1,1,'2013-12-04','2013-12-04');

/*Table structure for table `jenispenyelesaianperselisihan` */

DROP TABLE IF EXISTS `jenispenyelesaianperselisihan`;

CREATE TABLE `jenispenyelesaianperselisihan` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `penyelesaianperselisihan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `jenispenyelesaianperselisihan` */

insert into `jenispenyelesaianperselisihan` values 
(1,'Bipartit'),
(2,'Mediasi PB'),
(3,'Mediasi Anjuran'),
(4,'PHI');

/*Table structure for table `jenisperselisihan` */

DROP TABLE IF EXISTS `jenisperselisihan`;

CREATE TABLE `jenisperselisihan` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `jenisperselisihan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `jenisperselisihan` */

insert into `jenisperselisihan` values 
(1,'Hak'),
(2,'Kepentingan'),
(3,'PHK'),
(4,'Antar PB/SP');

/*Table structure for table `jenistenagapeneyelesaian` */

DROP TABLE IF EXISTS `jenistenagapeneyelesaian`;

CREATE TABLE `jenistenagapeneyelesaian` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `jenistenagapeneyelesaian` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `jenistenagapeneyelesaian` */

insert into `jenistenagapeneyelesaian` values 
(1,'Arbiter'),
(2,'Konsiliato'),
(3,'Hakim Ad-H');

/*Table structure for table `kabupaten` */

DROP TABLE IF EXISTS `kabupaten`;

CREATE TABLE `kabupaten` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `kd_kab` varchar(4) DEFAULT NULL,
  `nama_kab` varchar(30) DEFAULT NULL,
  `status` varchar(7) DEFAULT NULL,
  `buletanx` int(10) DEFAULT NULL,
  `buletany` int(10) DEFAULT NULL,
  `warna` varchar(10) DEFAULT NULL,
  `diameter` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=391 DEFAULT CHARSET=latin1;

/*Data for the table `kabupaten` */

insert into `kabupaten` values 
(1,'0101','Aceh Selatan','Kab.',NULL,NULL,NULL,NULL),
(2,'0102','Aceh Tenggara','Kab.',NULL,NULL,NULL,NULL),
(3,'0103','Aceh Timur','Kab.',NULL,NULL,NULL,NULL),
(4,'0104','Aceh Barat','Kab.',NULL,NULL,NULL,NULL),
(5,'0105','Aceh Tengah','Kab.',NULL,NULL,NULL,NULL),
(6,'0106','Aceh Besar','Kab.',NULL,NULL,NULL,NULL),
(7,'0107','Piedi','Kab.',NULL,NULL,NULL,NULL),
(8,'0108','Aceh Utara','Kab.',NULL,NULL,NULL,NULL),
(9,'0109','Banda Aceh','Kodya',NULL,NULL,NULL,NULL),
(10,'0110','Sabang','Kab.',NULL,NULL,NULL,NULL),
(11,'0111','Aceh Singkil','Kab.',NULL,NULL,NULL,NULL),
(12,'0112','Bireuen','Kab.',NULL,NULL,NULL,NULL),
(13,'0113','Simeulee','Kab.',NULL,NULL,NULL,NULL),
(14,'0201','Nias','Kab.',NULL,NULL,NULL,NULL),
(15,'0202','Tapanuli Selatan','Kab.',NULL,NULL,NULL,NULL),
(16,'0203','Tapanuli Tengah','Kab.',NULL,NULL,NULL,NULL),
(17,'0204','Tapanuli Utara','Kab.',NULL,NULL,NULL,NULL),
(18,'0205','Labuhan Batu','Kab.',NULL,NULL,NULL,NULL),
(19,'0206','Asahan','Kab.',NULL,NULL,NULL,NULL),
(20,'0207','Simalungun','Kab.',NULL,NULL,NULL,NULL),
(21,'0208','Dairi','Kab.',NULL,NULL,NULL,NULL),
(22,'0209','Tanah Karo','Kab.',NULL,NULL,NULL,NULL),
(23,'0210','Deli Serdang','Kab.',NULL,NULL,NULL,NULL),
(24,'0211','Langkat','Kab.',NULL,NULL,NULL,NULL),
(25,'0212','Sibolga','Kodya',NULL,NULL,NULL,NULL),
(26,'0213','Tanjung Balai','Kodya',NULL,NULL,NULL,NULL),
(27,'0214','Pematang Siantar','Kodya',NULL,NULL,NULL,NULL),
(28,'0215','Tanah Tinggi','Kodya',NULL,NULL,NULL,NULL),
(29,'0216','Medan','Kodya',NULL,NULL,NULL,NULL),
(30,'0217','Binjai','Kodya',NULL,NULL,NULL,NULL),
(31,'0218','Mandailing Natal','Kab.',NULL,NULL,NULL,NULL),
(32,'0219','Toba Samosir','Kab.',NULL,NULL,NULL,NULL),
(33,'0301','Pesisir Selatan','Kab.',NULL,NULL,NULL,NULL),
(34,'0302','Solok','Kab.',NULL,NULL,NULL,NULL),
(35,'0303','Sawahlunto Sijunjung','Kab.',NULL,NULL,NULL,NULL),
(36,'0304','Tanah Datar','Kab.',NULL,NULL,NULL,NULL),
(37,'0305','Padang Pariaman','Kab.',NULL,NULL,NULL,NULL),
(38,'0306','Agam','Kab.',NULL,NULL,NULL,NULL),
(39,'0308','Pasaman','Kab.',NULL,NULL,NULL,NULL),
(40,'0309','Padang','Kodya',NULL,NULL,NULL,NULL),
(41,'0310','Solok','Kodya',NULL,NULL,NULL,NULL),
(42,'0311','Sawah Lunto','Kodya',NULL,NULL,NULL,NULL),
(43,'0312','Padang Panjang','Kodya',NULL,NULL,NULL,NULL),
(44,'0313','Bukit Tinggi','Kodya',NULL,NULL,NULL,NULL),
(45,'0314','Payakumbuh','Kodya',NULL,NULL,NULL,NULL),
(46,'0315','Padang Sidempuan','Kotip.',NULL,NULL,NULL,NULL),
(47,'0316','Kepulauan Mentawai','Kab.',NULL,NULL,NULL,NULL),
(48,'0401','Indragiri Hulu','Kab.',NULL,NULL,NULL,NULL),
(49,'0402','Indragiri Hilir','Kab.',NULL,NULL,NULL,NULL),
(50,'0403','Kepulauan Riau','Kab.',NULL,NULL,NULL,NULL),
(51,'0404','Kampar','Kab.',NULL,NULL,NULL,NULL),
(52,'0405','Bengkalis','Kab.',NULL,NULL,NULL,NULL),
(53,'0406','Pakan Baru','Kodya',NULL,NULL,NULL,NULL),
(54,'0407','Batam','Kodya',NULL,NULL,NULL,NULL),
(55,'0408','Dumai','Kotip',NULL,NULL,NULL,NULL),
(56,'0409','Tanjung Pinang','Kab.',NULL,NULL,NULL,NULL),
(57,'0410','Karimun','Kab.',NULL,NULL,NULL,NULL),
(58,'0411','Kuantan Sengingi','Kab.',NULL,NULL,NULL,NULL),
(59,'0412','Natuna','Kab.',NULL,NULL,NULL,NULL),
(60,'0413','Pelalawan','Kab.',NULL,NULL,NULL,NULL),
(61,'0414','Rokan Hilir','Kab.',NULL,NULL,NULL,NULL),
(62,'0415','Rokan Hulu','Kab.',NULL,NULL,NULL,NULL),
(63,'0416','Siak','Kab.',NULL,NULL,NULL,NULL),
(64,'0501','Kerinci','Kab.',NULL,NULL,NULL,NULL),
(65,'0502','Sarolangun','Kab.',NULL,NULL,NULL,NULL),
(66,'0503','Batanghari','Kab.',NULL,NULL,NULL,NULL),
(67,'0504','Tanjung Jabung','Kab.',NULL,NULL,NULL,NULL),
(68,'0505','Muara Bungo Tebo','Kab.',NULL,NULL,NULL,NULL),
(69,'0506','Muaro Jambi','Kab.',NULL,NULL,NULL,NULL),
(70,'0507','Jambi','Kodya',NULL,NULL,NULL,NULL),
(71,'0508','Tanjung Jabung Timur','Kab.',NULL,NULL,NULL,NULL),
(72,'0509','Tebo','Kab.',NULL,NULL,NULL,NULL),
(73,'0510','Bungo','Kab.',NULL,NULL,NULL,NULL),
(74,'0511','Merangin','Kab.',NULL,NULL,NULL,NULL),
(75,'0601','Ogan Komering Hulu','Kab.',NULL,NULL,NULL,NULL),
(76,'0602','Ogan Komering Hilir','Kab.',NULL,NULL,NULL,NULL),
(77,'0603','Muara EnimLiot','Kab.',NULL,NULL,NULL,NULL),
(78,'0604','Lahat','Kab.',NULL,NULL,NULL,NULL),
(79,'0605','Musi Rawas','Kab.',NULL,NULL,NULL,NULL),
(80,'0606','Musi Banyuasin','Kab.',NULL,NULL,NULL,NULL),
(81,'0609','Palembang','Kodya',NULL,NULL,NULL,NULL),
(82,'0611','Lubuk Linggau','Kotip',NULL,NULL,NULL,NULL),
(83,'0612','Prabumulih','Kotip',NULL,NULL,NULL,NULL),
(84,'0613','Baturaja','Kotip',NULL,NULL,NULL,NULL),
(85,'0701','Bengkulu Selatan','Kab.',NULL,NULL,NULL,NULL),
(86,'0702','Rejang Lebong','Kab.',NULL,NULL,NULL,NULL),
(87,'0703','Bengkulu  Utara','Kab.',NULL,NULL,NULL,NULL),
(88,'0704','Bengkulu','Kodya',NULL,NULL,NULL,NULL),
(89,'0801','Lampung Selatan','Kab.',NULL,NULL,NULL,NULL),
(90,'0802','Lampung Tengah','Kab.',NULL,NULL,NULL,NULL),
(91,'0803','Lampung Utara','Kab.',NULL,NULL,NULL,NULL),
(92,'0804','Bandar Lampung','Kodya',NULL,NULL,NULL,NULL),
(93,'0805','Lampung Timur','Kodya',NULL,NULL,NULL,NULL),
(94,'0806','Tanggamus','Kab.',NULL,NULL,NULL,NULL),
(95,'0807','Metro','Kab.',NULL,NULL,NULL,NULL),
(96,'0808','Lampung Barat','Kab.',NULL,NULL,NULL,NULL),
(97,'0809','Tulang Bawang','Kab.',NULL,NULL,NULL,NULL),
(98,'0810','Way Kanan','Kab.',NULL,NULL,NULL,NULL),
(99,'0901','Jakarta Selatan','undefin',115,183,'#0afb0c',15),
(100,'0902','Jakarta Pusat','undefin',171,103,'#01d5fa',15),
(101,'0903','Jakarta Utara','undefin',234,62,'#01d5fa',15),
(102,'0904','Jakarta Barat','undefin',87,107,'#fced0a',15),
(103,'0905','Jakarta Timur','undefin',253,143,'#86aace',15),
(104,'1001','Pandeglang','Kab.',NULL,NULL,NULL,NULL),
(105,'1002','Lebak','Kab.',NULL,NULL,NULL,NULL),
(106,'1003','Bogor','Kab.',NULL,NULL,NULL,NULL),
(107,'1004','Sukabumi','Kab.',NULL,NULL,NULL,NULL),
(108,'1005','Cianjur','Kab.',NULL,NULL,NULL,NULL),
(109,'1006','Bandung','Kab.',NULL,NULL,NULL,NULL),
(110,'1007','Garut','Kab.',NULL,NULL,NULL,NULL),
(111,'1008','Tasikmalaya','Kab.',NULL,NULL,NULL,NULL),
(112,'1009','Ciamis','Kab.',NULL,NULL,NULL,NULL),
(113,'1010','Kuningan','Kab.',NULL,NULL,NULL,NULL),
(114,'1011','Cirebon','Kab.',NULL,NULL,NULL,NULL),
(115,'1012','Majalengka','Kab.',NULL,NULL,NULL,NULL),
(116,'1013','Sumedang','Kab.',NULL,NULL,NULL,NULL),
(117,'1014','Indramayu','Kab.',NULL,NULL,NULL,NULL),
(118,'1015','Subang','Kab.',NULL,NULL,NULL,NULL),
(119,'1016','Purwakarta','Kab.',NULL,NULL,NULL,NULL),
(120,'1017','Karawang','Kab.',NULL,NULL,NULL,NULL),
(121,'1018','Bekasi','Kab.',NULL,NULL,NULL,NULL),
(122,'1019','Tangerang','Kab.',NULL,NULL,NULL,NULL),
(123,'1020','Serang','Kab.',NULL,NULL,NULL,NULL),
(124,'1021','Bogor','Kodya',NULL,NULL,NULL,NULL),
(125,'1022','Sukabumi','Kodya',NULL,NULL,NULL,NULL),
(126,'1023','Bandung','Kodya',NULL,NULL,NULL,NULL),
(127,'1024','Cirebon','Kodya',NULL,NULL,NULL,NULL),
(128,'1025','Depok','Kotip.',NULL,NULL,NULL,NULL),
(129,'1026','Cimahi','Kotip.',NULL,NULL,NULL,NULL),
(130,'1027','Tasikmalaya','Kotip.',NULL,NULL,NULL,NULL),
(131,'1028','Bekasi','Kotip.',NULL,NULL,NULL,NULL),
(132,'1029','Tangerang','Kotip.',NULL,NULL,NULL,NULL),
(133,'1101','Cilacap','Kab.',NULL,NULL,NULL,NULL),
(134,'1102','Banyumas','Kab.',NULL,NULL,NULL,NULL),
(135,'1103','Purbalingga','Kab.',NULL,NULL,NULL,NULL),
(136,'1104','Banjarnegara','Kab.',NULL,NULL,NULL,NULL),
(137,'1105','Kebumen','Kab.',NULL,NULL,NULL,NULL),
(138,'1106','Purworejo','Kab.',NULL,NULL,NULL,NULL),
(139,'1107','Wonosobo','Kab.',NULL,NULL,NULL,NULL),
(140,'1108','Magelang','Kab.',NULL,NULL,NULL,NULL),
(141,'1109','Boyolali','Kab.',NULL,NULL,NULL,NULL),
(142,'1110','Klaten','Kab.',NULL,NULL,NULL,NULL),
(143,'1111','Sukoharjo','Kab.',NULL,NULL,NULL,NULL),
(144,'1112','Wonogiri','Kab.',NULL,NULL,NULL,NULL),
(145,'1113','Karanganyar','Kab.',NULL,NULL,NULL,NULL),
(146,'1114','Sragen','Kab.',NULL,NULL,NULL,NULL),
(147,'1115','Grobogan','Kab.',NULL,NULL,NULL,NULL),
(148,'1116','Blora','Kab.',NULL,NULL,NULL,NULL),
(149,'1117','Rembang','Kab.',NULL,NULL,NULL,NULL),
(150,'1118','Pati','Kab.',NULL,NULL,NULL,NULL),
(151,'1119','Kudus','Kab.',NULL,NULL,NULL,NULL),
(152,'1120','Jepara','Kab.',NULL,NULL,NULL,NULL),
(153,'1121','Demak','Kab.',NULL,NULL,NULL,NULL),
(154,'1122','Semarang','Kab.',NULL,NULL,NULL,NULL),
(155,'1123','Temanggung','Kab.',NULL,NULL,NULL,NULL),
(156,'1124','Kendal','Kab.',NULL,NULL,NULL,NULL),
(157,'1125','Batang','Kab.',NULL,NULL,NULL,NULL),
(158,'1126','Pekalongan','Kab.',NULL,NULL,NULL,NULL),
(159,'1127','Pemalang','Kab.',NULL,NULL,NULL,NULL),
(160,'1128','Tegal','Kab.',NULL,NULL,NULL,NULL),
(161,'1129','Brebes','Kab.',NULL,NULL,NULL,NULL),
(162,'1130','Magelang','Kodya',NULL,NULL,NULL,NULL),
(163,'1131','Surakarta','Kodya',NULL,NULL,NULL,NULL),
(164,'1132','Salatiga','Kodya',NULL,NULL,NULL,NULL),
(165,'1133','Semarang','Kotip',NULL,NULL,NULL,NULL),
(166,'1134','Pekalongan','Kodya',NULL,NULL,NULL,NULL),
(167,'1135','Tegal','Kodya',NULL,NULL,NULL,NULL),
(168,'1136','Cilacap','Kotip.',NULL,NULL,NULL,NULL),
(169,'1137','Purwakerta','Kotip.',NULL,NULL,NULL,NULL),
(170,'1201','Kulonprogo','undefin',88,127,'#808080',15),
(171,'1202','Bantul','undefin',169,157,'#ffff00',15),
(172,'1203','Gunung Kidul','Kab.',NULL,NULL,NULL,NULL),
(173,'1204','Sleman','undefin',194,64,'#0080ff',15),
(174,'1205','Yogyakarta','Kodya',NULL,NULL,NULL,NULL),
(175,'1301','Pacitan','Kab.',NULL,NULL,NULL,NULL),
(176,'1302','Ponorogo','Kab.',NULL,NULL,NULL,NULL),
(177,'1303','Trenggalek','Kab.',NULL,NULL,NULL,NULL),
(178,'1304','Tulungagung','Kab.',NULL,NULL,NULL,NULL),
(179,'1305','Blitar','Kab.',NULL,NULL,NULL,NULL),
(180,'1306','Kediri','Kab.',NULL,NULL,NULL,NULL),
(181,'1307','Malang','Kab.',NULL,NULL,NULL,NULL),
(182,'1308','Lumajang','Kab.',NULL,NULL,NULL,NULL),
(183,'1309','Jember','Kab.',NULL,NULL,NULL,NULL),
(184,'1310','Banyuwangi','Kab.',NULL,NULL,NULL,NULL),
(185,'1311','Bondowoso','Kab.',NULL,NULL,NULL,NULL),
(186,'1312','Panarukan','Kab.',NULL,NULL,NULL,NULL),
(187,'1313','Probolinggo','Kab.',NULL,NULL,NULL,NULL),
(188,'1314','Pasuruan','Kab.',NULL,NULL,NULL,NULL),
(189,'1315','Sidoarjo','Kab.',NULL,NULL,NULL,NULL),
(190,'1316','Mojokerto','Kab.',NULL,NULL,NULL,NULL),
(191,'1317','Jombang','Kab.',NULL,NULL,NULL,NULL),
(192,'1318','Nganjuk','Kab.',NULL,NULL,NULL,NULL),
(193,'1319','Madiun','Kab.',NULL,NULL,NULL,NULL),
(194,'1320','Magetan','Kab.',NULL,NULL,NULL,NULL),
(195,'1321','Ngawi','Kab.',NULL,NULL,NULL,NULL),
(196,'1322','Bojonegoro','Kab.',NULL,NULL,NULL,NULL),
(197,'1323','Tuban','Kab.',NULL,NULL,NULL,NULL),
(198,'1324','Lamongan','Kab.',NULL,NULL,NULL,NULL),
(199,'1325','Gresik','Kab.',NULL,NULL,NULL,NULL),
(200,'1326','Bangkalan','Kab.',NULL,NULL,NULL,NULL),
(201,'1327','Sampang','Kab.',NULL,NULL,NULL,NULL),
(202,'1328','Pamekasan','Kab.',NULL,NULL,NULL,NULL),
(203,'1329','Sumenep','Kab.',NULL,NULL,NULL,NULL),
(204,'1330','Kediri','Kodya',NULL,NULL,NULL,NULL),
(205,'1331','Blitar','Kodya',NULL,NULL,NULL,NULL),
(206,'1332','Malang','Kodya',NULL,NULL,NULL,NULL),
(207,'1333','Probolinggo','Kodya',NULL,NULL,NULL,NULL),
(208,'1334','Pasuruan','Kodya',NULL,NULL,NULL,NULL),
(209,'1335','Mojokerto','Kodya',NULL,NULL,NULL,NULL),
(210,'1336','Madiun','Kodya',NULL,NULL,NULL,NULL),
(211,'1337','Surabaya','Kodya',NULL,NULL,NULL,NULL),
(212,'1338','Jember','Kotip',NULL,NULL,NULL,NULL),
(213,'1339','Situbondo','Kotip',NULL,NULL,NULL,NULL),
(214,'1401','Jembrana','Kab.',NULL,NULL,NULL,NULL),
(215,'1402','Tabanan','Kab.',NULL,NULL,NULL,NULL),
(216,'1403','Badung','Kab.',NULL,NULL,NULL,NULL),
(217,'1404','Gianyar','Kab.',NULL,NULL,NULL,NULL),
(218,'1405','Klungkung','Kab.',NULL,NULL,NULL,NULL),
(219,'1406','Bangli','Kab.',NULL,NULL,NULL,NULL),
(220,'1407','Karangasem','Kab.',NULL,NULL,NULL,NULL),
(221,'1408','Buleleng','Kab.',NULL,NULL,NULL,NULL),
(222,'1409','Denpasar','Kotip',NULL,NULL,NULL,NULL),
(223,'1501','Lombok Barat','Kab.',NULL,NULL,NULL,NULL),
(224,'1502','Lombok Tengah','Kab.',NULL,NULL,NULL,NULL),
(225,'1503','Lombok Timur','Kab.',NULL,NULL,NULL,NULL),
(226,'1504','Sumbawa','Kab.',NULL,NULL,NULL,NULL),
(227,'1505','Dompu','Kab.',NULL,NULL,NULL,NULL),
(228,'1506','Bima','Kab.',NULL,NULL,NULL,NULL),
(229,'1507','Mataram','Kotip',NULL,NULL,NULL,NULL),
(230,'1601','Kupang','Kotip.',NULL,NULL,NULL,NULL),
(231,'1602','Sumba Barat','Kab.',NULL,NULL,NULL,NULL),
(232,'1603','Sumba Timur','Kab.',NULL,NULL,NULL,NULL),
(233,'1604','Kupang','Kab.',NULL,NULL,NULL,NULL),
(234,'1605','Timor Tengah Selatan','Kab.',NULL,NULL,NULL,NULL),
(235,'1606','Timur Tengah Utara','Kab.',NULL,NULL,NULL,NULL),
(236,'1607','Belu','Kab.',NULL,NULL,NULL,NULL),
(237,'1608','Alor','Kab.',NULL,NULL,NULL,NULL),
(238,'1609','Flores Timur','Kab.',NULL,NULL,NULL,NULL),
(239,'1610','Sikka','Kab.',NULL,NULL,NULL,NULL),
(240,'1611','Ende','Kab.',NULL,NULL,NULL,NULL),
(241,'1612','Ngada','Kab.',NULL,NULL,NULL,NULL),
(242,'1613','Manggarai','Kab.',NULL,NULL,NULL,NULL),
(243,'1614','Lembata','Kab.',NULL,NULL,NULL,NULL),
(244,'1701','Sambas','Kab.',NULL,NULL,NULL,NULL),
(245,'1702','Pontianak','Kab.',NULL,NULL,NULL,NULL),
(246,'1703','Sanggau','Kab.',NULL,NULL,NULL,NULL),
(247,'1704','Ketapang','Kab.',NULL,NULL,NULL,NULL),
(248,'1705','Sintang','Kab.',NULL,NULL,NULL,NULL),
(249,'1706','Kapuas Hulu','Kab.',NULL,NULL,NULL,NULL),
(250,'1707','Pontianak','Kodya',NULL,NULL,NULL,NULL),
(251,'1708','Singkawang','Kotip',NULL,NULL,NULL,NULL),
(252,'1709','Banjarbaru','Kab.',NULL,NULL,NULL,NULL),
(253,'1710','Bengkayang','Kab.',NULL,NULL,NULL,NULL),
(254,'1711','Landak','Kab.',NULL,NULL,NULL,NULL),
(255,'1801','Kota Waringin Barat','Kab.',NULL,NULL,NULL,NULL),
(256,'1802','Kota Waringin Timur','Kab.',NULL,NULL,NULL,NULL),
(257,'1803','Katingan','Kab.',NULL,NULL,NULL,NULL),
(258,'1804','Kapuas','Kab.',NULL,NULL,NULL,NULL),
(259,'1805','Barito Selatan','Kab.',NULL,NULL,NULL,NULL),
(260,'1806','Barito Timur','Kab.',NULL,NULL,NULL,NULL),
(261,'1807','Barito Utara','Kab.',NULL,NULL,NULL,NULL),
(262,'1808','Gunung Mas','Kab.',NULL,NULL,NULL,NULL),
(263,'1809','Murung Raya','Kab.',NULL,NULL,NULL,NULL),
(264,'1810','Seruyan','Kab.',NULL,NULL,NULL,NULL),
(265,'1811','Palangka Raya','Kodya',NULL,NULL,NULL,NULL),
(266,'1812','Buntok','Kab.',NULL,NULL,NULL,NULL),
(267,'1901','Tanah Laut','Kab.',NULL,NULL,NULL,NULL),
(268,'1902','Kota Baru','Kab.',NULL,NULL,NULL,NULL),
(269,'1903','Banjar','Kab.',NULL,NULL,NULL,NULL),
(270,'1904','Barito Kuala','Kab.',NULL,NULL,NULL,NULL),
(271,'1905','TapinTapian','Kab.',NULL,NULL,NULL,NULL),
(272,'1906','Hulu Sungai Selatan','Kab.',NULL,NULL,NULL,NULL),
(273,'1907','Hulu Sungai Tengah','Kab.',NULL,NULL,NULL,NULL),
(274,'1908','Hulu Sungai Utara','Kab.',NULL,NULL,NULL,NULL),
(275,'1909','Tabalong','Kab.',NULL,NULL,NULL,NULL),
(276,'1910','Banjarmasin','Kodya',NULL,NULL,NULL,NULL),
(277,'1911','Banjar Baru','Kotip.',NULL,NULL,NULL,NULL),
(278,'2001','Pasir','Kab.',NULL,NULL,NULL,NULL),
(279,'2002','Kutai','Kab.',NULL,NULL,NULL,NULL),
(280,'2003','Bulungan','Kab.',NULL,NULL,NULL,NULL),
(281,'2004','Balikpapan','Kodya',NULL,NULL,NULL,NULL),
(282,'2005','Samarinda','Kodya',NULL,NULL,NULL,NULL),
(283,'2006','Berau','Kab.',NULL,NULL,NULL,NULL),
(284,'2007','Tarakan','Kotip.',NULL,NULL,NULL,NULL),
(285,'2008','Bontang','Kab.',NULL,NULL,NULL,NULL),
(286,'2009','Kutai Barat','Kab.',NULL,NULL,NULL,NULL),
(287,'2010','Kutai Timur','Kab.',NULL,NULL,NULL,NULL),
(288,'2011','Malinau','Kab.',NULL,NULL,NULL,NULL),
(289,'2012','Nunukan','Kab.',NULL,NULL,NULL,NULL),
(290,'2102','Bolaang Mongondow','Kab.',NULL,NULL,NULL,NULL),
(291,'2103','Minahasa','Kab.',NULL,NULL,NULL,NULL),
(292,'2104','Sangihe Talaud','Kab.',NULL,NULL,NULL,NULL),
(293,'2106','Manado','Kodya',NULL,NULL,NULL,NULL),
(294,'2107','Bitung','Kotip.',NULL,NULL,NULL,NULL),
(295,'2201','Luwuk Banggai','Kab.',NULL,NULL,NULL,NULL),
(296,'2202','Poso','Kab.',NULL,NULL,NULL,NULL),
(297,'2203','Donggala','Kab.',NULL,NULL,NULL,NULL),
(298,'2204','Bual ToliToli','Kab.',NULL,NULL,NULL,NULL),
(299,'2205','Palu','Kab.',NULL,NULL,NULL,NULL),
(300,'2206','Banggai Kepulauan','Kab.',NULL,NULL,NULL,NULL),
(301,'2207','Buol','Kab.',NULL,NULL,NULL,NULL),
(302,'2208','Morowali','Kab.',NULL,NULL,NULL,NULL),
(303,'2301','Selayar','Kab.',NULL,NULL,NULL,NULL),
(304,'2302','Bulukumba','Kab.',NULL,NULL,NULL,NULL),
(305,'2303','Bantaeng','Kab.',NULL,NULL,NULL,NULL),
(306,'2304','Jeneponto','Kab.',NULL,NULL,NULL,NULL),
(307,'2305','Takalar','Kab.',NULL,NULL,NULL,NULL),
(308,'2306','Gowa','Kab.',NULL,NULL,NULL,NULL),
(309,'2307','Sinjai','Kab.',NULL,NULL,NULL,NULL),
(310,'2308','Bone','Kab.',NULL,NULL,NULL,NULL),
(311,'2309','Maros','Kab.',NULL,NULL,NULL,NULL),
(312,'2310','Pangkajene','Kab.',NULL,NULL,NULL,NULL),
(313,'2311','Barru','Kab.',NULL,NULL,NULL,NULL),
(314,'2313','Wajo','Kab.',NULL,NULL,NULL,NULL),
(315,'2314','Sid. Rappang','Kab.',NULL,NULL,NULL,NULL),
(316,'2315','Pinrang','Kab.',NULL,NULL,NULL,NULL),
(317,'2316','Enrekang','Kab.',NULL,NULL,NULL,NULL),
(318,'2317','Luwu','Kab.',NULL,NULL,NULL,NULL),
(319,'2318','Tana Toraja','Kab.',NULL,NULL,NULL,NULL),
(320,'2319','Polewali Mamasa','Kab.',NULL,NULL,NULL,NULL),
(321,'2320','Majene','Kab.',NULL,NULL,NULL,NULL),
(322,'2321','Mamuju','Kab.',NULL,NULL,NULL,NULL),
(323,'2322','Ujung Pandang','Kodya',NULL,NULL,NULL,NULL),
(324,'2323','Pare Pare','Kodya',NULL,NULL,NULL,NULL),
(325,'2324','Pangkep','Kab.',NULL,NULL,NULL,NULL),
(326,'2325','Watampone','Kab.',NULL,NULL,NULL,NULL),
(327,'2326','Luwu Utara','Kab.',NULL,NULL,NULL,NULL),
(328,'2401','Buton','Kab.',NULL,NULL,NULL,NULL),
(329,'2402','Muna','Kab.',NULL,NULL,NULL,NULL),
(330,'2403','Kendari','Kab.',NULL,NULL,NULL,NULL),
(331,'2404','Kolaka','Kab.',NULL,NULL,NULL,NULL),
(332,'2405','Baubau','Kotip.',NULL,NULL,NULL,NULL),
(333,'2406','Kendari','Kotip.',NULL,NULL,NULL,NULL),
(334,'2501','Maluku Tenggara','Kab.',NULL,NULL,NULL,NULL),
(335,'2502','Maluku Tengah','Kab.',NULL,NULL,NULL,NULL),
(336,'2505','Ambon','Kodya',NULL,NULL,NULL,NULL),
(337,'2507','Maluku Tenggara Barat','Kab.',NULL,NULL,NULL,NULL),
(338,'2508','Pulau Buru','Kab.',NULL,NULL,NULL,NULL),
(339,'2601','Merauke','Kab.',NULL,NULL,NULL,NULL),
(340,'2602','Pegunungan Jayawijaya','Kab.',NULL,NULL,NULL,NULL),
(341,'2603','Jayapura','Kab.',NULL,NULL,NULL,NULL),
(342,'2604','Paniai Nabire','Kab.',NULL,NULL,NULL,NULL),
(343,'2605','Fakfak','Kab.',NULL,NULL,NULL,NULL),
(344,'2606','Sorong','Kab.',NULL,NULL,NULL,NULL),
(345,'2607','Manokwari','Kab.',NULL,NULL,NULL,NULL),
(346,'2608','Yapen Waropen','Kab.',NULL,NULL,NULL,NULL),
(347,'2609','Teluk Cendrawasih','Kab.',NULL,NULL,NULL,NULL),
(348,'2610','Jayapura','Kotip.',NULL,NULL,NULL,NULL),
(349,'2611','Biak Numfor','Kab.',NULL,NULL,NULL,NULL),
(350,'2612','Mimika','Kab.',NULL,NULL,NULL,NULL),
(351,'2613','Puncak Jaya','Kab.',NULL,NULL,NULL,NULL),
(352,'2614','Sorong','Kota',NULL,NULL,NULL,NULL),
(353,'2801','Bangka','Kab.',NULL,NULL,NULL,NULL),
(354,'2802','Belitung','Kab.',NULL,NULL,NULL,NULL),
(355,'2803','Pangkal Pinang','Kab.',NULL,NULL,NULL,NULL),
(356,'2901','Boalemo','Kab.',NULL,NULL,NULL,NULL),
(357,'2902','Gorontalo','Kab.',NULL,NULL,NULL,NULL),
(358,'2903','Gorontalo','Kota',NULL,NULL,NULL,NULL),
(359,'3001','Maluku Utara','Kab.',NULL,NULL,NULL,NULL),
(360,'3002','Halmahera Tengah','Kab.',NULL,NULL,NULL,NULL),
(361,'3003','Ternate','Kota',NULL,NULL,NULL,NULL),
(362,'3101','Banten','Kab.',NULL,NULL,NULL,NULL),
(363,'1410','Semarapura','Kab.',NULL,NULL,NULL,NULL),
(364,'1712','Melawi','Kab.',NULL,NULL,NULL,NULL),
(365,'0307','Limapuluhkota','Kab.',NULL,NULL,NULL,NULL),
(366,'0607','Bangka','Kab.',NULL,NULL,NULL,NULL),
(367,'0608','Belitung','Kab.',NULL,NULL,NULL,NULL),
(368,'0610','Pangkal Pinang','Kodya',NULL,NULL,NULL,NULL),
(369,'2101','Gorontalo','Kab.',NULL,NULL,NULL,NULL),
(370,'2105','Gorontalo','Kodya',NULL,NULL,NULL,NULL),
(371,'2312','Soppeng','Kab.',NULL,NULL,NULL,NULL),
(372,'2503','Halmahera Tengah','Kab.',NULL,NULL,NULL,NULL),
(373,'2504','Maluku Utara','Kab.',NULL,NULL,NULL,NULL),
(374,'2506','Ternate','Kotip',NULL,NULL,NULL,NULL),
(375,'2701','Dili','Kab.',NULL,NULL,NULL,NULL),
(376,'2702','Baucau','Kab.',NULL,NULL,NULL,NULL),
(377,'2703','Manaturo','Kab.',NULL,NULL,NULL,NULL),
(378,'2704','Lautem','Kab.',NULL,NULL,NULL,NULL),
(379,'2705','Viqueque','Kab.',NULL,NULL,NULL,NULL),
(380,'2706','Ainaro','Kab.',NULL,NULL,NULL,NULL),
(381,'2707','Manufahi','Kab.',NULL,NULL,NULL,NULL),
(382,'2708','Covalima','Kab.',NULL,NULL,NULL,NULL),
(383,'2709','Ambeno','Kab.',NULL,NULL,NULL,NULL),
(384,'2710','Bobonaro','Kab.',NULL,NULL,NULL,NULL),
(385,'2711','Liquica','Kab.',NULL,NULL,NULL,NULL),
(386,'2712','Ermera','Kab.',NULL,NULL,NULL,NULL),
(387,'2713','Aileu','Kab.',NULL,NULL,NULL,NULL),
(389,'','','undefin',0,0,'',15),
(390,'','','undefin',0,0,'',15);

/*Table structure for table `kamusstatuspemodalan` */

DROP TABLE IF EXISTS `kamusstatuspemodalan`;

CREATE TABLE `kamusstatuspemodalan` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `namakamusstatuspemodalan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `kamusstatuspemodalan` */

insert into `kamusstatuspemodalan` values 
(1,'PMDN'),
(2,'PMA'),
(3,'Swasta Nasional'),
(4,'Joint Venture');

/*Table structure for table `kasus` */

DROP TABLE IF EXISTS `kasus`;

CREATE TABLE `kasus` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `NamaPerusahaan` int(10) DEFAULT NULL,
  `idkota` int(10) DEFAULT NULL,
  `idpropinsi` int(10) DEFAULT NULL,
  `Alamat` text,
  `jmltenagakerjaterlibat` int(10) DEFAULT NULL,
  `lamamogokkerja` int(10) DEFAULT NULL,
  `jamkerjahilang` int(10) DEFAULT NULL,
  `idjenisperselisihan` int(10) DEFAULT NULL,
  `idpenyelesianperselisihan` int(10) DEFAULT NULL,
  `tglkejadian` date DEFAULT NULL,
  `upayapenyelesaian` text,
  `keterangan` text,
  `idpengisi` int(10) DEFAULT NULL,
  `tglisi` datetime DEFAULT NULL,
  `idstatuspenyelesian` int(10) DEFAULT '2',
  `idmediator` int(10) DEFAULT NULL,
  `tuntutan` text,
  `jamkejadian` time DEFAULT NULL,
  `tglselesai` date DEFAULT NULL,
  `jamselesai` time DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `timestam` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `kasus` */

insert into `kasus` values 
(1,1,170,12,'sskdjksa',10,5,74,3,NULL,'2013-12-06','belum','',1,'2013-12-07 00:00:00',2,NULL,'hsk','22:30:00',NULL,NULL,NULL,NULL,NULL),
(2,4,171,12,'bantul ',18,4,4,3,2,'2013-12-05','musyawarah','Sip',1,'2013-12-07 00:00:00',1,1,'pendidikan','21:50:00','2013-12-15','01:05:00',NULL,NULL,NULL),
(3,4,171,12,'bantul ',10,20,12,2,0,'2013-12-13','','',1,'2013-12-07 00:00:00',2,0,'penting','16:35:00','0000-00-00','00:00:00','-7.7504955','110.452788','Sat Dec 07 2013 22:41:10 GMT+0700 (WIT)');

/*Table structure for table `komponen` */

DROP TABLE IF EXISTS `komponen`;

CREATE TABLE `komponen` (
  `idkomponen` int(10) NOT NULL AUTO_INCREMENT COMMENT 'header,menu,iklan,isi,keterangan filed form',
  `NmKomponen` varchar(40) DEFAULT NULL,
  `isshow` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idkomponen`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `komponen` */

insert into `komponen` values 
(1,'Menu View','T'),
(2,'Menu Admin','T');

/*Table structure for table `linkdatagambar` */

DROP TABLE IF EXISTS `linkdatagambar`;

CREATE TABLE `linkdatagambar` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idkasus` int(10) DEFAULT NULL,
  `linkphoto` varchar(200) DEFAULT NULL,
  `linkvideo` varchar(200) DEFAULT NULL,
  `tipe` varchar(10) DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  `randcode` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `linkdatagambar` */

insert into `linkdatagambar` values 
(1,2,'25172517.jpg',NULL,'PICTURE','keteranfan 1',NULL),
(2,2,'2518',NULL,'PICTURE','ketrangan 3',NULL),
(3,2,'2465',NULL,'VIDEO','keterangan 3',NULL),
(4,3,'25152515.jpg',NULL,'PICTURE','Lambang hati hati',NULL),
(5,3,'2507',NULL,'PICTURE','wong bagus banget',NULL),
(6,3,'2466',NULL,'VIDEO','dapurku',NULL);

/*Table structure for table `mediator` */

DROP TABLE IF EXISTS `mediator`;

CREATE TABLE `mediator` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(100) DEFAULT NULL,
  `NIP` varchar(30) DEFAULT NULL,
  `NamaInstansi` varchar(100) DEFAULT NULL,
  `idpropinsi` int(10) DEFAULT NULL,
  `idkabupaten` int(10) DEFAULT NULL,
  `Pangkat` varchar(30) DEFAULT NULL,
  `struktural` varchar(100) DEFAULT NULL,
  `jnsmediator` enum('B','K') DEFAULT 'B' COMMENT 'biasa , khusus',
  `NoSKmediator` varchar(100) DEFAULT NULL,
  `NomorHP` varchar(20) DEFAULT NULL,
  `Keterangan` text,
  `idjenistenagapeneyelesaian` int(10) DEFAULT NULL,
  `photo` text,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mediator` */

insert into `mediator` values 
(1,'Samsudin Raharja','12346767','badan arbiter',1,2,'IVB','Kepala Dinas','B','12343434','','',1,NULL),
(2,'Rudi Sugiarto','4456','badan Kosoliator',2,18,'IVC','Kepala Cabang','K','2334545','234324','',2,NULL),
(3,'uji coba','test','testt',12,171,'sdsad','asdasd','B','asdasd','asdasd','',1,'7 dashboard.jpg');

/*Table structure for table `member` */

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `idMember` varchar(50) DEFAULT NULL,
  `Nama` varchar(60) DEFAULT NULL,
  `Alamat` varchar(255) DEFAULT NULL,
  `NoRekening` varchar(255) DEFAULT NULL,
  `NoHP` varchar(50) DEFAULT NULL,
  `UplineCode` varchar(20) DEFAULT NULL,
  `IsBlockir` enum('Y','N') DEFAULT 'N',
  `idx` int(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `member` */

insert into `member` values 
('root11','ZoeTronik','','','111111','','N',6),
('g99imt','M00021',NULL,NULL,'08100021','root116vbx61','N',21),
('6vbx61','M0002',NULL,NULL,'0810002','root11','N',20),
('wb961w','M0001',NULL,NULL,'0810001','root11','N',19);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `idmenu` int(10) NOT NULL AUTO_INCREMENT,
  `nmmenu` varchar(100) DEFAULT NULL,
  `tipemenu` int(1) DEFAULT NULL COMMENT '1 : page singgle 2: Page list',
  `idkomponen` int(10) DEFAULT NULL,
  `iduser` int(10) DEFAULT '0',
  `parentmenu` int(10) DEFAULT NULL,
  `urlci` varchar(100) DEFAULT NULL,
  `urut` int(10) DEFAULT NULL,
  `jmlgambar` int(1) DEFAULT '0',
  `settingform` text,
  PRIMARY KEY (`idmenu`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert into `menu` values 
(34,'UMUM',1,1,0,0,'',10,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(2,'Setting Menu',1,2,0,34,'ctrmenu',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(6,'LOGOUT',1,2,0,0,'webadmindo/logout',21,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(35,'Hak User',1,2,0,34,'ctrusermenu',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(36,'Setting User Group',1,2,0,34,'ctrusergroup',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(37,'PEMANTAUAN',1,0,0,0,'',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(38,'Pemantauan',1,2,0,37,'ctrkasus',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(39,'Tenaga Perselisihan',1,2,0,52,'ctrmediator',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(40,'KAMUS DATA',1,2,0,0,'',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(41,'Jenis Perselisihan',1,2,0,40,'ctrjenisperselisihan',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(42,'Jenis Penyelesaian Perselisihan',1,2,0,40,'ctrjenispenyelesaianperselisihan',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(43,'Jenis Tenaga Penyelesaian',1,2,0,40,'ctrjenistenagapeneyelesaian',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(44,'Status Penyelesaian',1,2,0,40,'ctrstatuspenyelesaian',4,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(45,'LAPORAN',1,2,0,0,'',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(46,'Data Perselisihan Hubungan Industrial Dari Propinsi',1,2,0,45,'ctrrekapdataperselisihan',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(47,'Grafik Jenis Perselisiahn Dan Upaya Penyelesian',1,2,0,45,'ctrrekapdataperselisihangrafik',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(48,'Isi/Edit Pengguna Sistem',1,2,0,34,'ctrusersistem',4,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(49,'Kabupaten / Kota',1,1,0,52,'ctrkabupaten',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(50,'Propinsi',1,1,0,52,'ctrpropinsi',1,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(51,'Perusahaan',1,1,0,52,'ctrperusahaan',4,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(52,'MASTER',1,2,0,0,'',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(53,'Serikat Pekerja',1,2,0,52,'ctrserikatpekerja',5,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(54,'UMP-KHL',1,2,0,52,'ctrump',6,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(55,'Serah Terima Mobil Tanggap Darurat',1,2,0,52,'ctrmobiltanggapdarurat',7,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(56,'Mobil Monitoring',1,2,0,37,'ctrpphimap',2,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(57,'Data perselisihan Perjenis',1,2,0,45,'ctrrekapdataperselisihanperjenis',3,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(58,'Data Perselisihan Perpropinsi per jenis',1,2,0,45,'ctrrekapdataperselisihanperjenisperprop',4,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(59,'Data perselisihan Berdasar Status Penyelesian',1,2,0,45,'ctrrekapdataperselisihanstatusselesai',5,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(60,'Data perselisihan Berdasar Status Penyelesian Per Propinsi',1,2,0,45,'ctrrekapdataperselisihanperstatusperprop',6,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(61,'Laporan Data UMP-KHL',1,2,0,45,'ctrrekapumpgrafik',7,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(62,'Laporan Data UMK',1,2,0,45,'ctrrekapumkgrafik',8,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;'),
(63,'Jenis Pemodalan',1,2,0,40,'ctrkamusstatuspemodalan',5,0,'xbahasa:Bahasa,;xjudul:Judul,;xisi:Isi / Keterangan,kontent;xisiawal:Isi Awal,Isikan Jika Diperlukan;xurut:urutan,urutan saat ditampilkan diweb;xgb1:,Upload Gambar 1;xgb2:,Upload Gambar 2;xgb3:,Upload Gambar 3;');

/*Table structure for table `mobiltanggapdarurat` */

DROP TABLE IF EXISTS `mobiltanggapdarurat`;

CREATE TABLE `mobiltanggapdarurat` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idpropinsi` int(10) DEFAULT NULL,
  `jeniskendaraan` varchar(100) DEFAULT NULL,
  `nopolisi` varchar(20) DEFAULT NULL,
  `norangka` varchar(50) DEFAULT NULL,
  `nomesin` varchar(50) DEFAULT NULL,
  `tglserahterima` date DEFAULT NULL,
  `namapenyerah` varchar(100) DEFAULT NULL,
  `jabatanpenyerah` varchar(100) DEFAULT NULL,
  `namapenerima` varchar(100) DEFAULT NULL,
  `jabatanpenerima` varchar(100) DEFAULT NULL,
  `detailperalatan` text,
  `keterangan` text,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mobiltanggapdarurat` */

insert into `mobiltanggapdarurat` values 
(1,12,'Minibus','AB2345HC','as333dsad55','asdsd3343343','2013-12-09','Sujono','Pimpinan','Suwiryo','Sopir','Kamera, Satelit,power','');

/*Table structure for table `perusahaan` */

DROP TABLE IF EXISTS `perusahaan`;

CREATE TABLE `perusahaan` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `NamaPerusahaan` varchar(200) DEFAULT NULL,
  `Alamat` text,
  `NoTelpon` varchar(100) DEFAULT NULL,
  `NoTelpon1` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `kodepos` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `idPropinsi` int(10) DEFAULT NULL,
  `idKabupaten` int(10) DEFAULT NULL,
  `ContactPerson` varchar(100) DEFAULT NULL,
  `notelpcontactperson` varchar(100) DEFAULT NULL,
  `NoHpCP` varchar(100) DEFAULT NULL,
  `emailCP` varchar(100) DEFAULT NULL,
  `jenisusaha` varchar(100) DEFAULT NULL,
  `NoPKB` varchar(100) DEFAULT NULL,
  `tglmasberlakupkb` date DEFAULT NULL,
  `bidang` text,
  `jmlkaryawantetap` int(10) DEFAULT NULL,
  `jmlkaryawankontrak` int(10) DEFAULT NULL,
  `jmlkaryawanoutsourching` int(10) DEFAULT NULL,
  `idjnspermodalan` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `perusahaan` */

insert into `perusahaan` values 
(1,'Pabrik garment','sskdjksa','ssdadsd','','','','sadsd',12,170,'adsd','asdsad','','','Pakaian','','0000-00-00','Garmen',0,0,0,NULL),
(2,'Pabrik Batu Bata','-','02747474636',NULL,NULL,NULL,'',9,99,'coba','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(4,'Pabik lele','bantul coba  bantul dengan alamat yang panjang\nbantul coba  bantul dengan alamat yang panjangbantul coba  bantul dengan alamat yang panjang','sdsad','sdsad','sadsad','','',12,171,'','','','','ternak','','2013-12-04','Ternak lele',0,0,0,1);

/*Table structure for table `propinsi` */

DROP TABLE IF EXISTS `propinsi`;

CREATE TABLE `propinsi` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `kd_prop` varchar(7) DEFAULT NULL,
  `nama_prop` varchar(30) DEFAULT NULL,
  `linkpeta` varchar(100) DEFAULT NULL,
  `buletanx` int(10) DEFAULT NULL,
  `buletany` int(10) DEFAULT NULL,
  `diameter` int(10) DEFAULT NULL,
  `warna` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `kd_prop` (`kd_prop`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `propinsi` */

insert into `propinsi` values 
(1,'01','Daerah Istimewa Aceh','',37,21,15,'#ae8e51'),
(2,'02','Sumatera Utara',NULL,NULL,NULL,NULL,NULL),
(3,'03','Sumatera Barat',NULL,NULL,NULL,NULL,NULL),
(4,'04','Riau',NULL,NULL,NULL,NULL,NULL),
(5,'05','Jambi',NULL,NULL,NULL,NULL,NULL),
(6,'06','Sumatera Selatan',NULL,NULL,NULL,NULL,NULL),
(7,'07','Bengkulu',NULL,NULL,NULL,NULL,NULL),
(8,'08','Lampung',NULL,NULL,NULL,NULL,NULL),
(9,'09','DKI','dki.png',263,262,15,'#47916e'),
(10,'10','Jawa Barat',NULL,NULL,NULL,NULL,NULL),
(11,'11','Jawa Tengah',NULL,NULL,NULL,NULL,NULL),
(12,'12','Daerah Istimewa Yogyakarta','diy.jpg',336,301,15,'#9dad26'),
(13,'13','Jawa Timur',NULL,NULL,NULL,NULL,NULL),
(14,'14','Bali','',454,311,15,'#3720a3'),
(15,'15','Nusa Tenggara Barat',NULL,NULL,NULL,NULL,NULL),
(16,'16','Nusa Tenggara Timur',NULL,NULL,NULL,NULL,NULL),
(17,'17','Kalimantan Barat','',354,122,15,'#6735a3'),
(18,'18','Kalimantan Tengah','',417,152,15,'#2f8548'),
(19,'19','Kalimantan Selatan',NULL,NULL,NULL,NULL,NULL),
(20,'20','Kalimantan Timur',NULL,NULL,NULL,NULL,NULL),
(21,'21','Sulawesi Utara',NULL,NULL,NULL,NULL,NULL),
(22,'22','Sulawesi Tengah',NULL,NULL,NULL,NULL,NULL),
(23,'23','Sulawesi Selatan',NULL,NULL,NULL,NULL,NULL),
(24,'24','Sulawesi Tenggara',NULL,NULL,NULL,NULL,NULL),
(25,'25','Maluku',NULL,NULL,NULL,NULL,NULL),
(26,'26','Irian Jaya',NULL,NULL,NULL,NULL,NULL),
(27,'28','Bangka Belitung',NULL,NULL,NULL,NULL,NULL),
(28,'29','Gorontalo',NULL,NULL,NULL,NULL,NULL),
(29,'30','Maluku Utara',NULL,NULL,NULL,NULL,NULL),
(30,'31','Banten',NULL,NULL,NULL,NULL,NULL),
(31,'27','Timor Timur',NULL,NULL,NULL,NULL,NULL),
(33,'','','',0,0,15,''),
(34,'','','',0,0,15,''),
(35,'','','',0,0,15,'');

/*Table structure for table `serikatpekerja` */

DROP TABLE IF EXISTS `serikatpekerja`;

CREATE TABLE `serikatpekerja` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `kodeSP` varchar(40) DEFAULT NULL,
  `NamaSP` varchar(100) DEFAULT NULL,
  `Alamat` text,
  `NoTelp` varchar(20) DEFAULT NULL,
  `noAdART` text,
  `lambang` text,
  `NoPerjanjianKerja` varchar(200) DEFAULT NULL,
  `NamaKetuaSP` varchar(100) DEFAULT NULL,
  `NoPhoneKetuaSP` varchar(30) DEFAULT NULL,
  `NoHPKetuaSP` varchar(30) DEFAULT NULL,
  `NamaWakilKetuaSP` varchar(100) DEFAULT NULL,
  `NoPhoneWakil` varchar(30) DEFAULT NULL,
  `NoHpWakil` varchar(30) DEFAULT NULL,
  `ImgSusunanPengurus` varchar(100) DEFAULT NULL,
  `Keterangan` text,
  `idPegIn` int(10) DEFAULT NULL,
  `idPegUpdte` int(10) DEFAULT NULL,
  `tglin` int(10) DEFAULT NULL,
  `tglupdate` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `serikatpekerja` */

insert into `serikatpekerja` values 
(1,'0988','Serikat 1','Alamat 1','02747474','','undefined','','','','','','','','','',0,0,0,0);

/*Table structure for table `statuspenyelesaian` */

DROP TABLE IF EXISTS `statuspenyelesaian`;

CREATE TABLE `statuspenyelesaian` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `statuspenyelesaian` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `statuspenyelesaian` */

insert into `statuspenyelesaian` values 
(1,'Selesai'),
(2,'Belum Selesai');

/*Table structure for table `tipemenu` */

DROP TABLE IF EXISTS `tipemenu`;

CREATE TABLE `tipemenu` (
  `idTipeMenu` varchar(1) DEFAULT NULL,
  `NmTipeMenu` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tipemenu` */

insert into `tipemenu` values 
('1','Singgle'),
('2','Berbentuk Daftar');

/*Table structure for table `umk` */

DROP TABLE IF EXISTS `umk`;

CREATE TABLE `umk` (
  `idxumk` int(10) NOT NULL AUTO_INCREMENT,
  `tahunumk` varchar(4) DEFAULT NULL,
  `idpropinsiumk` int(10) DEFAULT NULL,
  `idkabupatenumk` int(10) DEFAULT NULL,
  `nominalumk` float(10,2) DEFAULT NULL,
  `keteranganumk` text,
  PRIMARY KEY (`idxumk`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `umk` */

insert into `umk` values 
(1,'2010',12,170,910000.00,''),
(2,'2011',12,170,920000.00,''),
(3,'2012',12,170,930000.00,'');

/*Table structure for table `ump` */

DROP TABLE IF EXISTS `ump`;

CREATE TABLE `ump` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `idpropinsi` int(10) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `ump` float(20,2) DEFAULT NULL,
  `khl` float(20,2) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ump` */

insert into `ump` values 
(1,12,'2013',1000000.00,1050000.00,'test 1'),
(2,12,'2010',950000.00,975000.00,''),
(3,12,'2011',975000.00,990000.00,''),
(4,12,'2012',980000.00,990000.00,'');

/*Table structure for table `usergroup` */

DROP TABLE IF EXISTS `usergroup`;

CREATE TABLE `usergroup` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `NmUserGroup` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `usergroup` */

insert into `usergroup` values 
(1,'Admin'),
(2,'Admin 2'),
(3,'Reviewer');

/*Table structure for table `usermenu` */

DROP TABLE IF EXISTS `usermenu`;

CREATE TABLE `usermenu` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `iduser` int(10) DEFAULT NULL,
  `idmenu` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=555 DEFAULT CHARSET=latin1;

/*Data for the table `usermenu` */

insert into `usermenu` values 
(553,1,34),
(552,1,62),
(551,1,61),
(550,1,55),
(549,1,60),
(548,1,54),
(547,1,63),
(546,1,59),
(545,1,53),
(544,1,58),
(543,1,51),
(542,1,48),
(541,1,44),
(540,1,57),
(539,1,45),
(538,1,43),
(537,1,40),
(536,1,39),
(535,1,36),
(534,1,56),
(533,1,52),
(532,1,49),
(531,1,47),
(530,1,42),
(529,1,35),
(528,1,50),
(527,1,46),
(526,1,41),
(525,1,38),
(524,1,37),
(523,1,2),
(554,1,6);

/*Table structure for table `usersistem` */

DROP TABLE IF EXISTS `usersistem`;

CREATE TABLE `usersistem` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `npp` varchar(20) DEFAULT NULL,
  `Nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL COMMENT 'Organisasi',
  `NoTelpon` varchar(50) DEFAULT NULL,
  `user` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `statuspeg` int(10) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `ym` varchar(100) DEFAULT NULL,
  `isaktif` enum('Y','N') DEFAULT 'N',
  `idusergroup` int(10) DEFAULT NULL,
  `idkabupaten` int(10) DEFAULT NULL,
  `idpropinsi` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `usersistem` */

insert into `usersistem` values 
(1,'undefined','User Demo','0','0274747474','demo','demo',0,'undefined','','','',1,173,12),
(2,'undefined','pengguna lain','0','04532','test','test',0,'undefined','','','',2,100,9);
